%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 12.03.2021
%%% @doc

%% ====================================================================
%% Types
%% ====================================================================

-record(wait_quota, {
    new_classes = [] :: [map()],
    hc :: integer(),
    monref :: reference()
}).

-record(pass_quota, {
    ref :: reference(),
    timerref :: reference(),
    loaded_classes :: [{CN :: binary(), Pid :: pid(), Status :: 'started' | 'failed' | 'ready' }]
}).

%% state for domain gen_server
-record(dstate, {
    regname :: binary(), % gen_server reg name
    domain :: binary(),
    self :: pid(),
    initts :: non_neg_integer(),
    model_classes :: [map()], % actual data of data-model classes from dc.
    model_hc :: term(), % hashcode of dc classes cached result
    waitors = [] :: [term()], % sync_waitors for class start
    %
    statref :: reference(), % ref for stat timer
    stattimerref :: reference(), % stat timer
    counter = 0 :: integer(),
    %
    quota :: #wait_quota{} | #pass_quota{} | undefined
  }).

%% accumulator of workers stat
-record(wrk_stat, {
    ts :: integer(),
    timerref :: reference(),
    workers_count :: integer(),
    workers :: [{pid(), module()}],
    workers_replies :: [{{pid(), module()}, [{Operation::atom(), {Count::integer(), Min::integer(), Max::integer(), Sum::integer()}}]}]
}).

%% state for facade gen_server of domain class
-record(cstate, {
    regname :: atom(), % gen_server reg name
    domain :: binary(),
    self :: pid(),
    ref :: reference(),
    initts :: non_neg_integer(),
    %
    classname :: binary(),
    class_meta :: map(), % actual data of class model from dc
    %
    storage_mode :: ets | ram | runtime | category | history | transactionlog,
    integrity_mode :: sync | async | sync_fast_read | sync_fast_notify,
    cache_mode :: full | temp | none,
    cache_filter_interval :: integer(),
    changehistory :: binary(),
    driver_state :: term(), % internal state of selected driver (ex. ets for RAM mode)
    integrity_state :: term(), % integrity module state (define next worker)
    %
    class_ets :: reference(), % ets for class cache purposes.
    cache_ref :: reference(), % reference for cache filter timer
    %
    sync_ref :: reference(), % one reference for all gen_servers in class
    srv_writer :: pid(), % pid of writer gen_server
    srv_reader :: pid() | undefined, % pid of reader gen_server
    srv_notify :: pid(), % pid of notify gen_server
    srv_cache :: pid() | undefined, % pid of cache gen_server
    srv_workers_r = [] :: [{Index::non_neg_integer(),pid()}], % list of worker pids (group 'r')
    srv_workers_w = [] :: [{Index::non_neg_integer(),pid()}], % list of worker pids (group 'w')
    %
    is_ready = false :: boolean(), % ready flag is set when server is ready for work after load (cache is loaded, db is connected, created, updated).
    is_paused = false :: boolean(), % paused flag is set to reply immediately and skip handle request. Could be used on overload
    pause_reason = <<>> :: binary(), % when paused then here is description
    slowing_pause = 0 :: non_neg_integer(), % every request should sleep a bit before pushing to workers
    slowing_key :: atom(), % key for sync performance with notify pid without queue
    crud_counter = 0 :: integer(), % counter of crud requests
    data_hc = 0 :: integer(), % random counter of data (increased on every create/replace/update/delete/clear, starts from random)
    %
    propnames = [], % names of properties (cache)
    proptuples = [], % tuples of properties {Name,Type,Multi} (cache)
    attachment_propnames = [] :: [binary()], % names of attachment properties
    %
    async_pid :: pid(), % pid of spawned process initializing db
    async_monref :: reference(), % monitor reference of spawned process initializing db
    %
    ext = #{} :: map(),
    %
    waitors = [] :: list(), % waitor funs for class load
    %
    statref :: reference(), % ref for stat timer
    stattimerref :: reference(), % stat timer
    cnt_crud = 0 :: integer(), % stat handled request counter
    cnt_paused = 0 :: integer(), % stat refused request counter (pause state)
    cnt_wait = 0 :: integer(), % stat request counter when passed to wait
    cnt_waittimeout = 0 :: integer(), % stat wait queue counter of requests that filtered by timeout
    cnt_slow = 0 :: integer(), % stat slowpaused counter
    wrk_stat :: #wrk_stat{}, % stat of workers
    %
    quota :: undefined | #wait_quota{} | #pass_quota{} % sequental loading of classes to reduce: cpu usage, pglib's pool connections, opened descriptors
}).

%% state for helper processes of domain class otp-tree
-record(hstate, {
    facade_regname :: atom(),
    domain :: binary(),
    classname :: binary(),
    class_meta :: map(),
    sync_ref :: reference(),
    ets_build :: ets:tab()
}).

%% ====================================================================
%% Constants and defaults
%% ====================================================================

-define(DefaultMaxMask, []).
-define(DefaultMaxLimit, 100).
-define(MaxMaxLimit, 10000000).

-define(SlowingKey(Domain,ClassName), ?BU:to_atom_new(?BU:str("slowing#~ts@~ts",[ClassName,Domain]))).

-define(PostgresqlSchema, <<"datamodel">>).

%% ====================================================================
%% Define modules
%% ====================================================================

-define(BASICLIB, basiclib).
-define(LOGGERLIB, loggerlib).
-define(MNESIALIB, mnesialib).
-define(PGLIB, pglib).

-define(APP, dmlib).

-define(SUPV, dmlib_supv).
-define(CFG, dmlib_config).

-define(IncrementSrv, dmlib_increment_srv).

-define(QueryWorkerSupv, dmlib_query_worker_supv).
-define(QueryWorkerMan, dmlib_query_worker_manager_srv).
-define(QueryWorker, dmlib_query_worker_srv).

%% ----
-define(DSUPV, dmlib_domain_supv).
-define(DSRV, dmlib_domain_srv).
-define(ModelCfg, dmlib_domain_model_config).
%%
-define(DWORKERSUPV, dmlib_domain_worker_supv).
-define(DWORKER, dmlib_domain_worker_srv).
%%
-define(ClassSupv, dmlib_domain_class_supv).
-define(ClassSrv, dmlib_domain_class_srv).
-define(ClassR, dmlib_domain_class_op_r_srv).
-define(ClassW, dmlib_domain_class_op_w_srv).
-define(Notify, dmlib_domain_class_notify_srv).
-define(ClassWrkSupv, dmlib_domain_class_worker_supv).
-define(ClassWrk, dmlib_domain_class_worker_srv).
-define(Cache, dmlib_domain_class_cache_srv).
-define(Format, dmlib_domain_class_format).
-define(Integrity, dmlib_domain_class_integrity).
-define(ChangeHistory, dmlib_domain_class_changehistory).
-define(ClassU, dmlib_domain_class_utils).

-define(Crud, dmlib_domain_class_crud).
-define(CrudEts, dmlib_domain_class_crud_ets).
-define(CrudMnesia, dmlib_domain_class_crud_mnesia).
-define(CrudPg, dmlib_domain_class_crud_pg).
-define(CrudKafka, dmlib_domain_class_crud_kafka).
-define(CrudKafkaClickhouse, dmlib_domain_class_crud_kafka_clickhouse).

-define(Query, dmlib_domain_query).

-define(StorageUtils, dmlib_storage_utils).

-define(StorageEts, dmlib_storage_ets).

-define(MnesiaMeta, dmlib_storage_mnesia_meta).
-define(MnesiaCrud, dmlib_storage_mnesia_crud).
-define(MnesiaDirtyCrud, dmlib_storage_mnesia_dirty_crud).

-define(Postgres, dmlib_storage_postgresql).
-define(PgU, dmlib_storage_postgresql_utils).
-define(PgCrudU, dmlib_storage_postgresql_crud_utils).
-define(PgStructure, dmlib_storage_postgresql_structure).
%% +
-define(PostgresMeta, dmlib_storage_postgresql_meta).
-define(PostgresCrud, dmlib_storage_postgresql_crud).
%% +
-define(PostgresPartMeta, dmlib_storage_postgresql_partitioned_meta).
-define(PostgresPartCrud, dmlib_storage_postgresql_partitioned_crud).
-define(PostgresPartUtils, dmlib_storage_postgresql_partitioned_utils).

-define(Kafka, dmlib_storage_kafka).
-define(KafkaMeta, dmlib_storage_kafka_meta).
-define(KafkaCrud, dmlib_storage_kafka_crud).
-define(KafkaU, dmlib_storage_kafka_utils).

-define(ClickHouse, dmlib_storage_clickhouse).
-define(ClickHouseMeta, dmlib_storage_clickhouse_meta).
-define(ClickHouseCrud, dmlib_storage_clickhouse_crud).
-define(ClickHouseHttp, dmlib_storage_clickhouse_http).
-define(ClickHouseUtils, dmlib_storage_clickhouse_utils).

%% ------
%% From basiclib
%% ------
-define(BU, basiclib_utils).
-define(FilterFun, basiclib_filter_fun).
-define(FilterPg, basiclib_filter_pg).
-define(FilterCh, basiclib_filter_ch).
-define(BLmulticall, basiclib_multicall).
-define(BLstore, basiclib_store).
-define(BLmonitor, basiclib_monitor_srv).
-define(BLlog, basiclib_log).
-define(BLquota, basiclib_quota_srv).

%% ------
%% From pglib
%% ------
-define(PgDBA, pglib).
-define(PgConnectionPool, pglib).

%% ====================================================================
%% Define logs
%% ====================================================================

-define(LOGFILE, {env,?APP}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), Text)).

-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), Text)).

%% ====================================================================
%% Define other
%% ====================================================================

-define(TablePrefix, "dms__").

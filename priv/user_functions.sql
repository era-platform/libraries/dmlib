----------------------------------------------------------
CREATE OR REPLACE FUNCTION {SCHEMA}.create_readonly_user(
	login character varying,
	passwd character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
declare
    targetschema varchar;
BEGIN

    if not exists (select * from pg_roles where rolname=login) then
        execute format ('create role %I with password %s NOINHERIT login;', login, quote_literal(passwd));
    end if;
    
    execute format ('grant connect on database %I to %I;', current_database(), login);
    
    for targetschema in select schema_name from information_schema.schemata loop
        execute format ('GRANT USAGE ON SCHEMA %I TO %I;', targetschema, login);
        -- Grant access to current tables and views
        execute format ('GRANT SELECT ON ALL TABLES IN SCHEMA %I TO %I;', targetschema, login);
        -- Now do the same for sequences
        execute format ('GRANT SELECT, USAGE ON ALL SEQUENCES IN SCHEMA %I TO %I;', targetschema, login);
        
    end loop;

    -- Now make sure that's also available on new tables and views by default
    execute format ('ALTER DEFAULT PRIVILEGES GRANT SELECT ON TABLES TO %I;', login);

    -- Now make sure that's also available on new sequences by default
    execute format ('ALTER DEFAULT PRIVILEGES GRANT SELECT, USAGE ON SEQUENCES TO %I;', login);
    
    return 1;
    
END;
$BODY$;

ALTER FUNCTION {SCHEMA}.create_readonly_user(character varying, character varying)
    OWNER TO {OWNER};
	
----------------------------------------------------------
CREATE OR REPLACE FUNCTION {SCHEMA}.create_temporary_user(
	login character varying,
	passwd character varying,
	readwrite boolean)
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    targetschema varchar;
    tmp_login varchar;
BEGIN

    tmp_login := 'tmp_'||login;

    IF NOT EXISTS (SELECT * FROM pg_roles WHERE rolname=tmp_login) THEN
        EXECUTE format('CREATE ROLE %I WITH PASSWORD %s NOINHERIT LOGIN VALID UNTIL %s;', tmp_login, quote_literal(passwd), quote_literal(timestamp 'tomorrow'));
    END IF;

    EXECUTE format('GRANT CONNECT ON database %I TO %I;', current_database(), tmp_login);
    IF readwrite THEN
        EXECUTE format('GRANT ALL privileges ON database %I TO %I;', current_database(), tmp_login);
    ELSE
        FOR targetschema IN SELECT schema_name FROM information_schema.schemata LOOP
            EXECUTE format('GRANT USAGE ON SCHEMA %I TO %I;', targetschema, tmp_login);
            -- Grant access to current tables and views
            EXECUTE format('GRANT SELECT ON ALL tables IN SCHEMA %I TO %I;', targetschema, tmp_login);
            -- Now do the same for sequences
            EXECUTE format('GRANT SELECT, USAGE ON ALL sequences IN SCHEMA %I TO %I;', targetschema, tmp_login);
        END LOOP;
    END IF;

    -- Now make sure that's also available ON new tables and views by default
    EXECUTE format('ALTER DEFAULT privileges GRANT SELECT ON tables TO %I;', tmp_login);

    -- Now make sure that's also available ON new sequences by default
    EXECUTE format('ALTER DEFAULT privileges GRANT SELECT, USAGE ON sequences TO %I;', tmp_login);

    RETURN tmp_login;

END;
$BODY$;

ALTER FUNCTION {SCHEMA}.create_temporary_user(character varying, character varying, boolean)
    OWNER TO {OWNER};

----------------------------------------------------------
DROP FUNCTION IF EXISTS {SCHEMA}.get_temporary_users();
CREATE OR REPLACE FUNCTION {SCHEMA}.get_temporary_users(
	)
    RETURNS TABLE(rolname character varying, rolpassword character varying, rolvaliduntil timestamp with time zone)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
begin

    return query(
                 select (res.val[1])::varchar as rolname,
                        pa.rolpassword::varchar as rolpassword,
                        pa.rolvaliduntil as rolvaliduntil
                 from (
                    select regexp_split_to_array(unnest(datacl)::varchar,'=|/') as val
                    from pg_database
                    where datname=current_database()
                    ) as res
                 join pg_authid as pa on res.val[1]=pa.rolname
                 where res.val[1] like 'tmp_%'
        );
    
end;
$BODY$;

ALTER FUNCTION {SCHEMA}.get_temporary_users()
    OWNER TO {OWNER};

----------------------------------------------------------
DROP FUNCTION IF EXISTS {SCHEMA}.get_readonly_users();
CREATE OR REPLACE FUNCTION {SCHEMA}.get_readonly_users(
	)
    RETURNS TABLE(rolname character varying, rolpassword character varying)
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
begin

    return query(
       Select a.rolname,
              a.rolpassword
	   From (
          Select trim('"' FROM (res.val[1]))::varchar as rolname,
                 pa.rolpassword::varchar as rolpassword
	 	  From (
              Select regexp_split_to_array(unnest(datacl)::varchar,'=|/') as val
              From pg_database
              Where datname=current_database()
          ) as res
          Join pg_authid as pa on res.val[1]=pa.rolname or res.val[1]='"'||pa.rolname||'"'
		) a
       Where a.rolname like '%_era_readonly%'
    );
    
end;
$BODY$;

ALTER FUNCTION {SCHEMA}.get_readonly_users()
    OWNER TO {OWNER};

----------------------------------------------------------
CREATE OR REPLACE FUNCTION {SCHEMA}.drop_readonly_user(
	login character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
declare
    targetschema varchar;
BEGIN

    if exists (select * from pg_roles where rolname=login) then

	    execute format ('DROP OWNED BY %I;', login);
	    execute format ('DROP ROLE %I;', login);
        
        return 1;
    else
        return -1;
    end if;

END;
$BODY$;

ALTER FUNCTION {SCHEMA}.drop_readonly_user(character varying)
    OWNER TO {OWNER};

----------------------------------------------------------
CREATE OR REPLACE FUNCTION {SCHEMA}.drop_temporary_user(
	login character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    targetschema varchar;
BEGIN

    IF EXISTS (SELECT * FROM pg_roles WHERE rolname=login AND rolname LIKE 'tmp_%') THEN

   	    execute format ('DROP OWNED BY %I;', login);
	    execute format ('DROP ROLE %I;', login);

        RETURN 1;
    ELSE
        RETURN -1;
    END IF;

END;
$BODY$;

ALTER FUNCTION {SCHEMA}.drop_temporary_user(character varying)
    OWNER TO {OWNER};

----------------------------------------------------------
CREATE OR REPLACE FUNCTION {SCHEMA}.drop_expired_temporary_users(
	)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    login varchar;
BEGIN

    FOR login IN select rolname from {SCHEMA}.get_temporary_users() WHERE rolvaliduntil <= timestamp 'now' LOOP
        PERFORM {SCHEMA}.drop_temporary_user(login);
    END LOOP;

    RETURN 1;

END;
$BODY$;

ALTER FUNCTION {SCHEMA}.drop_expired_temporary_users()
    OWNER TO {OWNER};
	
----------------------------------------------------------
CREATE OR REPLACE FUNCTION {SCHEMA}.purge_temporary_users(
	)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    login varchar;
BEGIN

    FOR login IN select rolname from {SCHEMA}.get_temporary_users() LOOP
        PERFORM {SCHEMA}.drop_temporary_user(login);
    END LOOP;

    RETURN 1;

END;
$BODY$;

ALTER FUNCTION {SCHEMA}.purge_temporary_users()
    OWNER TO {OWNER};

----------------------------------------------------------
CREATE OR REPLACE FUNCTION {SCHEMA}.ensure_readonly_user(
	login character varying,
	passwd character varying,
	passwdmd5 character varying)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN

    IF NOT EXISTS (SELECT * FROM {SCHEMA}.get_readonly_users() WHERE rolname LIKE login) THEN
        PERFORM * FROM {SCHEMA}.create_readonly_user(login,passwd);
    ELSE
	  IF NOT EXISTS (SELECT * FROM {SCHEMA}.get_temporary_users() WHERE rolname LIKE login AND rolpassword LIKE passwdmd5 ) THEN
		PERFORM * FROM {SCHEMA}.drop_readonly_user(login);
		PERFORM * FROM {SCHEMA}.create_readonly_user(login,passwd);
	  END IF;
    END IF;
    
    RETURN 1;

END;
$BODY$;

ALTER FUNCTION {SCHEMA}.ensure_readonly_user(character varying, character varying, character varying)
    OWNER TO {OWNER};
----------------------------------------------------------    

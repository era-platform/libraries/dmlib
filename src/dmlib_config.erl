%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.05.2021
%%% @doc Configuration functions

-module(dmlib_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    log_destination/1,
    get_storages_function/0,
    get_readonly_credentials_function/0,
    notify_function/0,
    drop_subscriptions_function/0,
    store_changehistory_function/0,
    validate_function/0,
    store_function/0,
    delete_attachment_function/0,
    delete_attachments_function/0,
    clear_attachments_function/0,
    cache_filter_timeout/0,
    load_quota_limit_domains/0,
    load_quota_limit_classes/0,
    quota_pass_timeout_domains/0,
    quota_pass_timeout_classes/0,
    stat_enabled_function/0,
    stat_enabled/0,
    class_readers_count/0,
    class_writers_count/0,
    pg_max_connections_default/0,
    query_workers_count/0,
    decrypt_function/0
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% return log destination for loggerlib, ex. {app,prefix}
log_destination(Default) ->
    ?BU:get_env(?APP,'log_destination',Default).

%% return value from opts
%% fun(Domain::term(), StorageType::binary(), StorageInstanceTerm::binary() | map()) -> {ok,[StorageParams]} | {error,Reason::term()}.
get_storages_function() ->
    ?BU:get_env(?APP,'get_storages_function',undefined).

%% return value from opts
%% fun(Domain, DbType, DbParams) -> {ok,Login,Pwd} | false.
get_readonly_credentials_function() ->
    ?BU:get_env(?APP,'get_readonly_credentials_function',undefined).

%% return value from opts
%% fun(Domain::term(), CN::binary(), Operation::atom(), EntityInfo::{Id,E0,E1}|undefined, ModifierInfo::{Type,Id}|undefined) -> ok.
notify_function() ->
    ?BU:get_env(?APP,'notify_function',undefined).

%% return value from opts
%% fun(Domain::term(), CN::binary()) -> ok.
drop_subscriptions_function() ->
    ?BU:get_env(?APP,'drop_subscriptions_function',undefined).

%% return value from opts
%% fun(Domain::term(), CN::binary(), Operation::atom(), EntityInfo::{Id,E0,E1}|undefined, ModifierInfo::{Type,Id}|undefined) -> ok.
store_changehistory_function() ->
    ?BU:get_env(?APP,'store_changehistory_function',undefined).

%% return value from opts
%% fun(Domain::term(), Validator::term(), CN::binary(), Operation::atom(), EntityInfo::{Id,E0,E1}, ModifierInfo::{Type,Id}|undefined) -> {ok,E2} | {error,Reason::term()}.
validate_function() ->
    ?BU:get_env(?APP,'validate_function',undefined).

%% return value from opts
%% fun(push, {Key,Value}) -> ok | error.
%% fun(pull, Key) -> {ok,Value} | error.
store_function() ->
    ?BU:get_env(?APP,'store_function',undefined).

%% return value from opts
%% fun(Domain::term(), ClassItem::map(), Entity::map(), Property::binary(), StoreFilename::binary()) -> ok | {error,Reason::term()}.
delete_attachment_function() ->
    ?BU:get_env(?APP,'delete_attachment_function',undefined).

%% return value from opts
%% fun(Domain::term(), ClassItem::map(), Entity::map()) -> ok | {error,Reason::term()}.
delete_attachments_function() ->
    ?BU:get_env(?APP,'delete_attachments_function',undefined).

%% return value from opts
%% fun(Domain::term(), ClassItem::map()) -> ok | {error,Reason::term()}.
clear_attachments_function() ->
    ?BU:get_env(?APP,'clear_attachments_function',undefined).

%% return value from opts
%% integer
cache_filter_timeout() ->
    ?BU:get_env(?APP,'cache_filter_timeout',300000).

%% return value from opts
%% integer
load_quota_limit_domains() ->
    ?BU:get_env(?APP,'load_quota_limit_domains',3).

%% return value from opts
%% integer
load_quota_limit_classes() ->
    ?BU:get_env(?APP,'load_quota_limit_classes',3).

%% return value from opts
%% integer
quota_pass_timeout_domains() ->
    ?BU:get_env(?APP,'quota_pass_timeout_domains',5000).

%% return value from opts
%% integer
quota_pass_timeout_classes() ->
    ?BU:get_env(?APP,'quota_pass_timeout_classes',1000).

%% return value from opts
%% fun(Domain::term()) -> boolean().
stat_enabled_function() ->
    ?BU:get_env(?APP,'stat_enabled_function',undefined).

%% return value from opts
%% fun(Domain::term()) -> boolean().
stat_enabled() ->
    ?BU:get_env(?APP,'stat_enabled',undefined).


%% return value from opts
%% integer
class_readers_count() ->
    ?BU:get_env(?APP,'class_readers_count',10).

%% return value from opts
%% integer
class_writers_count() ->
    ?BU:get_env(?APP,'class_writers_count',4). % 10

%% return value from opts
%% integer
pg_max_connections_default() ->
    min(256, max(1, ?BU:get_env(?APP,'pg_max_connections_default',10))).

%% return value from opts
%% integer
query_workers_count() ->
    ?BU:get_env(?APP,'query_workers_count',4).

%% return value from opts
%% integer
decrypt_function() ->
    ?BU:get_env(?APP,'decrypt_function',undefined).

%% ====================================================================
%% Internal functions
%% ====================================================================
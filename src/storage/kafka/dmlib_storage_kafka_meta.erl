%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 14.04.2021
%%% @doc

-module(dmlib_storage_kafka_meta).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([topic_name/2,
         ensure_topic/4]).

-export([read_topic_metadata/1,
         read_topic_metadata/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------
%% Build topic name
%%   Available: a-zA-Z0-9._-
%%   MaxLength: 249
%% -------------------------
topic_name(Domain,ClassName) ->
    F = fun(A,B) ->
                 lists:foldl(fun(S,{Acc,Bad}) when S>=$a, S=<$z -> {[S|Acc],Bad};
                                (S,{Acc,Bad}) when S>=$A, S=<$Z -> {[S|Acc],Bad};
                                (S,{Acc,Bad}) when S>=$0, S=<$9 -> {[S|Acc],Bad};
                                (S,{Acc,Bad}) when S==$.;S==$_;S==$- -> {[S|Acc],Bad};
                                (S,{Acc,Bad}) when S==$/ -> {[$.|Acc],Bad};
                                (_,{Acc,_}) -> {[$.,$.|Acc],true}
                             end, {[],false}, ?BU:str("~ts...~ts",[A,B]))
        end,
    {Res,Bad} = F(Domain,ClassName),
    case Bad of
        false when length(Res)=<249 -> ?BU:to_binary(lists:reverse(Res));
        true when length(Res)=<249-8 ->
            HC = erlang:phash2({Domain,ClassName}) rem 100000000, % max I have seen is 134217603,
            ?BU:strbin("~ts...~ts",[lists:reverse(Res),HC]);
        _ ->
            HC = erlang:phash2({Domain,ClassName}) rem 100000000, % max I have seen is 134217603,
            ExByteCount = length(Res)-249-8-3, % max-hash-...
            <<_:ExByteCount/binary,ClassName1/binary>> = ClassName,
            {Res1,_} = F(Domain,<<"...",ClassName1/binary>>),
            ?BU:strbin("~ts...~ts",[lists:reverse(Res1),HC])
    end.

%% -------------------------
%% Make table in Mnesia for domain-model-class
%% -------------------------
ensure_topic([DbParams|_],Domain,ClassName,Meta) when is_map(DbParams) ->
    TopicDomain = maps:get(<<"domain">>,DbParams,Domain),
    Topic = topic_name(TopicDomain,ClassName),
    case do_ensure_topic(DbParams,Topic,Meta) of
        ok -> ok;
        {ok,_Props}=Ok -> Ok;
        {error,Reason}=Err ->
            ?LOG('$error',"~ts. '~ts' * '~ts' kafka ensure topic error: ~n\t~120tp...",[?APP,Domain,ClassName,Reason]),
            Err;
        {retry_after,Timeout,Reason} ->
            ?LOG('$info',"~ts. '~ts' * '~ts' wait for kafka... (~ts)",[?APP,Domain,ClassName,Reason]),
            timer:sleep(Timeout),
            ensure_topic(DbParams,Domain,ClassName,Meta)
    end.

%% ----------------------------------------------
%% Return metadata for topics from kafka
%% #{
%%   brokers => [#{host,node_id,port,rack}],
%%   cluster_id :: binary()
%%   controller_id :: integer()
%%   topics => [#{name,partitions::[#{isr,leader,partition,replicas,error_code}],is_internal,error_code}]
%%  }
%% ----------------------------------------------
read_topic_metadata(Endpoints) ->
    case brod:get_metadata(Endpoints) of
        {ok,_KafkaMeta}=Ok ->
            ?LOG('$trace',"Kafka metadata: ~n\t~120tp",[_KafkaMeta]),
            Ok;
        T -> T
    end.
read_topic_metadata(Endpoints,Topic) ->
    case brod:get_metadata(Endpoints,[Topic]) of
        {ok,_KafkaMeta}=Ok ->
            ?LOG('$trace',"Kafka topic '~ts' metadata: ~n\t~120tp",[Topic,_KafkaMeta]),
            Ok;
        T -> T
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

do_ensure_topic(DbParams,Topic,Meta) when is_map(DbParams) ->
    Endpoints = maps:get(<<"endpoints">>, DbParams),
    Opts = maps:get(opts,Meta),
    NumPartitions = erlang:min(10,erlang:max(1,?BU:to_int(maps:get(<<"partition_count">>,DbParams,maps:get(<<"partition_count">>,Opts,2)),2))),
    ReplicationFactor = erlang:min(4,erlang:max(1,?BU:to_int(maps:get(<<"replication_factor">>,DbParams,maps:get(<<"replication_factor">>,Opts,2)),2))),
    Timeout = 3000, % TODO: setup timeout?
    TopicConfigs = [#{configs => maps:fold(fun(K,V,Acc) -> [#{name => K, value => V} | Acc] end, [], topic_configs(DbParams,Topic,Meta)),
                      num_partitions => NumPartitions,
                      assignments => [], % replica_assignment => []
                      replication_factor => ReplicationFactor,
                      name => Topic % topic => Topic
                     }],
    case brod:create_topics(Endpoints, TopicConfigs, #{timeout => Timeout}) of
        ok ->
            ?LOG('$trace',"Kafka topic '~ts' created",[Topic]),
            timer:sleep(100), % attempt to avoid error leader_not_available just after creation for 1 times
            case read_topic_metadata(Endpoints,Topic) of
                {ok,KafkaMeta} -> do_check_topic_meta_result(Topic,KafkaMeta#{kafka_storage_opts => DbParams});
                Other ->
                    ?LOG('$error',"Kafka topic has error(a): ~n\tTopic: '~ts',~n\tResult: ~120tp",[Topic,Other]),
                    {error,{internal_error,<<"Topic was not created. See logs">>}}
            end;
        {error,topic_already_exists} ->
            case read_topic_metadata(Endpoints,Topic) of
                {ok,KafkaMeta} -> do_check_topic_meta_result(Topic,KafkaMeta#{kafka_storage_opts => DbParams});
                Other ->
                    ?LOG('$error',"Kafka topic has error(b): ~n\tTopic: '~ts',~n\tResult: ~120tp",[Topic,Other]),
                    {error,{internal_error,<<"Topic was not created. See logs">>}}
            end;
        {error,invalid_replication_factor}=Err -> Err;
        {error,invalid_topic_exception}=Err -> Err;
        {error,_}=Err -> Err;
        T ->
            ?LOG('$error',"Kafka topic create error: ~n\tTopic: '~ts',~n\tEndpoints: ~120tp,~n\tResult: ~120tp",[Topic,Endpoints,T]),
            {error,{internal_error,<<"Topic was not created">>}}
    end.

%% @private
topic_configs(_DbParams,_Topic,_Meta) ->
    % https://kafka.apache.org/documentation/#topicconfigs
    #{
        <<"cleanup.policy">> => "delete" % delete, [delete | compact]; log.cleanup.policy
        %, <<"compression.type">> => "producer" % producer, [uncompressed, zstd, lz4, snappy, gzip, producer]; compression.type
        %, <<"delete.retention.ms">> => 86400000 % 86400000 (1 day); log.cleaner.delete.retention.ms
        %, <<"file.delete.delay.ms">> => 60000 % 60000 (1 minute); log.segment.delete.delay.ms
        %, <<"flush.messages">> => 9223372036854775807 % 9223372036854775807, [1,...]; log.flush.interval.messages
        %, <<"flush.ms">> => 9223372036854775807 % 9223372036854775807, [0,...]; log.flush.interval.ms
        %, <<"follower.replication.throttled.replicas">> => 9223372036854775807 % "", [partitionId]:[brokerId]; -
        %, <<"index.interval.bytes">> => 4096 % 4096 (4 kibibytes); log.index.interval.bytes
        %, <<"leader.replication.throttled.replicas">> => "" % "" [partitionId]:[brokerId]; -
        %, <<"local.retention.bytes">> => -2 % -2 [-2,...]; log.local.retention.bytes
        %, <<"local.retention.ms">> => -2 % -2 [-2,...]; log.local.retention.ms
        %, <<"max.compaction.lag.ms">> => 9223372036854775807 % 9223372036854775807 [1,...]; log.cleaner.max.compaction.lag.ms
        %, <<"max.message.bytes">> => 1048588 % 1048588 [0,...]; message.max.bytes
        %, <<"message.timestamp.after.max.ms">> => 9223372036854775807 % 9223372036854775807 [0,...]; log.message.timestamp.after.max.ms
        %, <<"message.timestamp.before.max.ms">> => 9223372036854775807 % 9223372036854775807 [0,...]; log.message.timestamp.before.max.ms
        %, <<"message.timestamp.difference.max.ms">> => 9223372036854775807 % 9223372036854775807 [0,...]; log.message.timestamp.difference.max.ms
        %, <<"message.timestamp.type">> => "CreateTime" % CreateTime, [CreateTime, LogAppendTime]; log.message.timestamp.type
        %, <<"min.cleanable.dirty.ratio">> => 0.5 % 0.5, [0,...,1]; log.cleaner.min.cleanable.ratio
        %, <<"min.compaction.lag.ms">> => 0 % 0, [0,...]; log.cleaner.min.compaction.lag.ms
        %, <<"min.insync.replicas">> => 1 % 1, [1,...]; min.insync.replicas
        %, <<"preallocate">> => false % false, boolean(); log.preallocate
        %, <<"remote.storage.enable">> => false % false, boolean(); -
        %, <<"retention.bytes">> => -1 % -1 [-1,...]; log.retention.bytes
        %, <<"retention.ms">> => 10800000 % 604800000 (7 days) [-1,...]; log.retention.ms
        %, <<"segment.bytes">> => 1073741824 % 1073741824 (1 gibibyte), [14,...]; log.segment.bytes
        %, <<"segment.index.bytes">> => 10485760 % 10485760 (10 mebibytes), [4,...]; log.index.size.max.bytes
        %, <<"segment.jitter.ms">> => 0 % 0 [0,...]; log.roll.jitter.ms
        %, <<"segment.ms">> => 604800000, % 604800000 (7 days), [1,...]; log.roll.ms
        %, <<"unclean.leader.election.enable">> => false % false, boolean(); unclean.leader.election.enable
        %, <<"message.downconversion.enable">> => true % true, boolean(); log.message.downconversion.enable
    }.

%% @private
do_check_topic_meta_result(Topic,KafkaMeta) ->
    [TopicMeta|_] = maps:get('topics',KafkaMeta),
    ErrCode = maps:get('error_code',TopicMeta),
    IsInternal = maps:get('is_internal',TopicMeta),
    PartMeta = maps:get('partitions',TopicMeta),
    AnyPartOk = lists:any(fun(PartMap) -> maps:get('error_code',PartMap)=='no_error' end, PartMeta),
    case {ErrCode,AnyPartOk,IsInternal} of
        {'no_error',true,false} ->
            {ok,KafkaMeta};
        {'no_error',false,false} ->
            ?LOG('$error',"Kafka topic found internal(a): ~n\tTopic: '~ts',~n\tMetadata: ~120tp",[Topic,TopicMeta]),
            {error,{internal_error,<<"Topic has no ready partition. See logs">>}};
        {'no_error',true,true} ->
            ?LOG('$error',"Kafka topic found internal(b): ~n\tTopic: '~ts',~n\tMetadata: ~120tp",[Topic,TopicMeta]),
            {error,{internal_error,<<"Topic found internal. See logs">>}};
        {_,_,_} ->
            ?LOG('$error',"Kafka topic has error(c): ~n\tTopic: '~ts',~n\tMetadata: ~120tp",[Topic,TopicMeta]),
            {error,{internal_error,<<"Topic was not created. See logs">>}}
    end.

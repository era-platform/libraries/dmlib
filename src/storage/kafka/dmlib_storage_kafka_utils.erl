%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 14.04.2021
%%% @doc

-module(dmlib_storage_kafka_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([send_to_kafka/6]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------
%% send record to kafka
%% --------------------------------
send_to_kafka(KKey, KValue, Topic, Partition, Endpoints, OtherOpts) ->
    produce(KKey, KValue, Topic, Partition, Endpoints, OtherOpts, 0).

%% @private
produce(KKey, KValue, Topic, Partition, Endpoints, OtherOpts, Attempts) ->
    % debug({Endpoints,Topic,Partition,KKey,KValue}, 3)
    % TODO: topic own producer, may be even by partition
    KafkaClientKey = ?BU:to_atom_new(?BU:strbin("producer_~p_~p_~p",[Partition,erlang:phash2(Endpoints),erlang:phash2(lists:reverse(Endpoints))])),
    % TODO: connection pool
    % TODO: stop connections and remove from store when class down
    % TODO: auto remove from store when connection is down
    case check_producer(KafkaClientKey,Endpoints,Topic) of
        {error,_}=Err -> Err;
        ok ->
            case do_produce(KKey,KValue,Topic,Partition,KafkaClientKey,OtherOpts) of
                ok -> % when sync
                    ?LOG('$trace',"Produce kafka '~ts' : p=~p -> ok",[Topic,Partition]),
                    ok;
                {ok,_CallRef} -> % when async
                    ?LOG('$trace',"Produce kafka '~ts' : p=~p -> ok",[Topic,Partition]),
                    ok;
                {error,_}=Err when Attempts==0 -> % try again with new client connection
                    ?LOG('$info',"Produce kafka '~ts' : p=~p -> ~120tp. Reattempt",[Topic,Partition,Err]),
                    catch brod:stop_client(KafkaClientKey),
                    ?BLstore:delete_u(KafkaClientKey),
                    produce(KKey, KValue, Topic, Partition, Endpoints, OtherOpts, Attempts+1);
                {error,_}=Err ->
                    ?LOG('$error',"Produce to ~ts : p=~p -> ~120tp",[Topic,Partition,Err]),
                    Err
            end
    end.

%% @private
check_producer(KafkaClientKey,Endpoints,Topic) ->
    case ?BLstore:find_u(KafkaClientKey) of
        {_,true} -> ok;
        false ->
            % partitioner = random_by_key
            Opts = [{reconnect_cool_down_seconds,10}, % TODO from opts. brod' default: 1
                    {auto_start_producers,true}, % brod' default: false
                    {default_producer_config,[]}],
            case brod:start_client(Endpoints, KafkaClientKey, Opts) of
                {error,_}=Err ->
                    ?LOG('$error',"Starting kafka client '~ts' error: ~120tp",[KafkaClientKey,Err]),
                    Err;
                ok ->
                    case brod:start_producer(KafkaClientKey, Topic, []) of
                        {error,_}=Err ->
                            ?LOG('$error',"Starting producer '~ts' on '~ts' error: ~120tp",[KafkaClientKey,Topic,Err]),
                            Err;
                        ok ->
                            ?BLstore:store_u(KafkaClientKey,true),
                            ?LOG('$trace',"Started producer '~ts' on '~ts'",[KafkaClientKey,Topic]),
                            ok
                    end end end.

%% @private
do_produce(KKey, KValue, Topic, Partition, KafkaClientKey, OtherOpts) ->
    case ?BU:get_by_key(<<"mode">>,OtherOpts,<<"async">>) of
        <<"async">> -> brod:produce(KafkaClientKey, Topic, Partition, KKey, KValue);
        <<"sync">> -> brod:produce_sync(KafkaClientKey, Topic, Partition, KKey, KValue)
        %<<"notify">> -> brod:produce_cb(KafkaClientKey, Topic, Partition, KKey, KValue, fun(_Partition, _BaseOffset) -> ok end)
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%%%% @private
%%debug({Endpoints,Topic,Partition,KKey,KValue},ReturnErrorCallCount) when is_integer(ReturnErrorCallCount) ->
%%    ?LOG('$trace',"To Kafka. Topic: '~ts', Partition: ~120p,~n\tEndpoints: ~120tp,~n\tKey: ~ts,~n\tValue: ~ts",[Topic,Partition,Endpoints,KKey,KValue]),
%%    % DEBUG
%%    case ReturnErrorCallCount == 0 orelse ?BU:random(ReturnErrorCallCount) of
%%        true -> ok;
%%        0 -> {error,0};
%%        _ -> ok
%%    end.
%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 14.04.2021
%%% @doc

-module(dmlib_storage_kafka).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_db_params/2]).
-export([ensure_topic/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------------------
%% return kafka connection params for data-model-class in domain
%% -----------------------------------
get_db_params(Domain,Meta) ->
    db_params(Domain,Meta).

%% @private
db_params(Domain,Meta) ->
    case ?CFG:get_storages_function() of
        undefined -> {error,{internal_error,?BU:strbin("Invalid initial parameters. Expected 'get_storages_function'",[])}};
        Fun when not is_function(Fun,3) -> {error,{internal_error,?BU:strbin("Invalid initial parameters. Expected valid 'get_storages_function' -> fun/3",[])}};
        Fun ->
            case maps:get(<<"storage_instance">>,maps:get(opts,Meta),<<>>) of
                <<>> -> {error,{internal_error,?BU:strbin("Invalid class metadata. Expected existing opts.storage_instance",[])}};
                InstanceValue ->
                    SType = <<"kafka">>,
                    db_params_1(Fun(Domain,SType,InstanceValue),SType)
            end end.

%% @private
db_params_1({error,_}=Err,_SType) -> Err;
db_params_1([],SType) -> {error,{internal_error,?BU:strbin("Invalid class metadata. Expected existing storage_instance (opts.storage_instance should match at least 1 domain's storage of type: '~ts'",[SType])}};
db_params_1({ok,Storages},_SType) ->
    Fx = fun(M) when is_map(M) ->
            Res0 = #{
                      <<"endpoints">> => lists:map(fun(EP) -> {?BU:to_list(maps:get(<<"host">>,EP)),?BU:to_int(maps:get(<<"port">>,EP))} end, maps:get(<<"endpoints">>,M)),
                      <<"default_reconnect_cool_down_seconds">> => ?BU:to_int(maps:get(<<"default_reconnect_cool_down_seconds">>,M,10),10),
                      <<"partitioner">> => maps:get(<<"partitioner">>,M,<<"random_by_key">>)
                    },
            Res1 = case maps:get(<<"send_mode">>,M,undefined) of undefined -> Res0; SendMode -> Res0#{<<"send_mode">> => SendMode} end,
            Res2 = case maps:get(<<"partition_count">>,M,undefined) of undefined -> Res1; PartitionCount -> Res1#{<<"partition_count">> => PartitionCount} end,
            Res3 = case maps:get(<<"replication_factor">>,M,undefined) of undefined -> Res2; ReplicationFactor -> Res2#{<<"replication_factor">> => ReplicationFactor} end,
            _Res4 = case maps:get(<<"domain">>,M,undefined) of undefined -> Res3; Domain -> Res3#{<<"domain">> => Domain} end
         end,
    DbParams = lists:map(fun(St) -> Fx(St) end, Storages),
    {ok,DbParams}.

%% ====================================================================
%% Build structure facade
%% ====================================================================

%% -----------------------------------
%% make table for data-model-class
%% -----------------------------------
ensure_topic(Domain,ClassName,Meta) ->
    StorageMode = maps:get(storage_mode,Meta),
    case db_params(Domain,Meta) of
        {error,_}=Err -> Err;
        {ok,DbParams} when StorageMode == <<"transactionlog">> ->
            ?KafkaMeta:ensure_topic(DbParams,Domain,ClassName,Meta)
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

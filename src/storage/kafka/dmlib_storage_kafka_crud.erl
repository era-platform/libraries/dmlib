%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 14.04.2021
%%% @doc Implements CRUD operations on kafka

-module(dmlib_storage_kafka_crud).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([lookup/4, find/4,
         read/4,
         create/4, replace/4, update/4, delete/4,
         clear/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================


%% -----------------------------------------
%% Execute lookup select request to postgre db and return entity
%% -----------------------------------------
-spec lookup(Meta::map(),Content::binary(),DbParamsL::[map()],PropNames::[binary()]) -> {ok,[Id::binary()]} | {error,Reason::term()}.
%% -----------------------------------------
lookup(_Meta,_Content,_DbParamsL,_PropNames) ->
    {error,{405,<<"Method not allowed">>}}.

%% -----------------------------------------
%% Execute select request to postgre db and return entity
%% -----------------------------------------
-spec find(Meta::map(),Id::binary(),DbParamsL::[map()],PropNames::[binary()]) -> {ok,Entity::map()} | {error,Reason::term()} | false.
%% -----------------------------------------
find(_Meta,_Id,_DbParamsL,_PropNames) ->
    % TODO
    false.

%% -----------------------------------------
%% Execute select request to postgre db and return entity
%% -----------------------------------------
-spec read(Meta::map(),SelectOpts::map(),DbParamsL::[map()],PropNames::[binary()]) -> {ok,[Entity::map()]} | {error,Reason::term()}.
%% -----------------------------------------
read(_Meta,_SelectOpts,_DbParamsL,_PropNames) ->
    {error,{405,<<"Method not allowed">>}}.

%% -----------------------------------------
%% Execute insert/update request to postgre db
%% Means create entity (INSERT operation)
%% -----------------------------------------
-spec create(Meta::map(),Entity::map(),DbParamsL::[map()],KafkaProps::map()) -> ok | {error,Reason::term()}.
%% -----------------------------------------
create(_Meta,Entity,DbParamsL,KafkaProps) ->
    insert(Entity,DbParamsL,KafkaProps).

%% -----------------------------------------
%% Execute insert/update request to postgre db
%% Means replace entity (UPDATE operation)
%% -----------------------------------------
-spec replace(Meta::map(),Entity::map(),DbParamsL::[map()],KafkaProps::map()) -> ok | {error,Reason::term()}.
%% -----------------------------------------
replace(_Meta,Entity,DbParamsL,KafkaProps) ->
    insert(Entity,DbParamsL,KafkaProps).

%% -----------------------------------------
%% Execute insert/update request to postgre db
%% Means merge/update entity (UPDATE operation)
%% -----------------------------------------
-spec update(Meta::map(),Entity::map(),DbParamsL::[map()],KafkaProps::map()) -> ok | {error,Reason::term()}.
%% -----------------------------------------
update(_Meta,Entity,DbParamsL,KafkaProps) ->
    insert(Entity,DbParamsL,KafkaProps).

%% -----------------------------------------
%% Execute delete request to postgre db
%% Means delete entity (DELETE operation)
%% -----------------------------------------
-spec delete(Meta::map(),Id::binary(),DbParamsL::[map()],KafkaProps::map()) -> ok | {error,Reason::term()}.
%% -----------------------------------------
delete(_Meta,_Id,_DbParamsL,_KafkaProps) ->
    {error,{405,<<"Method not allowed">>}}.

%% -----------------------------------------
%% Execute delete request to postgre db
%% Means delete entity (DELETE operation)
%% -----------------------------------------
-spec clear(Meta::map(),DbParamsL::[map()]) -> ok | {error,Reason::term()}.
%% -----------------------------------------
clear(_Meta,_DbParamsL) ->
    {error,{405,<<"Method not allowed">>}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

insert(Entity,[DbParams|_],KafkaProps) ->
    Id = maps:get(<<"id">>,Entity),
    IdH = erlang:phash2(Id),
    Value0 = maps:map(fun(_,V) when is_map(V); is_list(V) -> jsx:encode(V);
                         (_,V) -> V
                      end, Entity),
    Value = jsx:encode(Value0),%#{<<"_idh">> => IdH}),
    Topic = maps:get(topic,KafkaProps),
    PartitionCount = maps:get(partition_count,KafkaProps),
    % TODO may be map into PartitionMeta.Item.'partition' values
    Partition = IdH rem PartitionCount, % TODO: from opts. Default: partitioner = random_by_key
    % TODO may be replace by PartitionMeta.Item.'partition' -> PartitionMeta.Item.'leader' -> Brokers.Item.'node_id' -> Brokers.Item.'host'&'port'
    Endpoints = maps:get(<<"endpoints">>, DbParams),
    %PartEPs = maps:get(partition_endpoints,KafkaProps),
    %Endpoint = maps:get(Partition,PartEPs),
    %Endpoints = [Endpoint | maps:get(<<"endpoints">>, DbParams)]
    OtherOpts = #{<<"mode">> => maps:get(mode,KafkaProps,<<"async">>)},
    ?LOG('$trace',"Put Value: ~ts", [Value]),
    Res = case ?KafkaU:send_to_kafka(Id,Value,Topic,Partition,Endpoints,OtherOpts) of
              ok -> ok;
              {error,_}=Err -> Err
          end,
    ?LOG('$trace',"Kafka params: topic='~ts', partition=~p, endpoints=~500tp, Opts=~500tp, Result=~500tp", [Topic, Partition, Endpoints, OtherOpts, Res]),
    Res.
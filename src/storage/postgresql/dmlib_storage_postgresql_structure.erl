%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.03.2021
%%% @doc Routines to ensure table structure in postgresql (with partitions)

-module(dmlib_storage_postgresql_structure).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    ensure_tables/2,
    ensure_table/2,
    ensure_partition/3,
    get_partitions/4,
    drop_all_indexes/2
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include_lib("epgsql/include/epgsql.hrl").

-define(SqlTout, 300000). % 5 min to create index for large tables
-define(SqlToutMove, 600000).

-define(cfun(), {?MODULE,element(2, element(2, process_info(self(), current_function)))}).

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------
-spec ensure_tables(DbParams::map() | list(), Metas::[map()]) -> ok | {error,Reason::list()}.
%% --------------------------
ensure_tables(_,[]) -> ok;
ensure_tables(DbParams,Metas) ->
    try do_ensure_tables(DbParams,Metas)
    catch
        throw:R -> R;
        _C:_E:StackTrace ->
            ?LOG('$crash', "PostgreDB. (~120tp) crash. {~120tp, ~120tp} ~n\tStack: ~160tp",[?cfun(),_C,_E,StackTrace]),
            {error,[{<<"reason">>,<<"operation crash">>}]}
    end.

%% --------------------------
%% DbParams - default r database params
%% Meta - #{
%%          <<"schema">> => schema name to create in database
%%          <<"tablename">> => table name to create in database
%%          <<"fields">> => [ - fields in created table
%%                           #{<<"name">> => FieldName, - name of field
%%                             <<"type">> => FieldType, - postgresql type of field (ex. varchar, timestamp)
%%                             <<"required">> => Boolean, - define the presence of 'not null' field parameter, true -> add 'not null'
%%                             <<"default">> => DefValue} - default value for field
%%                          ],
%%          <<"opts">> => #{ - optional parameters for create table
%%                          <<"partition">> => - defines if created table is partitioned
%%                                             #{<<"fields">> => FieldsList, - fields for set partititon, only 'range' strategy supported
%%                                               <<"template">> => TemplateName} - template for create default partition table
%%          }
%% --------------------------
-spec ensure_table(DbParams::map() | list(), Meta::map()) -> ok | {error,Reason::binary()}.
%% --------------------------
ensure_table(DbParams,Meta) when is_map(Meta) andalso is_map(DbParams) orelse is_list(DbParams) ->
    ?LOG('$trace', "PostgreDB. Struct. ensure_table ~n\t~120tp",[Meta]),
    try do_ensure_table(DbParams,Meta)
    catch
        C:E:StackTrace ->
            ?LOG('$crash', "PostgreDB. Struct. (~120tp) crash. {~120tp, ~120tp} ~n\tStack: ~160tp",[?cfun(),C,E,StackTrace]),
            {error,<<"operation crash">>}
    end.

%% -----------------------------
%% Attempts to create partition for DT for Meta description (NOT DATA-MODEL CLASS DESCRIPTION, but adapter's description)
%% -----------------------------
ensure_partition(DT,Meta,Conn) ->
    Schema = maps:get(<<"schema">>,Meta),
    TableName = maps:get(<<"tablename">>,Meta),
    do_ensure_partition(Schema,TableName,DT,Meta,Conn).

%% -----------------------------
%% Return existing partitions from DB of actual interval
%% -----------------------------
get_partitions(Schema,TableName,Meta,Conn) ->
    case do_get_partitions(Schema,TableName,Meta,Conn) of
        {ok,Correct,_Incorrect} -> {ok,Correct};
        {error,_}=Err -> Err
    end.

%% -----------------------------
%% Remove all existing indexes from table, excluding 'pkey_%'
%% -----------------------------
drop_all_indexes(Schema,TableName) ->
    sql_drop_all_indexes(Schema,TableName).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
%% return login from connection string
extract_login([P|_]) when is_map(P) -> maps:get(login,P);
extract_login([[T|_]=P|_]) when is_tuple(T) -> ?BU:get_by_key(login,P);
extract_login([T|_]=P) when is_tuple(T) -> ?BU:get_by_key(login,P);
extract_login(P) when is_map(P) -> maps:get(login,P).

%% --------------------------
do_ensure_tables(DbParams,Metas) ->
    case ?PgConnectionPool:get_connection(DbParams,20000) of
        {ok,Conn} ->
            DBUser = extract_login(DbParams),
            F = fun(Meta0,ok) ->
                        Meta = Meta0#{dbuser => DBUser},
                        ?LOG('$trace', "PostgreDB. ensure_table ~n\t~120tp)",[Meta]),
                        try do_ensure_table_struct(Meta,Conn)
                        catch _C:_E:StackTrace ->
                            ?LOG('$crash', "PostgreDB. (~120p) crash. {~120tp, ~120tp} ~n\tStack: ~160p",[?cfun(),_C,_E,StackTrace]),
                            {error,[{<<"reason">>,<<"operation crash">>},{<<"meta">>,Meta}]}
                        end;
                   (_,Acc) -> Acc
                end,
            R = lists:foldl(F, ok, Metas),
            ?PgConnectionPool:release_connection(DbParams,Conn),
            R;
        {error,timeout} -> {error,{timeout,<<"Connection pool is overloaded. Try later.">>}};
        {error,_}=Err -> Err
    end.

%% ---------------------------
do_ensure_table(DbParams,Meta0) ->
    case ?PgConnectionPool:get_connection(DbParams,20000) of
        {ok,Conn} ->
            DBUser = extract_login(DbParams),
            Meta = Meta0#{dbuser => DBUser},
            try do_ensure_table_struct(Meta,Conn)
            catch C:E:StackTrace -> erlang:raise(C,E,StackTrace)
            after ?PgConnectionPool:release_connection(DbParams,Conn)
            end;
        {error,timeout} -> {error,{timeout,<<"Connection pool is overloaded. Try later.">>}};
        {error,_}=Err -> Err
    end.

%% --------------------------
do_ensure_table_struct(Meta,Conn) ->
    do_ensure_table_struct(Meta,Conn,true).

do_ensure_table_struct(Meta,Conn,TryRenamePrevious) ->
    [Schema,TableName] = ?BU:maps_get([<<"schema">>,<<"tablename">>],Meta),
    Fnext = fun(Res) ->
                    case get_partition_info(Meta) of
                        undefined -> ensure_indexes(Schema,TableName,Meta,Conn,Res);
                        {ok,_,_} -> ensure_partitions(Schema,TableName,Meta,Conn,Res)
                    end end,
    case get_current_table_info(Schema,TableName,Conn) of
        not_exists when TryRenamePrevious ->
            do_try_rename_previous_table(Meta,Conn),
            do_ensure_table_struct(Meta,Conn,false);
        not_exists ->
            ?LOG('$trace', "PostgreDB. Table '~ts.~ts' not exists",[Schema,TableName]),
            Fnext(do_create_table(Meta,Conn));
        {ok,Struct,Opts} ->
            ?LOG('$trace', "PostgreDB. Table '~ts.~ts' already exists, start check. ~n\tStruct: ~120tp~n\tOpts: ~120tp",[Schema,TableName,Struct,Opts]),
            Fnext(do_check_exists_table(Meta,Struct,Opts,Conn));
        {error,_Err} ->
            ?LOG('$error', "PostgreDB. Table '~ts.~ts' error: ~120tp",[Schema,TableName,_Err]),
            {error, <<"get_current_table_info_error">>}
    end.

%% @private
get_partition_info(Meta) ->
    PartOpts = maps:get(<<"partition">>,maps:get(<<"opts">>,Meta),#{}),
    Fields = maps:get(<<"fields">>,PartOpts,undefined),
    Interval = maps:get(<<"interval">>,PartOpts,undefined),
    case {Fields,Interval} of
        {undefined,undefined} -> undefined;
        _ -> {ok,Fields,Interval}
    end.

%% @private
do_try_rename_previous_table(Meta,Conn) ->
    [Schema,TableName] = ?BU:maps_get([<<"schema">>,<<"tablename">>],Meta),
    PreviousNames = maps:get(<<"previous_tablenames">>,Meta,[]),
    lists:foldl(fun(_,true) -> true;
                   (PreviousName,false) ->
                       case get_current_table_info(Schema,PreviousName,Conn) of
                           not_exists -> false;
                           {error,_} -> false;
                           {ok,_Struct,_Opts} ->
                               try do_rename_previous_table(Meta,Conn,{Schema,PreviousName,TableName}),
                                   true
                               catch E:R:ST ->
                                   ?LOG('$error', "PostgreDB. Struct. Rename table failed: {~120tp, ~120tp}~n\tFrom: ~120tp, To: ~120tp~n\tStack: ~120tp", [E,R,PreviousName,TableName,ST]),
                                   false
                               end
                       end
                end, false, PreviousNames).

%% @private
do_rename_previous_table(Meta,Conn,{Schema,FromTableName,ToTableName}) ->
    % first partitions
    ?LOG('$trace', "PostgreDB. Struct. Rename table from '~ts' to '~ts'",[FromTableName,ToTableName]),
    MetaOpts = maps:get(<<"opts">>,Meta,#{}),
    case is_map(maps:get(<<"partition">>,MetaOpts,not_exists))  of
        false -> ok;
        true ->
            case do_get_partitions(Schema,FromTableName,Meta,Conn) of
                {error,_} -> throw({error,<<"get_partitions_error">>});
                {ok,Correct,Incorrect} ->
                    lists:foreach(fun(FromPartTable) ->
                                        PartPostfix = binary:replace(FromPartTable, FromTableName, <<>>),
                                        ToPartTable = <<ToTableName/binary,PartPostfix/binary>>,
                                        Res = rename_table(Conn,{Schema,FromPartTable,ToPartTable}),
                                        case ?PgU:ensure_dbresult_is_ok(Res,<<"rename_partition_table_error">>) of
                                            {error,_}=Err -> throw(Err);
                                            R -> R
                                        end end, Correct++Incorrect)
            end end,
    % then table
    Res = rename_table(Conn,{Schema,FromTableName,ToTableName}),
    store_classname_table(Meta,Conn),
    Res.

%% @private
rename_table(Conn,{Schema,FromTableName,ToTableName}) ->
    Sql = ?BU:to_list(?BU:strbin("
DO $$
DECLARE
    i varchar;
BEGIN
    ALTER TABLE ~ts.\"~ts\" RENAME TO \"~ts\";

    FOR i IN
	 	SELECT indexname
		FROM pg_indexes
		WHERE schemaname='~ts'
			AND tablename='~ts'
			AND indexname LIKE 'pkey_%_id'
			AND indexname NOT LIKE 'pkey_~ts_id'
	LOOP
	    EXECUTE FORMAT ('ALTER TABLE ~ts.\"~ts\" RENAME CONSTRAINT %I TO \"pkey_~ts_id\"', i);

    END LOOP;

END;$$;
",
        [Schema,FromTableName,ToTableName,
            Schema,ToTableName,ToTableName,
            Schema,ToTableName,ToTableName
        ])),
    ?LOG('$trace', "PostgreDB. Struct. rename_table sql: ~n~ts",[Sql]),
    Res = ?PgDBA:query_db_tout(Conn, Sql, ?SqlTout),
    ?LOG('$trace', "Rename result: ~120tp",[Res]),
    ?PgU:ensure_dbresult_is_ok(Res,<<"rename_table_error">>).

%% ---------------------------------------------------------------------------
%% Read table info
%% ---------------------------------------------------------------------------
get_current_table_info(Schema,TableName,Conn) ->
    case get_current_table_struct(Schema,TableName,Conn) of
        not_exists -> not_exists;
        {ok,Struct} ->
            case get_current_table_opts(Schema,TableName,Conn) of
                {ok,Opts} -> {ok,Struct,Opts};
                {error,_}=Err -> Err
            end;
        {error,_}=Err -> Err
    end.

%% @private
get_current_table_struct(Schema,TableName,Conn) ->
    Sql = ?BU:to_list(?BU:strbin("
SELECT column_name, data_type, is_nullable, column_default
FROM information_schema.columns
WHERE table_schema='~ts' AND table_name='~ts';
",
        [Schema, TableName])),
    ?LOG('$trace', "PostgreDB. Struct. get_current_table_struct sql: ~n~ts",[Sql]),
    case ?PgDBA:query_db_tout(Conn, Sql, ?SqlTout) of
        {ok,_,[]} -> not_exists;
        {ok,_,Rows} ->
            F = fun({ColName,DataType,DbIsNullable,Default0},Acc) ->
                        IsNullable = case ?BU:lowercase(DbIsNullable) of
                                         <<"yes">> -> true;
                                         <<"no">> -> false
                                     end,
                        Default = get_db_column_default(DataType,Default0),
                        Acc#{ColName => #{type => ?PgU:postgre_type_map(DataType),
                                          isnullable => IsNullable,
                                          default => Default}}
                end,
            {ok, lists:foldl(F,#{},Rows)};
        {error,_}=Err -> Err;
        Err -> {error,Err}
    end.

%% @private
get_db_column_default(_,null) -> null;
get_db_column_default(<<"boolean">>,Default) -> ?BU:to_bool(Default);
get_db_column_default(_,Default) -> Default.

%% @private
get_current_table_opts(Schema,TableName,Conn) ->
    SearchTableName1 =
        case ?BU:to_lower(Schema) of
            <<"public">> -> ?BU:strbin("\"~ts\"",[TableName]);
            _ -> ?BU:strbin("~ts.\"~ts\"",[Schema,TableName])
        end,
    SearchTableName2 =
        case ?BU:to_lower(Schema) of
            <<"public">> -> ?BU:strbin("\"~ts\"",[TableName]);
            _ -> ?BU:strbin("~ts.\"~ts\"",[Schema,TableName])
        end,
    Sql = ?BU:to_list(?BU:strbin("
SELECT string_agg(pa.attname,',') AS partition_fields,
       (CASE WHEN pt.partstrat='r' THEN 'range' ELSE 'list' END) AS partition_strategy
FROM pg_catalog.pg_partitioned_table AS pt
        JOIN pg_catalog.pg_attribute AS pa on pt.partrelid=pa.attrelid AND pa.attnum = any (pt.partattrs::int2[])
WHERE (pt.partrelid::regclass::text = '~ts') OR (pt.partrelid::regclass::text = '~ts')
GROUP BY pt.partstrat;
",
        [SearchTableName1,SearchTableName2])),
    ?LOG('$trace', "PostgreDB. Struct. get_current_table_opts sql: ~n~ts",[Sql]),
    case ?PgDBA:query_db_tout(Conn, Sql, ?SqlTout) of
        {ok,_,[]} -> {ok,#{}};
        {ok,_,[{PartFields,PartStrategy}]} ->
            PartitionFields = binary:split(PartFields,<<",">>,[global]),
            {ok,#{partition => #{fields => PartitionFields,
                                 strategy => PartStrategy}}};
        {error,_}=Err -> Err;
        Err -> {error,Err}
    end.

%% ---------------------------------------------------------------------------
%% Check if table exists and return it's structure
%% ---------------------------------------------------------------------------
do_check_exists_table(Meta,Struct,Opts,Conn) ->
    case do_check_exists_opts(Meta,Opts) of
        drop_table ->
            case do_drop_table(true,Meta,Conn) of
                ok -> do_create_table(Meta,Conn);
                {error,_}=Err -> Err
            end;
        ok -> do_check_exists_table_struct(Meta,Struct,Conn)
    end.

%% @private
do_check_exists_opts(Meta, Opts) ->
    MetaOpts = maps:get(<<"opts">>,Meta,#{}),
    MetaPartition = maps:get(<<"partition">>,MetaOpts,not_exists),
    Partition = maps:get(partition,Opts,not_exists),
    ?LOG('$trace', "PostgreDB. Struct. MetaPartition:~120p; ~n\tPartition:~120p",[MetaPartition,Partition]),
    do_check_exists_opts_partition(MetaPartition,Partition).

%% @private
do_check_exists_opts_partition(not_exists,not_exists) -> ok;
do_check_exists_opts_partition(MetaPart,Partition) when is_map(MetaPart) andalso is_map(Partition) ->
    [CurrFields,CurrStrategy] = ?BU:maps_get([fields,strategy],Partition),
    MetaFields = maps:get(<<"fields">>,MetaPart,[]),
    ?LOG('$trace', "check partition opts: ~120p, ~120p, ~120p", [CurrFields,MetaFields,CurrStrategy]),
    case lists:sort(CurrFields) /= lists:sort(MetaFields) orelse CurrStrategy /= <<"range">> of
        true -> drop_table;
        false -> ok
    end;
do_check_exists_opts_partition(_,_) -> drop_table.

%% @private
do_check_exists_table_struct(Meta,Struct,Conn) ->
    Fields = maps:get(<<"fields">>,Meta),
    F = fun(FieldData,Acc) ->
                StructAcc = maps:get(struct,Acc),
                [Name,DataType0] = ?BU:maps_get([<<"name">>,<<"type">>],FieldData),
                DataType = ?PgU:postgre_type_map(DataType0),
                case maps:get(Name,StructAcc,undefined) of
                    undefined -> maps:update_with(add, fun(V)-> [FieldData|V] end, [FieldData], Acc);
                    DbColumnInfo ->
                        %?LOG('$trace', "PostgreDB. Struct. do_check_exists_table_struct DbColumnInfo: ~200tp~n\tFieldData: ~200tp",[DbColumnInfo,FieldData]),
                        [DbType,DbIsNullable,DbDef] = ?BU:maps_get([type,isnullable,default],DbColumnInfo),
                        [Required,Default] = ?BU:maps_get_default([{<<"required">>,false},{<<"default">>,null}],FieldData),
                        case is_changed_column_definition({DbType,DbIsNullable,DbDef},{DataType,Required,Default}) of
                            false ->
                                Acc1 = maps:update_with(struct, fun(V)-> maps:remove(Name,V) end, [], Acc),
                                maps:update_with(not_changed, fun(V)-> [FieldData|V] end, [FieldData], Acc1);
                            true ->
                                Acc1 = maps:update_with(struct, fun(V)-> maps:remove(Name,V) end, [], Acc),
                                Acc2 = maps:update_with(upd, fun(V)-> [FieldData|V] end, [FieldData], Acc1),
                                UpdDetails = {FieldData,[{type,DbType,DataType},{default,DbDef,Default},{required,not DbIsNullable,Required}]},
                                maps:update_with(upd_details, fun(V)-> [UpdDetails|V] end, [UpdDetails], Acc2)
                        end
                end
        end,
    Result = lists:foldl(F, #{struct => Struct}, Fields),
    ?LOG('$trace', "PostgreDB. Struct. do_check_exists_table_struct result: ~n\t~200tp",[Result]),
    [AddFields,ModFields,DelFieldsS,ModFieldsD] = ?BU:maps_get_default([{add,[]},{upd,[]},{struct,[]},{upd_details,[]}],Result),
    ?LOG('$trace', "PostgreDB. Struct. do_check_exists_table_struct res: ~200tp",[{{add,AddFields},{upd,ModFields},{del,DelFieldsS}}]),
    do_ensure_table_struct_1([AddFields,DelFieldsS,ModFieldsD],Meta,Conn).

%% @private
%% special case for bigserial postgresql type, bigserial ignore required param
is_changed_column_definition({<<"bigint">>,false,<<"nextval",_/binary>>},{<<"bigserial">>,_,null}) -> false;
%% check other postgresql types
is_changed_column_definition({DbType,DbIsNullable,DbDefault},{DataType,Required,Default})
  when DbType==DataType andalso DbIsNullable/=Required andalso DbDefault==Default ->
    false;
is_changed_column_definition(_,_) -> true.

%% @private
do_ensure_table_struct_1([AddFields,DelFieldsStruct,[]],Meta,Conn) ->
    case do_add_table_fields(AddFields,Meta,Conn) of
        ok -> do_drop_table_fields(DelFieldsStruct,Meta,Conn);
        {error,_}=Err -> Err
    end;
do_ensure_table_struct_1([AddFields,DelFieldsStruct,ModFieldsDetails],Meta,Conn) ->
    case do_add_table_fields(AddFields,Meta,Conn) of
        ok ->
            case do_drop_table_fields(DelFieldsStruct,Meta,Conn) of
                ok -> do_modify_table_fields(ModFieldsDetails,Meta,Conn);
                {error,_}=Err -> Err
            end;
        {error,_}=Err -> Err
    end;
do_ensure_table_struct_1(_,Meta,Conn) ->
    case do_drop_table(true,Meta,Conn) of
        ok -> do_create_table(Meta,Conn);
        {error,_}=Err -> Err
    end.

%% ---------------------------------------------------------------------------
%% CREATE TABLE IF NOT EXISTS
%% ---------------------------------------------------------------------------
do_create_table(Meta,Conn) ->
    [Schema,TableName] = ?BU:maps_get([<<"schema">>,<<"tablename">>],Meta),
    SqlCreateTableBody = sqlbin_create_table_body(Meta),
    PartitionData = sqlbin_partitioned_data_bin(Meta),
    DBUser = maps:get(dbuser,Meta),
    %
    ensure_schema(Schema,DBUser,Conn),
    %
    Sql = ?BU:to_list(?BU:strbin("
CREATE TABLE ~ts.\"~ts\" (
    ~ts
) ~ts
WITH (OIDS=FALSE);

ALTER TABLE ~ts.\"~ts\" OWNER TO ~ts;
",
        [Schema,TableName,SqlCreateTableBody,PartitionData,
         Schema,TableName,DBUser])),
    ?LOG('$trace', "PostgreDB. Struct. do_create_table sql: ~n~ts",[Sql]),
    Res = ?PgU:ensure_dbresult_is_ok(?PgDBA:query_db_tout(Conn, Sql, ?SqlTout),<<"create_table_error">>),
    store_classname_table(Meta,Conn),
    Res.

%% @private
ensure_schema(Schema,DBUser,Conn) ->
    ensure_schema(Schema,DBUser,Conn,5).
%% @private
ensure_schema(Schema,DBUser,Conn,Attempts) ->
    Sql = ?BU:to_list(?BU:strbin("
CREATE schema IF NOT EXISTS ~ts AUTHORIZATION ~ts;
",
        [Schema,DBUser])),
    ?LOG('$trace', "PostgreDB. Struct. ensure_schema sql: ~n~ts",[Sql]),
    case ?PgU:ensure_dbresult_is_ok(?PgDBA:query_db_tout(Conn, Sql, ?SqlTout), <<"create_schema_error">>) of
        ok -> ok;
        {error,_} when Attempts==5 ->
            ensure_schema(Schema,DBUser,Conn,Attempts-1);
        {error,_} when Attempts>2 ->
            timer:sleep(10),
            ensure_schema(Schema,DBUser,Conn,Attempts-1);
        {error,_} when Attempts>0 ->
            timer:sleep(100),
            ensure_schema(Schema,DBUser,Conn,Attempts-1);
        {error,_}=Err -> Err
    end.

%% @private
sqlbin_partitioned_data_bin(Meta) ->
    case get_partition_info(Meta) of
        undefined -> <<"">>;
        {ok,PartFields,_} ->
            ?BU:strbin("PARTITION BY RANGE (~ts)", [?BU:join_binary([?BU:strbin("\"~ts\"",[Fld]) || Fld <- PartFields],<<",">>)])
    end.

%% @private
sqlbin_create_table_body(Meta) ->
    [TableName,Fields] = ?BU:maps_get([<<"tablename">>,<<"fields">>],Meta),
    F = fun(FieldData) ->
                [Name,DataType0] = ?BU:maps_get([<<"name">>,<<"type">>],FieldData),
                DataType = ?PgU:postgre_type_map(DataType0),
                [Required,Default] = ?BU:maps_get_default([{<<"required">>,<<>>},{<<"default">>,<<>>}],FieldData),
                _Sql = case prepare_table_field_constraints([{req,Required,<<"">>},{def,Default,<<"">>}],<<"">>) of
                           <<"">> -> ?BU:strbin("\t\"~ts\" ~ts", [Name,DataType]);
                           FldConstraints -> ?BU:strbin("\t\"~ts\" ~ts ~ts", [Name,DataType,FldConstraints])
                       end end,
    case get_partition_info(Meta) of
        undefined ->
            % PRIMARY KEY FOR id
            Constraints = ?BU:strbin("\tCONSTRAINT \"pkey_~ts_id\" PRIMARY KEY (id)",[TableName]),
            ?BU:join_binary(lists:map(F, Fields) ++ [Constraints], <<",\n">>);
        {ok,_,_} ->
            % NO CONSTRAINTS ON PARTITIONED TABLE
            ?BU:join_binary(lists:map(F, Fields), <<",\n">>)
    end.

%% ---------------------------------------------------------------------------
%% DROP TABLE IF EXISTS
%% ---------------------------------------------------------------------------
-spec do_drop_table(DoBackupRename::boolean(), Meta::map(), Conn::pid()) -> ok | {error,Reason::term()}.
%% ---------------------------------------------------------------------------
do_drop_table(false, Meta, Conn) ->
    [Schema,TableName] = ?BU:maps_get([<<"schema">>,<<"tablename">>],Meta),
    ?LOG('$trace', "PostgreDB. Struct. do_drop_table (~120p)",[?BU:join_binary([Schema,TableName],<<".">>)]),
    Sql = ?BU:to_list(?BU:strbin("
DROP TABLE IF EXISTS ~ts.\"~ts\" CASCADE;
",
        [Schema,TableName])),
    ?LOG('$trace', "PostgreDB. Struct. do_drop_table sql: ~n~ts",[Sql]),
    ?PgU:ensure_dbresult_is_ok(?PgDBA:query_db_tout(Conn, Sql, ?SqlTout),<<"drop_table_error">>);
%%
do_drop_table(true, Meta, Conn) ->
    [Schema,TableName] = ?BU:maps_get([<<"schema">>,<<"tablename">>],Meta),
    NowTS = ?BU:timestamp(),
    % first partitions
    MetaOpts = maps:get(<<"opts">>,Meta,#{}),
    case is_map(maps:get(<<"partition">>,MetaOpts,not_exists))  of
        false -> ok;
        true ->
            case do_get_partitions(Schema,TableName,Meta,Conn) of
                {error,_} -> throw({error,<<"get_partitions_error">>});
                {ok,Correct,Incorrect} ->
                    lists:foreach(fun(PartTable) ->
                                        Res = backup_drop_table(Conn,{Schema,PartTable,NowTS}),
                                        case ?PgU:ensure_dbresult_is_ok(Res,<<"bkp_drop_partition_table_error">>) of
                                            {error,_}=Err -> throw(Err);
                                            R -> R
                                        end end, Correct++Incorrect)
            end end,
    % then table
    Res = backup_drop_table(Conn,{Schema,TableName,NowTS}),
    store_classname_backup(Meta,Conn,{NowTS,bkphash(NowTS),backup_tablename(TableName,NowTS)}),
    Res.

%% @private
backup_drop_table(Conn,{Schema,TableName,NowTS}) ->
    BackupTableName = backup_tablename(TableName,NowTS),
    Sql = ?BU:to_list(?BU:strbin("
DROP TABLE IF EXISTS ~ts.\"~ts\" CASCADE;
~ts
ALTER TABLE ~ts.\"~ts\" RENAME TO \"~ts\";
",
        [Schema,BackupTableName,
         sqlbin_drop_key_and_indexes_text(Schema,TableName),
         Schema,TableName,BackupTableName])),
    ?LOG('$trace', "PostgreDB. Struct. do_drop_table sql: ~n~ts",[Sql]),
    Res = ?PgDBA:query_db_tout(Conn, Sql, ?SqlTout),
    ?LOG('$trace', "Backup/drop result: ~120tp",[Res]),
    ?PgU:ensure_dbresult_is_ok(Res,<<"bkp_drop_table_error">>).

%% @private
backup_tablename(TableName,NowTS) ->
    ?BU:strbin("~ts~ts", [TableName,?BU:strbin("_bk_~ts",[bkphash(NowTS)])]).

%% @private
bkphash(TS) ->
    F = fun(X) when X<10 -> X+48;
           (X) when X<36 -> X-10+65;
           (X) when X<62 -> X-36+97
        end,
    A = TS div 1000,
    A1 = A rem 62,
    B = A div 62,
    B1 = B rem 62,
    C = B div 62,
    C1 = C rem 62,
    D = C div 62,
    D1 = D rem 62,
    lists:foldl(fun(I,Acc) -> <<Acc/binary,(F(I))>> end, <<>>, [D1,C1,B1,A1]).

%% ---------------------------------------------------------------------------
%% ADD/DROP COLUMNS
%% ---------------------------------------------------------------------------

%% ----------------------------------------
%% Add table fields
%% ----------------------------------------
do_add_table_fields([],_,_) -> ok;
do_add_table_fields(AddFields,Meta,Conn) ->
    [Schema,TableName] = ?BU:maps_get([<<"schema">>,<<"tablename">>],Meta),
    Faddcol = fun(FieldData) ->
                    [Name,DataType0] = ?BU:maps_get([<<"name">>,<<"type">>],FieldData),
                    DataType = ?PgU:postgre_type_map(DataType0),
                    [Required,Default] = ?BU:maps_get_default([{<<"required">>,<<>>},{<<"default">>,<<>>}],FieldData),
                    Constraints = prepare_table_field_constraints([{req,Required,<<"">>},{def,Default,<<"">>}],<<"">>),
                    ?BU:strbin("ADD COLUMN IF NOT EXISTS \"~ts\" ~ts ~ts", [Name,DataType,Constraints])
              end,
    AddCols = ?BU:join_binary([Faddcol(FieldData) || FieldData <- AddFields], <<",\n    ">>),
    Sql = ?BU:to_list(?BU:strbin("
ALTER TABLE ~ts.\"~ts\" \n
    ~ts;
",
        [Schema,TableName,AddCols])),
    ?LOG('$trace', "PostgreDB. Struct. do_add_table_fields sql: ~n~ts",[Sql]),
    % apply
    case ?PgDBA:query_db_tout(Conn,Sql,?SqlTout) of
        ok -> ok;
        Ok when is_tuple(Ok), element(1,Ok) == ok -> ok;
        {error,Err} ->
            FBackupCreate = fun() ->
                                case do_drop_table(true,Meta,Conn) of
                                    ok -> do_create_table(Meta,Conn);
                                    {error,_}=Err -> Err
                                end end,
            case Err of
                #error{code= <<"54011">>} ->
                    ?LOG('$info',"PostgreDB. Struct. do_add_table_fields detect 'column limit error'. Start backup rename table: ~ts.\"~ts\"",[Schema,TableName]),
                    FBackupCreate();
                #error{code= <<"23502">>} ->
                    ?LOG('$info',"PostgreDB. Struct. do_add_table_fields detect 'column not_null_violation error'. Start backup rename table: ~ts.\"~ts\"",[Schema,TableName]),
                    FBackupCreate();
                _ ->
                    ?LOG('$error',"PostgreDB. Struct. do_add_table_fields detect other error: ~160tp",[Err]),
                    {error, <<"add_fields_error">>}
            end end.

%% ----------------------------------------
%% Drop table fields
%% ----------------------------------------
do_drop_table_fields(DelFieldsStruct,_,_) when map_size(DelFieldsStruct)==0 -> ok;
do_drop_table_fields(DelFieldsStruct,Meta,Conn) ->
    Fields = maps:keys(DelFieldsStruct),
    [Schema,TableName] = ?BU:maps_get([<<"schema">>,<<"tablename">>],Meta),
    DropCols = ?BU:join_binary([?BU:strbin("DROP COLUMN IF EXISTS \"~ts\"", [FName]) || FName <- Fields],<<",\n    ">>),
    Sql = ?BU:to_list(?BU:strbin("
ALTER TABLE ~ts.\"~ts\"
    ~ts;
",
        [Schema,TableName,DropCols])),
    ?LOG('$trace',"PostgreDB. Struct. do_drop_table_fields sql: ~n~ts",[Sql]),
    ?PgU:ensure_dbresult_is_ok(?PgDBA:query_db_tout(Conn, Sql, ?SqlTout), <<"drop_fields_error">>).

%% ----------------------------------------
%% Modify table fields
%% ----------------------------------------
do_modify_table_fields([],_,_) -> ok;
do_modify_table_fields(ModFieldsDetails,Meta,Conn) ->
    [Schema,TableName] = ?BU:maps_get([<<"schema">>,<<"tablename">>],Meta),
    % Apply sql to db
    FunApply =
        fun(Sql) ->
                ?LOG('$trace', "PostgreDB. Struct. do_modify_table_fields sql: ~n~ts",[Sql]),
                case ?PgDBA:query_db_tout(Conn,Sql,?SqlTout) of
                    ok -> ok;
                    Ok when is_tuple(Ok), element(1,Ok) == ok -> ok;
                    {error,Err} ->
                        FBackupCreate = fun() ->
                                            case do_drop_table(true,Meta,Conn) of
                                                ok ->
                                                    case do_create_table(Meta,Conn) of
                                                        ok -> created;
                                                        {error,_}=Err -> Err
                                                    end;
                                                {error,_}=Err -> Err
                                            end end,
                        case Err of
                            #error{code= <<"54011">>} ->
                                ?LOG('$info',"PostgreDB. Struct. do_modify_table_fields detect 'column limit error'. Start backup rename table: ~ts.\"~ts\"",[Schema,TableName]),
                                FBackupCreate();
                            #error{code= <<"23502">>} ->
                                ?LOG('$info',"PostgreDB. Struct. do_modify_table_fields detect 'column not_null_violation error'. Start backup rename table: ~ts.\"~ts\"",[Schema,TableName]),
                                FBackupCreate();
                            #error{code= <<"22",_/binary>>=Code}=Err ->
                                ?LOG('$info',"PostgreDB. Struct. do_modify_table_fields detect 'column data exception ~ts'. Start backup rename table: ~ts.\"~ts\"~n\tErr: ~120tp",[Code,Schema,TableName,Err]),
                                FBackupCreate();
                            #error{code= <<"23",_/binary>>=Code}=Err ->
                                ?LOG('$info',"PostgreDB. Struct. do_modify_table_fields detect 'column integrity constraint violation ~ts'. Start backup rename table: ~ts.\"~ts\"~n\tErr: ~120tp",[Code,Schema,TableName,Err]),
                                FBackupCreate();
                            #error{code= <<"42846">>} ->
                                ?LOG('$info',"PostgreDB. Struct. do_modify_table_fields detect 'column cannot_coerce error'. Start backup rename table: ~ts.\"~ts\"",[Schema,TableName]),
                                FBackupCreate();
                            _ ->
                                ?LOG('$error',"PostgreDB. Struct. do_modify_table_fields detect other error: ~160tp",[Err]),
                                {error, <<"modify_fields_error">>}
                        end
                end
        end,
    % Sql for table
    FunSql = fun(ModColumnSql) ->
                    ?BU:to_list(?BU:strbin("
ALTER TABLE ~ts.\"~ts\"
    ~ts;
",
                        [Schema,TableName,ModColumnSql]))
            end,
    % separately every field, every constraint
    Res = lists:foldl(
        fun(_, Acc) when Acc/=ok -> Acc;
           ({FieldData,Details}, ok) ->
               [Name,_DataType0] = ?BU:maps_get([<<"name">>,<<"type">>],FieldData),
               {type,Type0,Type1} = lists:keyfind(type,1,Details),
               {default,Def0,Def1} = lists:keyfind(default,1,Details),
               {required,Req0,Req1} = lists:keyfind(required,1,Details),
               {hasdefault,HasDefault0,HasDefault1} = {hasdefault,has_default(Def0),has_default(Def1)},
               % Change type of field
               ResType1 =
                   case Type0/=Type1 of
                       true ->
                           ConstraintType = <<" TYPE ", Type1/binary, " USING \"", Name/binary, "\"::", Type1/binary>>,
                           ModColumnSqlType = ?BU:strbin("ALTER COLUMN \"~ts\" ~ts", [Name,ConstraintType]),
                           FunApply(FunSql(ModColumnSqlType));
                       false ->
                           ok
                   end,
               % Change default constraint
               ResType2 =
                   case ResType1 of
                       ok when HasDefault0/=HasDefault1 orelse (HasDefault1 andalso Def0/=Def1) ->
                           ConstraintDef = case has_default(Def1) of
                                               true -> <<" SET DEFAULT ",Def1/binary>>;
                                               false -> <<" DROP DEFAULT">>
                                           end,
                           ModColumnSqlDef = ?BU:strbin("ALTER COLUMN \"~ts\" ~ts", [Name,ConstraintDef]),
                           FunApply(FunSql(ModColumnSqlDef));
                       _ -> ResType1
                   end,
               % Update NULL values to default (if defined) before set required
               ResType3 =
                   case ResType2 of
                       ok when Req0/=Req1, Req1==true ->
                           % attempt update null fields by default before set required constraint
                           case has_default(Def1) of
                               true ->
                                   SqlUpdate = ?BU:to_list(?BU:strbin("
UPDATE ~ts.\"~ts\" SET \"~ts\" = ~ts WHERE \"~ts\" is NULL;
",
                                       [Schema,TableName,Name,Def1,Name])),
                                   FunApply(SqlUpdate);
                               false -> ok
                           end;
                       _ -> ResType2
                   end,
               % Change required constraint
               ResType4 =
                   case ResType3 of
                       ok when Req0/=Req1 ->
                           ConstraintReq = case ?BU:to_bool(Req1) of
                                               true -> <<" SET NOT NULL">>;
                                               false -> <<" DROP NOT NULL">>
                                           end,
                           ModColumnSqlReq = ?BU:strbin("ALTER COLUMN \"~ts\" ~ts", [Name,ConstraintReq]),
                           FunApply(FunSql(ModColumnSqlReq));
                       _ -> ResType2
                   end,
               ResType4
        end, ok, ModFieldsDetails),
    case Res of
        ok -> ok;
        created -> ok;
        {error,_}=Err -> Err
    end.

%% ---------------------------------------------------------------------------
%% PARTITIONS
%% ---------------------------------------------------------------------------

ensure_partitions(_,_,_,_,{error,_}=Err) -> Err;
ensure_partitions(Schema,TableName,Meta,Conn,ok=_Res) ->
    case do_get_partitions(Schema,TableName,Meta,Conn) of
        {error,_}=Err -> Err;
        {ok,Correct,Incorrect} ->
            case {Correct,Incorrect} of
                {[],[]} -> ok;
                {_,[]} -> check_existing_partitions_indexes(Schema,TableName,Meta,Conn,Correct); % check indexes
                {[],_} -> resize_partitions(Schema,TableName,Meta,Conn,Incorrect); % detach old tabs, create new tabs, copy data, drop old tabs, create indexes
                {_,_} -> resize_partitions(Schema,TableName,Meta,Conn,Incorrect) % ensure new tabs, copy data, drop old tables, check indexes
            end
    end.

%% @private
check_existing_partitions_indexes(Schema,_TableName,Meta,Conn,CurrPartitions) ->
    lists:foldl(fun(_,{error,_}=Err) -> Err;
                   (TableNamePart,ok) -> ensure_indexes(Schema,TableNamePart,Meta,Conn)
                end, ok, CurrPartitions).

%% @private
resize_partitions(Schema,TableName,Meta,Conn,OldPartitions) ->
    % Get old partitions
    Len = size(TableName),
    R = lists:filtermap(fun(PTabName) ->
                                case PTabName of
                                    <<TableName:Len/binary,"#",Y:4/binary>> ->
                                        Y0 = ?BU:to_int(Y),
                                        {true, {PTabName,{Y0,1,1},{Y0+1,1,1}}};
                                    <<TableName:Len/binary,"#",Y:4/binary,M:2/binary>> ->
                                        {Y0,M0} = {?BU:to_int(Y),?BU:to_int(M)},
                                        {Y1,M1} = case M0 of 12 -> {Y0+1,1}; _ -> {Y0,M0+1} end,
                                        {true, {PTabName,{Y0,M0,1},{Y1,M1,1}}};
                                    <<TableName:Len/binary,"#",Y:4/binary,M:2/binary,D:2/binary>> ->
                                        {Y0,M0,D0} = {?BU:to_int(Y),?BU:to_int(M),?BU:to_int(D)},
                                        {{Y1,M1,D1},_} = calendar:gregorian_seconds_to_datetime(calendar:datetime_to_gregorian_seconds({{Y0,M0,D0},{0,0,0}}) + 86400),
                                        {true, {PTabName,{Y0,M0,D0},{Y1,M1,D1}}};
                                    _ -> false
                                end end, OldPartitions),
    Ordered = lists:keysort(2,R),
    % Detaching old partitions
    Fsqlbin = fun({PTabName,_,_}) -> ?BU:strbin("ALTER TABLE ~ts.\"~ts\" DETACH PARTITION ~ts.\"~ts\";", [Schema,TableName,Schema,PTabName]) end,
    SqlDetach = ?BU:to_list(?BU:join_binary(lists:map(Fsqlbin, Ordered), <<"\n">>)),
    ?LOG('$trace', "PostgreDB. Struct. resize_partitions detach sql: ~n~ts",[SqlDetach]),
    ?PgDBA:query_db_tout(Conn, SqlDetach, ?SqlTout), % NO ERROR CHECK!
    % Data diapason in old partitions
    [{_,MinDt,_}|_] = Ordered,
    [{_,_,MaxDt}|_] = lists:reverse(Ordered),
    % List new partitions
    PartInterval = maps:get(<<"interval">>,maps:get(<<"partition">>,maps:get(<<"opts">>,Meta))),
    StartDt = ?PostgresPartUtils:prev(PartInterval,MinDt),
    F = fun F(DT,Acc) when DT >= MaxDt -> Acc;
            F(FromDt,Acc) ->
                NextDt = ?PostgresPartUtils:next(PartInterval,FromDt),
                F(NextDt,[{FromDt,NextDt} | Acc])
        end,
    Partitions = F(StartDt,[]),
    % Applying changes
    [PartField] = maps:get(<<"fields">>,maps:get(<<"partition">>,maps:get(<<"opts">>,Meta))),
    FAcc = fun(_,{error,_}=Err) -> Err;
              ({FromDt,ToDt},ok) ->
                    % Create new partition
                    case do_ensure_partition(Schema,TableName,FromDt,Meta,Conn) of
                        {error,_}=Err -> Err;
                        ok ->
                            % Moving data
                            OldParts = [TN || {TN,_,_} <- lists:filter(fun({_,FromDt1,ToDt1}) -> ToDt>FromDt1 andalso FromDt<ToDt1 end, Ordered)],
                            lists:foldl(fun(_,{error,_}=Err) -> Err;
                                           (OldPart,ok) ->
                                                SqlMove = ?BU:to_list(?BU:strbin("
INSERT INTO ~ts.\"~ts\"
SELECT * FROM ~ts.\"~ts\"
WHERE \"~ts\" >= '~ts' AND \"~ts\" < '~ts'
ON CONFLICT (id) DO NOTHING;
",
                                                    [Schema,?PostgresPartUtils:parttabname(PartInterval,FromDt,TableName),
                                                     Schema,OldPart,
                                                     PartField,?BU:strdate(FromDt),PartField,?BU:strdate(ToDt)])),
                                                ?LOG('$trace', "PostgreDB. Struct. resize_partitions move sql: ~n~ts",[SqlMove]),
                                                ?PgU:ensure_dbresult_is_ok(?PgDBA:query_db_tout(Conn, SqlMove, ?SqlToutMove), <<"move_partition_data_error">>)
                                end, ok, OldParts)
                    end end,
    case lists:foldr(FAcc,ok,Partitions) of
        {error,_}=Err -> Err;
        ok ->
            % Removing old partitions
            Fdropsql = fun({PTabName,_,_}) -> ?BU:strbin("DROP TABLE ~ts.\"~ts\" CASCADE;", [Schema,PTabName]) end,
            SqlDrop = ?BU:to_list(?BU:join_binary(lists:map(Fdropsql, Ordered), <<"\n">>)),
            ?LOG('$trace',"PostgreDB. Struct. resize_partitions drop sql: ~n~n~ts~n",[SqlDrop]),
            ?PgDBA:query_db_tout(Conn, SqlDrop, ?SqlTout), % NO ERROR CHECK!
            ok
    end.

%% -----------------------------------------------------------
%% Return existing partitions, separated by correctness of names: Actual interval, Invalid interval.
%% -----------------------------------------------------------
do_get_partitions(Schema,TableName,Meta,Conn) ->
    PostfixLen = case maps:get(<<"interval">>,maps:get(<<"partition">>,maps:get(<<"opts">>,Meta))) of
                     <<"year">> -> 1+4; % #YYYY
                     <<"month">> -> 1+6; % #YYYYMM
                     <<"day">> -> 1+8 % #YYYYMMDD
                 end,
    SqlRead = ?BU:to_list(?BU:strbin("
SELECT tablename
FROM pg_tables
WHERE schemaname='~ts'
    AND tablename like '~ts#%'
    AND tablename not like '~ts#%_backup_%'
    AND tablename not like '~ts#%_bkp_%'
    AND tablename not like '~ts#%_bk_%';
",
        [Schema,TableName,TableName,TableName,TableName])),
    ?LOG('$trace',"PostgreDB. Struct. do_get_partitions sql: ~n~ts",[SqlRead]),
    case ?PgDBA:query_db_tout(Conn, SqlRead, ?SqlTout) of
        {ok,_Cols,Vals} ->
            Tables = [TabName || {TabName} <- Vals],
            {Correct,Incorrect} = lists:partition(fun(T) -> size(T)==(size(TableName)+PostfixLen) end, Tables),
            {ok,Correct,Incorrect};
        _ -> {error, <<"get_partitions_error">>}
    end.

%% -----------------------------------------------------------
%% Create partition for DT if not exists
%% -----------------------------------------------------------
do_ensure_partition(Schema,TableName,DT,Meta,Conn) ->
    {TableNamePart, FromDt, ToDt} = get_partition_sides(DT,TableName,Meta),
    IndexFields = maps:get(<<"index_fields">>,Meta),
    SqlCreateIndexes = ?BU:join_binary([sql_create_index(Schema,TableNamePart,Fld) || Fld <- IndexFields], <<"\n    ">>),
    SqlCreate = ?BU:to_list(?BU:strbin("
DO $$ BEGIN
    CREATE TABLE IF NOT EXISTS ~ts.\"~ts\" PARTITION OF ~ts.\"~ts\" FOR VALUES FROM ('~ts') TO ('~ts');

    IF NOT EXISTS ( SELECT 1 FROM pg_indexes WHERE schemaname='~ts' AND tablename='~ts' AND indexname LIKE 'pkey_%') THEN
        ALTER TABLE ~ts.\"~ts\" ADD CONSTRAINT \"pkey_~ts_id\" PRIMARY KEY (id);
    END IF;

    ~ts
END;$$;
",
        [Schema,TableNamePart,Schema,TableName,?BU:strdate(FromDt),?BU:strdate(ToDt),
         Schema,TableNamePart,
         Schema,TableNamePart,TableNamePart,
         SqlCreateIndexes])),
    ?LOG('$trace', "PostgreDB. Struct. do_ensure_partition sql: ~n~ts",[SqlCreate]),
    Res = ?PgU:ensure_dbresult_is_ok(?PgDBA:query_db_tout(Conn, SqlCreate, ?SqlTout), <<"create_partition_error">>),
    ensure_indexes(Schema,TableNamePart,Meta,Conn,Res).

%% @private
get_partition_sides(DT,TableName,Meta) ->
    {DT,_,_} = ?BU:parse_datetime(DT),
    PartInterval = maps:get(<<"interval">>,maps:get(<<"partition">>,maps:get(<<"opts">>,Meta))),
    {?PostgresPartUtils:parttabname(PartInterval,DT,TableName), ?PostgresPartUtils:prev(PartInterval,DT), ?PostgresPartUtils:next(PartInterval,DT)}.

%% ---------------------------------------------------------------------------
%% INDEXES
%% ---------------------------------------------------------------------------

ensure_indexes(_,_,_,_,{error,_}=Err) -> Err;
ensure_indexes(Schema,TableName,Meta,Conn,ok) ->
    ensure_indexes(Schema,TableName,Meta,Conn).

ensure_indexes(Schema,TableName,Meta,Conn) ->
    SqlRead = sql_read_all_index_names(Schema,TableName),
    ?LOG('$trace', "PostgreDB. Struct. read_all_indexes sql: ~n~ts",[SqlRead]),
    case ?PgDBA:query_db_tout(Conn, SqlRead, ?SqlTout) of
        {ok,_Cols,Values} when is_list(Values) -> ensure_indexes_1(Schema,TableName,Meta,Conn,[?BU:to_binary(IndexName) || {IndexName} <- Values]);
        {ok,_,_Cols,Values} when is_list(Values) -> ensure_indexes_1(Schema,TableName,Meta,Conn,[?BU:to_binary(IndexName) || {IndexName} <- Values]);
        {ok,_,[]} -> {ok,#{}};
        {ok,_,[{PartFields,PartStrategy}]} ->
            PartitionFields = binary:split(PartFields,<<",">>,[global]),
            {ok,#{partition => #{fields => PartitionFields,
                                 strategy => PartStrategy}}};
        {error,_}=Err -> Err;
        Err -> {error,Err}
    end.
%% @private
ensure_indexes_1(Schema,TableName,Meta,Conn,ExistingIndexes) ->
    IndexFields = maps:get(<<"index_fields">>,Meta),
    IndexNames = [{index_name(Schema,TableName,FieldName),FieldName}|| FieldName <- IndexFields],
    ToDrop = ExistingIndexes -- lists:map(fun({IndexName,_FieldName}) -> IndexName end, IndexNames),
    ToCreate = lists:filter(fun({IndexName,_FieldName}) -> not lists:member(IndexName,ExistingIndexes) end, IndexNames),
    Res0 = ok,
    Res1 = lists:foldl(fun(_,{error,_}=Err) -> Err;
                          (IndexName,ok) ->
                                SqlDrop = sql_drop_index(Schema,IndexName),
                                ?LOG('$trace', "PostgreDB. Drop index: '~ts', sql: ~n~ts",[IndexName,SqlDrop]),
                                ?PgU:ensure_dbresult_is_ok(?PgDBA:query_db_tout(Conn, SqlDrop, ?SqlTout), <<"drop_indexes_error">>)
                       end, Res0, ToDrop),
    Res2 = lists:foldl(fun(_,{error,_}=Err) -> Err;
                          ({_IndexName,FieldName},ok) ->
                                SqlCreate = sql_create_index(Schema,TableName,FieldName),
                                ?LOG('$trace', "PostgreDB. Create index: '~ts', sql: ~n~ts",[_IndexName,SqlCreate]),
                                ?PgU:ensure_dbresult_is_ok(?PgDBA:query_db_tout(Conn, SqlCreate, ?SqlTout), <<"create_indexes_error">>)
                       end, Res1, ToCreate),
    ?LOG('$trace', "PostgreDB. Struct. ensure_indexes: ~120tp",[Res2]),
    Res2.

%% @private
sql_read_all_index_names(Schema,TableName) ->
    ?BU:to_list(?BU:strbin("
SELECT indexname
FROM pg_indexes
WHERE schemaname = '~ts' AND tablename = '~ts' AND NOT indexname LIKE 'pkey_%';
",
        [Schema,TableName])).

%% @private
sql_drop_index(Schema,IndexName) ->
    ?BU:to_list(?BU:strbin("
DROP INDEX IF EXISTS ~ts.\"~ts\"
",
        [Schema,IndexName])).

%% @private
sql_create_index(Schema,TableName,FieldName) ->
    IndexName = index_name(Schema,TableName,FieldName),
    ?BU:to_list(?BU:strbin("
CREATE INDEX IF NOT EXISTS \"~ts\" ON ~ts.\"~ts\" (\"~ts\") WITH (FILLFACTOR=80);
",
        [IndexName,Schema,TableName,FieldName])).

%% @private
index_name(_Schema,TableName,FieldName) ->
    TableNameB = ?BU:to_binary(TableName),
    FieldNameB = ?BU:to_binary(FieldName),
    case {size(TableNameB), size(FieldNameB)} of
        {T,F} when T+F =< 60 -> ?BU:strbin("i_~ts_~ts", [TableName,FieldName]); % 63 - 'i_' - '_'
        {T,F} ->
            THC = ?StorageUtils:build_hc(TableNameB),
            FHC = ?StorageUtils:build_hc(FieldNameB),
            case {size(THC), size(FHC)} of
                {NT,_} when NT+F =< 59 -> ?BU:strbin("i_!~ts_~ts", [THC,FieldName]); % 63 - 'i_' - '_'
                {_,NF} when T + NF =< 59 -> ?BU:strbin("i_~ts_!~ts", [TableName,FHC]);
                _ -> ?BU:strbin("i_!~ts_!~ts", [THC,FHC])
            end end.

%% @private
sql_drop_all_indexes(Schema,TableName) ->
    ?BU:to_list(?BU:strbin("
do $$
DECLARE n varchar;
BEGIN
	n = '';
	WHILE (NOT n IS NULL) LOOP

		SELECT indexname INTO n
		FROM pg_indexes
		WHERE schemaname = '~ts' AND tablename = '~ts' AND NOT indexname LIKE 'pkey_%'
		LIMIT 1;

		IF (n IS NOT NULL) THEN
			EXECUTE ( SELECT 'DROP INDEX ~ts.\"' || n || '\"');
			RAISE NOTICE 'index % ', n;
		END IF;

	END LOOP;
END;
$$;
", [Schema, TableName,
        Schema])).

%% @private
sqlbin_drop_key_and_indexes_text(Schema,TableName) ->
    ?BU:strbin("
do $$
DECLARE n varchar;
BEGIN
	n = '';
	ALTER TABLE ~ts.\"~ts\" DROP CONSTRAINT IF EXISTS \"pkey_~ts_id\";
	WHILE (NOT n IS NULL) LOOP

		SELECT indexname INTO n
		FROM pg_indexes
		WHERE schemaname = '~ts' AND tablename = '~ts'
		LIMIT 1;

		IF (n IS NOT NULL) THEN
			EXECUTE ( SELECT 'DROP INDEX ~ts.\"' || n || '\"');
			RAISE NOTICE 'index % ', n;
		END IF;

	END LOOP;
END;
$$;
", [Schema,TableName,TableName,
        Schema,TableName,
        Schema]).

%% ====================================================================
%% Support functions
%% ====================================================================

prepare_table_field_constraints([],Acc) -> Acc;
prepare_table_field_constraints([{_,<<>>,_}|T],Acc) ->
    prepare_table_field_constraints(T,Acc);
prepare_table_field_constraints([{req,Required,<<"">>}|T],Acc) ->
    AccX = case ?BU:to_bool(Required) of
               true -> <<" NOT NULL">>;
               false -> <<"">>
           end,
    Acc1 = <<Acc/binary,AccX/binary>>,
    prepare_table_field_constraints(T,Acc1);
prepare_table_field_constraints([{def,Default,Prefix}|T],Acc) ->
    AccX = case has_default(Default) of
               true -> <<" ", Prefix/binary, " DEFAULT ", Default/binary>>;
               false -> <<"">>
           end,
    Acc1 = <<Acc/binary, AccX/binary>>,
    prepare_table_field_constraints(T,Acc1).

%% @private
has_default(undefined) -> false;
has_default(null) -> false;
has_default(_) -> true.

%% --------------------------------------------------
%% @private
%% Put classname -> tablename into table
%% --------------------------------------------------
store_classname_table(Meta,Conn) ->
    DbUser = maps:get(dbuser,Meta),
    ClassName = maps:get(<<"classname">>,Meta),
    TableName = maps:get(<<"tablename">>,Meta),
    MetaSchema = <<"metamodel">>,
    MetaTable = <<"classtables">>,
    Sql = ?BU:to_list(?BU:strbin("
CREATE schema IF NOT EXISTS ~ts AUTHORIZATION ~ts;

CREATE table IF NOT EXISTS ~ts.\"~ts\" (
  \"classname\" varchar,
  \"tablename\" varchar,
  CONSTRAINT pkey_classtables_classname PRIMARY KEY (\"classname\")
)
WITH (OIDS=FALSE);

INSERT INTO ~ts.\"~ts\" (\"classname\",\"tablename\")
VALUES (
  '~ts',
  '~ts'
)
ON CONFLICT (\"classname\") DO UPDATE SET
  \"tablename\" = '~ts';
",
        [MetaSchema,DbUser,
            MetaSchema,MetaTable,
            MetaSchema,MetaTable,ClassName,TableName,TableName])),
    ?LOG('$trace', "PostgreDB. Struct. store_classname_table sql: ~n~ts",[Sql]),
    Res = ?PgDBA:query_db_tout(Conn, Sql, ?SqlTout),
    ?LOG('$trace', "Rename result: ~120tp",[Res]),
    ?PgU:ensure_dbresult_is_ok(Res,<<"store_classname_table_error">>).

%% --------------------------------------------------
%% @private
%% Put classname, db, backup_table into table
%% --------------------------------------------------
store_classname_backup(Meta,Conn,{Ts,BkpHash,BkpTableName}) ->
    Dt = ?BU:strdatetime(?BU:timestamp_to_datetime(Ts)),
    DbUser = maps:get(dbuser,Meta),
    ClassName = maps:get(<<"classname">>,Meta),
    TableName = maps:get(<<"tablename">>,Meta),
    MetaSchema = <<"metamodel">>,
    MetaTable = <<"classbackups">>,
    Sql = ?BU:to_list(?BU:strbin("
CREATE schema IF NOT EXISTS ~ts AUTHORIZATION ~ts;

CREATE table IF NOT EXISTS ~ts.\"~ts\" (
  \"classname\" varchar,
  \"dt\" timestamp without time zone,
  \"ts\" bigint,
  \"hash\" varchar,
  \"backup_tablename\" varchar,
  \"tablename\" varchar,
  CONSTRAINT pkey_classbackups_classname_dt PRIMARY KEY (\"classname\",\"dt\")
)
WITH (OIDS=FALSE);

INSERT INTO ~ts.\"~ts\" (\"classname\",\"dt\",\"ts\",\"hash\",\"backup_tablename\",\"tablename\")
VALUES (
  '~ts',
  '~ts',
  ~p,
  '~ts',
  '~ts',
  '~ts'
)
ON CONFLICT (\"classname\",\"dt\") DO UPDATE SET
  \"ts\" = ~p,
  \"hash\" = '~ts',
  \"backup_tablename\" = '~ts',
  \"tablename\" = '~ts';
",
        [MetaSchema,DbUser,
            MetaSchema,MetaTable,
            MetaSchema,MetaTable,
            ClassName,Dt,Ts,BkpHash,BkpTableName,TableName,
            Ts,BkpHash,BkpTableName,TableName])),
    ?LOG('$trace', "PostgreDB. Struct. store_classname_table sql: ~n~ts",[Sql]),
    Res = ?PgDBA:query_db_tout(Conn, Sql, ?SqlTout),
    ?LOG('$trace', "Rename result: ~120tp",[Res]),
    ?PgU:ensure_dbresult_is_ok(Res,<<"store_classname_table_error">>).

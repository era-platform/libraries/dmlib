%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.03.2021
%%% @doc Implements CRUD operations on PostgreSql with partitions

-module(dmlib_storage_postgresql_partitioned_crud).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([lookup/6, lookup_ex/6,
         find/5,
         read/4,
         create/5,
         replace/5,
         update/5,
         delete/5,
         clear/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(UpdateTimeout, 20000).

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------------------------
%% Execute lookup select request to postgre db and return entity
%% TODO: if content is UUID then no transform to varchar, else exclude uuid fields from lookup. Also is for numbers and datetimes
%% -----------------------------------------
-spec lookup(Meta::map(),QS::map()|list(),Content::binary(),DbParams::map(),PropNames::[binary()],LookupKeyArg::binary()|[binary()])
      -> {ok,[Id::binary()]} | {error,Reason::term()}.
%% -----------------------------------------
lookup(Meta,_QS,Content,DbParams,_PropNames,LookupKeyField) when is_binary(LookupKeyField) ->
    %lookup(Meta,_QS,Content,DbParams,PropNames,[LookupKeyField]);
    Opts = maps:get(opts,Meta),
    PartField = maps:get(<<"partition_property">>,Opts),
    % SELECT ...
    Fields = ?BU:str("\"id\", \"~ts\"", [PartField]),
    % FROM ...
    Schema = ?PostgresqlSchema,
    ClassName = maps:get(classname,Meta),
    TableName = ?PgU:tablename(ClassName),
    % WHERE ...
    Where = ?PgCrudU:lookup_where(Meta,LookupKeyField,Content),
    %
    ReadSql = ?BU:str("
SELECT ~ts
FROM ~ts.\"~ts\"
WHERE ~ts;",[Fields,Schema,TableName,Where]),
    ?PgCrudU:apply_lookup(DbParams,ReadSql);
lookup(Meta,QS,Content,DbParams,PropNames,LookupKeyFields) when is_list(LookupKeyFields) ->
    Opts = maps:get(opts,Meta),
    PartField = maps:get(<<"partition_property">>,Opts),
    LookupKeyFields1 = [<<"id">>|lists:delete(<<"id">>,LookupKeyFields)],
    Filter = [<<"||">> | [[<<"==">>,[<<"string">>,[<<"property">>,KeyField]],[<<"string">>,Content]] || KeyField <- LookupKeyFields1]],
    % Prepare
    ?LOG('$trace',"Content: ~120tp", [Content]),
    ?LOG('$trace',"Filter: ~120tp", [Filter]),
    Interval = case ?BU:get_by_key(<<"interval">>,QS,<<>>) of
                   <<>> -> 'lookup';
                   [_,_]=Val when is_list(Val) -> Val;
                   Val when is_map(Val) -> Val;
                   Val when is_binary(Val) -> jsx:decode(Val, [return_maps])
               end,
    SelectOpts = #{<<"interval">> => Interval,
                   <<"filter">> => Filter,
                   <<"order">> => [],
                   <<"mask">> => [<<"id">>,PartField],
                   <<"limit">> => 100,
                   <<"count_only">> => false},
    ReadSql = sql_read_items(Meta,SelectOpts,PropNames),
    ?PgCrudU:apply_lookup(DbParams,ReadSql).

%% -----------------------------------------
-spec lookup_ex(Meta::map(),QS::map()|list(),Content::binary(),DbParams::map(),PropNames::[binary()],LookupKeyArg::binary()|[binary()])
      -> {ok,[Id::binary()]} | {error,Reason::term()}.
%% -----------------------------------------
lookup_ex(Meta,_QS,Content,DbParams,PropNames,LookupKeyField) when is_binary(LookupKeyField) ->
    %lookup_ex(Meta,_QS,Content,DbParams,PropNames,[LookupKeyField]);
    % SELECT ...
    Fields = ?BU:to_list(?BU:join_binary([<<"\"",Field/binary,"\"">> || Field <- PropNames], <<", ">>)),
    % FROM ...
    Schema = ?PostgresqlSchema,
    ClassName = maps:get(classname,Meta),
    TableName = ?PgU:tablename(ClassName),
    % WHERE ...
    Where = ?PgCrudU:lookup_where(Meta,LookupKeyField,Content),
    %
    ReadSql = ?BU:str("
SELECT ~ts
FROM ~ts.\"~ts\"
WHERE ~ts;",[Fields,Schema,TableName,Where]),
    ?PgCrudU:apply_read(DbParams,ReadSql,#{},Meta,PropNames);
lookup_ex(Meta,QS,Content,DbParams,PropNames,LookupKeyFields) ->
    %PartField = maps:get(<<"partition_property">>,Opts),
    LookupKeyFields1 = [<<"id">>|lists:delete(<<"id">>,LookupKeyFields)],
    Filter = [<<"||">> | [[<<"==">>,[<<"string">>,[<<"property">>,KeyField]],[<<"string">>,Content]] || KeyField <- LookupKeyFields1]],
    % Prepare
    ?LOG('$trace',"Content: ~120tp", [Content]),
    ?LOG('$trace',"Filter: ~120tp", [Filter]),
    Interval = case ?BU:get_by_key(<<"interval">>,QS,<<>>) of
                   <<>> -> 'lookup';
                   [_,_]=Val when is_list(Val) -> Val;
                   Val when is_map(Val) -> Val;
                   Val when is_binary(Val) -> jsx:decode(Val, [return_maps])
               end,
    SelectOpts = #{<<"interval">> => Interval,
                   <<"filter">> => Filter,
                   <<"order">> => [],
                   <<"mask">> => PropNames,
                   <<"limit">> => 100,
                   <<"count_only">> => false},
    ReadSql = sql_read_items(Meta,SelectOpts,PropNames),
    ?PgCrudU:apply_read(DbParams,ReadSql,SelectOpts,Meta,PropNames).


%% -----------------------------------------
%% Execute select request to postgre db and return entity
%% -----------------------------------------
-spec find(Meta::map(),{PartDt::binary()},Id::binary(),DbParams::map(),PropNames::[binary()]) -> {ok,Entity::map()} | {error,Reason::term()} | false.
%% -----------------------------------------
find(Meta,{PartDt},Id,DbParams,PropNames) ->
    {Dt,_,_} = ?PgU:parse_dt(PartDt),
    FindSql = sql_find(Meta,Dt,Id,PropNames),
    ?PgCrudU:apply_find(DbParams,FindSql,Meta,PropNames,fun(_Err) -> false end).

%% -----------------------------------------
%% Execute select request to postgre db and return entity
%% -----------------------------------------
-spec read(Meta::map(),SelectOpts::map(),DbParams::map(),PropNames::[binary()]) -> {ok,[Entity::map()]} | {error,Reason::term()}.
%% -----------------------------------------
read(Meta,SelectOpts,DbParams,PropNames) ->
    AggrMap = ?BU:get_by_key(<<"aggr">>,SelectOpts,#{}),
    GroupByMap = ?BU:get_by_key(<<"groupby">>,SelectOpts,#{}),
    ReadSql = case maps:size(AggrMap) + maps:size(GroupByMap) > 0 of
                  false -> sql_read_items(Meta,SelectOpts,PropNames);
                  true -> sql_read_aggr(Meta,SelectOpts,PropNames)
              end,
    ?PgCrudU:apply_read(DbParams,ReadSql,SelectOpts,Meta,PropNames).

%% -----------------------------------------
%% Execute insert/update request to postgre db
%% Means create entity (INSERT operation)
%% -----------------------------------------
-spec create(Meta::map(),{PartDt::binary()},Entity::map(),DbParams::map(),PropNames::[binary()]) -> ok | {error,Reason::term()}.
%% -----------------------------------------
create(Meta,{PartDt},Entity,DbParams,PropNames) ->
    update(Meta,{undefined,PartDt},Entity,DbParams,PropNames).

%% -----------------------------------------
%% Execute insert/update request to postgre db
%% Means replace entity (UPDATE operation)
%% -----------------------------------------
-spec replace(Meta::map(),{PartDt0::binary(),PartDt1::binary()},Entity::map(),DbParams::map(),PropNames::[binary()]) -> ok | {error,Reason::term()}.
%% -----------------------------------------
replace(Meta,{PartDt0,PartDt1},Entity,DbParams,PropNames) ->
    update(Meta,{PartDt0,PartDt1},Entity,DbParams,PropNames).

%% -----------------------------------------
%% Execute insert/update request to postgre db
%% Means merge/update entity (UPDATE operation)
%% -----------------------------------------
-spec update(Meta::map(),{PartDt0::binary(),PartDt1::binary()},Entity::map(),DbParams::map(),PropNames::[binary()]) -> ok | {error,Reason::term()}.
%% -----------------------------------------
update(Meta,{PartDt0,PartDt1},Entity,DbParams,PropNames) ->
    {Dt1,_,_} = ?PgU:parse_dt(PartDt1),
    % insert/update into partition
    PutSql = sql_put(Meta,Dt1,Entity,PropNames),
    % delete from old partition
    DelSql = case PartDt0 of
                 undefined -> "";
                 PartDt1 -> "";
                 _ ->
                     {Dt0,_,_} = ?PgU:parse_dt(PartDt0),
                     T0 = ?PostgresPartUtils:parttabname(Dt0,Meta),
                     T1 = ?PostgresPartUtils:parttabname(Dt1,Meta),
                     case T0 == T1 of
                         true -> "";
                         false -> sql_delete(Meta,Dt0,maps:get(<<"id">>,Entity))
                     end end,
    % complex sql
    Sql = PutSql ++ DelSql,
    ?LOG('$trace',"Put SQL: ~ts", [?BU:to_binary(PutSql)]),
    ?LOG('$trace',"DbParams: ~120tp", [DbParams]),
    case timer:tc(fun() -> ?PgConnectionPool:get_connection(DbParams,5000) end) of
        {Mks1,{ok,Conn}} ->
            ?LOG('$trace',"Conn: ~120tp",[Conn]),
            {Mks2,ResA} = timer:tc(fun() -> (catch ?PgDBA:query_db_tout(Conn,Sql,?UpdateTimeout)) end),
            ?LOG('$trace',"ResA: ~120tp",[ResA]),
            FunRollback = fun() ->
                                TableMeta = ?PostgresPartMeta:get_table_meta(Meta),
                                case ?PgStructure:ensure_partition(Dt1,TableMeta,Conn) of
                                    {error,_}=ErrB ->
                                        ?LOG('$trace',"ErrB: ~120tp",[ErrB]),
                                        timer:sleep(200);
                                    ok -> ok
                                end,
                                ResB = (catch ?PgDBA:query_db_tout(Conn,Sql,?UpdateTimeout)),
                                ?LOG('$trace',"ResB: ~120tp",[ResB]),
                                ?PgU:ensure_dbresult_is_ok(ResB,<<"insert/update_error">>)
                          end,
            Res = case ?PgU:ensure_dbresult_is_ok(ResA,<<"insert/update_error">>) of
                      ok -> ok;
                      {error,_} -> FunRollback();
                      {'EXIT',Reason} ->
                          ?LOG('$error',"Connection pid ~tp found down: ~120tp",[Conn,Reason]),
                          FunRollback()
                  end,
            {Mks3,_} = timer:tc(fun() -> ?PgConnectionPool:release_connection(DbParams,Conn) end),
            ?PgU:log_mks(update,Mks1,Mks2,Mks3),
            Res;
        {_,{error,timeout}} -> {error,{service_unavailable,<<"Connection pool is overloaded. Try later.">>}};
        {_,{error,_}=Err} -> Err
    end.

%% -----------------------------------------
%% Execute delete request to postgre db
%% Means delete entity (DELETE operation)
%% -----------------------------------------
-spec delete(Meta::map(),{PartDt::binary()},Id::binary(),DbParams::map(),PropNames::[binary()]) -> ok | {error,Reason::term()}.
%% -----------------------------------------
delete(Meta,{PartDt},Id,DbParams,_PropNames) ->
    {Dt,_,_} = ?PgU:parse_dt(PartDt),
    DelSql = sql_delete(Meta,Dt,Id),
    ?PgCrudU:apply_delete(DbParams,DelSql).

%% -----------------------------------------
%% Execute delete request to postgre db
%% Means delete entity (DELETE operation)
%% -----------------------------------------
-spec clear(Meta::map(),DbParams::map()) -> ok | {error,Reason::term()}.
%% -----------------------------------------
clear(Meta,DbParams) ->
    TrnSql = sql_truncate(Meta),
    ?PgCrudU:apply_clear(DbParams,TrnSql).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------------------------
%% FIND
%% ----------------------------------------------
sql_find(Meta,Dt,Id,PropNames) ->
    TableNamePart = ?PostgresPartUtils:parttabname(Dt,Meta),
    ?PgCrudU:sql_find(Id,PropNames,TableNamePart).


%% ----------------------------------------------
%% SELECT ITEMS
%% ----------------------------------------------
%% TODO: make union of intersecting partitions
%% TODO: filter by existing partitions
sql_read_items(Meta,SelectOpts,PropNames) ->
    % WARNING: without unicode transform
    % SELECT ...
    {Fields,CountOnly} = ?PgCrudU:read_fields_external(Meta,SelectOpts,PropNames),
    % FROM ...
    Schema = ?PostgresqlSchema,
    TableName = ?PostgresPartUtils:tabname(maps:get(classname,Meta)),
    % WHERE ...
    Where = ?BU:to_list(read_filter(Meta,SelectOpts,PropNames)),
    % ORDER BY ...
    Order = ?BU:to_list(?PgCrudU:read_order(false,Meta,SelectOpts,PropNames)),
    case CountOnly of
        false ->
            % LIMIT ...
            Limit = ?PgCrudU:read_limit(Meta,SelectOpts),
            Offset = ?PgCrudU:read_offset(Meta,SelectOpts),
            %
            ?BU:str("
SELECT ~ts
FROM ~ts.\"~ts\" ~ts ~ts
LIMIT ~p OFFSET ~p;",[Fields,Schema,TableName,Where,Order,Limit,Offset]);
        true ->
            ?BU:str("
SELECT ~ts
FROM ~ts.\"~ts\" ~ts;",[Fields,Schema,TableName,Where])
    end.

%% ----------------------------------------------
%% SELECT AGGREGATE
%% ----------------------------------------------
sql_read_aggr(Meta,SelectOpts,PropNames) ->
    % WARNING: without unicode transform
    % SELECT ...
    % GROUP BY ...
    {Fields,GroupBy} = ?PgCrudU:read_fields_aggr(Meta,SelectOpts,PropNames),
    % FROM ...
    Schema = ?PostgresqlSchema,
    TableName = ?PostgresPartUtils:tabname(maps:get(classname,Meta)),
    % WHERE ...
    Where = ?BU:to_list(read_filter(Meta,SelectOpts,PropNames)),
    % HAVING
    Having = <<>>,
    % ORDER BY ...
    Order = ?BU:to_list(?PgCrudU:read_order(false,Meta,SelectOpts,?PgCrudU:get_aggr_select_propnames(SelectOpts))),
    % LIMIT ...
    Limit = ?PgCrudU:read_limit_default(Meta,SelectOpts),
    Offset = ?PgCrudU:read_offset(Meta,SelectOpts),
    %
    ?BU:str("
SELECT ~ts
FROM ~ts.\"~ts\" ~ts ~ts ~ts ~ts
LIMIT ~p OFFSET ~p;",[Fields,Schema,TableName,Where,GroupBy,Having,Order,Limit,Offset]).


%% ------------------
%% @private
read_filter(Meta,SelectOpts,_PropNames) ->
    case ?BU:get_by_key(<<"interval">>,SelectOpts) of
        [] -> throw({error,{invalid_params,?BU:strbin("Expected 'interval' parameter: [From, To]",[])}});
        [FromDt,ToDt] ->
            NullDt = {{1970,0,0},{0,0,0},0},
            case {?PgU:parse_dt(FromDt,false), ?PgU:parse_dt(ToDt,false)} of
                {NullDt,_} -> throw({error,{invalid_params,?BU:strbin("Invalid 'interval'. Expected valid datetime values",[])}});
                {_,NullDt} -> throw({error,{invalid_params,?BU:strbin("Invalid 'interval'. Expected valid datetime values",[])}});
                {A,B} when A > B -> throw({error,{invalid_params,?BU:strbin("Expected 'interval' From is less or equal than To",[])}});
                {_A,_B} ->
                    PartField = maps:get(<<"partition_property">>,maps:get(opts,Meta)),
                    Filter = ?BU:get_by_key(<<"filter">>,SelectOpts),
                    Filter1 = case Filter of
                                  [] -> [<<"between">>,[<<"property">>,PartField],FromDt,ToDt];
                                  _ -> [<<"and">>,[<<"between">>,[<<"property">>,PartField],FromDt,ToDt],Filter]
                              end,
                    case ?FilterPg:build_filter_string(Filter1) of
                        {error,_}=Err -> throw(Err);
                        <<>> -> <<>>;
                        Bin -> <<"\nWHERE ",Bin/binary>>
                    end end;
        'lookup' ->
            Filter = ?BU:get_by_key(<<"filter">>,SelectOpts),
            case ?FilterPg:build_filter_string(Filter) of
                {error,_}=Err -> throw(Err);
                <<>> -> <<>>;
                Bin -> <<"\nWHERE ",Bin/binary>>
            end
    end.

%% ----------------------------------------------
%% INSERT OR UPDATE
%% ----------------------------------------------
sql_put(Meta,Dt,Entity,PropNames) ->
    TableNamePart = ?PostgresPartUtils:parttabname(Dt,Meta),
    ?PgCrudU:sql_put(Meta,Entity,PropNames,TableNamePart).

%% ----------------------------------------------
%% DELETE
%% ----------------------------------------------
sql_delete(Meta,Dt,Id) ->
    TableNamePart = ?PostgresPartUtils:parttabname(Dt,Meta),
    ?PgCrudU:sql_delete(Id,TableNamePart).

%% ----------------------------------------------
%% TRUNCATE
%% ----------------------------------------------
sql_truncate(Meta) ->
    ClassName = maps:get(classname,Meta),
    TableName = ?PostgresPartUtils:tabname(ClassName),
    ?PgCrudU:sql_truncate(TableName).


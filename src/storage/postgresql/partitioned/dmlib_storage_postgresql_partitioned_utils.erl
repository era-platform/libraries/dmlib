%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 03.04.2021
%%% @doc

-module(dmlib_storage_postgresql_partitioned_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([parttabname/2, parttabname/3,
         tabname/1, tabname_prev/1,
         prev/2,
         next/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% Return partition table name for dt
parttabname(Dt,Meta) ->
    ClassName = ?BU:to_binary(maps:get(classname,Meta)),
    PartInterval = maps:get(<<"partition_interval">>,maps:get(opts,Meta)),
    parttabname(PartInterval,Dt,tabname(ClassName)).

%% Return partition table name for dt
parttabname(<<"year">>,{Y,_,_},TableName) -> ?BU:str("~ts#~4..0B",[TableName,Y]);
parttabname(<<"month">>,{Y,M,_},TableName) -> ?BU:str("~ts#~4..0B~2..0B",[TableName,Y,M]);
parttabname(<<"day">>,{Y,M,D},TableName) -> ?BU:str("~ts#~4..0B~2..0B~2..0B",[TableName,Y,M,D]).

%% Return previous general table name
tabname_prev(ClassName) ->
    TableName = ?StorageUtils:shrink_tablename(?BU:to_binary(ClassName),38), %(63 - 1*postfix - 1*partition - 8*partition_date - 15*backup)
    <<TableName/binary,"$">>.

%% Return general table name
tabname(ClassName) ->
    TableName = ?StorageUtils:shrink_tablename(?BU:to_binary(ClassName),45), %(63 - 1*postfix - 1*partition - 8*partition_date - 8*backup)
    <<TableName/binary,"$">>.

%% Return partition start dt
prev(<<"year">>,{Y,_M,_D}) -> {Y,1,1};
prev(<<"month">>,{Y,M,_D}) -> {Y,M,1};
prev(<<"day">>,{Y,M,D}) -> {Y,M,D}.

%% Return partition end dt
next(<<"year">>,{Y,_M,_D}) -> {Y+1,1,1};
next(<<"month">>,{Y,M,_D}) -> case M of 12 -> {Y+1,1,1}; _ -> {Y,M+1,1} end;
next(<<"day">>,{Y,M,D}) -> element(1,calendar:gregorian_seconds_to_datetime(calendar:datetime_to_gregorian_seconds({{Y,M,D},{0,0,0}}) + 86400)).

%% ====================================================================
%% Internal functions
%% ====================================================================
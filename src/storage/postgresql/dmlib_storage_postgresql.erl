%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.03.2021
%%% @doc Storage-postgresql facade

-module(dmlib_storage_postgresql).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_db_params/2]).
-export([ensure_table/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------------------
%% return db connection params for data-model-class in domain
%% -----------------------------------
-spec get_db_params(Domain::binary()|atom(), Meta::map())
      -> {ok,{default,PoolSize::integer(),[DbParams::map()]}} | {error,Reason::term()}.
%% -----------------------------------
get_db_params(Domain,Meta) when is_map(Meta) ->
    db_params(Domain,{meta,Meta});
get_db_params(Domain,{meta,Meta}) ->
    db_params(Domain,{meta,Meta});
get_db_params(Domain,{instance,InstanceValue}) ->
    db_params(Domain,{instance,InstanceValue}).

%% @private
db_params(Domain,{meta,Meta}) ->
    case maps:get(<<"storage_instance">>,maps:get(opts,Meta),<<>>) of
        <<>> -> {error,{internal_error,?BU:strbin("Invalid class metadata. Expected existing opts.storage_instance",[])}};
        InstanceValue ->
            db_params(Domain,{instance,InstanceValue})
    end;
db_params(Domain,{instance,InstanceValue}) ->
    case ?CFG:get_storages_function() of
        undefined -> {error,{internal_error,?BU:strbin("Invalid initial parameters. Expected 'get_storages_function'",[])}};
        Fun when not is_function(Fun,3) -> {error,{internal_error,?BU:strbin("Invalid initial parameters. Expected valid 'get_storages_function' -> fun/3",[])}};
        Fun ->
            StorageType = <<"postgresql">>,
            case db_params_1(Fun(Domain,StorageType,InstanceValue),StorageType) of
                {error,_}=Err -> Err;
                {ok,[]} -> {error,{invalid_storage,?BU:strbin("Invalid storage ~ts@~ts of type '~ts'. Expected host to connect.",[InstanceValue,Domain,StorageType])}};
                {ok,_}=Ok -> Ok
            end
    end.

%% @private
%% --------
-spec db_params_1(Res::{ok,[Storage::map()]} | [] | {error,Reason::term()}, StorageType::binary())
      -> {ok,{default,PoolSize::integer(),[DbParams::map()]}} | {error,Reason::term()}.
%% --------
db_params_1({error,_}=Err,_SType) -> Err;
db_params_1([],SType) -> {error,{internal_error,?BU:strbin("Invalid class metadata. Expected existing storage_instance (opts.storage_instance should match at least 1 domain's storage of type: '~ts'",[SType])}};
db_params_1({ok,Storages},_SType) ->
    FunDecrypt = case ?CFG:decrypt_function() of
                     Fun when is_function(Fun,1) -> Fun;
                     _ -> fun(X) -> X end
                 end,
    DbParams = lists:foldl(fun(M,Acc) ->
                                    M0 = #{host => Host0 = ?BU:to_list(maps:get(<<"host">>,M,undefined)),
                                           port => ?BU:to_int(maps:get(<<"port">>,M,5432)),
                                           login => ?BU:to_list(maps:get(<<"login">>,M,<<>>)),
                                           pwd => ?BU:to_list(FunDecrypt(maps:get(<<"pwd">>,M,<<>>))),
                                           database => ?BU:to_list(maps:get(<<"database">>,M))},
                                    case maps:get(<<"endpoints">>,M,[]) of
                                        [] when Host0 /= <<>> -> [M0|Acc];
                                        List when is_list(List) ->
                                            lists:foldl(
                                                fun(EP,Acc1) ->
                                                    Keys = [{host,<<"host">>,to_list},
                                                            {port,<<"port">>,to_int},
                                                            {login,<<"login">>,to_list},
                                                            {pwd,<<"pwd">>,to_list}],
                                                    M2 = lists:foldl(
                                                        fun({Key1,Key2,Transform}, Acc2) ->
                                                            case maps:get(Key2,EP,undefined) of
                                                                undefined -> Acc2;
                                                                Value when Key1==pwd -> Acc2#{Key1 => ?BU:Transform(FunDecrypt(Value))};
                                                                Value -> Acc2#{Key1 => ?BU:Transform(Value)}
                                                            end
                                                        end, M0, Keys),
                                                    [M2|Acc1]
                                                end, Acc, List);
                                        Other when not is_list(Other), Host0 /= <<>> -> [M0|Acc];
                                        _Other -> Acc
                                    end
                           end, [], Storages),
    PoolSize = lists:foldl(fun(M,Acc) -> min(256, max(Acc, ?BU:to_int(maps:get(<<"max_connections">>, M, Acc),Acc))) end, ?CFG:pg_max_connections_default(), Storages),
    {ok,{default,PoolSize,lists:reverse(DbParams)}}.

%% ====================================================================
%% Build structure facade
%% ====================================================================

%% -----------------------------------
%% make table for data-model-class
%% -----------------------------------
ensure_table(Domain,_ClassName,Meta) ->
    StorageMode = maps:get(storage_mode,Meta),
    case db_params(Domain,{meta,Meta}) of
        {error,_}=Err -> Err;
        {ok,DbParams} when StorageMode == <<"category">> ->
            {_,_,ConnParams}=DbParams,
            ?PostgresMeta:ensure_table(ConnParams,_ClassName,Meta);
        {ok,DbParams} when StorageMode == <<"history">> ->
            {_,_,ConnParams}=DbParams,
            ?PostgresPartMeta:ensure_table(ConnParams,_ClassName,Meta)
    end.

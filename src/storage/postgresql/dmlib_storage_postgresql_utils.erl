%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.04.2021
%%% @doc

-module(dmlib_storage_postgresql_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([tablename/1, tablename_prev/1]).

-export([parse_dt/1, parse_dt/2]).
-export([db_name/1,
         field_name/1,
         map_db_field_type/2,
         postgre_type_map/1]).

-export([ensure_dbresult_is_ok/2, ensure_dbresult_is_ok/3]).
-export([try_transform_err/1]).
-export([log_mks/4, log_mks/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include_lib("epgsql/include/epgsql.hrl").

-define(cfun(), {?MODULE,element(2, element(2, process_info(self(), current_function)))}).

%% ====================================================================
%% Public functions
%% ====================================================================

%%% ----------------------------------------------------------
tablename_prev(CN) ->
    TableName = ?BU:to_binary(CN),
    ?StorageUtils:shrink_tablename(TableName,48). % 63 - 15*backup

tablename(CN) ->
    TableName = ?BU:to_binary(CN),
    ?StorageUtils:shrink_tablename(TableName,55). % 63 - 8*backup

%%% ----------------------------------------------------------
parse_dt(Value) -> parse_dt(Value,true).
parse_dt(Value,FallbackToNow) ->
    try ?BU:parse_datetime(Value) of
        {{1970,1,1},{0,0,0},0}=DT ->
            case FallbackToNow of
                true -> ?BU:ticktoutc(?BU:timestamp());
                _ -> DT
            end;
        DT -> DT
    catch _:_ -> ?BU:ticktoutc(?BU:timestamp())
    end.

%% ------------------------------------------------
%%
db_name(N) -> ?BU:to_binary(N).

%%
field_name(N) -> ?BU:to_binary(N).

%%
map_db_field_type(Type,IsMulti) -> db_type(Type,IsMulti).

%% @private
db_type(<<"uuid">>,false) -> <<"uuid">>;
db_type(<<"string">>,false) -> <<"varchar">>;
db_type(<<"integer">>,false) -> <<"integer">>;
db_type(<<"increment">>,false) -> <<"bigint">>;
db_type(<<"long">>,false) -> <<"bigint">>;
db_type(<<"float">>,false) -> <<"float">>;
db_type(<<"boolean">>,false) -> <<"boolean">>;
db_type(<<"datetime">>,false) -> <<"timestamp">>;
db_type(<<"date">>,false) -> <<"timestamp">>;
db_type(<<"time">>,false) -> <<"timestamp">>;
db_type(<<"duration">>,false) -> <<"integer">>;
db_type(<<"entity">>,false) -> <<"uuid">>;
db_type(<<"any">>,false) -> <<"jsonb">>;
db_type(<<"attachment">>,false) -> <<"jsonb">>;
db_type(<<"enum">>,false) -> <<"varchar">>; % TODO ICP: to integer
db_type(_,true) -> <<"jsonb">>.

%% ------
postgre_type_map(<<"varchar">>) -> <<"character varying">>;
postgre_type_map(<<"timestamp">>) -> <<"timestamp without time zone">>;
postgre_type_map(<<"float">>) -> <<"double precision">>;
postgre_type_map(Type) -> Type.

%% ------------------------------------------------
ensure_dbresult_is_ok(DbRes,DefErrorText) ->
    ?LOG('$trace', "DbRes: ~120tp", [DbRes]),
    ensure_dbresult_is_ok(DbRes,fun(_) -> ok end,DefErrorText).

ensure_dbresult_is_ok(ok,_,_) -> ok;
ensure_dbresult_is_ok({error,Err}, FErr, DefErrorText) ->
    FErr(Err),
    ?LOG('$error', "PostgreDB. (~120tp) error: ~120tp",[?cfun(),Err]),
    {error,DefErrorText};
ensure_dbresult_is_ok(DbRes,_,_) when is_tuple(DbRes) andalso element(1,DbRes)==ok -> ok;
ensure_dbresult_is_ok(DbRes,_,DefErrorText) when is_list(DbRes) ->
    FCheck = fun(Resp) when is_tuple(Resp) andalso element(1,Resp)==ok -> true;
                (_) -> false
             end,
    case lists:all(FCheck,DbRes) of
        true -> ok;
        false ->
            ?LOG('$error', "PostgreDB. (~120tp) errors: ~120tp",[?cfun(),DbRes]),
            {error,DefErrorText}
    end;
ensure_dbresult_is_ok(_Err,_,DefErrorText) ->
    ?LOG('$error', "PostgreDB. (~120tp) other error: ~120tp",[?cfun(),_Err]),
    {error,DefErrorText}.

%% ------------------------------------------------
try_transform_err({error,Reason}=Err) when is_binary(Reason) -> Err;
try_transform_err({error,{_,Reason}}=Err) when is_binary(Reason); is_atom(Reason) -> Err;
try_transform_err({error,#error{severity=Sev,code=Code,codename=CodeN,message=Msg,extra=Extra}}) ->
    JSON = #{severity => Sev,
             code => Code,
             codename => CodeN,
             message => Msg,
             extra => case Extra of _ when is_list(Extra) -> ?BU:json_to_map(Extra); _ when is_map(Extra) -> Extra end},
    {error,jsx:encode(JSON)};
try_transform_err({error,Reason}) -> {error,term_to_binary(Reason)}.

%% ------------------------------------------------
log_mks(Operation,Mks1,Mks2,Mks3) ->
    case Mks1+Mks2+Mks3 of
        T when T > 1000000 -> ?LOG('$warning',"DUR. PG ~p ms on ~ts (~p + ~p + ~p)", [T div 1000, Operation, Mks1 div 1000, Mks2 div 1000, Mks3 div 1000]);
        T when T > 100000 -> ?LOG('$info',"DUR. PG ~p ms on ~ts (~p + ~p + ~p)", [T div 1000, Operation, Mks1 div 1000, Mks2 div 1000, Mks3 div 1000]);
        _ -> ok
    end.

log_mks(Operation,MksList) when is_list(MksList) ->
    case lists:sum(MksList) of
        T when T > 1000000 ->
            Parts = ?BU:join_binary(lists:map(fun(Mks) -> ?BU:to_binary(Mks div 1000) end, MksList), <<" + ">>),
            ?LOG('$warning',"DUR. PG ~p ms on ~ts (~ts)", [T div 1000, Operation, Parts]);
        T when T > 100000 ->
            Parts = ?BU:join_binary(lists:map(fun(Mks) -> ?BU:to_binary(Mks div 1000) end, MksList), <<" + ">>),
            ?LOG('$info',"DUR. PG ~p ms on ~ts (~ts)", [T div 1000, Operation, Parts]);
        _ -> ok
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================
%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 05.04.2021
%%% @doc

-module(dmlib_storage_postgresql_crud_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([sleep/2]).

-export([apply_lookup/2,
         apply_read/5,
         apply_find/5,
         apply_delete/2,
         apply_clear/2]).

-export([sql_find/3]). % list

-export([sql_put/4]). % list
-export([build_value/3]).

-export([lookup_where/3,
         read_fields_external/3,  % list
         read_fields_aggr/3,  % list
         read_order/4, % binary
         read_limit/2, % binary (simple)
         read_limit_default/2, % binary (simple)
         read_offset/2]). % binary (simple)

-export([sql_delete/2]).

-export([sql_truncate/1]).

-export([get_aggr_select_propnames/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(ReadTimeout, 60000).
-define(FindTimeout, 20000).
-define(UpdateTimeout, 20000).
-define(DeleteTimeout, 20000).
-define(ClearTimeout, 20000).

%% ====================================================================
%% Public functions
%% ====================================================================

%%% ----------------------------------------------------------
sleep(Timeout,Sql) ->
    ?BU:str("
DO $$
DECLARE aaa varchar;
BEGIN
Select pg_sleep(~p) into aaa;
~ts
END;$$;",[Timeout/1000,Sql]).

%%% ----------------------------------------------------------
apply_lookup(DbParams,LookupSql) ->
    ?LOG('$trace',"Read(lookup) SQL: ~ts", [?BU:to_binary(LookupSql)]),
    ?LOG('$trace',"DbParams: ~120tp", [DbParams]),
    % DB
    case timer:tc(fun() -> ?PgConnectionPool:get_connection(DbParams,5000) end) of
        {Mks1,{ok,Conn}} ->
            ?LOG('$trace',"Conn: ~120tp",[Conn]),
            {Mks2,PgRes} = timer:tc(fun() -> (catch ?PgDBA:query_db_tout(Conn,LookupSql,?FindTimeout)) end),
            {Mks3,ok} = timer:tc(fun() -> ?PgConnectionPool:release_connection(DbParams,Conn) end),
            {Mks4,ReadRes} = timer:tc(fun() ->
                case PgRes of
                    {ok,_Cols,Values} ->
                        F = fun({Id}=_ValueTuple) -> ?BU:to_binary(Id);
                               ({Id,PrePath}=_ValueTuple) -> ?BU:strbin("~ts/~ts",[?BU:strdate(element(1,?BU:parse_datetime(PrePath))),Id])
                            end,
                        case lists:map(F, Values) of
                            [] -> {ok,[]};
                            [_|_]=Ids -> {ok,Ids}
                        end;
                    {error,_}=Err -> ?PgU:try_transform_err(Err);
                    {'EXIT',Reason} ->
                        ?LOG('$error',"Connection pid ~tp found down: ~120tp",[Conn,Reason]),
                        {error,{service_unavailable,<<"Connection error">>}}
                end end),
            ?PgU:log_mks(lookup,[Mks1,Mks2,Mks3,Mks4]),
            ReadRes;
        {_,{error,timeout}} -> {error,{service_unavailable,<<"Connection pool is overloaded. Try later.">>}};
        {_,{error,_}=Err} -> ?PgU:try_transform_err(Err)
    end.

%%% ----------------------------------------------------------
apply_read(DbParams,ReadSql,SelectOpts,Meta,PropNames) ->
    ?LOG('$trace',"Read(coll) SQL: ~ts", [ReadSql]),
    ?LOG('$trace',"DbParams: ~120tp", [DbParams]),
    case timer:tc(fun() -> ?PgConnectionPool:get_connection(DbParams,5000) end) of
        {Mks1,{ok,Conn}} ->
            ?LOG('$trace',"Conn: ~120tp",[Conn]),
            {Mks2,PgRes} = timer:tc(fun() -> (catch ?PgDBA:query_db_tout(Conn,ReadSql,?ReadTimeout)) end),
            {Mks3,ok} = timer:tc(fun() -> ?PgConnectionPool:release_connection(DbParams,Conn) end),
            {Mks4,ReadRes} = timer:tc(fun() ->
                case PgRes of
                    {ok,Cols,Values}=Response ->
                        case [{Name,ColType} || {column,Name,ColType,_,_,_,_,_,_} <- Cols] of
                            [{<<"count_only">>,_}] ->
                                [{Count}|_]=Values,
                                Limit = case ?BU:get_by_key(<<"limit">>,SelectOpts,undefined) of undefined -> undefined; V -> ?BU:to_int(V,undefined) end,
                                Offset = ?BU:to_int(?BU:get_by_key(<<"offset">>,SelectOpts,0),0),
                                {ok,#{<<"count">> => erlang:min(?BU:to_int(Count)-Offset,Limit)}};
                            Cols1 ->
                                ColNames = lists:map(fun({Name,_Type}) -> Name end, Cols1),
                                AggrFields = maps:keys(maps:get(<<"aggr">>,SelectOpts,#{})),
                                GroupByFields = maps:keys(maps:get(<<"groupby">>,SelectOpts,#{})),
                                IsAggregation = length(AggrFields) + length(GroupByFields) > 0,
                                F = fun(ValueTuple) ->
                                        JSON = maps:from_list(lists:zip(ColNames,erlang:tuple_to_list(ValueTuple))),
                                        XPropsEx = [{<<"id">>,{<<"uuid">>,false,#{},0}} | maps:get(x_props,Meta)],
                                        case IsAggregation of
                                            false ->
                                                Entity = ?Format:decorate_entity(?Format:format_entity(JSON, XPropsEx, [db]), PropNames),
                                                maps:merge(JSON,Entity);
                                            true ->
                                                Entity = ?Format:decorate_entity(?Format:format_entity(maps:without(AggrFields,JSON), XPropsEx, [db]), PropNames),
                                                Ex = maps:merge(JSON,maps:map(fun(_,V) -> ?BU:to_int(V,V) end, maps:with(AggrFields,JSON))),
                                                Ex1 = maps:map(fun(K,V) -> retype_aggr_field(V,?BU:get_by_key(K,Cols1)) end, Ex),
                                                maps:merge(Ex1, Entity)
                                        end end,
                                Entities = lists:map(F, Values),
                                ?LOG('$trace',"Reply: ~120tp",[Response]),
                                {ok,Entities}
                        end;
                    {error,_}=Err -> ?PgU:try_transform_err(Err);
                    {'EXIT',Reason} ->
                        ?LOG('$error',"Connection pid ~tp found down: ~120tp",[Conn,Reason]),
                        {error,{service_unavailable,<<"Connection error">>}}
                end end),
            ?PgU:log_mks(read,[Mks1,Mks2,Mks3,Mks4]),
            ReadRes;
        {_,{error,timeout}} -> {error,{service_unavailable,<<"Connection pool is overloaded. Try later.">>}};
        {_,{error,_}=Err} -> ?PgU:try_transform_err(Err)
    end.

%%% ----------------------------------------------------------
apply_find(DbParams,FindSql,Meta,PropNames,FunOnError) ->
    ?LOG('$trace',"Find(entity) SQL: ~ts", [?BU:to_binary(FindSql)]),
    ?LOG('$trace',"DbParams: ~120tp", [DbParams]),
    % DB
    case timer:tc(fun() -> ?PgConnectionPool:get_connection(DbParams,5000) end) of
        {Mks1,{ok,Conn}} ->
            ?LOG('$trace',"Conn: ~120tp",[Conn]),
            {Mks2,PgRes} = timer:tc(fun() -> (catch ?PgDBA:query_db_tout(Conn,FindSql,?FindTimeout)) end),
            {Mks3,ok} = timer:tc(fun() -> ?PgConnectionPool:release_connection(DbParams,Conn) end),
            {Mks4,ReadRes} = timer:tc(fun() ->
                case PgRes of
                    {ok,Cols,Values} ->
                        Cols1 = [Name || {column,Name,_ColType,_,_,_,_,_,_} <- Cols],
                        F = fun(ValueTuple) ->
                                JSON = maps:from_list(lists:zip(Cols1,erlang:tuple_to_list(ValueTuple))),
                                XPropsEx = [{<<"id">>,{<<"uuid">>,false,#{},0}} | maps:get(x_props,Meta)],
                                _Entity = ?Format:decorate_entity(?Format:format_entity(JSON, XPropsEx, [db]),PropNames)
                            end,
                        case lists:map(F, Values) of
                            [] -> false;
                            [Entity] ->
                                ?LOG('$trace',"Reply: ~120tp",[Entity]),
                                {ok,Entity}
                        end;
                    {error,_}=Err -> FunOnError(?PgU:try_transform_err(Err));
                    {'EXIT',Reason} ->
                        ?LOG('$error',"Connection pid ~tp found down: ~120tp",[Conn,Reason]),
                        {error,{service_unavailable,<<"Connection error">>}}
                end end),
            ?PgU:log_mks(find,[Mks1,Mks2,Mks3,Mks4]),
            ReadRes;
        {_,{error,timeout}} -> {error,{service_unavailable,<<"Connection pool is overloaded. Try later.">>}};
        {_,{error,_}=Err} -> ?PgU:try_transform_err(Err)
    end.

%%% ----------------------------------------------------------
apply_delete(DbParams,DelSql) ->
    ?LOG('$trace',"Del SQL: ~ts", [?BU:to_binary(DelSql)]),
    ?LOG('$trace',"DbParams: ~120tp", [DbParams]),
    case timer:tc(fun() -> ?PgConnectionPool:get_connection(DbParams,5000) end) of
        {Mks1,{ok,Conn}} ->
            ?LOG('$trace',"Conn: ~120tp",[Conn]),
            {Mks2,Res} = timer:tc(fun() -> (catch ?PgDBA:query_db_tout(Conn,DelSql,?DeleteTimeout)) end),
            ?LOG('$trace',"Res: ~120tp",[Res]),
            {Mks3,ok} = timer:tc(fun() -> ?PgConnectionPool:release_connection(DbParams,Conn) end),
            ?PgU:log_mks(delete,Mks1,Mks2,Mks3),
            case Res of
                {ok,_Cnt} -> ok;
                {ok,_,_} -> ok;
                {error,_}=Err -> ?PgU:try_transform_err(Err);
                {'EXIT',Reason} ->
                    ?LOG('$error',"Connection pid ~tp found down: ~120tp",[Conn,Reason]),
                    {error,{service_unavailable,<<"Connection error">>}}
            end;
        {_,{error,timeout}} -> {error,{service_unavailable,<<"Connection pool is overloaded. Try later.">>}};
        {_,{error,_}=Err} -> ?PgU:try_transform_err(Err)
    end.

%%% ----------------------------------------------------------
apply_clear(DbParams,TrnSql) ->
    ?LOG('$trace',"Truncate SQL: ~ts", [?BU:to_binary(TrnSql)]),
    ?LOG('$trace',"DbParams: ~120tp", [DbParams]),
    case timer:tc(fun() -> ?PgConnectionPool:get_connection(DbParams,5000) end) of
        {Mks1,{ok,Conn}} ->
            ?LOG('$trace',"Conn: ~120tp",[Conn]),
            {Mks2,Res} = timer:tc(fun() -> (catch ?PgDBA:query_db_tout(Conn,TrnSql,?ClearTimeout)) end),
            ?LOG('$trace',"Res: ~120tp",[Res]),
            {Mks3,ok} = timer:tc(fun() -> ?PgConnectionPool:release_connection(DbParams,Conn) end),
            ?PgU:log_mks(clear,Mks1,Mks2,Mks3),
            case Res of
                {ok,_} -> ok;
                {ok,_,_} -> ok;
                {error,_}=Err -> ?PgU:try_transform_err(Err);
                {'EXIT',Reason} ->
                    ?LOG('$error',"Connection pid ~tp found down: ~120tp",[Conn,Reason]),
                    {error,{service_unavailable,<<"Connection error">>}}
            end;
        {_,{error,timeout}} -> {error,{service_unavailable,<<"Connection pool is overloaded. Try later.">>}};
        {_,{error,_}=Err} -> ?PgU:try_transform_err(Err)
    end.

%%% ----------------------------------------------------------
sql_put(Meta,Entity,PropNames,Tab) ->
    % WARNING: without unicode transform
    Id = maps:get(<<"id">>,Entity),
    Schema = ?PostgresqlSchema,
    FieldsList = lists:nthtail(1,put_fields(Meta,PropNames)),
    Fields = ?BU:to_list(?BU:join_binary(FieldsList,<<",\n\t">>)),
    ValuesList = lists:nthtail(1,put_values(Entity,Meta)),
    Values = ?BU:to_list(?BU:join_binary(ValuesList,<<",\n\t">>)),
    Update = ?BU:to_list(?BU:join_binary(lists:flatten([[<<N/binary," = ",(?BU:to_binary(V))/binary>>] || {N,V} <- lists:zip(FieldsList,ValuesList)]), ",\n\t")),
    ?BU:str("
INSERT INTO ~ts.\"~ts\" (
\t~ts,
\t~ts
)
VALUES (
\t'~ts'::uuid,
\t~ts
)
ON CONFLICT (id) DO UPDATE SET
\t~ts;
",[Schema,Tab,<<"id">>,Fields,?BU:to_list(Id),Values,Update]).

%%
put_fields(_Meta,PropNames) ->
    [<<"\"id\"">> | PropNames1] = [<<"\"",N/binary,"\"">> || N <- PropNames],
    [<<"id">>,<<"item_json_data">> | PropNames1].

%%
put_values(Entity,Meta) ->
    PropTuples = [{N,T,M} || {N,{T,M,_P,_Idx}} <- maps:get('x_props',Meta)],
    [
        % <<"id">>
        build_value(<<"uuid">>,false,maps:get(<<"id">>,Entity)),
        % <<"item_json_data">>
        case maps:get('x_item_json_data',Meta,false) of
            true -> build_value(<<"any">>,false,Entity);
            false -> build_value(<<"any">>,false,#{})
        end
        %
        % data fields
     | [build_value(Type,Multi,maps:get(Name,Entity,undefined)) || {Name,Type,Multi} <- PropTuples]].

%% @private
build_value(_,_,null) -> <<"NULL">>;
build_value(_,_,undefined) -> <<"NULL">>;
build_value(_,true,Value) -> build_jsonb(Value);
build_value(<<"uuid">>,_,Value) -> build_uuid(Value);
build_value(<<"string">>,_,Value) -> build_varchar(Value);
build_value(<<"integer">>,_,Value) -> build_integer(Value);
build_value(<<"increment">>,_,Value) -> build_integer(Value);
build_value(<<"long">>,_,Value) -> build_integer(Value);
build_value(<<"float">>,_,Value) -> build_float(Value);
build_value(<<"boolean">>,_,Value) -> build_boolean(Value);
build_value(<<"datetime">>,_,Value) -> build_datetime(Value);
build_value(<<"date">>,_,Value) -> build_datetime(Value);
build_value(<<"time">>,_,Value) -> build_datetime(Value);
build_value(<<"duration">>,_,Value) -> build_integer(Value);
build_value(<<"entity">>,_,Value) -> build_uuid(Value);
build_value(<<"any">>,_,Value) -> build_jsonb(Value);
build_value(<<"attachment">>,_,Value) -> build_jsonb(Value);
build_value(<<"enum">>,_,Value) -> build_varchar(Value).

%% @private
build_varchar(Value) -> <<"'",(binary:replace(Value,<<"'">>,<<"''">>,[global]))/binary,"'::varchar">>.
build_uuid(Value) -> <<"'",(binary:replace(Value,<<"'">>,<<"''">>,[global]))/binary,"'::uuid">>.
build_jsonb(Value) -> <<"'",(binary:replace(jsx:encode(Value),<<"'">>,<<"''">>,[global]))/binary,"'::jsonb">>.
build_integer(Value) -> ?BU:to_binary(Value).
build_float(Value) -> ?BU:to_binary(float_to_list(Value,[compact,{decimals,2}])).
build_boolean(Value) -> case ?BU:to_bool(Value) of true -> <<"true">>; false -> <<"false">> end.
build_datetime(Value) -> <<"'",(binary:replace(Value,<<"'">>,<<"''">>,[global]))/binary,"'::timestamp">>.

%%% ----------------------------------------------------------
sql_find(Id,PropNames,Tab) ->
    % SELECT ...
    Fields = ?BU:to_list(read_fields([],[],PropNames)),
    % FROM ...
    Schema = ?PostgresqlSchema,
    ?BU:str("
SELECT ~ts
FROM ~ts.\"~ts\"
WHERE id = '~ts'
LIMIT 1 OFFSET 0;",[Fields,Schema,Tab,?BU:to_list(Id)]).

%%% ----------------------------------------------------------
%%
read_fields_external(Meta,SelectOpts,PropNames) ->
    case ?BU:to_bool(?BU:get_by_key(<<"countonly">>,SelectOpts,false)) of
        true -> {<<"COUNT(id) as count_only">>,true};
        false ->
            MaxMask = ?BU:get_by_key(<<"max_mask">>,maps:get(opts,Meta),?DefaultMaxMask),
            Mask = ?BU:get_by_key(<<"mask">>,SelectOpts),
            {?BU:to_list(read_fields(Mask,MaxMask,PropNames)),false}
    end.

%% @private
read_fields(Mask,MaxMask,PropNames) when Mask==<<>> -> read_fields([],MaxMask,PropNames);
read_fields(Mask,MaxMask,PropNames) when is_binary(Mask) ->
    case re:run(Mask,<<"(?<x>[^ ,;\t]+)">>, [{capture,["x"],binary},global]) of
        {match,FldsInGrp} ->
            %io:format("DEBUG. FldsInGrp: ~120tp~n", [FldsInGrp]),
            Mask1 = lists:map(fun([Fld]) -> Fld end, FldsInGrp),
            read_fields(Mask1,MaxMask,PropNames);
        _ ->
            read_fields([],MaxMask,PropNames)
    end;
read_fields(Mask,MaxMask,PropNames) ->
    SelectMask = case {Mask,MaxMask} of
                     {[],[]} -> PropNames;
                     {_,[]} -> Mask--Mask--PropNames;
                     {[],_} -> MaxMask--MaxMask--PropNames;
                     {_,_} ->
                         MaxMask1 = [<<"id">> | MaxMask],
                         Mask1 = Mask--Mask--PropNames,
                         Mask1--Mask1--MaxMask1
                 end,
    ?BU:join_binary([<<"\"",Field/binary,"\"">> || Field <- SelectMask], <<", ">>).

%% ------------------------------------------------
read_fields_aggr(_Meta,SelectOpts,PropNames) ->
    AggrMap = maps:get(<<"aggr">>,SelectOpts,#{}),
    GroupByMap = maps:get(<<"groupby">>,SelectOpts,#{}),
    _AggrFieldNames = extract_aggr_fields(AggrMap,PropNames),
    AggrFields = build_aggr_fields(AggrMap),
    case {maps:keys(GroupByMap),maps:keys(AggrMap)} of
        {[],_} ->
            AggrFields = build_aggr_fields(AggrMap),
            {?BU:to_list(?BU:strbin("~ts", [AggrFields])), <<>>};
        {GroupByFieldNames,[]} ->
            SelectGroupByFieldsBin = build_groupby_select_fields(GroupByMap),
            GroupByFields = ?BU:strbin("~nGROUP BY ~ts",[?BU:to_list(read_fields(GroupByFieldNames,GroupByFieldNames,GroupByFieldNames))]),
            {?BU:to_list(?BU:strbin("~ts", [SelectGroupByFieldsBin])), ?BU:to_list(GroupByFields)};
        {GroupByFieldNames,_} ->
            AggrFields = build_aggr_fields(AggrMap),
            SelectGroupByFieldsBin = build_groupby_select_fields(GroupByMap),
            GroupByFields = ?BU:strbin("~nGROUP BY ~ts",[?BU:to_list(read_fields(GroupByFieldNames,GroupByFieldNames,GroupByFieldNames))]),
            {?BU:to_list(?BU:strbin("~ts, ~ts", [SelectGroupByFieldsBin, AggrFields])), ?BU:to_list(GroupByFields)}
    end.

%% @private
extract_aggr_fields(Expr,PropNames) ->
    F = fun F([<<"property">>,PropName|_], Acc) ->
                case lists:member(PropName,PropNames) of
                    true -> ordsets:add_element(PropName,Acc);
                    false -> throw({error,{invalid_params,?BU:strbin("Invalid aggr. Unknown property: '~ts'",[PropName])}})
                end;
            F([_|Rest], Acc) -> lists:foldl(fun(Elt, Acc1) -> F(Elt,Acc1) end, Acc, Rest);
            F(_, Acc) -> Acc
        end,
    maps:fold(fun(_K,[AggrFN|Args]=V,Acc) ->
                    case get_aggr_function(AggrFN,length(Args)) of
                        {ok,_} -> F(V,Acc);
                        false -> throw({error,{invalid_params,?BU:strbin("Invalid aggr. Unknown function: '~ts'/~p. Expected: ~ts",[AggrFN,length(Args),expected_aggr_functions()])}})
                    end end, [], Expr).

%% @private
aggr_functions() ->
    [{<<"count">>,1,[],<<"COUNT">>},
     {<<"sum">>,1,[],<<"SUM">>},
     {<<"min">>,1,[],<<"MIN">>},
     {<<"max">>,1,[],<<"MAX">>},
     {<<"avg">>,1,[],<<"AVG">>},
     {<<"and">>,1,[],<<"BOOL_AND">>},
     {<<"or">>,1,[],<<"BOOL_OR">>},
     {<<"join">>,2,[],<<"STRING_AGG">>}].

%% @private
expected_aggr_functions() ->
    ?BU:join_binary([<<"'",FN/binary,"'/",(?BU:to_binary(Arity))/binary>> || {FN,Arity,_,_} <- aggr_functions()], <<", ">>).

%% @private
get_aggr_function(AggrFunName,Arity) ->
    lists:foldl(fun({FN,Ar,_,_}=Descriptor,false) when FN==AggrFunName, Ar==Arity -> {ok,Descriptor};
                   (_,Acc) -> Acc
                end, false, aggr_functions()).

%% @private
build_groupby_select_fields(GroupBy) ->
    GroupFields = maps:fold(fun(K,K,Acc) when is_binary(K) -> [?BU:strbin("\"~ts\"",[K]) | Acc];
                               (K,Value,Acc) when is_binary(Value) -> [?BU:strbin("\"~ts\" as \"~ts\"",[Value,K]) | Acc];
                               (K,Expr,Acc) ->
                                   ExprBin = ?FilterPg:build_expression_string(Expr),
                                   [?BU:strbin("~ts AS \"~ts\"",[ExprBin,K]) | Acc]
                            end, [], GroupBy),
    ?BU:join_binary(GroupFields,<<", ">>).

%% @private
build_aggr_fields(Aggr) ->
    AggrFields = maps:fold(fun(K,[AggrFN|Args],Acc) ->
                                {ok,{_,_,_,PgFN}} = get_aggr_function(AggrFN,length(Args)),
                                ArgsBin = ?BU:join_binary([case ?FilterPg:build_expression_string(Arg) of
                                                               {error,_}=Err -> throw(Err);
                                                               Bin -> Bin
                                                           end  || Arg <- Args], <<", ">>),
                                [?BU:strbin("~ts(~ts) AS \"~ts\"",[PgFN,ArgsBin,K]) | Acc]
                            end, [], Aggr),
    ?BU:join_binary(AggrFields,<<", ">>).

%% return result fields of aggregation select
get_aggr_select_propnames(SelectOpts) ->
    AggrMap = maps:get(<<"aggr">>,SelectOpts,#{}),
    GroupByMap = maps:get(<<"groupby">>,SelectOpts,#{}),
    maps:keys(AggrMap) ++ maps:keys(GroupByMap).

%% ------------------------------------------------

lookup_where(Meta,LookupField,LookupValue) ->
    FieldType = case ?BU:get_by_key(LookupField,maps:get(x_props,Meta,#{}),un) of un -> un; Tuple -> element(1,Tuple) end,
    case FieldType of
        T when T==<<"integer">>; T==<<"increment">>; T==<<"long">> ->
            ?BU:str("\"~ts\" = ~p",[LookupField,?BU:to_int(LookupValue,-292815030349304753041)]);
        T when T==<<"float">> ->
            ?BU:str("\"~ts\" = ~p",[LookupField,?BU:to_float(LookupValue,-292815030349304753041)]);
        T when T==<<"boolean">> ->
            ?BU:str("\"~ts\" = ~p",[LookupField,?BU:to_bool(LookupValue)]);
        _ ->
            Content1 = binary:replace(?BU:to_binary(LookupValue),<<"'">>,<<"''">>,[global]),
            ?BU:str("\"~ts\" = '~ts'",[LookupField,Content1])
    end.

%%
read_order(Backwards,_Meta,SelectOpts,PropNames) ->
    case ?BU:get_by_key(<<"order">>,SelectOpts,[]) of
        [] -> <<>>;
        SortFields when is_list(SortFields) ->
            F = fun(Fld,Acc) when is_binary(Fld) ->
                        case lists:member(Fld,PropNames) of
                            true when Backwards -> [<<"\"",Fld/binary,"\" DESC">> | Acc];
                            true -> [<<"\"",Fld/binary,"\" ASC">> | Acc];
                            false -> Acc % TODO: split by dot, build json
                        end;
                   ({Fld,Dir},Acc) ->
                       case lists:member(Fld,PropNames) of
                           true ->
                               case Dir of
                                   <<"asc">> when Backwards -> [<<"\"",Fld/binary,"\" DESC">> | Acc];
                                   <<"desc">> when Backwards -> [<<"\"",Fld/binary,"\" ASC">> | Acc];
                                   <<"asc">> -> [<<"\"",Fld/binary,"\" ASC">> | Acc];
                                   <<"desc">> -> [<<"\"",Fld/binary,"\" DESC">> | Acc];
                                   _ -> Acc
                               end;
                           false -> Acc % TODO: split by dot, build json
                       end;
                   (Map,Acc) when is_map(Map) ->
                        case maps:keys(Map) of
                            [Fld] when is_binary(Fld) ->
                                case lists:member(Fld,PropNames) of
                                    true ->
                                        case maps:get(Fld,Map) of
                                            <<"asc">> when Backwards -> [<<"\"",Fld/binary,"\" DESC">> | Acc];
                                            <<"desc">> when Backwards -> [<<"\"",Fld/binary,"\" ASC">> | Acc];
                                            <<"asc">> -> [<<"\"",Fld/binary,"\" ASC">> | Acc];
                                            <<"desc">> -> [<<"\"",Fld/binary,"\" DESC">> | Acc];
                                            _ -> Acc
                                        end;
                                    false -> Acc % TODO: split by dot, build json
                                end;
                            _ -> Acc
                        end end,
            case lists:reverse(lists:foldl(F, [], SortFields)) of
                [] -> <<>>;
                Order -> <<"\nORDER BY ", (?BU:join_binary(Order, <<", ">>))/binary>>
            end;
        _ -> <<>>
    end.

%%
read_limit(Meta,SelectOpts) ->
    MaxLimit = ?ClassU:max_limit(Meta,SelectOpts),
    erlang:max(1,erlang:min(MaxLimit,?BU:to_int(?BU:get_by_key(<<"limit">>,SelectOpts,MaxLimit),MaxLimit))).
read_limit_default(Meta,SelectOpts) ->
    MaxLimit = ?ClassU:max_limit(Meta,SelectOpts),
    erlang:max(1,?BU:to_int(?BU:get_by_key(<<"limit">>,SelectOpts,MaxLimit))).

%%
read_offset(_Meta,SelectOpts) ->
    erlang:max(0,?BU:to_int(?BU:get_by_key(<<"offset">>,SelectOpts,0),0)).

%%% ----------------------------------------------------------
sql_delete(Id,Tab) ->
    Schema = ?PostgresqlSchema,
    ?BU:str("
DELETE FROM ~ts.\"~ts\"
WHERE id = '~ts';
",[Schema,Tab,?BU:to_list(Id)]).

%%% ----------------------------------------------------------
sql_truncate(Tab) ->
    Schema = ?PostgresqlSchema,
    ?BU:str("
TRUNCATE TABLE ~ts.\"~ts\";
",[Schema,Tab]).

%% ====================================================================
%% Internal functions
%% ====================================================================

retype_aggr_field(null,_) -> null;
retype_aggr_field(Value,PgType) ->
    try PgType of
        bool -> ?BU:to_bool(Value);
        T when T==int2; T==int4; T==int8 -> ?BU:to_int(Value);
        T when T==float4; T==float8 ->
            case catch ?BU:to_int(Value) of
                I when is_integer(I) -> I;
                _ -> ?BU:to_float(Value)
            end;
        T when T==timestamptz; T==timestamp -> ?BU:strdatetime3339(?BU:parse_datetime(Value));
        date -> ?BU:strdate(?BU:parse_datetime(Value));
        time -> ?BU:strtime(?BU:parse_time(Value));
        T when T==jsonb; T==json ->
            case catch jsx:decode(Value,[return_maps]) of
                {'EXIT',_} -> Value;
                JSON -> JSON
            end;
        {unknown_oid,_} when Value== <<"t">> -> true;
        {unknown_oid,_} when Value== <<"f">> -> false;
        {unknown_oid,_} ->
            case catch binary_to_integer(Value) of
                I when is_integer(I) -> I;
                _ ->
                    case catch binary_to_float(Value) of
                        N when is_number(N) -> N;
                        _ ->
                            case ?BU:parse_datetime(Value) of
                                {{1970,1,1},_,_} -> Value;
                                DT -> ?BU:strdatetime3339(DT)
                            end end end;
%%        {bytea, 17, 1001},
%%        {cidr, 650, 651},
%%        {daterange, 3912, 3913},
%%        {inet, 869, 1041},
%%        {int4range, 3904, 3905},
%%        {int8range, 3926, 3927},
%%        {interval, 1186, 1187},
%%        {macaddr, 829, 1040},
%%        {macaddr8, 774, 775},
%%        {point, 600, 1017},
%%        {time, 1083, 1183},
%%        {timetz, 1266, 1270},
%%        {tsrange, 3908, 3909},
%%        {tstzrange, 3910, 3911},
        _ -> Value
    catch
        _:_ -> Value
    end.

%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.03.2021
%%% @doc Storage-postgresql routines to
%%%         - build metadata for class table create/modify
%%%         - apply metadata to PostgreSQL server.

-module(dmlib_storage_postgresql_meta).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([ensure_table/3,
         get_table_meta/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------
%% Make table in PostgreDB for domain-model-class
%% -------------------------
ensure_table(DbParams,_ClassName,Meta) ->
    TableMeta = table_meta(Meta),
    ?PgStructure:ensure_tables(DbParams,[TableMeta]).

%% -------------------------
%% Return data-model class table metadata (for pg structure module)
%% -------------------------
get_table_meta(Meta) ->
    table_meta(Meta).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
table_meta(Item) ->
    ClassName = maps:get(classname,Item),
    ItemOpts = maps:get(opts,Item),
    PreviousClassName = maps:get(<<"previous_classname">>,ItemOpts,<<>>),
    % properties
    Props = lists:map(fun({N,{T,M,Prop,Idx}}) -> {N,T,M,Prop,Idx} end, maps:get(x_props,Item)),
    Props1 = lists:sort(fun({_,_,_,_,Idx1},{_,_,_,_,Idx2}) -> Idx1=<Idx2 end, Props),
    FieldInfo = [#{<<"name">> => ?PgU:field_name(N),
                   <<"type">> => ?PgU:map_db_field_type(T,M),
                   <<"default">> => case maps:get(<<"default">>, Prop, null) of null -> null; Def -> ?PgCrudU:build_value(T,M,Def) end,
                   <<"required">> => maps:get(<<"required">>, Prop, false)}
                   || {N,T,M,Prop,_Idx} <- Props1],
    FieldInfo1 = [#{<<"name">> => <<"id">>,
                    <<"type">> => <<"uuid">>,
                    <<"required">> => true},
                    #{<<"name">> => <<"item_json_data">>,
                      <<"type">> => <<"jsonb">>,
                      <<"required">> => true}
                    | FieldInfo],
    % index fields
    IndexFields = maps:get(<<"lookup_properties">>, ItemOpts, []) -- [<<"id">>],
    % build meta
    TableName = ?PgU:tablename(ClassName),
    PreviousTablenames = previous_tablenames(TableName,ClassName,PreviousClassName),
    #{<<"schema">> => ?PostgresqlSchema,
      <<"tablename">> => TableName,
      <<"previous_tablenames">> => PreviousTablenames, % for auto rename routine
      <<"classname">> => ClassName, % for meta table
      <<"fields">> => FieldInfo1,
      <<"index_fields">> => IndexFields,
      <<"opts">> => #{}}.

%% @private
previous_tablenames(TableName,ClassName,PreviousClassName) ->
    List1 = [?PgU:tablename_prev(ClassName)],
    List2 = case PreviousClassName of
                <<>> -> List1;
                _ -> List1 ++ [?PgU:tablename(PreviousClassName), ?PgU:tablename_prev(PreviousClassName)]
            end,
    lists:filter(fun(T) -> T/=TableName end, List2).
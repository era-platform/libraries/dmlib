%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 20.04.2021
%%% @doc

-module(dmlib_storage_clickhouse_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([parse_dt/1, parse_dt/2]).

-export([db_name/1,
         field_name/1,
         tabname/2,tabname_kafka/1,tabname_view/1,tabname_distributed/1]).

-export([map_db_field_type/2,
         map_db_field_queuetype/2]).

-export([ch_format/0]).
-export([parse_result/1]).
-export([transform_result/1]).
-export([log_mks/4, log_mks/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

parse_dt(Value) -> parse_dt(Value,true).
parse_dt(Value,FallbackToNow) ->
    try ?BU:parse_datetime(Value) of
        {{1970,1,1},{0,0,0},0}=DT ->
            case FallbackToNow of
                true -> ?BU:ticktoutc(?BU:timestamp());
                _ -> DT
            end;
        DT -> DT
    catch _:_ -> ?BU:ticktoutc(?BU:timestamp())
    end.

%% ---------
db_name(Domain) -> ?BU:to_binary(Domain).

%% ---------
tabname(CN,false=_UsePartitions) -> CN;
tabname(CN,true=_UsePartitions) -> ?BU:strbin("~ts$",[CN]).
tabname_kafka(CN) -> ?BU:strbin("~ts$_queue",[CN]).
tabname_view(CN) -> ?BU:strbin("~ts$_view",[CN]).
tabname_distributed(CN) -> ?BU:strbin("~ts$_distributed",[CN]).

%% ---------
field_name(N) -> ?BU:to_binary(N).

%% ---------
map_db_field_type(Type,IsMulti) -> db_type(Type,IsMulti).
map_db_field_queuetype(Type,IsMulti) -> db_type_queue(Type,IsMulti).

%% @private
db_type_queue(<<"datetime">>,false) -> <<"String">>;
db_type_queue(T,IsM) -> db_type(T,IsM).

%% @private
db_type(<<"uuid">>,false) -> <<"UUID">>;
db_type(<<"string">>,false) -> <<"String">>;
db_type(<<"integer">>,false) -> <<"Int32">>;
db_type(<<"increment">>,false) -> <<"Int64">>;
db_type(<<"long">>,false) -> <<"Int64">>;
db_type(<<"float">>,false) -> <<"Float32">>;
db_type(<<"boolean">>,false) -> <<"UInt8">>;
db_type(<<"datetime">>,false) -> <<"DateTime">>;
db_type(<<"date">>,false) -> <<"Date">>;
db_type(<<"time">>,false) -> <<"String">>;
db_type(<<"duration">>,false) -> <<"UInt32">>;
db_type(<<"entity">>,false) -> <<"UUID">>;
db_type(<<"any">>,false) -> <<"String">>;
db_type(<<"attachment">>,false) -> <<"String">>;
db_type(<<"enum">>,false) -> <<"String">>; % TODO ICP: to integer
db_type(_,true) -> <<"String">>.

%% -----------------------------------
ch_format() -> "JSONCompactEachRowWithNamesAndTypes".

parse_result(Body) ->
    Rows = binary:split(string:trim(?BU:to_binary(Body),both,[$[,$],$\n]),<<"]\n[">>,[global]),
    [Columns,Types|Values] = [jsx:decode(<<"[",Row/binary,"]">>,[return_maps]) || Row <- Rows],
    {ok,Columns,Types,Values}.

%% -----------------------------------
transform_result({ok,Body}) -> {ok,?BU:to_binary(Body)};
transform_result({error,{Code,Reason}}=Err) when is_list(Reason) ->
    try {error,{Code,?BU:to_binary(Reason)}}
    catch _:_ -> ?LOG('$error',"Clickhouse error: ~160tp",[Err]), {error,{internal_error,<<"Clickhouse request error">>}}
    end;
transform_result({error,Reason}=Err) when is_list(Reason) ->
    try {error,?BU:to_binary(Reason)}
    catch _:_ -> ?LOG('$error',"Clickhouse error: ~160tp",[Err]), {error,{internal_error,<<"Clickhouse request error">>}}
    end;
transform_result(Other) -> Other.

%% ------------------------------------------------
log_mks(Operation,Mks1,Mks2,Mks3) ->
    case Mks1+Mks2+Mks3 of
        T when T > 1000000 -> ?LOG('$warning',"DUR. CH ~p ms on ~ts (~p + ~p + ~p)", [T div 1000, Operation, Mks1 div 1000, Mks2 div 1000, Mks3 div 1000]);
        T when T > 100000 -> ?LOG('$info',"DUR. CH ~p ms on ~ts (~p + ~p + ~p)", [T div 1000, Operation, Mks1 div 1000, Mks2 div 1000, Mks3 div 1000]);
        _ -> ok
    end.

log_mks(Operation,MksList) when is_list(MksList) ->
    case lists:sum(MksList) of
        T when T > 1000000 ->
            Parts = ?BU:join_binary(lists:map(fun(Mks) -> ?BU:to_binary(Mks div 1000) end, MksList), <<" + ">>),
            ?LOG('$warning',"DUR. CH ~p ms on ~ts (~ts)", [T div 1000, Operation, Parts]);
        T when T > 100000 ->
            Parts = ?BU:join_binary(lists:map(fun(Mks) -> ?BU:to_binary(Mks div 1000) end, MksList), <<" + ">>),
            ?LOG('$info',"DUR. CH ~p ms on ~ts (~ts)", [T div 1000, Operation, Parts]);
        _ -> ok
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================
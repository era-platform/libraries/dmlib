%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 20.04.2021
%%% @doc

-module(dmlib_storage_clickhouse_http).
-author(['Evgeniy Grebenyuk <llceceron@gmail.com>','Peter Bukashin <tbotc@yandex.ru>']).

-export([connect_db/1, % on start
         query_db_tout/3, % on crud
         query_db_tout/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(ConnectTimeout, 1000).
-define(StoreErrorTimeout, 3000).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------------------
-spec connect_db(DbParamsL::[map()]) -> {ok,Props::map()} | {error,Reason::term()}.
%% ------------------------------
connect_db([DbParams|Rest]) ->
    case do_connect_db(DbParams) of
        {ok,Props} -> {ok,Props};
        {error,_}=Err when Rest==[] -> Err;
        {error,_} -> connect_db(Rest)
    end.

%% @private
do_connect_db(DbParams) ->
    UrlProfile = url_profile(DbParams),
    Key = {clickhouse_http,connect,url(UrlProfile)},
    Fun = fun() -> do_connect_db_1(UrlProfile,DbParams) end,
    ?BLstore:lazy_t(Key,Fun,{5000,3000,1500},{true,true}).

%% @private
do_connect_db_1(UrlProfile,DbParams) ->
    inets:start(),
    case inets:start(httpc, [{profile, profile(UrlProfile)}]) of
        {ok,Pid} -> on_started(Pid,DbParams,UrlProfile);
        {error,{already_started,Pid}} -> on_started(Pid,DbParams,UrlProfile);
        {error,_}=Err -> Err
    end.

%% @private
on_started(Pid,DbParams,UrlProfile) ->
    Options = [{max_sessions,1000},
               {keep_alive_timeout,5000}],
    httpc:set_options(Options,profile(UrlProfile)),
    case check_db(UrlProfile) of
        ok ->
            {ok, #{url_profile => UrlProfile, dbparams => DbParams}};
        {error,_}=Err ->
            inets:stop(httpc, Pid),
            Err
    end.

%% @private
check_db(UrlProfile) ->
    case query_db_tout(UrlProfile,<<"SELECT version();">>,20000) of
        {ok,_Body} -> ok;
        {error,_}=Err -> Err
    end.

%% ------------------------------
-spec query_db_tout(UrlProfile::{Url::string(),Profile::atom()}, Data::binary(), TOut::integer())
      -> {ok,Data::binary()} | {error,{ErrCode::integer(),Reason::binary()}} | {error,Reason::binary()}.
%% ------------------------------
query_db_tout(UrlProfile,Data,TOut) when is_binary(Data) ->
    do_query_db_tout(UrlProfile,"",Data,TOut).

%% ------------------------------
-spec query_db_tout(UrlProfile::{Url::string(),Profile::atom()}, Query::binary()|string(), Data::binary(), TOut::integer())
      -> {ok,Data::binary()} | {error,{ErrCode::integer(),Reason::binary()}} | {error,Reason::binary()}.
%% ------------------------------
query_db_tout(UrlProfile,Query,Data,TOut) when is_binary(Data) ->
    do_query_db_tout(UrlProfile,Query,Data,TOut).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---
do_query_db_tout(UrlProfile,Query,Data,TOut) when is_binary(Data) ->
    Url1 = case ?BU:to_unicode_list(Query) of
               "" -> ?BU:to_unicode_list(url(UrlProfile));
               Qr -> ?BU:to_unicode_list(?BU:strbin("~ts&query=~ts",[url(UrlProfile),Qr]))
           end,
    Headers = [{"te","trailers"},{"X-Clickhouse-Format",?ClickHouseUtils:ch_format()}],
    HttpOpts = [{timeout, TOut},{connect_timeout, ?ConnectTimeout}],
    case httpc:request(post, {Url1, Headers, "text/plain", Data}, HttpOpts, [], profile(UrlProfile)) of
        {ok, {{_,Code,_}, _, Body}} when Code>=200,Code=<299 -> {ok, Body};
        {ok, {{_,Code,_}, _, Body}} -> {error,{Code,Body}};
        {error, _Reason}=Err -> Err
    end.

%% @private
url_profile(DbParams) ->
    % url
    [Host,User,Pwd0] = ?BU:maps_get([host,login,password],DbParams),
    % decrypt pwd (dc keep endrypted in storage)
    Pwd = case ?CFG:decrypt_function() of
              Fun when is_function(Fun,1) -> Fun(Pwd0);
              _ -> Pwd0
          end,
    [Scheme,Port,_Db] = ?BU:maps_get_default([{scheme,<<"http">>},{port,8123},{database,<<>>}],DbParams),
    Url = ?BU:str("~ts://~ts:~p/?user=~ts&password=~ts&max_execution_time=~p", [Scheme,Host,?BU:to_int(Port),User,Pwd,10]),
    % profile
    BaseUrl = re:replace(Url,<<"([^/]+//[^/]+).*">>,<<"\\1">>,[{return,binary}]),
    InitialDomain = maps:get(initial_domain,DbParams),
    CN = maps:get(classname,DbParams),
    Profile = ?BU:to_atom_new(?BU:str("clickhouse_~ts/~ts/~ts",[BaseUrl,InitialDomain,CN])),
    % complex
    {Url,Profile}.

%% @private
url({Url,_Profile}) -> Url;
url(Url) -> Url.

%% @private
profile({_Url,Profile}) -> Profile;
profile(Url) when is_list(Url) ->
    %?BU:to_atom_new(?BU:strbin("clickhouse_~ts",[Url]));
    %?BU:to_atom_new(?BU:strbin("clickhouse_~p",[erlang:phash2(Url)]));
    ?BU:to_atom_new(?BU:strbin("clickhouse_~ts",[re:replace(Url,<<"([^/]+//[^/]+).*">>,<<"\\1">>,[{return,binary}])])).



%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.04.2021
%%% @doc

-module(dmlib_storage_clickhouse_crud).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([lookup/4,
         find/5,
         read/4,
         create/5,
         replace/5,
         update/5,
         delete/4,
         clear/2,
         optimize/2,
         read_partitions/2,
         drop_partition/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(UpdateTimeout, 20000).
-define(ReadTimeout, 60000).

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------------------------
%% Execute lookup select request to clickhouse db and return entity
%% TODO: use MaterializedView tables (LookupKey -> Id, PartitionProperty)
%% -----------------------------------------
-spec lookup(Meta::map(),Content::binary(),ChProps::map(),PropNames::[binary()]) -> {ok,[Id::binary()]} | {error,Reason::term()}.
%% -----------------------------------------
lookup(_Meta,_Content,_ChProps,_PropNames) ->
    {error,{405,<<"Method not allowed">>}}.

%% -----------------------------------------
%% Execute select request to clickhouse db and return entity
%% -----------------------------------------
-spec find(Meta::map(),Part::{term()}|{undefined},Id::binary(),ChProps::map(),PropNames::[binary()]) -> {ok,Entity::map()} | {error,Reason::term()} | false.
%% -----------------------------------------
find(Meta,{PartDt},Id,ChProps,PropNames) ->
    DtFilter = parse_dt_filter(PartDt),
    PartPropName = maps:get(<<"partition_property">>,maps:get(opts,Meta)),
    FindSql = sql_find(Meta,DtFilter,Id,PartPropName,PropNames,ChProps),
    apply_find(ChProps,FindSql,Meta,PropNames,fun(Err) -> Err end).

%% -----------------------------------------
%% Execute select request to clickhouse db and return entity
%% -----------------------------------------
-spec read(Meta::map(),SelectOpts::map(),ChProps::map(),PropNames::[binary()]) -> {ok,[Entity::map()]} | {error,Reason::term()}.
%% -----------------------------------------
read(Meta,SelectOpts,ChProps,PropNames) ->
    AggrMap = ?BU:get_by_key(<<"aggr">>,SelectOpts,#{}),
    GroupByMap = ?BU:get_by_key(<<"groupby">>,SelectOpts,#{}),
    case maps:size(AggrMap) + maps:size(GroupByMap) == 0 of
        true ->
            {Order0,FirstPartProp} = read_order(false,true,Meta,SelectOpts,PropNames),
            Order = ?BU:to_list(Order0),
            case narrow_down_interval(FirstPartProp,Meta,SelectOpts,ChProps,PropNames) of
                {ok,SelectOpts1} ->
                    {ok,ReadSql,_,Attrs} = sql_read_items(Meta,SelectOpts1,Order,PropNames,ChProps),
                    apply_read_items(ChProps,ReadSql,SelectOpts,Meta,PropNames,Attrs);
                reply_empty ->
                    ?LOG('$trace',"Reply (offset exceeded): []",[]),
                    {ok,[]};
                {error,_}=Err -> Err
            end;
        false ->
            ReadSql = sql_read_aggr(Meta,SelectOpts,PropNames,ChProps),
            apply_read_aggr(ChProps,ReadSql,SelectOpts,Meta,PropNames)
    end.

%% -----------------------------------------
%% Execute insert/update request to clickhouse db
%% Means create entity (INSERT operation)
%% Applied only over KAFKA to use bulks
%% -----------------------------------------
-spec create(Meta::map(),Dummy::term(),Entity::map(),ChProps::map(),PropNames::[binary()]) -> ok | {error,Reason::term()}.
%% -----------------------------------------
create(_Meta,_dummy,_Entity,_ChProps,_PropNames) ->
    {error,{405,<<"Method not allowed">>}}.

%% -----------------------------------------
%% Execute insert/update request to clickhouse db
%% Means replace entity (UPDATE operation)
%% Applied only over KAFKA to use bulks
%% -----------------------------------------
-spec replace(Meta::map(),Dummy::term(),Entity::map(),ChProps::map(),PropNames::[binary()]) -> ok | {error,Reason::term()}.
%% -----------------------------------------
replace(_Meta,_dummy,_Entity,_ChProps,_PropNames) ->
    {error,{405,<<"Method not allowed">>}}.

%% -----------------------------------------
%% Execute insert/update request to clickhouse db
%% Means merge/update entity (UPDATE operation)
%% Applied only over KAFKA to use bulks
%% -----------------------------------------
-spec update(Meta::map(),Dummy::term(),Entity::map(),ChProps::map(),PropNames::[binary()]) -> ok | {error,Reason::term()}.
%% -----------------------------------------
update(_Meta,_dummy,_Entity,_ChProps,_PropNames) ->
    {error,{405,<<"Method not allowed">>}}.

%% -----------------------------------------
%% Execute delete request to clickhouse db
%% Means lightweight delete entity from Replicated MergeTree (DELETE operation with async appliance)
%% -----------------------------------------
-spec delete(Meta::map(),Dummy::term(),Id::binary(),ChProps::map()) -> ok | {error,Reason::term()}.
%% -----------------------------------------
delete(Meta,{PartDt},Id,ChProps) ->
    %{error,{405,<<"Method not allowed">>}};
    DtFilter = parse_dt_filter(PartDt),
    PartPropName = maps:get(<<"partition_property">>,maps:get(opts,Meta)),
    DelSql = sql_delete(DtFilter,Id,PartPropName,ChProps),
    apply_no_content("Delete(entity)",ChProps,DelSql,fun(Err) -> Err end).

%% -----------------------------------------
%% Execute clear request to clickhouse db
%% Means truncate table (TRUNCATE operation)
%% -----------------------------------------
-spec clear(Meta::map(),ChProps::map()|undefined) -> ok | {error,Reason::term()}.
%% -----------------------------------------
clear(_Meta,ChProps) ->
    %{error,{405,<<"Method not allowed">>}};
    ClearSql = sql_truncate(ChProps),
    apply_no_content("Truncate(coll)",ChProps,ClearSql,fun(Err) -> Err end).

%% -----------------------------------------
%% Execute optimize request to clickhouse db
%% Means optimize table (OPTIMIZE operation, apply async late requests)
%% -----------------------------------------
-spec optimize(Meta::map(),ChProps::map()|undefined) -> ok | {error,Reason::term()}.
%% -----------------------------------------
optimize(_Meta,ChProps) ->
    %{error,{405,<<"Method not allowed">>}}.
    ClearSql = sql_optimize(ChProps),
    apply_no_content("Optimize(coll)",ChProps,ClearSql,fun(Err) -> Err end).

%% -----------------------------------------
%% Execute drop partition request to clickhouse db
%% Means lightweight drop partition table (DROP PARTITION)
%% -----------------------------------------
-spec read_partitions(Meta::map(),ChProps::map()|undefined) -> {ok,[map()]} | {error,Reason::term()}.
%% -----------------------------------------
read_partitions(_Meta,ChProps) ->
    %{error,{405,<<"Method not allowed">>}}.
    Sql = sql_read_partitions(ChProps),
    apply_content("ReadPartitions(tab)",ChProps,Sql,fun(Err) -> Err end).

%% -----------------------------------------
%% Execute drop partition request to clickhouse db
%% Means lightweight drop partition table (DROP PARTITION)
%% -----------------------------------------
-spec drop_partition(Meta::map(),Partition::binary(),ChProps::map()|undefined) -> ok | {error,Reason::term()}.
%% -----------------------------------------
drop_partition(_Meta,Partition,ChProps) ->
    %{error,{405,<<"Method not allowed">>}}.
    Sql = sql_drop_partition(Partition,ChProps),
    apply_no_content("DropPartition(tab)",ChProps,Sql,fun(Err) -> Err end).

%% ====================================================================
%% Internal functions
%% Routines
%% ====================================================================

-spec parse_dt_filter(PartitionPathDt :: undefined | binary()) -> {ExactDT::binary()} | {FromDT::binary(),ToDT::binary()}.
parse_dt_filter(PartitionPathDt) ->
    FunLastDay = fun() ->
                    NowDT = calendar:universal_time(),
                    NowGS = calendar:datetime_to_gregorian_seconds(NowDT),
                    FromDT = calendar:gregorian_seconds_to_datetime(NowGS - 12 * 3600),
                    ToDT = calendar:gregorian_seconds_to_datetime(NowGS + 2),
                    {?BU:strdatetime(FromDT), ?BU:strdatetime(ToDT)}
                 end,
    case ?ClickHouseUtils:parse_dt(PartitionPathDt) of % could be undefined for non partitioned table
        _ when PartitionPathDt==undefined ->
            % search in last day (by primary index)
            FunLastDay();
        {Dt,{0,0,0},0} ->
            % search in selected day (by primary index)
            GS = calendar:datetime_to_gregorian_seconds({Dt,{0,0,0}}),
            FromDT = calendar:gregorian_seconds_to_datetime(GS),
            ToDT = calendar:gregorian_seconds_to_datetime(GS + 24 * 3600),
            {?BU:strdatetime(FromDT), ?BU:strdatetime(ToDT)};
        {Dt,Tm,_} ->
            % search directly by date (by primary index)
            {?BU:strdatetime({Dt,Tm})};
        _ ->
            % search in last day (by primary index)
            FunLastDay()
    end.

%% ====================================================================
%% Internal functions
%% Find / Read
%% ====================================================================

%% ----------------------------------------------
%% FIND
%% Use filter by dt. If undefined then search in last day only
%% ----------------------------------------------
%% by period of dates
sql_find(_Meta,{DtFrom,DtTo},Id,PartPropName,PropNames,ChProps) ->
    DbName = maps:get(database,ChProps),
    Tab = maps:get(tablename,ChProps),
    Final = <<>>, % <<"FINAL">>, % SETTINGS max_final_threads = 1,2,3,...
    Fields = ?BU:to_list(read_fields([],[],PropNames)),
    % FROM ...
    ?BU:strbin("
SELECT ~ts
FROM \"~ts\".\"~ts\" ~ts
WHERE (\"~ts\" >= '~ts') AND (\"~ts\" < '~ts') AND (id = '~ts');",
        [Fields,DbName,Tab,Final,PartPropName,DtFrom,PartPropName,DtTo,Id]);
%% by direct date
sql_find(_Meta,{DtDirect},Id,PartPropName,PropNames,ChProps) ->
    DbName = maps:get(database,ChProps),
    Tab = maps:get(tablename,ChProps),
    Final = <<>>, % <<"FINAL">>, % SETTINGS max_final_threads = 1,2,3,...
    Fields = ?BU:to_list(read_fields([],[],PropNames)),
    % FROM ...
    ?BU:strbin("
SELECT ~ts
FROM \"~ts\".\"~ts\" ~ts
WHERE (\"~ts\" = '~ts') AND (id = '~ts');",
        [Fields,DbName,Tab,Final,PartPropName,DtDirect,Id]).

%% ----------------------------------------------
%% SELECT ITEMS
%% ----------------------------------------------
-spec sql_read_items(Meta::map(), SelectOpts::map(), Order::binary(), PropNames::[binary()], ChProps::map())
      -> {ok,Sql::binary(),CountOnly::false,Attrs::{Mask::[binary()],MaxMask::[binary()],Offset::integer(),Limit::integer(),ExpandLeading::integer()}}
        | {ok,Sql::binary(),CountOnly::true,Attrs::undefined}.
sql_read_items(Meta,SelectOpts,Order,PropNames,ChProps) ->
    % WARNING: without unicode transform
    % FROM ...
    DbName = maps:get(database,ChProps),
    TableName = maps:get(tablename,ChProps),
    Final = <<>>, % <<"FINAL">>, % SETTINGS max_final_threads = 1,2,3,...
    % WHERE ...
    Where = case read_filter(Meta,SelectOpts,PropNames) of
                {Where0,_,_} -> ?BU:to_list(Where0);
                Where0 -> ?BU:to_list(Where0)
            end,
    % ORDER BY ...
    %{Order,_} = ?BU:to_list(read_order(false,true,Meta,SelectOpts,PropNames)),
    % SELECT ...
    case read_fields_external(Meta,SelectOpts,PropNames) of
        {Fields,{false=CountOnly,Mask,MaxMask}} ->
            % select set
            % LIMIT ...
            Offset = read_offset(Meta,SelectOpts),
            Limit = read_limit(Meta,SelectOpts),
            % to provide correct merge on paged requests:
            Offset1 = case Offset > 10 of
                          true -> Offset-10;
                          false -> 0
                      end,
            Limit1 = Limit+10,
            %
            Sql = ?BU:strbin("
SELECT ~ts
FROM \"~ts\".\"~ts\" ~ts ~ts ~ts
LIMIT ~p OFFSET ~p;",[Fields,DbName,TableName,Final,Where,Order,Limit1,Offset1]),
            {ok,Sql,CountOnly,{Mask,MaxMask,Offset,Limit,Offset-Offset1}};
        {Fields,true=CountOnly} ->
            % select count_only
            Sql = ?BU:strbin("
SELECT ~ts
FROM \"~ts\".\"~ts\" ~ts ~ts;",[Fields,DbName,TableName,Final,Where]),
            {ok,Sql,CountOnly,undefined}
    end.

%% ----------------------------------------------
%% SELECT AGGREGATE
%% ----------------------------------------------
sql_read_aggr(Meta,SelectOpts,PropNames,ChProps) ->
    % WARNING: without unicode transform
    % SELECT ...
    % GROUP BY ...
    {Fields,GroupBy} = read_fields_aggr(Meta,SelectOpts,PropNames),
    % FROM ...
    DbName = maps:get(database,ChProps),
    TableName = maps:get(tablename,ChProps),
    Final = <<>>, % <<"FINAL">>, % SETTINGS max_final_threads = 1,2,3,...
    % WHERE ...
    Where = case read_filter(Meta,SelectOpts,PropNames) of
                {Where0,_,_} -> ?BU:to_list(Where0);
                Where0 -> ?BU:to_list(Where0)
            end,
    % HAVING
    Having = <<>>,
    % ORDER BY ...
    {Order0,_} = read_order(false,false,Meta,SelectOpts,get_aggr_select_propnames(SelectOpts)),
    Order = ?BU:to_list(Order0),
    % LIMIT ...
    Limit = read_limit_default(Meta,SelectOpts),
    Offset = read_offset(Meta,SelectOpts),
    %
    ?BU:strbin("
SELECT ~ts
FROM \"~ts\".\"~ts\" ~ts ~ts ~ts ~ts ~ts
LIMIT ~p OFFSET ~p;",[Fields,DbName,TableName,Final,Where,GroupBy,Having,Order,Limit,Offset]).

%% ---------------------------------------------
read_fields_external(Meta,SelectOpts,PropNames) ->
    case ?BU:to_bool(?BU:get_by_key(<<"countonly">>,SelectOpts,false)) of
        true -> {<<"COUNT() as count_only">>,true};
        false ->
            Mask = ?BU:get_by_key(<<"mask">>,SelectOpts),
            MaxMask = ?BU:get_by_key(<<"max_mask">>,maps:get(opts,Meta),?DefaultMaxMask),
            SelectMask = selected_mask(Mask,MaxMask,PropNames),
            case lists:member(<<"id">>,SelectMask) of
                true -> {?BU:to_list(read_fields(SelectMask)),{false,Mask,MaxMask}};
                false -> {?BU:to_list(read_fields([<<"id">>|SelectMask])),{false,Mask,MaxMask}}
            end
    end.

%%
read_fields(Mask,MaxMask,PropNames) when Mask==<<>> -> read_fields([],MaxMask,PropNames);
read_fields(Mask,MaxMask,PropNames) when is_binary(Mask) ->
    case re:run(Mask,<<"(?<x>[^ ,;\t]+)">>, [{capture,["x"],binary},global]) of
        {match,FldsInGrp} ->
            %io:format("DEBUG. FldsInGrp: ~120tp~n", [FldsInGrp]),
            Mask1 = lists:map(fun([Fld]) -> Fld end, FldsInGrp),
            read_fields(Mask1,MaxMask,PropNames);
        _ ->
            read_fields([],MaxMask,PropNames)
    end;
read_fields(Mask,MaxMask,PropNames) ->
    SelectMask = selected_mask(Mask,MaxMask,PropNames),
    read_fields(SelectMask).
read_fields(SelectMask) ->
    ?BU:join_binary([<<"\"",Field/binary,"\"">> || Field <- SelectMask], <<", ">>).

%% @private
selected_mask(Mask,MaxMask,PropNames) ->
    case {Mask,MaxMask} of
        {[],[]} -> PropNames;
        {_,[]} -> Mask--Mask--PropNames;
        {[],_} -> MaxMask--MaxMask--PropNames;
        {_,_} ->
            MaxMask1 = [<<"id">> | MaxMask],
            Mask1 = Mask--Mask--PropNames,
            Mask1--Mask1--MaxMask1
    end.

%% ------------------------------------------------
read_fields_aggr(_Meta,SelectOpts,PropNames) ->
    AggrMap = maps:get(<<"aggr">>,SelectOpts,#{}),
    GroupByMap = maps:get(<<"groupby">>,SelectOpts,#{}),
    _AggrFieldNames = extract_aggr_fields(AggrMap,PropNames),
    case {maps:keys(GroupByMap),maps:keys(AggrMap)} of
        {[],_} ->
            AggrFields = build_aggr_fields(AggrMap),
            {?BU:strbin("~ts", [AggrFields]), <<>>};
        {GroupByFieldNames,[]} ->
            SelectGroupByFieldsBin = build_groupby_select_fields(GroupByMap),
            GroupByFields = ?BU:strbin("~nGROUP BY ~ts",[?BU:to_list(read_fields(GroupByFieldNames,GroupByFieldNames,GroupByFieldNames))]),
            {?BU:strbin("~ts", [SelectGroupByFieldsBin]), GroupByFields};
        {GroupByFieldNames,_} ->
            AggrFields = build_aggr_fields(AggrMap),
            SelectGroupByFieldsBin = build_groupby_select_fields(GroupByMap),
            GroupByFields = ?BU:strbin("~nGROUP BY ~ts",[?BU:to_list(read_fields(GroupByFieldNames,GroupByFieldNames,GroupByFieldNames))]),
            {?BU:strbin("~ts, ~ts", [SelectGroupByFieldsBin, AggrFields]), GroupByFields}
    end.

%% @private
extract_aggr_fields(Expr,PropNames) ->
    F = fun F([<<"property">>,PropName|_], Acc) ->
                case lists:member(PropName,PropNames) of
                    true -> ordsets:add_element(PropName,Acc);
                    false -> throw({error,{invalid_params,?BU:strbin("Invalid aggr. Unknown property: '~ts'",[PropName])}})
                end;
            F([_|Rest], Acc) -> lists:foldl(fun(Elt, Acc1) -> F(Elt,Acc1) end, Acc, Rest);
            F(_, Acc) -> Acc
        end,
    maps:fold(fun(_K,[AggrFN|Args]=V,Acc) ->
                    case get_aggr_function(AggrFN,length(Args)) of
                        {ok,_} -> F(V,Acc);
                        false -> throw({error,{invalid_params,?BU:strbin("Invalid aggr. Unknown function: '~ts'/~p. Expected: ~ts",[AggrFN,length(Args),expected_aggr_functions()])}})
                    end end, [], Expr).

%% @private
aggr_functions() ->
    [{<<"count">>,1,[],<<"COUNT">>},
     {<<"sum">>,1,[],<<"SUM">>},
     {<<"min">>,1,[],<<"MIN">>},
     {<<"max">>,1,[],<<"MAX">>},
     {<<"avg">>,1,[],<<"AVG">>}].

%% @private
expected_aggr_functions() ->
    ?BU:join_binary([<<"'",FN/binary,"'/",(?BU:to_binary(Arity))/binary>> || {FN,Arity,_,_} <- aggr_functions()], <<", ">>).

%% @private
get_aggr_function(AggrFunName,Arity) ->
    lists:foldl(fun({FN,Ar,_,_}=Descriptor,false) when FN==AggrFunName, Ar==Arity -> {ok,Descriptor};
                   (_,Acc) -> Acc
                end, false, aggr_functions()).

%% @private
build_groupby_select_fields(GroupBy) ->
    GroupFields = maps:fold(fun(K,K,Acc) when is_binary(K) -> [?BU:strbin("\"~ts\"",[K]) | Acc];
                               (K,Value,Acc) when is_binary(Value) -> [?BU:strbin("\"~ts\" as \"~ts\"",[Value,K]) | Acc];
                               (K,Expr,Acc) ->
                                    ExprBin = ?FilterCh:build_expression_string(Expr),
                                    [?BU:strbin("~ts AS \"~ts\"",[ExprBin,K]) | Acc]
                            end, [], GroupBy),
    ?BU:join_binary(GroupFields,<<", ">>).

%% @private
build_aggr_fields(Aggr) ->
    AggrFields = maps:fold(fun(K,[AggrFN|Args],Acc) ->
                                {ok,{_,_,_,PgFN}} = get_aggr_function(AggrFN,length(Args)),
                                ArgsBin = ?BU:join_binary([case ?FilterCh:build_expression_string(Arg) of
                                                               {error,_}=Err -> throw(Err);
                                                               Bin -> Bin
                                                           end  || Arg <- Args], <<", ">>),
                                [?BU:strbin("~ts(~ts) AS \"~ts\"",[PgFN,ArgsBin,K]) | Acc]
                           end, [], Aggr),
    ?BU:join_binary(AggrFields,<<", ">>).

%% ---------------------------------------------
%% @private
%% ---------------
-spec read_filter(Meta::map(), SelectOpts::map(), PropNames::[binary])
      -> Where :: binary() | {WhereTerm::binary(), ADT::calendar:datetime(), BDT::calendar:datetime()}.
%% ---------------
read_filter(Meta,SelectOpts,PropNames) ->
    case maps:get(<<"partition_property">>,maps:get(opts,Meta)) of
        <<>> -> read_filter_nopart(Meta,SelectOpts,PropNames);
        _ -> read_filter_part(Meta,SelectOpts,PropNames)
    end.

%% @private
-spec read_filter_nopart(map(),map(),[binary]) -> binary().
read_filter_nopart(_Meta,SelectOpts,_PropNames) ->
    case ?BU:get_by_key(<<"filter">>,SelectOpts) of
        [] -> <<>>;
        Filter ->
            case ?FilterCh:build_filter_string(Filter) of
                {error,_}=Err -> throw(Err);
                <<>> -> <<>>;
                Bin -> <<"\nWHERE ",Bin/binary>>
            end end.

%% @private
-spec read_filter_part(map(),map(),[binary]) -> {WhereTerm::binary(), ADT::calendar:datetime(), BDT::calendar:datetime()}.
read_filter_part(Meta,SelectOpts,_PropNames) ->
    case ?BU:get_by_key(<<"interval">>,SelectOpts) of
        [] -> throw({error,{invalid_params,?BU:strbin("Expected 'interval' parameter: [From, To]",[])}});
        [FromDt,ToDt] ->
            case {parse_dt(FromDt), parse_dt(ToDt)} of
                {A,B} when A > B -> throw({error,{invalid_params,?BU:strbin("Expected 'interval' From is less or equal than To",[])}});
                {{AD,AT},{BD,BT}} ->
                    PartField = maps:get(<<"partition_property">>,maps:get(opts,Meta)),
                    Filter = ?BU:get_by_key(<<"filter">>,SelectOpts),
                    FromDt1 = ?BU:strdatetime({AD,AT}),
                    ToDt1 = ?BU:strdatetime({BD,BT}),
                    Filter1 = case Filter of
                                  [] -> [<<"between">>,[<<"property">>,PartField],FromDt1,ToDt1];
                                  _ -> [<<"and">>,[<<"between">>,[<<"property">>,PartField],FromDt1,ToDt1],Filter]
                              end,
                    case ?FilterCh:build_filter_string(Filter1) of
                        {error,_}=Err -> throw(Err);
                        <<>> -> {<<>>,un,un}; % unexpected
                        Bin -> {<<"\nWHERE ",Bin/binary>>, {AD,AT}, {BD,BT}}
                    end end end.

%% @private
parse_dt(undefined) -> undefined;
parse_dt({D,T}) -> {D,T};
parse_dt({D,T,_}) -> {D,T};
parse_dt(GS) when is_integer(GS) -> calendar:gregorian_seconds_to_datetime(GS);
parse_dt(Dt) when is_binary(Dt) ->
    NullDt = {{1970,0,0},{0,0,0},0},
    case ?PgU:parse_dt(Dt,false) of
        NullDt -> throw({error,{invalid_params,?BU:strbin("Invalid 'interval'. Expected valid datetime values",[])}});
        {D,T,_} -> {D,T}
    end.

%% ---------------------------------------------
%% @private
-spec read_order(Backwards::boolean(),AppendPartAndId::boolean(),Meta::map(),SelectOpts::map(),PropNames::[binary()])
      -> {OrderTerm::binary(), FistPartitionMode::'none' | {'asc' | 'desc', PartPropName::binary()}}.
read_order(Backwards,AppendPartAndId,Meta,SelectOpts,PropNames) ->
    {AppendFields,FirstPartMode} =
        case AppendPartAndId of
            false -> {[], 'none'};
            true ->
                case maps:get(<<"partition_property">>,maps:get(opts,Meta)) of
                    <<>> -> {[<<"id">>], 'none'};
                    PartPropNameX -> {[PartPropNameX,<<"id">>], {'asc',PartPropNameX}}
                end end,
    FunDefaultOrder = fun() -> ?BU:join_binary([<<"\"",Fld/binary,"\"">> || Fld <- AppendFields], <<", ">>) end,
    case ?BU:get_by_key(<<"order">>,SelectOpts,[]) of
        [] when AppendFields == [] -> {<<>>,FirstPartMode};
        [] -> {<<"\nORDER BY ", (FunDefaultOrder())/binary>>,FirstPartMode};
        SortFields when is_list(SortFields) ->
            F = fun(Fld,{OrderAcc,FldAcc,DirAcc}=Acc) when is_binary(Fld) ->
                        case lists:member(Fld,PropNames) of
                            true when Backwards -> {[<<"\"",Fld/binary,"\" DESC">> | OrderAcc], [Fld|FldAcc], [desc|DirAcc]};
                            true -> {[<<"\"",Fld/binary,"\" ASC">> | OrderAcc], [Fld|FldAcc], [asc|DirAcc]};
                            false -> Acc % TODO: split by dot, build json
                        end;
                   ({Fld,Dir},{OrderAcc,FldAcc,DirAcc}=Acc) ->
                        case lists:member(Fld,PropNames) of
                            true ->
                                case Dir of
                                    <<"asc">> when Backwards -> {[<<"\"",Fld/binary,"\" DESC">> | OrderAcc], [Fld|FldAcc], [desc|DirAcc]};
                                    <<"desc">> when Backwards -> {[<<"\"",Fld/binary,"\" ASC">> | OrderAcc], [Fld|FldAcc], [asc|DirAcc]};
                                    <<"asc">> -> {[<<"\"",Fld/binary,"\" ASC">> | OrderAcc], [Fld|FldAcc], [asc|DirAcc]};
                                    <<"desc">> -> {[<<"\"",Fld/binary,"\" DESC">> | OrderAcc], [Fld|FldAcc], [desc|DirAcc]};
                                    _ -> Acc % TODO: split by dot, build json
                                end;
                            false -> Acc
                        end;
                   (Map,{OrderAcc,FldAcc,DirAcc}=Acc) when is_map(Map) ->
                        case maps:keys(Map) of
                            [Fld] when is_binary(Fld) ->
                                case lists:member(Fld,PropNames) of
                                    true ->
                                        case maps:get(Fld,Map) of
                                            <<"asc">> when Backwards -> {[<<"\"",Fld/binary,"\" DESC">> | OrderAcc], [Fld|FldAcc], [desc|DirAcc]};
                                            <<"desc">> when Backwards -> {[<<"\"",Fld/binary,"\" ASC">> | OrderAcc], [Fld|FldAcc], [asc|DirAcc]};
                                            <<"asc">> -> {[<<"\"",Fld/binary,"\" ASC">> | OrderAcc], [Fld|FldAcc], [asc|DirAcc]};
                                            <<"desc">> -> {[<<"\"",Fld/binary,"\" DESC">> | OrderAcc], [Fld|FldAcc], [desc|DirAcc]};
                                            _ -> Acc
                                        end;
                                    false -> Acc % TODO: split by dot, build json
                                end;
                            _ -> Acc
                        end end,
            case lists:foldl(F, {[],[],[]}, SortFields) of
                {[],_,_} when AppendFields==[] -> {<<>>, FirstPartMode};
                {[],_,_} -> {<<"\nORDER BY ", (FunDefaultOrder())/binary>>, FirstPartMode};
                {Order,Flds,Dirs} ->
                    AppendOrder = [<<"\"",Fld/binary,"\"">> || Fld <- AppendFields -- Flds],
                    FirstOrdField = lists:last(Flds),
                    FirstPartMode1 = case FirstPartMode of
                                         {_,PartPropName} when PartPropName==FirstOrdField -> {lists:last(Dirs),PartPropName};
                                         _ -> 'none'
                                     end,
                    {<<"\nORDER BY ", (?BU:join_binary(lists:reverse(Order) ++ AppendOrder, <<", ">>))/binary>>, FirstPartMode1}
            end;
        _ when AppendFields==[] -> {<<>>, FirstPartMode};
        _ -> {<<"\nORDER BY ", (FunDefaultOrder())/binary>>, FirstPartMode}
    end.

%% return result fields of aggregation select
get_aggr_select_propnames(SelectOpts) ->
    AggrMap = maps:get(<<"aggr">>,SelectOpts,#{}),
    GroupByMap = maps:get(<<"groupby">>,SelectOpts,#{}),
    maps:keys(AggrMap) ++ maps:keys(GroupByMap).

%% ---------------------------------------------
%% @private
read_limit(Meta,SelectOpts) ->
    MaxLimit = ?ClassU:max_limit(Meta,SelectOpts),
    erlang:max(1,erlang:min(MaxLimit,?BU:to_int(?BU:get_by_key(<<"limit">>,SelectOpts,MaxLimit),MaxLimit))).
read_limit_default(Meta,SelectOpts) ->
    MaxLimit = ?ClassU:max_limit(Meta,SelectOpts),
    erlang:max(1,?BU:to_int(?BU:get_by_key(<<"limit">>,SelectOpts,MaxLimit))).

%% ---------------------------------------------
%% @private
read_offset(_Meta,SelectOpts) ->
    erlang:max(0,?BU:to_int(?BU:get_by_key(<<"offset">>,SelectOpts,0),0)).

%% ---------------------------------------------
apply_find(ChProps,FindSql,Meta,PropNames,FunOnError) ->
    UrlProfile = maps:get(url_profile,ChProps),
    ?LOG('$trace',"Find(entity) SQL: ~ts", [?BU:to_binary(FindSql)]),
    ?LOG('$trace',"Url/Profile: ~120tp", [UrlProfile]),
    % DB
    {Mks1,ChRes} = timer:tc(fun() -> ?ClickHouseUtils:transform_result(
                                       ?ClickHouseHttp:query_db_tout(UrlProfile,FindSql,?ReadTimeout)) end),
    ?LOG('$debug',"Find(entity) result: ~120tp", [ChRes]),
    {Mks2,ReadRes} = timer:tc(fun() ->
        case ChRes of
            {ok,Body} ->
                {ok,Cols,_Types,Values}=?ClickHouseUtils:parse_result(Body),
                case Values of
                    [] -> false;
                    [Value] ->
                        JSON = maps:from_list(lists:zip(Cols,Value)),
                        XPropsEx = [{<<"id">>,{<<"uuid">>,false,#{},0}} | maps:get(x_props,Meta)],
                        Entity = ?Format:decorate_entity(?Format:format_entity(JSON, XPropsEx, [db]),PropNames),
                        ?LOG('$trace',"Reply (~p mks): ~120tp",[Mks1,Entity]),
                        {ok,Entity};
                    [_|_]=Values ->
                        F = fun(Value,Acc) ->
                                JSON = maps:from_list(lists:zip(Cols,Value)),
                                merge_rows(Acc,JSON)
                            end,
                        JSON = lists:foldl(F,#{},Values),
                        XPropsEx = [{<<"id">>,{<<"uuid">>,false,#{},0}} | maps:get(x_props,Meta)],
                        Entity = ?Format:decorate_entity(?Format:format_entity(JSON, XPropsEx, [db]),PropNames),
                        ?LOG('$trace',"Reply (~p mks): ~120tp",[Mks1,Entity]),
                        {ok,Entity}
                end;
            {error,_}=Err -> FunOnError(Err)
        end end),
    ?ClickHouseUtils:log_mks(find, [Mks1,Mks2]),
    ReadRes.

%% ---------------------------------------------
%% @private
apply_read_items(ChProps,ReadSql,SelectOpts,Meta,PropNames,Attrs) ->
    UrlProfile = maps:get(url_profile,ChProps),
    ?LOG('$trace',"Read(coll) SQL: ~ts", [ReadSql]),
    ?LOG('$trace',"Url/Profile: ~120tp", [UrlProfile]),
    {Mks1,ChRes} = timer:tc(fun() -> ?ClickHouseUtils:transform_result(
                                        ?ClickHouseHttp:query_db_tout(UrlProfile,ReadSql,?ReadTimeout)) end),
    ?LOG('$debug',"Read(coll) result: ~120tp", [ChRes]),
    {Mks2,ReadRes} = timer:tc(fun() ->
        case ChRes of
            {ok,Body} ->
                {ok,ColNames,_ColTypes,Values} = ?ClickHouseUtils:parse_result(Body),
                case ColNames of
                    [<<"count_only">>] ->
                        [[Count]|_]=Values,
                        Limit = case ?BU:get_by_key(<<"limit">>,SelectOpts,undefined) of undefined -> undefined; V -> ?BU:to_int(V,undefined) end,
                        Offset = ?BU:to_int(?BU:get_by_key(<<"offset">>,SelectOpts,0),0),
                        {ok,#{<<"count">> => erlang:min(?BU:to_int(Count)-Offset,Limit)}};
                    _ ->
                        {Mask,MaxMask,_Offset,Limit,ExpandLeading}=Attrs,
                        if
                            length(Values) == 0 orelse Limit==0 ->
                                Entities = [],
                                ?LOG('$trace',"Reply (~p mks): ~120tp",[Mks1,Entities]),
                                {ok,Entities};
                            true ->
                                % merge border-split entity (it was merged in previous page)
                                % merge same ids
                                % trim offsets
                                % remove id if not selected by mask
                                % -----
                                % 1. build json maps by returned list (prev++page++tail)
                                JSONs = lists:map(fun(Value) -> maps:from_list(lists:zip(ColNames,Value)) end, Values),
                                % 2. merge border-split entities (append from prev, append from tail, merge if same ids)
                                FunMergePrev = % merge previous to first while id is the same
                                    fun(List) ->
                                        case ExpandLeading >= length(List) of
                                            _ when ExpandLeading==0 -> List;
                                            true -> [];
                                            false ->
                                                {Prev,[FirstItem|Rest]} = lists:split(ExpandLeading,List),
                                                IdFirst = maps:get(<<"id">>,FirstItem),
                                                UpFirstItem = lists:foldl(fun(JSON,Acc) -> merge_rows(Acc,JSON) end,
                                                                          FirstItem,
                                                                          lists:takewhile(fun(JSON) -> maps:get(<<"id">>,JSON)==IdFirst end, lists:reverse(Prev))),
                                                [UpFirstItem|Rest]
                                        end end,
                                FunMergeTail = % merge tail to last while id is the same
                                    fun([]) -> [];
                                       (List) ->
                                           % merge tail
                                           {PageX,Tail} = case Limit < length(List) of
                                                              true -> lists:split(Limit,List);
                                                              false -> {List,[]}
                                                          end,
                                           case length(PageX) of
                                               0 -> [];
                                               _ ->
                                                   LastX = lists:last(PageX),
                                                   IdLast = maps:get(<<"id">>,LastX),
                                                   case lists:takewhile(fun(JSON) -> maps:get(<<"id">>,JSON)==IdLast end, Tail) of
                                                       [] -> PageX;
                                                       TailToMerge ->
                                                           LastX1 = lists:foldl(fun(TailItem,Acc) -> merge_rows(Acc,TailItem) end, LastX, TailToMerge),
                                                           lists:reverse([LastX1|lists:nthtail(1,lists:reverse(PageX))])
                                                   end
                                           end end,
                                Page = FunMergeTail(FunMergePrev(JSONs)), % merge prev, then merge tail
                                % 3. merge same ids inside page
                                {_,LastItem,MergedPage0} =
                                    lists:foldr(fun(JSON,{LastId,LastItem,Acc}) ->
                                                      case maps:get(<<"id">>,JSON) of
                                                          LastId -> {LastId,merge_rows(LastItem,JSON),Acc};
                                                          Id when LastItem==undefined -> {Id,JSON,Acc};
                                                          Id -> {Id,JSON,[LastItem|Acc]}
                                                      end
                                                end, {undefined,undefined,[]}, Page),
                                Page1 = case LastItem of
                                            undefined -> MergedPage0;
                                            _ -> [LastItem|MergedPage0]
                                        end,
                                % ----
                                Entities = case Page1 of
                                               [] -> [];
                                               _ ->
                                                   % 4. may be remove first element if it has same id as last in prev page (and also remain not empty list)
                                                   Page2 = case length(Page1) of
                                                               1 -> Page1;
                                                               _ when ExpandLeading==0 -> Page1;
                                                               _ ->
                                                                   [FirstItem|Rest] = Page1,
                                                                   IdFirst = maps:get(<<"id">>,FirstItem),
                                                                   case maps:get(<<"id">>,lists:nth(ExpandLeading,JSONs)) of
                                                                       IdFirst -> Rest;
                                                                       _ -> Page1
                                                                   end
                                                           end,
                                                   % 5. remove id if not selected by mask
                                                   SelectMask = selected_mask(Mask,MaxMask,PropNames),
                                                   Page3 = case lists:member(<<"id">>,SelectMask) of
                                                               true -> Page2;
                                                               false -> lists:map(fun(JSON) -> maps:without([<<"id">>],JSON) end, Page2)
                                                           end,
                                                   % 6.
                                                   F = fun(JSON) ->
                                                           XPropsEx = [{<<"id">>,{<<"uuid">>,false,#{},0}} | maps:get(x_props,Meta)],
                                                           Entity = ?Format:decorate_entity(?Format:format_entity(JSON, XPropsEx, [db]),PropNames),
                                                           maps:merge(JSON,Entity)
                                                       end,
                                                   lists:map(F, Page3)
                                           end,
                                ?LOG('$trace',"Reply (~p mks): ~120tp",[Mks1,Entities]),
                                {ok,Entities}
                        end
                end;
            {error,_}=Err -> Err
        end end),
    ?ClickHouseUtils:log_mks(read, [Mks1,Mks2]),
    ReadRes.

%% -------------
%% @private
apply_read_aggr(ChProps,ReadSql,SelectOpts,Meta,PropNames) ->
    UrlProfile = maps:get(url_profile,ChProps),
    ?LOG('$trace',"Read(coll,aggr) SQL: ~ts", [ReadSql]),
    ?LOG('$trace',"Url/Profile: ~120tp", [UrlProfile]),
    {Mks1,ChRes} = timer:tc(fun() -> ?ClickHouseUtils:transform_result(
                                         ?ClickHouseHttp:query_db_tout(UrlProfile,ReadSql,?ReadTimeout)) end),
    ?LOG('$debug',"Read(coll,aggr) result: ~120tp", [ChRes]),
    {Mks2,ReadRes} = timer:tc(fun() ->
        case ChRes of
            {ok,Body} ->
                {ok,ColNames,ColTypes,Values} = ?ClickHouseUtils:parse_result(Body),
                Cols1 = lists:zip(ColNames,ColTypes),
                AggrFields = maps:keys(maps:get(<<"aggr">>,SelectOpts,#{})),
                %GroupByFields = maps:keys(maps:get(<<"groupby">>,SelectOpts,#{})),
                %IsAggregation = length(AggrFields) + length(GroupByFields) > 0,
                F = fun(Value) ->
                        JSON = maps:from_list(lists:zip(ColNames,Value)),
                        XPropsEx = [{<<"id">>,{<<"uuid">>,false,#{},0}} | maps:get(x_props,Meta)],
                        Entity = ?Format:decorate_entity(?Format:format_entity(maps:without(AggrFields,JSON), XPropsEx, [db]), PropNames),
                        Ex = maps:merge(JSON,maps:map(fun(_,V) -> ?BU:to_int(V,V) end, maps:with(AggrFields,JSON))),
                        Ex1 = maps:map(fun(K,V) -> retype_aggr_field(V,?BU:get_by_key(K,Cols1)) end, Ex),
                        maps:merge(Ex1, Entity)
                    end,
                AggrItems = lists:map(F, Values),
                ?LOG('$trace',"Reply (~p mks): ~120tp",[Mks1,AggrItems]),
                {ok,AggrItems};
            {error,_}=Err -> Err
        end end),
    ?ClickHouseUtils:log_mks(read_aggr,[Mks1,Mks2]),
    ReadRes.

%% @private
retype_aggr_field(null,_) -> null;
retype_aggr_field(Value,PgType) ->
    try PgType of
        T when T==<<"Int8">>; T==<<"Int16">>; T==<<"Int32">>; T==<<"Int64">>; T==<<"Int128">>; T==<<"Int256">>;
            T==<<"UInt8">>; T==<<"UInt16">>; T==<<"UInt32">>; T==<<"UInt64">>; T==<<"UInt256">> ->
            ?BU:to_int(Value);
        T when T==<<"Float32">>; T==<<"Float64">> ->
            case catch ?BU:to_int(Value) of
                I when is_integer(I) -> I;
                _ -> ?BU:to_float(Value)
            end;
        T when T==<<"Date">>; T==<<"DateTime">>; T==<<"DateTime('UTC')">> ->
            ?BU:strdatetime3339(?BU:parse_datetime(Value));
        <<"Array",_/binary>> ->
            case catch jsx:decode(Value,[return_maps]) of
                {'EXIT',_} -> Value;
                JSON -> JSON
            end;
        _ -> Value
    catch
        _:_ -> Value
    end.

%% ====================================================================
%% Internal functions
%% Delete / Truncate / Optimize
%% ====================================================================

%% ----------------------------------------------
sql_delete({DtFrom,DtTo},Id,PartPropName,ChProps) ->
    DbName = maps:get(database,ChProps),
    Tab = maps:get(tablename_orig,ChProps),
    Cluster = maps:get(cluster,ChProps),
    ?BU:strbin("
DELETE FROM \"~ts\".\"~ts\" ~ts
WHERE (\"~ts\" >= '~ts') AND (\"~ts\" < '~ts') AND (id = '~ts');",
        [DbName,Tab,on_cluster(Cluster),PartPropName,DtFrom,PartPropName,DtTo,Id]);
%
sql_delete({DtExact},Id,PartPropName,ChProps) ->
    DbName = maps:get(database,ChProps),
    Tab = maps:get(tablename_orig,ChProps),
    Cluster = maps:get(cluster,ChProps),
    ?BU:strbin("
DELETE FROM \"~ts\".\"~ts\" ~ts
WHERE (\"~ts\" = '~ts') AND (id = '~ts');",
        [DbName,Tab,on_cluster(Cluster),PartPropName,DtExact,Id]).

%% ----------------------------------------------
sql_truncate(ChProps) ->
    DbName = maps:get(database,ChProps),
    Tab = maps:get(tablename_orig,ChProps),
    Cluster = maps:get(cluster,ChProps),
    ?BU:strbin("
TRUNCATE TABLE \"~ts\".\"~ts\" ~ts;", [DbName,Tab,on_cluster(Cluster)]).

%% ----------------------------------------------
sql_optimize(ChProps) ->
    DbName = maps:get(database,ChProps),
    Tab = maps:get(tablename_orig,ChProps),
    Cluster = maps:get(cluster,ChProps),
    ?BU:strbin("
OPTIMIZE TABLE \"~ts\".\"~ts\" ~ts;", [DbName,Tab,on_cluster(Cluster)]).

%% ----------------------------------------------
sql_read_partitions(ChProps) ->
    DbName = maps:get(database,ChProps),
    Tab = maps:get(tablename_orig,ChProps),
    ?BU:strbin("
SELECT partition FROM system.parts WHERE database='~ts' AND table='~ts'", [DbName,Tab]).

%% ----------------------------------------------
sql_drop_partition(Partition,ChProps) ->
    DbName = maps:get(database,ChProps),
    Tab = maps:get(tablename_orig,ChProps),
    Cluster = maps:get(cluster,ChProps),
    ?BU:strbin("
ALTER TABLE \"~ts\".\"~ts\" ~ts DROP PARTITION '~ts';", [DbName,Tab,on_cluster(Cluster),Partition]).

%% ----------------------------------------------
%% @private
on_cluster(Cluster) ->
    case Cluster of
        <<>> -> <<>>;
        _ -> ?BU:strbin("ON CLUSTER \"~ts\"",[Cluster])
    end.

%% -----------
%% @private
apply_no_content(OperationName,ChProps,Sql,FunOnError) ->
    UrlProfile = maps:get(url_profile,ChProps),
    ?LOG('$trace',"~ts SQL: ~ts", [OperationName,?BU:to_binary(Sql)]),
    ?LOG('$trace',"Url/Profile: ~120tp", [UrlProfile]),
    % DB
    {Mks,Res} = timer:tc(fun() -> ?ClickHouseUtils:transform_result(
                                    ?ClickHouseHttp:query_db_tout(UrlProfile,Sql,?ReadTimeout)) end),
    ?LOG('$debug',"~ts result: ~120tp", [OperationName, Res]),
    case Res of
        {ok,Body} ->
            {ok,_Cols,_Types,_Values}=?ClickHouseUtils:parse_result(Body),
            ?LOG('$debug',"Reply (~p mks): ok", [Mks]),
            ok;
        {error,_}=Err -> FunOnError(Err)
    end.

%% -----------
%% @private
apply_content(OperationName,ChProps,Sql,FunOnError) ->
    UrlProfile = maps:get(url_profile,ChProps),
    ?LOG('$trace',"~ts SQL: ~ts", [OperationName,?BU:to_binary(Sql)]),
    ?LOG('$trace',"Url/Profile: ~120tp", [UrlProfile]),
    % DB
    {Mks,Res} = timer:tc(fun() -> ?ClickHouseUtils:transform_result(
                                    ?ClickHouseHttp:query_db_tout(UrlProfile,Sql,?ReadTimeout)) end),
    ?LOG('$debug',"~ts result: ~120tp", [OperationName, Res]),
    case Res of
        {ok,Body} ->
            {ok,Cols,_Types,Values}=?ClickHouseUtils:parse_result(Body),
            F = fun(Value) -> maps:from_list(lists:zip(Cols,Value)) end,
            case lists:map(F, Values) of
                [] -> false;
                [Entity] ->
                    ?LOG('$trace',"Reply (~p mks): ~120tp",[Mks,Entity]),
                    {ok,Entity};
                [_|_]=List ->
                    ?LOG('$trace',"Reply (~p mks): ~120tp",[Mks,List]),
                    {ok,List}
            end;
        {error,_}=Err -> FunOnError(Err)
    end.

%% ========================================================
%% Routines
%% ========================================================

%% combine two rows into one entity
%% it is necessary for clickhouse ReplacingMergeTree using replace and update (few minutes after modification entity could exist in several rows).
%% merge entities that have same id
%% Assumption that transactionlog entity is just filling up. So non-default value of option is more priority than absent or default (0 | <<>>).
merge_rows(MapA,MapB) ->
    % TODO: use maps:merge_with(FunCombiner,JSON,Acc)
    Map1 = maps:map(fun(K,V) ->
                        case maps:get(K,MapB,undefined) of
                            undefined -> V;
                            V -> V;
                            0 -> V;
                            <<>> -> V;
                            V1 when V==0 -> V1;
                            V1 when V==<<>> -> V1;
                            V1 -> V1
                        end
                    end, MapA),
    maps:merge(MapB,Map1).

%% =========================================================
%% Narrow down interval and recompute offset
%% When selecting items with general order by partition column
%% When too-large offset (paging)
%% =========================================================
%% Если первое поле в ордере - PartitionProperty
%% Если офсет большой - больше чем N (N=50000, в 1000 раз больше лимита 50)
%% Если диапазон времени больше M минут (M=30)
%% ---
%% ? Если передан ключ первой записи с первой страницы (время и ид)
%%     - то вычисляем сколько лишних (новых) записей берется в выбранном диапазоне.
%%     - дальше всякий раз делаем поправку на это значение (*).
%% ---
%% Агрегируем количества по часам (с направлением сортировки в ту же сторону)
%% Выделяем диапазон подходящих часов (*)
%% Агрегируем количества по минутам (с направлением сортировки в ту же сторону)
%% Выделяем диапазон подходящих минут (*)
%% Пересчитываем офсет (*)
%% Переназначаем интервал
%% --------------------------------------------
-define(AtOffset, 10000).
-define(AtIntervalSec, 600).

%% --------------------------------------------
-spec narrow_down_interval(FirstPartitionProp :: none | {asc|desc, PartPropName::binary()},
                           Meta::map(), SelectOpts::map(), ChProps::map(), PropNames::[binary()])
      -> {ok, NewSelectOpts::map()} | reply_empty | {error,Reason::term()}.
%% --------------------------------------------
%% check partition property is general order field
%% check offset is big
narrow_down_interval('none',_,SelectOpts,_,_) -> {ok,SelectOpts};
narrow_down_interval({Dir,PartPropName},Meta,SelectOpts,ChProps,PropNames) ->
    Offset = read_offset(Meta,SelectOpts),
    case read_offset(Meta,SelectOpts) of
        Offset when Offset < ?AtOffset -> {ok,SelectOpts};
        Offset ->
            {Where0,{AD,AT},{BD,BT}} = read_filter_part(Meta,SelectOpts,PropNames),
            Where = ?BU:to_list(Where0),
            AGS = calendar:datetime_to_gregorian_seconds({AD,AT}),
            BGS = calendar:datetime_to_gregorian_seconds({BD,BT}),
            Limit = read_limit(Meta,SelectOpts),
            do_narrow_down_interval({Dir,PartPropName,Offset,Limit,{AGS,BGS},Where},Meta,SelectOpts,ChProps,PropNames)
    end.

%% @private
%% check interval is over-sized
do_narrow_down_interval({_,_,_,_,{AGS,BGS},_},_,SelectOpts,_,_) when BGS-AGS < ?AtIntervalSec -> {ok,SelectOpts};
do_narrow_down_interval({Dir,PartPropName,Offset,Limit,{AGS,BGS},Where},Meta,SelectOpts,ChProps,PropNames) ->
    GroupInterval = case BGS-AGS of
                        Interval when Interval > 86400 * 500 -> months;
                        Interval when Interval > 86400 * 15 -> days;
                        Interval when Interval > 3600 * 6 -> hours;
                        _ -> minutes
                    end,
    case select_groups({Dir,PartPropName,Offset,Where,GroupInterval},ChProps) of
        {error,_}=Err -> Err;
        {ok,[]} -> reply_empty;
        {ok,Groups} -> % [{DtStart,Count}]
            {SkippedCount, DtA, _, DtB, _} =
                lists:foldl(fun({GroupDtStart,GroupSize}, {AccCount,undefined,_,_,_}) ->
                                    case AccCount + GroupSize > Offset of
                                        true -> {AccCount, GroupDtStart, AccCount + GroupSize - Offset, GroupDtStart, AccCount + GroupSize - Offset >= Limit};
                                        false -> {AccCount + GroupSize, undefined, 0, undefined, false}
                                    end;
                               (_,{_,_,_,_,true}=Acc) -> Acc;
                               ({GroupDtStart,GroupSize},{AccCount,DtA,AccLimit,DtB,false}) ->
                                    AccLimit1 = AccLimit+GroupSize,
                                    {AccCount,DtA,AccLimit1,GroupDtStart,DtB,AccLimit1 >= Limit}
                            end, {0,undefined,0,undefined,false}, Groups),
            case {parse_dt(DtA), parse_dt(DtB)} of
                {undefined,_} -> reply_empty;
                {A,B} when A > B -> throw({error,{invalid_params,?BU:strbin("Expected 'interval' From is less or equal than To",[])}});
                {{AD,AT},{BD,BT}} ->
                    AGS1 = calendar:datetime_to_gregorian_seconds({AD,AT}),
                    BGS0 = calendar:datetime_to_gregorian_seconds({BD,BT}),
                    BGS1 = case GroupInterval of
                               months -> BGS0 + 86400 * 31;
                               days -> BGS0 + 86400;
                               hours -> BGS0 + 3600;
                               minutes -> BGS0 + 60
                           end,
                    % new opts
                    Offset1 = Offset - SkippedCount,
                    AGS2 = max(AGS, AGS1),
                    BGS2 = min(BGS, BGS1),
                    SelectOpts1 = build_select_opts(SelectOpts, AGS2, BGS2, Offset1),
                    % recursive
                    narrow_down_interval({Dir,PartPropName},Meta,SelectOpts1,ChProps,PropNames)
            end
    end.

%% @private
build_select_opts(SelectOpts, NewAGS, NewBGS, NewOffset) ->
    NewFromDt = calendar:gregorian_seconds_to_datetime(NewAGS),
    NewToDt = calendar:gregorian_seconds_to_datetime(NewBGS),
    SelectOpts#{<<"interval">> => [NewFromDt,NewToDt],
                <<"offset">> => NewOffset}.

%% @private
select_groups({Dir,PartPropName,Offset,Where,GroupInterval},ChProps) ->
    Sql = sql_select_groups({Dir,PartPropName,Where,GroupInterval},ChProps),
    case apply_content(?BU:str("SelectNarrowDown(offset=~p)",[Offset]),ChProps,Sql,fun(Err) -> Err end) of
        {error,_}=Err -> Err;
        {ok,[]} -> {ok,[]};
        {ok,Element} when is_map(Element) ->
            {ok, [{maps:get(<<"dt">>,Element), ?BU:to_int(maps:get(<<"cnt">>,Element))}]};
        {ok,List} when is_list(List) ->
            {ok, lists:map(fun(Element) -> {maps:get(<<"dt">>,Element), ?BU:to_int(maps:get(<<"cnt">>,Element))} end, List)}
    end.

%% @private
sql_select_groups({Dir,PartPropName,Where,GroupInterval},ChProps) ->
    % tab
    DbName = maps:get(database,ChProps),
    Tab = maps:get(tablename,ChProps),
    Final = <<>>, % <<"FINAL">>, % SETTINGS max_final_threads = 1,2,3,...
    % groupby field
    GroupFld = case GroupInterval of
                     months -> <<"toStartOfMonth(\"",PartPropName/binary,"\")">>;
                     days -> <<"toStartOfDay(\"",PartPropName/binary,"\")">>;
                     hours -> <<"toStartOfHour(\"",PartPropName/binary,"\")">>;
                     minutes -> <<"toStartOfMinute(\"",PartPropName/binary,"\")">>
                 end,
    % group by
    GroupBy = GroupFld,
    % order
    Order = case Dir of
                asc -> GroupFld;
                desc -> <<GroupFld/binary," DESC">>
            end,
    %
    ?BU:strbin("
SELECT ~ts as dt, count() as cnt
FROM \"~ts\".\"~ts\" ~ts ~ts
GROUP BY ~ts
ORDER BY ~ts",
        [GroupFld,DbName,Tab,Final,Where,GroupBy,Order]).



%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 20.04.2021
%%% @doc

-module(dmlib_storage_clickhouse).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_db_params/2]).
-export([ensure_table/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------------------
%% return db connection params for data-model-class in domain
%% -----------------------------------
get_db_params(Domain,Meta) ->
    db_params(Domain,Meta).

%% @private
db_params(Domain,Meta) ->
    case ?CFG:get_storages_function() of
        undefined -> {error,{internal_error,?BU:strbin("Invalid initial parameters. Expected 'get_storages_function'",[])}};
        Fun when not is_function(Fun,3) -> {error,{internal_error,?BU:strbin("Invalid initial parameters. Expected valid 'get_storages_function' -> fun/3",[])}};
        Fun ->
            case maps:get(<<"storage_instance">>,maps:get(opts,Meta),<<>>) of
                <<>> -> {error,{internal_error,?BU:strbin("Invalid class metadata. Expected existing opts.storage_instance",[])}};
                InstanceValue ->
                    StorageType = <<"clickhouse">>,
                    ClassName = maps:get(classname,Meta),
                    case db_params_1(Fun(Domain,StorageType,InstanceValue),{Domain,ClassName,StorageType,InstanceValue}) of
                        {error,_}=Err -> Err;
                        {ok,[]} -> {error,{invalid_storage,?BU:strbin("Invalid storage ~ts@~ts of type '~ts'. Expected host to connect.",[InstanceValue,Domain,StorageType])}};
                        {ok,_}=Ok -> Ok
                    end
            end end.

%% @private
db_params_1({error,_}=Err,_) -> Err;
db_params_1([],{Domain,_CN,SType,InstanceValue}) -> {error,{storage_instance_not_found,?BU:strbin("Domain doesn't contain storage ~ts@~ts of type '~ts'",[InstanceValue,Domain,SType])}};
%db_params_1([],{_,_,SType,InstanceValue}) -> {error,{internal_error,?BU:strbin("Invalid class metadata. Expected existing storage_instance (opts.storage_instance should match at least 1 domain's storage of type: '~ts'",[SType])}};
db_params_1({ok,Storages},{Domain,CN,_,_}) ->
    DbParams = lists:foldl(fun(M,Acc) ->
                                    M0 = #{
                                        scheme => ?BU:to_list(maps:get(<<"scheme">>,M,<<"http">>)),
                                        host => Host0 = ?BU:to_list(maps:get(<<"host">>,M,<<>>)),
                                        port => ?BU:to_int(maps:get(<<"port">>,M,8123)),
                                        login => ?BU:to_list(maps:get(<<"login">>,M,<<>>)),
                                        password => ?BU:to_list(maps:get(<<"password">>,M,<<>>)),
                                        %
                                        cluster => maps:get(<<"cluster">>,M,<<>>),
                                        initial_domain => Domain,
                                        classname => CN
                                    },
                                    M1 = case maps:get(<<"domain">>,M,undefined) of
                                             undefined -> M0;
                                             OtherDomain -> M0#{domain => OtherDomain} % Use custom domain-dbname (of other system may be)
                                         end,
                                    case maps:get(<<"endpoints">>,M,[]) of
                                        [] when Host0 /= <<>> -> [M1|Acc];
                                        List when is_list(List) ->
                                            lists:foldl(
                                                fun(EP,Acc1) ->
                                                    Keys = [{scheme,<<"scheme">>},
                                                            {host,<<"host">>},
                                                            {port,<<"port">>},
                                                            {login,<<"login">>},
                                                            {password,<<"password">>}],
                                                    M2 = lists:foldl(
                                                        fun({Key1,Key2}, Acc2) ->
                                                            case maps:get(Key2,EP,undefined) of
                                                                undefined -> Acc2;
                                                                Value -> Acc2#{Key1 => Value}
                                                            end
                                                        end, M1, Keys),
                                                    [M2|Acc1]
                                                end, Acc, List);
                                        Other when not is_list(Other), Host0 /= <<>> -> [M1|Acc];
                                        _Other -> Acc
                                    end
                          end, [], Storages),
    {ok,lists:reverse(DbParams)}.

%% ====================================================================
%% Build structure facade
%% ====================================================================

%% -----------------------------------
%% make table for data-model-class
%% -----------------------------------
ensure_table(Domain,_ClassName,Meta) ->
    case db_params(Domain,Meta) of
        {error,_}=Err -> Err;
        {ok,DbParams} -> ?ClickHouseMeta:ensure_table(DbParams,_ClassName,Meta)
    end.

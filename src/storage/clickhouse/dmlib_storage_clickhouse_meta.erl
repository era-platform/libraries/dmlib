%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 20.04.2021
%%% @doc Storage-clickhouse routines to
%%%         - build metadata for class table create/modify
%%%         - apply metadata to ClickHouse server.

-module(dmlib_storage_clickhouse_meta).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([ensure_table/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(ReadMetaTimeout,5000).
-define(ModifyMetaTimeout,10000).

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------
%% Make table in PostgreDB for domain-model-class
%% -------------------------
-spec ensure_table(DbParams::[map()],_ClassName::binary(),Meta::map())
      -> {ok,Props::map()} | {error,Reason::term()}.
%% -------------------------
ensure_table(DbParamsL,ClassName,Meta) ->
    Res = lists:foldl(fun%(_,{AccOk,AccErr}) when length(AccOk) >= 2 -> {AccOk,AccErr};
                         (DbParams,{AccOk,AccErr}) ->
                            case ?ClickHouseHttp:connect_db([DbParams]) of
                                {error,_}=Err -> {AccOk,[Err|AccErr]};
                                {ok,Props} -> {[Props|AccOk],AccErr}
                            end end, {[],[]}, DbParamsL),
    case Res of
        {[],[Err|_]=Errors} ->
            ?LOG('$error',"ClickHouse connect: ~n\t~120tp",[Errors]),
            Err;
        {PropsL,Errors} ->
            do_ensure_table(PropsL,ClassName,Meta,Errors==[])
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

do_ensure_table([Props|Rest],_ClassName,Meta,AllIn) ->
    UrlProfile = maps:get(url_profile,Props),
    DbParams = maps:get(dbparams,Props),
    ?LOG('$trace',"Url/Profile: ~ts",[UrlProfile]),
    ?LOG('$trace',"DbParams of clickhouse: ~120tp",[DbParams]),
    %
    TMeta = table_meta(DbParams,Meta),
    DbName = maps:get(<<"database">>,TMeta),
    Cluster = maps:get(cluster,DbParams),
    % Read structure
    {ClusterX,ClusterNodes} =
        case read_cluster(Cluster,UrlProfile) of
            {error,_}=ErrA -> throw(ErrA);
            [] -> {<<>>,[]};
            NodesX -> {Cluster,NodesX}
        end,
    CurrentColumns =
        case read_columns(TMeta,UrlProfile) of
            {error,_}=ErrB -> throw(ErrB);
            ColumnsX -> ColumnsX
        end,
    Attributes =
        case read_attributes(TMeta,UrlProfile) of
            {error,_}=ErrC -> throw(ErrC);
            false -> false;
            AttrsX -> AttrsX
        end,
    % Write (create/drop/update structure)
    case AllIn of
        true ->
            % TODO: indexes as separate materialized views (lookup_property -> id, partition_property)
            Primary = [ {"ensure_database",sql_database(DbName,ClusterX)},
                        {"backup_table",sql_backup_table(TMeta,ClusterX,Attributes)},
                        {"ensure_table",sql_create_table(TMeta,ClusterX)},
                        {"alter_table_drop_columns",sql_table_alter_drop_columns(TMeta,ClusterX,CurrentColumns)},
                        {"alter_table_add_columns",sql_table_alter_add_columns(TMeta,ClusterX,CurrentColumns)},
                        {"drop_distributed",sql_distributed_drop(TMeta,ClusterX)},
                        {"ensure_distributed",sql_distributed_create(TMeta,ClusterX)},
                        {"drop_queue",sql_queue_drop(Meta,TMeta,ClusterNodes)},
                        {"ensure_queue",sql_queue_create(Meta,TMeta,ClusterNodes,<<"auto1">>)},
                        {"drop_view",sql_view_drop(TMeta,ClusterNodes)},
                        {"ensure_view",sql_view_create(TMeta,ClusterNodes)} ],
            Replica = case Rest of
                          [] -> [];
                          [_|_] ->
                              lists:foldl(fun(Props2,Acc) ->
                                                    UrlProfile2 = maps:get(url_profile,Props2),
                                                    Acc ++ [
                                                        {"drop_queue",sql_queue_drop(Meta,TMeta,ClusterNodes),UrlProfile2},
                                                        {"ensure_queue",sql_queue_create(Meta,TMeta,ClusterNodes,<<"auto1">>),UrlProfile2}, % TODO: make same group when clickhouse-kafka failover #21267 is fixed
                                                        {"drop_view",sql_view_drop(TMeta,ClusterNodes),UrlProfile2},
                                                        {"ensure_view",sql_view_create(TMeta,ClusterNodes),UrlProfile2} ]
                                          end, [], Rest)
                      end,
            lists:foldl(fun({OpName,Sql},ok) when is_binary(Sql) -> ensure_query(OpName,Sql,UrlProfile);
                           ({OpName,Sql,UrlProfileX},ok) when is_binary(Sql) -> ensure_query(OpName,Sql,UrlProfileX);
                           (_,Acc) -> Acc
                        end, ok, Primary++Replica),
            {ok,Props#{database => DbName,
                       tablename => case ClusterX of <<>> -> maps:get(<<"tablename">>,TMeta); _ -> maps:get(<<"tablename_distributed">>,TMeta) end,
                       tablename_orig => case ClusterX of <<>> -> maps:get(<<"tablename">>,TMeta); _ -> maps:get(<<"tablename">>,TMeta) end,
                       cluster => ClusterX }};
        false ->
            Drop = drop_columns(TMeta,CurrentColumns),
            Add = add_columns(TMeta,CurrentColumns),
            IsReadyStructure = (Drop == []) andalso (Add == []),
            case IsReadyStructure of
                true ->
                    ?LOG('$warning',"CH ensure_table '~ts' / '~ts'. Structure is ready. Pass.", [DbName,_ClassName]),
                    {ok,Props#{database => DbName,
                               tablename => case ClusterX of <<>> -> maps:get(<<"tablename">>,TMeta); _ -> maps:get(<<"tablename_distributed">>,TMeta) end,
                               tablename_orig => case ClusterX of <<>> -> maps:get(<<"tablename">>,TMeta); _ -> maps:get(<<"tablename">>,TMeta) end,
                               cluster => ClusterX }};
                false ->
                    ?LOG('$warning',"CH ensure_table '~ts' / '~ts'. Structure is not ready. Abort.~n\tColumns to drop: ~120tp. Columns to add: ~120tp.", [DbName,_ClassName, Drop, Add]),
                    {error,{unavailable_cluster_nodes,?BU:strbin("Some cluster nodes unavailable. Cannot setup/modify structure.",[])}}
            end
    end.

%% ------------------------------------------------
%% Operation for reading cluster info
%% ------------------------------------------------
-spec read_cluster(Cluster::binary(),Url::binary()) -> [[]] | {error,Reason::term()}.
%% ------------------------------------------------
read_cluster(<<>>,_) -> [];
read_cluster(Cluster,UrlProfile) ->
    Sql = sql_read_cluster(Cluster),
    ?LOG('$trace',"Clickhouse read_cluster sql: ~ts",[Sql]),
    Res = ?ClickHouseHttp:query_db_tout(UrlProfile,Sql,?ReadMetaTimeout),
    ?LOG('$trace',"Clickhouse read_cluster result: ~120tp",[Res]),
    case Res of
        {error,_}=Err -> Err;
        {ok,Body} ->
            case ?ClickHouseUtils:parse_result(Body) of
                {ok,_Cols,_Types,Values} ->
                    [{ShardNum, ReplicaNum, HostName, HostAddr, Port} || [ShardNum, ReplicaNum, HostName, HostAddr, Port] <- Values];
                _ -> []
            end end.

%% @private
sql_read_cluster(Cluster) ->
    ?BU:strbin("
SELECT shard_num, replica_num, host_name, host_address, port
FROM system.clusters
WHERE cluster = '~ts';
",[Cluster]).

%% ------------------------------------------------
%% Operation for reading table attributes
%% ------------------------------------------------
-spec read_attributes(TMeta::map(),Url::binary())
      -> [{ColName::binary(),ColType::binary()}] | {error,Reason::term()}.
%% ------------------------------------------------
read_attributes(TMeta,UrlProfile) ->
    Sql = sql_read_attributes(TMeta),
    ?LOG('$trace',"Clickhouse read_attributes sql: ~ts",[Sql]),
    Res = ?ClickHouseHttp:query_db_tout(UrlProfile,Sql,?ReadMetaTimeout),
    ?LOG('$trace',"Clickhouse read_attributes result: ~120tp",[Res]),
    case Res of
        {error,_}=Err -> Err;
        {ok,Body} ->
            case ?ClickHouseUtils:parse_result(Body) of
                {ok,_Cols,_Types,[[Name,Engine,PartKey,SortKey,PrimKey,SampKey]]} -> {ok,{Name,Engine,PartKey,SortKey,PrimKey,SampKey}};
                _ -> false
            end end.

%% @private
sql_read_attributes(TMeta) ->
    DbName = maps:get(<<"database">>,TMeta),
    TableName = maps:get(<<"tablename">>,TMeta),
    ?BU:strbin("
SELECT name, engine, partition_key, sorting_key, primary_key, sampling_key
FROM system.tables
WHERE database = '~ts' AND name = '~ts'
", [DbName,TableName]).

%% ------------------------------------------------
%% Operation for reading columns and types
%% ------------------------------------------------
-spec read_columns(TMeta::map(),Url::binary())
      -> [{ColName::binary(),ColType::binary()}] | {error,Reason::term()}.
%% ------------------------------------------------
read_columns(TMeta,UrlProfile) ->
    Sql = sql_read_columns(TMeta),
    ?LOG('$trace',"Clickhouse read_columns sql: ~ts",[Sql]),
    Res = ?ClickHouseHttp:query_db_tout(UrlProfile,Sql,?ReadMetaTimeout),
    ?LOG('$trace',"Clickhouse read_columns result: ~120tp",[Res]),
    case Res of
        {error,_}=Err -> Err;
        {ok,Body} ->
            case ?ClickHouseUtils:parse_result(Body) of
                {ok,_Cols,_Types,Values} -> [{Name,Type} || [Name,Type] <- Values];
                _ -> []
            end end.

%% @private
sql_read_columns(TMeta) ->
    DbName = maps:get(<<"database">>,TMeta),
    TableName = maps:get(<<"tablename">>,TMeta),
    ?BU:strbin("
SELECT name, type
FROM system.columns
WHERE database='~ts' AND table='~ts'
", [DbName,TableName]).

%% ------------------------------------------------
%% Utilite to query modifying meta requests to clickhouse without returning data
%% ------------------------------------------------
ensure_query(_OpName,<<>>,_UrlProfile) -> ok;
ensure_query(OpName,Sql,UrlProfile) ->
    ?LOG('$trace',"Clickhouse ~ts sql: ~ts",[OpName,Sql]),
    Res = ?ClickHouseHttp:query_db_tout(UrlProfile,Sql,?ModifyMetaTimeout),
    ?LOG('$trace',"Clickhouse ~ts result: ~120tp",[OpName,Res]),
    case Res of
        {error,_}=Err -> Err;
        {ok,_Body} -> ok
    end.

%% ------------------------------------------------
%% SQL for creating database
%% ------------------------------------------------
sql_database(DbName,Cluster) ->
    ?BU:strbin("
CREATE DATABASE IF NOT EXISTS \"~ts\" ~ts ENGINE=Atomic;",[DbName,on_cluster(Cluster)]).

%% ------------------------------------------------
%% SQL for creating destination table (ON CLUSTER)
%% ------------------------------------------------
sql_backup_table(_TMeta,_Cluster,false) -> false;
sql_backup_table(TMeta,Cluster,{ok,{_Name,Engine,PartKey,SortKey,PrimKey,_SampKey}}) ->
    case Engine of
        <<"MaterializedView">> -> false; % support manual change of storage table into view
        <<"ReplicatedReplacingMergeTree">> ->
            % Sample of returned values:
            %   {<<"toYYYYMM(timeStart)">>, <<"timeStart, id">>, <<"timeStart, id">>}
            Opts = maps:get(<<"opts">>,TMeta),
            {PartKey1,SortKey1,PrimKey1} =
                case maps:get(<<"partition">>,Opts,undefined) of
                    undefined -> {<<>>,<<"id">>,<<"id">>};
                    PartInfo ->
                        [PartField] = maps:get(<<"fields">>,PartInfo),
                        case maps:get(<<"interval">>,PartInfo) of
                            <<"year">> -> {?BU:strbin("toYear(~ts)",[PartField]), ?BU:strbin("~ts, id",[PartField]), ?BU:strbin("~ts, id",[PartField])};
                            <<"month">> -> {?BU:strbin("toYYYYMM(~ts)",[PartField]), ?BU:strbin("~ts, id",[PartField]), ?BU:strbin("~ts, id",[PartField])};
                            <<"day">> -> {?BU:strbin("toYYYYMMDD(~ts)",[PartField]), ?BU:strbin("~ts, id",[PartField]), ?BU:strbin("~ts, id",[PartField])}
                        end
                end,
            case {PartKey, SortKey, PrimKey} of
                {PartKey1,SortKey1,PrimKey1} -> false;
                _ -> sql_backup_table_1(TMeta,Cluster)
            end;
        _ -> sql_backup_table_1(TMeta,Cluster)
    end;
sql_backup_table(TMeta,Cluster,_Err) ->
    sql_backup_table_1(TMeta,Cluster).

%% @private
sql_backup_table_1(TMeta,Cluster) ->
    DbName = maps:get(<<"database">>,TMeta),
    TableName = maps:get(<<"tablename">>,TMeta),
    BackupTableName = backup_tablename(TableName,?BU:timestamp()),
    ?BU:strbin("
RENAME TABLE \"~ts\".\"~ts\" TO \"~ts\".\"~ts\" ~ts;",[DbName,TableName,DbName,BackupTableName,on_cluster(Cluster)]).

%% @private
backup_tablename(TableName,NowTS) ->
    ?BU:strbin("~ts~ts", [TableName,?BU:strbin("_bk_~ts",[bkphash(NowTS)])]).

%% @private
bkphash(TS) ->
    F = fun(X) when X<10 -> X+48;
           (X) when X<36 -> X-10+65;
           (X) when X<62 -> X-36+97
        end,
    A = TS div 1000,
    A1 = A rem 62,
    B = A div 62,
    B1 = B rem 62,
    C = B div 62,
    C1 = C rem 62,
    D = C div 62,
    D1 = D rem 62,
    lists:foldl(fun(I,Acc) -> <<Acc/binary,(F(I))>> end, <<>>, [D1,C1,B1,A1]).

%% ------------------------------------------------
%% SQL for creating destination table (ON CLUSTER)
%% ------------------------------------------------
sql_create_table(TMeta,Cluster) ->
    DbName = maps:get(<<"database">>,TMeta),
    TableName = maps:get(<<"tablename">>,TMeta),
    %% no index is used: https://clickhouse.com/docs/en/optimize/skipping-indexes#skip-best-practices
    ?BU:strbin("
CREATE TABLE IF NOT EXISTS \"~ts\".\"~ts\" ~ts (
    ~ts
)
ENGINE=ReplicatedReplacingMergeTree()
~tsORDER BY (~ts);", [DbName,TableName,on_cluster(Cluster),
                      fields(TMeta),
                      partition_by(TMeta),order_by(TMeta)]).

%% @private
on_cluster(Cluster) ->
    case Cluster of
        <<>> -> <<>>;
        _ -> ?BU:strbin("ON CLUSTER \"~ts\"",[Cluster])
    end.

%% @private
partition_by(TMeta) ->
    Opts = maps:get(<<"opts">>,TMeta),
    case maps:get(<<"partition">>,Opts,undefined) of
        undefined -> <<>>;
        PartInfo ->
            [PartField] = maps:get(<<"fields">>,PartInfo),
            case maps:get(<<"interval">>,PartInfo) of
                <<"year">> -> ?BU:strbin("PARTITION BY toYear(\"~ts\")~n",[PartField]);
                <<"month">> -> ?BU:strbin("PARTITION BY toYYYYMM(\"~ts\")~n",[PartField]);
                <<"day">> -> ?BU:strbin("PARTITION BY toYYYYMMDD(\"~ts\")~n",[PartField])
            end end.

%% @private
order_by(TMeta) ->
    Opts = maps:get(<<"opts">>,TMeta),
    case maps:get(<<"partition">>,Opts,undefined) of
        undefined -> <<"id">>;
        PartInfo ->
            [PartField] = maps:get(<<"fields">>,PartInfo),
            ?BU:strbin("\"~ts\", id",[PartField])
    end.

%% @private
fields(TMeta) ->
    FunFldDescr = fun(FldInfo) -> <<"\"",(maps:get(<<"name">>,FldInfo))/binary,"\" ",(maps:get(<<"type">>,FldInfo))/binary>> end,
    ?BU:join_binary([FunFldDescr(FldInfo)|| FldInfo <- maps:get(<<"fields">>,TMeta)],<<",\n    ">>).

%% ------------------------------------------------
%% SQL for altering table to drop columns (ON CLUSTER)
%% ------------------------------------------------
sql_table_alter_drop_columns(_TMeta,_Cluster,[]) -> <<>>;
sql_table_alter_drop_columns(TMeta,Cluster,CurrentColumns) ->
    DbName = maps:get(<<"database">>,TMeta),
    TableName = maps:get(<<"tablename">>,TMeta),
    case drop_columns_sqlpart(TMeta,CurrentColumns) of
        <<>> -> <<>>;
        DropColumns ->
            ?BU:strbin("
ALTER TABLE \"~ts\".\"~ts\" ~ts
~ts
", [DbName,TableName,on_cluster(Cluster),DropColumns])
    end.

%% @private
%% @private
drop_columns_sqlpart(TMeta,CurrentColumns) ->
    ToDrop = drop_columns(TMeta,CurrentColumns),
    ?BU:join_binary([?BU:strbin("DROP COLUMN \"~ts\"",[Name]) || {Name,_Type} <- ToDrop], <<", ">>).

%% @private
drop_columns(TMeta,CurrentColumns) ->
    ToBe = [{maps:get(<<"name">>,FldInfo), maps:get(<<"type">>,FldInfo)} || FldInfo <- maps:get(<<"fields">>,TMeta)],
    CurrentColumns -- ToBe.

%% ------------------------------------------------
%% SQL for altering table to add columns (ON CLUSTER)
%% ------------------------------------------------
sql_table_alter_add_columns(_TMeta,_Cluster,[]) -> <<>>;
sql_table_alter_add_columns(TMeta,Cluster,CurrentColumns) ->
    DbName = maps:get(<<"database">>,TMeta),
    TableName = maps:get(<<"tablename">>,TMeta),
    case add_columns_sqlpart(TMeta,CurrentColumns) of
        <<>> -> <<>>;
        AddColumns ->
            ?BU:strbin("
ALTER TABLE \"~ts\".\"~ts\" ~ts
~ts
", [DbName,TableName,on_cluster(Cluster),AddColumns])
    end.

%% @private
add_columns_sqlpart(TMeta,CurrentColumns) ->
    ToAdd = add_columns(TMeta,CurrentColumns),
    ?BU:join_binary([?BU:strbin("ADD COLUMN \"~ts\" ~ts",[Name,Type]) || {Name,Type} <- ToAdd], <<", ">>).

%% @private
add_columns(TMeta,CurrentColumns) ->
    ToBe = [{maps:get(<<"name">>,FldInfo), maps:get(<<"type">>,FldInfo)} || FldInfo <- maps:get(<<"fields">>,TMeta)],
    ToBe -- CurrentColumns.

%% ------------------------------------------------
%% SQL for creating distributed table (ON CLUSTER)
%% ------------------------------------------------
sql_distributed_drop(TMeta,Cluster) ->
    DbName = maps:get(<<"database">>,TMeta),
    TableNameDistr = maps:get(<<"tablename_distributed">>,TMeta),
    ?BU:strbin("
DROP TABLE IF EXISTS \"~ts\".\"~ts\" ~ts;
",[DbName,TableNameDistr,on_cluster(Cluster)]).

%% ------
sql_distributed_create(TMeta,Cluster) ->
    DbName = maps:get(<<"database">>,TMeta),
    TableNameDistr = maps:get(<<"tablename_distributed">>,TMeta),
    TableName = maps:get(<<"tablename">>,TMeta),
    ?BU:strbin("
CREATE TABLE IF NOT EXISTS \"~ts\".\"~ts\" ~ts (
    ~ts
)
ENGINE=Distributed(\"~ts\", \"~ts\", \"~ts\", halfMD5(id));
", [DbName,TableNameDistr,on_cluster(Cluster),fields(TMeta),Cluster,DbName,TableName]).

%% ------------------------------------------------
%% SQL for creating queue table
%% ------------------------------------------------
sql_queue_drop(_Item,TMeta,_ClusterNodes) ->
    DbName = maps:get(<<"database">>,TMeta),
    TableName = maps:get(<<"tablename_kafka">>,TMeta),
    ?BU:strbin("
DROP TABLE IF EXISTS \"~ts\".\"~ts\";
",[DbName,TableName]).

%% ------
sql_queue_create(Item,TMeta,_ClusterNodes,ConsumerGroup) ->
    DbName = maps:get(<<"database">>,TMeta),
    TableName = maps:get(<<"tablename_kafka">>,TMeta),
    Topic = maps:get(<<"topic">>,TMeta),
    ?BU:strbin("
CREATE TABLE IF NOT EXISTS \"~ts\".\"~ts\" (
    ~ts
)
ENGINE = Kafka() SETTINGS kafka_broker_list = '~ts',
                          kafka_topic_list = '~ts',
                          kafka_group_name = '~ts',
                          kafka_format = 'JSONEachRow',
                          --kafka_num_consumers = 2,
                          kafka_max_block_size = 1048576;
",[DbName,TableName,fields_queue(TMeta),
        brokers(Item),Topic,ConsumerGroup]).

%% @private
fields_queue(TMeta) ->
    FunFldDescr = fun(FldInfo) -> <<"\"",(maps:get(<<"name">>,FldInfo))/binary,"\" ",(maps:get(<<"qtype">>,FldInfo))/binary>> end,
    ?BU:join_binary([FunFldDescr(FldInfo)|| FldInfo <- maps:get(<<"fields">>,TMeta)],<<",\n    ">>).

%% @private
brokers(Item) ->
    Domain = maps:get('domain',Item),
    {ok,[KafkaDbParams|_]} = ?Kafka:get_db_params(Domain,Item),
    Endpoints = maps:get(<<"endpoints">>,KafkaDbParams),
    ?BU:join_binary([?BU:strbin("~ts:~p",[H,P]) || {H,P} <- Endpoints],<<",">>).

%% ------------------------------------------------
%% SQL for creating materialized view table
%% ------------------------------------------------
sql_view_drop(TMeta,_ClusterNodes) ->
    DbName = maps:get(<<"database">>,TMeta),
    ViewName = maps:get(<<"tablename_view">>,TMeta),
    ?BU:strbin("
DROP TABLE IF EXISTS \"~ts\".\"~ts\";
",[DbName,ViewName]).

%% ------
sql_view_create(TMeta,_ClusterNodes) ->
    DbName = maps:get(<<"database">>,TMeta),
    ViewName = maps:get(<<"tablename_view">>,TMeta),
    TableName = maps:get(<<"tablename">>,TMeta),
    QueueName = maps:get(<<"tablename_kafka">>,TMeta),
    Topic = maps:get(<<"topic">>,TMeta),
    ?BU:strbin("
CREATE MATERIALIZED VIEW IF NOT EXISTS \"~ts\".\"~ts\" TO \"~ts\".\"~ts\" AS
   SELECT ~ts
   FROM \"~ts\".\"~ts\"
   WHERE _topic='~ts';
",[DbName,ViewName,DbName,TableName,fields_view(TMeta),DbName,QueueName,Topic]).

%% @private
fields_view(TMeta) ->
    FunFldDescr = fun(FldInfo) ->
                        N = maps:get(<<"name">>,FldInfo),
                        case maps:get(<<"etype">>,FldInfo) of
                            {<<"datetime">>,false} -> ?BU:strbin("parseDateTimeBestEffort(\"~ts\") as \"~ts\"",[N,N]);
                            {_,_} -> <<"\"",N/binary,"\"">>
                        end end,
    ?BU:join_binary([FunFldDescr(FldInfo)|| FldInfo <- maps:get(<<"fields">>,TMeta)],<<", ">>).

%% ------------------------------------------------
%% Build table builder metadata
%% ------------------------------------------------
table_meta(DbParams,Item) when is_map(DbParams) ->
    Domain = maps:get('domain',Item),
    ClassName = maps:get('classname',Item),
    ItemOpts = maps:get('opts',Item),
    % properties
    PartitionPropName = maps:get(<<"partition_property">>, ItemOpts, <<>>),
    PartitionInterval = maps:get(<<"partition_interval">>, ItemOpts, <<"month">>),
    Props = lists:map(fun({N,{T,M,Prop,Idx}}) -> {N,T,M,Prop,Idx} end, maps:get(x_props,Item)),
    Props1 = lists:sort(fun({_,_,_,_,Idx1},{_,_,_,_,Idx2}) -> Idx1=<Idx2 end, Props),
    FieldInfo = [#{<<"name">> => ?ClickHouseUtils:field_name(N),
                   <<"type">> => ?ClickHouseUtils:map_db_field_type(T,M),
                   <<"qtype">> => ?ClickHouseUtils:map_db_field_queuetype(T,M),
                   <<"etype">> => {T,M},
                   %<<"default">> => maps:get(<<"default">>, Prop, undefined),
                   <<"required">> => maps:get(<<"required">>, Prop, false)}
                 || {N,T,M,Prop,_Idx} <- Props1],
    FieldInfo1 = [#{<<"name">> => <<"id">>,
                    <<"type">> => ?ClickHouseUtils:map_db_field_type(<<"uuid">>,false),
                    <<"qtype">> => ?ClickHouseUtils:map_db_field_queuetype(<<"uuid">>,false),
                    <<"etype">> => {<<"uuid">>,false},
                    <<"required">> => true},
                  #{<<"name">> => <<"item_json_data">>,
                    <<"type">> => ?ClickHouseUtils:map_db_field_type(<<"any">>,false),
                    <<"qtype">> => ?ClickHouseUtils:map_db_field_queuetype(<<"any">>,false),
                    <<"etype">> => {<<"any">>,false},
                    <<"required">> => true}
                  | FieldInfo],
    % index fields
    IndexFields0 = maps:get(<<"lookup_properties">>, ItemOpts, []) -- [<<"id">>],
    IndexFields = case lists:member(PartitionPropName,IndexFields0) of
                      true when PartitionPropName/=<<>> -> lists:delete(PartitionPropName, IndexFields0);
                      _ -> IndexFields0
                  end,
    % build meta
    Opts = #{},
    Opts1 = case PartitionPropName of
                <<>> -> Opts;
                _ -> Opts#{<<"partition">> => #{<<"fields">> => [PartitionPropName],
                                                <<"interval">> => PartitionInterval}}
            end,
    TopicDomain = maps:get(domain,DbParams,Domain), % TODO: override from kafka storage, at least from CH storage
    DatabaseName = ?ClickHouseUtils:db_name(TopicDomain),
    #{<<"topic">> => ?BU:to_binary(?KafkaMeta:topic_name(TopicDomain,ClassName)),
      <<"database">> => DatabaseName,
      <<"tablename">> => ?ClickHouseUtils:tabname(ClassName,PartitionPropName/=<<>>),
      <<"tablename_kafka">> => ?ClickHouseUtils:tabname_kafka(ClassName),
      <<"tablename_view">> => ?ClickHouseUtils:tabname_view(ClassName),
      <<"tablename_distributed">> => ?ClickHouseUtils:tabname_distributed(ClassName),
      <<"fields">> => FieldInfo1,
      <<"index_fields">> => IndexFields,
      <<"opts">> => Opts1}.



%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 30.03.2021
%%% @doc Implements CRUD operations on mnesia

-module(dmlib_storage_mnesia_crud).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([find/2,
         lookup/3, lookup_ex/3,
         read/4,
         create/2,replace/2,update/2,delete/2,
         clear/1]).
-export([fold/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

find(TableName,Id) ->
    ?LOG('$trace',"Find '~ts': ~120tp",[TableName,Id]),
    {Mks,Res} =
        timer:tc(fun() ->
                    mnesia:ets(fun() ->
                                    case mnesia:read(TableName,Id) of
                                        [{rec,_,Item}] -> {ok,Item};
                                        [] -> false
                                    end
                               end)
                 end),
    log_mks(lookup,Mks),
    Res.

%% ------------------
lookup(TableName,Content,LookupKeyFields) ->
    {Mks,Res} = timer:tc(fun() -> mnesia:ets(fun() -> ?StorageEts:lookup({mnesia,TableName},Content,LookupKeyFields) end) end),
    log_mks(lookup,Mks),
    Res.

%% ------------------
lookup_ex(TableName,Content,LookupKeyFields) ->
    {Mks,Res} = timer:tc(fun() -> mnesia:ets(fun() -> ?StorageEts:lookup_ex({mnesia,TableName},Content,LookupKeyFields) end) end),
    log_mks(lookup,Mks),
    Res.

%% ------------------
-spec read(ETS::ets:tab(), SelectOpts::map(), LimitOpts::map(), AggrCache::undefined | {ets:tab(), TTL::integer()})
      -> {ok,Res::[map()]}.
read(TableName,SelectOpts,LimitOpts,AggrCache) ->
    ?LOG('$trace',"Read '~ts': ~120tp",[TableName,SelectOpts]),
    {Mks,Res} = timer:tc(fun() -> mnesia:ets(fun() -> ?StorageEts:read({mnesia,TableName},SelectOpts,LimitOpts,AggrCache) end) end),
    log_mks(read,Mks),
    Res.

%% ------------------
create(TableName,Item) ->
    Id = maps:get(<<"id">>,Item),
    ?LOG('$trace',"Create '~ts': ~120tp",[TableName,Id]),
    {Mks,{atomic,ok}} = timer:tc(fun() -> mnesia:transaction(fun() -> mnesia:write(TableName, {rec,Id,Item}, write) end) end),
    log_mks(create,Mks),
    ok.

%% ------------------
replace(TableName,Item) ->
    Id = maps:get(<<"id">>,Item),
    ?LOG('$trace',"Replace '~ts': ~120tp",[TableName,Id]),
    {Mks,{atomic,ok}} = timer:tc(fun() -> mnesia:transaction(fun() -> mnesia:write(TableName, {rec,Id,Item}, write) end) end),
    log_mks(create,Mks),
    ok.

%% ------------------
update(TableName,Item) ->
    Id = maps:get(<<"id">>,Item),
    ?LOG('$trace',"Update '~ts': ~120tp",[TableName,Id]),
    {Mks,{atomic,ok}} = timer:tc(fun() -> mnesia:transaction(fun() -> mnesia:write(TableName, {rec,Id,Item}, write) end) end),
    log_mks(create,Mks),
    ok.

%% ------------------
delete(TableName,Id) ->
    ?LOG('$trace',"Delete '~ts': ~120tp",[TableName,Id]),
    {Mks,{atomic,ok}} = timer:tc(fun() -> mnesia:transaction(fun() -> mnesia:delete(TableName,Id,write) end) end),
    log_mks(create,Mks),
    ok.

%% ------------------
clear(TableName) ->
    ?LOG('$trace',"Clear '~ts'",[TableName]),
    {Mks,{atomic,ok}} = timer:tc(fun() -> mnesia:clear_table(TableName) end),
    log_mks(create,Mks),
    ok.

%% ------------------
fold(Fun,Acc0,TableName) ->
    {Mks,Res} = timer:tc(fun() -> mnesia:ets(fun() -> ?StorageEts:fold(Fun,Acc0,{mnesia,TableName}) end) end),
    log_mks(create,Mks),
    Res.

%% ====================================================================
%% Internal functions
%% ====================================================================

log_mks(Operation,Mks) when Mks > 1000000 -> ?LOG('$warning',"DUR. Mnesia ~p ms on ~ts", [Mks div 1000,Operation]);
log_mks(Operation,Mks) when Mks > 100000 -> ?LOG('$info',"DUR. Mnesia ~p ms on ~ts", [Mks div 1000,Operation]);
log_mks(_,_) -> ok.
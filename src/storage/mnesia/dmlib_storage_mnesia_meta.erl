%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 30.03.2021
%%% @doc

-module(dmlib_storage_mnesia_meta).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([table_name/2,
         ensure_table/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------
%% Build table
%% -------------------------
table_name(Domain,ClassName) ->
    ?BU:to_atom_new(?BU:str("dom:~ts;cn:~ts",[Domain,binary:replace(ClassName,<<"/">>,<<"|">>,[global])])).

%% -------------------------
%% Make table in Mnesia for domain-model-class
%% -------------------------
ensure_table(Domain,ClassName,Meta) ->
    TableName = table_name(Domain,ClassName),
    Opts = #{'app_name' => ?APP,
             'app_version' => undefined,
             'load_timeout' => 20000,
             'mode' => case maps:get(storage_mode,Meta) of <<"runtime">> -> disc; <<"ram">> -> ram end,
             'update_function' => fun({_AppName,_AppVersion},Rec0) -> Rec0 end},
    case ensure_mnesialib_started() of
        ok ->
            case ?MNESIALIB:ensure_table(TableName, rec, [key,item], Opts) of
                ok -> ok;
                {error,Reason}=Err ->
                    ?LOG('$error',"~ts. '~ts' * '~ts' mnesia load table error: ~120tp...",[?APP,Domain,ClassName,Reason]),
                    Err;
                {retry_after,Timeout,Reason} ->
                    ?LOG('$info',"~ts. '~ts' * '~ts' wait for mnesia to ensure and load table... (~ts)",[?APP,Domain,ClassName,Reason]),
                    timer:sleep(Timeout),
                    ensure_table(Domain,ClassName,Meta)
            end;
        {retry_after,Timeout,Reason} ->
            ?LOG('$info',"~ts. '~ts' * '~ts' wait for mnesialib to start properly... (~ts)",[?APP,Domain,ClassName,Reason]),
            timer:sleep(Timeout),
            ensure_table(Domain,ClassName,Meta)
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

ensure_mnesialib_started() ->
    FunWait = case lists:keyfind(?MNESIALIB,1,application:which_applications()) of
                  {_,_,_} -> fun() -> ok end;
                  false -> fun() ->
                                F = fun F(I) when I =< 0 -> ok;
                                        F(I) ->
                                              case ?MNESIALIB:get_schema_status() of
                                                  'running' -> 0;
                                                  _ -> timer:sleep(100), F(I-1)
                                              end
                                    end, F(10)
                           end
              end,
    application:ensure_all_started(?BASICLIB),
    application:ensure_all_started(?MNESIALIB),
    FunWait(),
    case ?MNESIALIB:get_schema_status() of
        'running' -> ok;
        _ -> {retry_after, 5000, "wait for mnesia to sync schema"}
    end.
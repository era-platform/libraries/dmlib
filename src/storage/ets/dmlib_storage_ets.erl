%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 19.03.2021
%%% @doc

-module(dmlib_storage_ets).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([filter/3]).
-export([size/1]).
-export([find/2,find_ex/2,
         lookup/3, lookup_ex/3,
         read/4,
         create/2, create/3,
         replace/2, replace/3,
         update/2, update/3,
         delete/2,
         clear/1]).
-export([fold/3, fold_ex/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

filter(ETS,Expires,MaxCacheSize) ->
    % by expires
    NowTS = ?BU:timestamp(),
    ets:foldl(fun({K,{_,LastTouchTs,_}},_) when NowTS > LastTouchTs+Expires -> ets:delete(ETS,K);
                 (_,_) -> ok
              end, ok, ETS),
    % by size
    case ets:info(ETS,size) of
        L when L =< MaxCacheSize -> ok;
        L ->
            {ToDel,_} = lists:split(L - ?BU:to_int(MaxCacheSize*0.8),
                            lists:keysort(2,
                                ets:foldl(fun({K,{_,LastTouchTs,_}},Acc) ->  [{K,LastTouchTs}|Acc] end, [], ETS))),
            lists:foreach(fun({K,_}) -> ets:delete(ETS,K) end, ToDel)
    end.

%%
size(ETS) ->
    ets:info(ETS,size).

%% ---------------------------------------------
%% CRUD
%% ---------------------------------------------

find({mnesia,TabName},Id) -> find(TabName,Id,mnesia);
find(ETS,Id) -> find(ETS,Id,ets).
%% @private
find(ETS,Id,EtsModule) ->
    case EtsModule:lookup(ETS,Id) of
        [{_,{Item,_LastTouchTs,ExData}}] when is_map(Item), is_integer(_LastTouchTs) ->
            ets:insert(ETS,{Id,{Item,?BU:timestamp(),ExData}}), % !!! Mnesia doesn't have insert
            {ok,Item};
        [{rec,_,Item}] -> % mnesia
            {ok,Item};
        [] -> false
    end.

%% -------------
find_ex({mnesia,TabName},Id) -> find_ex(TabName,Id,mnesia);
find_ex(ETS,Id) -> find_ex(ETS,Id,ets).
%% @private
find_ex(ETS,Id,EtsModule) ->
    case EtsModule:lookup(ETS,Id) of
        [{_,{Item,_LastTouchTs,ExData}}] when is_map(Item), is_integer(_LastTouchTs) ->
            ets:insert(ETS,{Id,{Item,?BU:timestamp(),ExData}}), % !!! Mnesia doesn't have insert
            {ok,Item,ExData};
        [{rec,_,Item}] -> % mnesia
            {ok,Item,undefined};
        [] -> false
    end.

%% -------------
lookup(ETS,Content,LookupKeyField) when is_binary(LookupKeyField) ->
    Filter = [<<"==">>,[<<"property">>,LookupKeyField],Content],
    FilterFun = ?FilterFun:build_filter_fun(Filter),
    {Mks,LookupRes} = timer:tc(fun() -> do_lookup(ETS,FilterFun) end),
    log_mks(lookup,Mks),
    LookupRes;
lookup(ETS,Content,LookupKeyFields) when is_list(LookupKeyFields) ->
    LookupKeyFields1 = [<<"id">>|lists:delete(<<"id">>,LookupKeyFields)],
    Filter = [<<"||">> | [[<<"==">>,[<<"property">>,KeyField],Content] || KeyField <- LookupKeyFields1]],
    FilterFun = ?FilterFun:build_filter_fun(Filter),
    {Mks,LookupRes} = timer:tc(fun() -> do_lookup(ETS,FilterFun) end),
    log_mks(lookup,Mks),
    LookupRes.

%% @private
do_lookup(ETS,FilterFun) ->
    case apply_filter(ETS,FilterFun) of
        [] -> {ok,[]};
        [Item] -> {ok,[maps:get(<<"id">>,Item)]};
        Items -> {ok,[maps:get(<<"id">>,Item) || Item <- Items]}
    end.

%% -------------
lookup_ex(ETS,Content,LookupKeyField) when is_binary(LookupKeyField) ->
    Filter = [<<"==">>,[<<"property">>,LookupKeyField],Content],
    FilterFun = ?FilterFun:build_filter_fun(Filter),
    {Mks,LookupRes} = timer:tc(fun() -> do_lookup_ex(ETS,FilterFun) end),
    log_mks(lookup,Mks),
    LookupRes;
lookup_ex(ETS,Content,LookupKeyFields) when is_list(LookupKeyFields) ->
    LookupKeyFields1 = [<<"id">>|lists:delete(<<"id">>,LookupKeyFields)],
    Filter = [<<"||">> | [[<<"==">>,[<<"property">>,KeyField],Content] || KeyField <- LookupKeyFields1]],
    FilterFun = ?FilterFun:build_filter_fun(Filter),
    {Mks,LookupRes} = timer:tc(fun() -> do_lookup_ex(ETS,FilterFun) end),
    log_mks(lookup,Mks),
    LookupRes.

%% @private
do_lookup_ex(ETS,FilterFun) ->
    case apply_filter(ETS,FilterFun) of
        [] -> {ok,[]};
        [Item] -> {ok,[Item]};
        Items -> {ok,Items}
    end.

%% -------------
-spec read(ETS::ets:tab(), SelectOpts::map(), LimitOpts::map(), AggrCache::undefined | {ets:tab(), TTL::integer()})
      -> {ok,Res::[map()]}.
read(ETS,SelectOpts,LimitOpts,AggrCache) ->
    AggrMap = ?BU:get_by_key(<<"aggr">>,SelectOpts,#{}),
    GroupByMap = ?BU:get_by_key(<<"groupby">>,SelectOpts,#{}),
    case maps:size(AggrMap) + maps:size(GroupByMap) > 0 of
        false -> read_items(ETS,SelectOpts,LimitOpts);
        true -> read_aggr(ETS,SelectOpts,LimitOpts,AggrCache,AggrMap,GroupByMap)
    end.

%% -------------
create(ETS,Item) ->
    Id = maps:get(<<"id">>,Item),
    ets:insert(ETS,{Id,{Item,?BU:timestamp(),undefined}}),
    ok.

%% -------------
create(ETS,Item,ExData) ->
    Id = maps:get(<<"id">>,Item),
    ets:insert(ETS,{Id,{Item,?BU:timestamp(),ExData}}),
    ok.

%% -------------
replace(ETS,Item) ->
    Id = maps:get(<<"id">>,Item),
    ets:insert(ETS,{Id,{Item,?BU:timestamp(),undefined}}),
    ok.

%% -------------
replace(ETS,Item,ExData) ->
    Id = maps:get(<<"id">>,Item),
    ets:insert(ETS,{Id,{Item,?BU:timestamp(),ExData}}),
    ok.

%% -------------
update(ETS,Item) ->
    Id = maps:get(<<"id">>,Item),
    ets:insert(ETS,{Id,{Item,?BU:timestamp(),undefined}}),
    ok.

%% -------------
update(ETS,Item,ExData) ->
    Id = maps:get(<<"id">>,Item),
    ets:insert(ETS,{Id,{Item,?BU:timestamp(),ExData}}),
    ok.

%% -------------
delete(ETS,Id) ->
    ets:delete(ETS,Id),
    ok.

%% -------------
clear(ETS) ->
    ets:delete_all_objects(ETS),
    ok.

%% -------------
fold(Fun,Acc0,{mnesia,TabName}) -> fold(Fun,Acc0,TabName,mnesia);
fold(Fun,Acc0,ETS) -> fold(Fun,Acc0,ETS,ets).
%% @private
fold(Fun,Acc0,ETS,EtsModule) ->
    EtsModule:foldl(fun({_Id,{Item,_Timestamp,_ExData}}, Acc) -> Fun(Item,Acc);
                       ({rec,_,Item}, Acc) -> Fun(Item,Acc) % mnesia
                    end, Acc0, ETS).
%% -------------
fold_ex(Fun,Acc0,{mnesia,TabName}) -> fold_ex(Fun,Acc0,TabName,mnesia);
fold_ex(Fun,Acc0,ETS) -> fold_ex(Fun,Acc0,ETS,ets).
%% @private
fold_ex(Fun,Acc0,ETS,EtsModule) ->
    EtsModule:foldl(fun({_Id,{Item,_Timestamp,ExData}}, Acc) -> Fun({Item,ExData},Acc);
                       ({rec,_,Item}, Acc) -> Fun({Item,undefined},Acc) % mnesia
                    end, Acc0, ETS).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---------------------------------
%% Build select functions and values
%% ---------------------------------

%%
build_filter_fun(SelectOpts) ->
    case ?BU:get_by_key(<<"filter">>,SelectOpts,[]) of
        [] -> fun(_) -> true end;
        [_|_]=Filter -> ?FilterFun:build_filter_fun(Filter);
        _ -> fun(_) -> false end % TODO ICP: send error
    end.

%%
build_mask_fun(SelectOpts,[]) ->
    case ?BU:get_by_key(<<"mask">>,SelectOpts,[]) of
        [] -> fun(Item) -> Item end;
        Mask when is_list(Mask) -> fun(Item) -> maps:with(Mask,Item) end;
        Mask when is_binary(Mask) -> fun(Item) -> maps:with(binary:split(Mask,[<<",">>,<<";">>,<<" ">>],[global,trim_all]),Item) end;
        _ -> fun(Item) -> Item end % TODO ICP: send error
    end;
build_mask_fun(SelectOpts,MaxMask) ->
    MaxMask1 = [<<"id">>|MaxMask],
    case ?BU:get_by_key(<<"mask">>,SelectOpts,[]) of
        [] -> fun(Item) -> maps:with(MaxMask1,Item) end;
        Mask when is_list(Mask) -> fun(Item) -> maps:with(MaxMask1--MaxMask1--Mask,Item) end;
        Mask when is_binary(Mask) -> fun(Item) -> maps:with(MaxMask1--MaxMask1--binary:split(Mask,[<<",">>,<<";">>,<<" ">>],[global,trim_all]),Item) end;
        _ -> fun(Item) -> Item end % TODO ICP: send error
    end.

%%
build_order_fun(SelectOpts) ->
    case ?BU:get_by_key(<<"order">>,SelectOpts,[]) of
        [] -> fun(List) -> List end;
        SortFields when is_list(SortFields) ->
            Fcmp = fun(A,B,Fld,Less) ->
                        case {maps:get(Fld,A,n), maps:get(Fld,B,n)} of
                            {n,n} -> n;
                            {n,_} -> not (false xor Less);
                            {_,n} -> not (true xor Less);
                            {X,X} -> n;
                            {X,Y} -> not ((X<Y) xor Less)
                        end end,
            FI = fun(A,B) ->
                        FF = fun FF(Fld,n) when is_binary(Fld) -> Fcmp(A,B,Fld,true);
                                 FF(Map,n) when is_map(Map) ->
                                     case maps:keys(Map) of
                                         [] -> n;
                                         [_,_|_] -> n;
                                         [Fld] ->
                                             case maps:get(Fld,Map) of
                                                 <<"asc">> -> FF(Fld,n);
                                                 <<"desc">> -> Fcmp(A,B,Fld,false);
                                                 _ -> n
                                             end end;
                                 FF(_,Acc) -> Acc
                             end,
                        case lists:foldl(FF, n, SortFields) of
                            n -> true;
                            Bool -> Bool
                        end end,
            fun(List) -> lists:sort(FI,List) end;
        _ -> fun(List) -> List end % TODO ICP: send error
    end.

%%
build_limit(SelectOpts,MaxLimit) ->
    erlang:min(MaxLimit,build_limit_default(SelectOpts,MaxLimit)).
build_limit_default(SelectOpts,DefaultLimit) ->
    erlang:max(1,?BU:to_int(?BU:get_by_key(<<"limit">>,SelectOpts,DefaultLimit))).

%%
build_offset(SelectOpts) ->
    erlang:max(0,?BU:to_int(?BU:get_by_key(<<"offset">>,SelectOpts,0),0)).

%% ---------------------------------
%% Apply functions
%% ---------------------------------

%%
apply_filter({mnesia,TabName},FilterFun) -> apply_filter(TabName,FilterFun,mnesia);
apply_filter(ETS,FilterFun) -> apply_filter(ETS,FilterFun,ets).
%% @private
apply_filter(ETS,FilterFun,EtsModule) ->
    F = fun(Item,Acc) ->
                case FilterFun(Item) of
                    true -> [Item|Acc];
                    false -> Acc
                end end,
    EtsModule:foldl(fun({_K,{Item,_LastTouchTs,_}},Acc) -> F(Item,Acc);
                       ({rec,_K,Item},Acc) when is_map(Item) -> F(Item,Acc) % mnesia
                    end, [], ETS).

%%
apply_order(List,OrderFun) ->
    OrderFun(List).

%%
apply_offset_limit(List,0,Limit) ->
    case length(List) of
        Len when Len>Limit ->
            {Res,_} = lists:split(Limit,List),
            Res;
        _ -> List
    end;
apply_offset_limit(List,Offset,Limit) ->
    case length(List) of
        Len when Len>Offset ->
            apply_offset_limit(lists:nthtail(Offset,List),0,Limit);
        _ -> []
    end.

%%
apply_mask(List,MaskFun) ->
    [MaskFun(Item) ||Item <- List].

%% -----------------------------------------------------
%% Read functions
%% -----------------------------------------------------

%% @private
read_items(ETS,SelectOpts,LimitOpts) ->
    FilterFun = case build_filter_fun(SelectOpts) of
                    {error,_}=Err -> throw(Err);
                    FFun -> FFun
                end,
    case ?BU:to_bool(?BU:get_by_key(<<"countonly">>,SelectOpts,false)) of
        false ->
            MaskFun = build_mask_fun(SelectOpts,?BU:get_by_key(max_mask,LimitOpts,?DefaultMaxMask)),
            OrderFun = build_order_fun(SelectOpts),
            Limit = build_limit(SelectOpts,?BU:get_by_key(max_limit,LimitOpts,?DefaultMaxLimit)),
            Offset = build_offset(SelectOpts),
            % apply
            {Mks,ReadRes} = timer:tc(fun() -> apply_mask(apply_offset_limit(apply_order(apply_filter(ETS,FilterFun),OrderFun),Offset,Limit),MaskFun) end),
            log_mks(read,Mks),
            {ok,ReadRes};
        true ->
            Length = case ?BU:get_by_key(<<"filter">>,SelectOpts,[]) of
                         [] ->
                             case ETS of
                                 {mnesia,TabName} -> ets:info(TabName,size); % mnesia
                                 _ -> ets:info(ETS,size) % ets
                             end;
                         _ -> length(apply_filter(ETS,FilterFun))
                     end,
            Limit = case ?BU:get_by_key(<<"limit">>,SelectOpts,undefined) of undefined -> undefined; V -> ?BU:to_int(V,undefined) end,
            Offset = ?BU:to_int(?BU:get_by_key(<<"offset">>,SelectOpts,0),0),
            {ok,#{<<"count">> => erlang:min(Length - Offset,Limit)}}
    end.

%% ---------------------------------------
%% @private
read_aggr(ETS,SelectOpts,LimitOpts,undefined,AggrMap,GroupByMap) ->
    do_read_aggr(ETS,SelectOpts,LimitOpts,AggrMap,GroupByMap);
read_aggr(ETS,SelectOpts,LimitOpts,{AggrCacheEts,ExpireMs,FunRegTimer},AggrMap,GroupByMap) ->
    Key = {req_phash,erlang:phash2(SelectOpts),erlang:phash2(AggrMap)},
    NowTS = ?BU:timestamp(),
    FunRead = fun() ->
                    NewValue = do_read_aggr(ETS,SelectOpts,LimitOpts,AggrMap,GroupByMap),
                    Ref = FunRegTimer(Key), % call to start timer (by key) and return ref to store in value for timer guard
                    ets:insert(AggrCacheEts, {Key, {NewValue,NowTS,Ref}}),
                    NewValue
              end,
    case ets:lookup(AggrCacheEts, Key) of
        [{_,{ValueFromCache,TS,_}}] when TS + ExpireMs > NowTS ->
            case TS + ExpireMs > NowTS of
                true -> ValueFromCache;
                false -> FunRead()
            end;
        _ -> FunRead()
    end.

%% @private
do_read_aggr(ETS,SelectOpts,LimitOpts,AggrMap,GroupByMap) ->
    % filter fun
    FilterFun = case build_filter_fun(SelectOpts) of
                    {error,_}=Err -> throw(Err);
                    FFun -> FFun
                end,
    % aggregates counter fun
    _AggrFieldNames = extract_aggr_fields(AggrMap,[]),
    Aggrs = maps:fold(fun(AggrFieldName,AggrExpr,Acc) ->
                            case ?FilterFun:build_aggregation_fun(AggrExpr) of
                                {error,_}=Err2 -> throw(Err2);
                                {FunAggr,Acc0} -> [{AggrFieldName,FunAggr,Acc0}|Acc]
                            end end, [], AggrMap),
    % group by hash fun
    GroupByFuns = maps:map(fun(K,K) -> fun(Item) -> maps:get(K,Item) end;
                              (_K,V) when is_binary(V) -> fun(Item) -> maps:get(V,Item) end;
                              (_K,V) when is_list(V) -> ?FilterFun:build_expression_fun(V)
                           end, GroupByMap),
    GroupByFun = fun(Item) ->
                    maps:fold(fun(K,Fun,Acc) ->
                                    Acc#{K => Fun(Item)}
                              end, #{}, GroupByFuns)
                 end,
    % post order fun
    OrderFun = build_order_fun(SelectOpts),
    %
    Limit = build_limit_default(SelectOpts,?BU:get_by_key(max_limit,LimitOpts,?DefaultMaxLimit)),
    Offset = build_offset(SelectOpts),
    %
    {Mks,ReadRes} = timer:tc(fun() -> apply_offset_limit(apply_order(apply_aggregation(ETS,FilterFun,GroupByFun,Aggrs),OrderFun),Offset,Limit) end),
    log_mks(read_aggr,Mks),
    {ok,ReadRes}.

%% @private
extract_aggr_fields(AggrMap,PropNamesByMask) ->
    F = fun F([<<"property">>,PropName|_], Acc) ->
                case lists:member(PropName,PropNamesByMask) of
                    true -> ordsets:add_element(PropName,Acc);
                    false when PropNamesByMask==[] -> ordsets:add_element(PropName,Acc);
                    false -> throw({error,{invalid_params,?BU:strbin("Invalid aggr. Invalid property: '~ts'. Expected one of masked fields",[PropName])}})
                end;
            F([_|Rest], Acc) -> lists:foldl(fun(Elt, Acc1) -> F(Elt,Acc1) end, Acc, Rest);
            F(_, Acc) -> Acc
        end,
    maps:fold(fun(_K,V,Acc) -> F(V,Acc) end, [], AggrMap).

%% @private
apply_aggregation({mnesia,TabName},FilterFun,GroupByFun,Aggregates0) -> apply_aggregation(TabName,FilterFun,GroupByFun,Aggregates0);
apply_aggregation(ETS,FilterFun,GroupByFun,Aggregates0) -> apply_aggregation(ETS,FilterFun,GroupByFun,Aggregates0,ets).
%% @private
apply_aggregation(ETS,FilterFun,GroupByFun,Aggregates0,EtsModule) ->
    F = fun(Item,AccComplex) ->
            case FilterFun(Item) of
                false -> AccComplex;
                true ->
                    X = GroupByFun(Item),
                    Fagg = fun(AggregatesX) ->
                                AccComplex#{X => [{AggrFieldName, FunAggr, FunAggr(Item,AccX)} || {AggrFieldName,FunAggr,AccX} <- AggregatesX]}
                           end,
                    case maps:get(X,AccComplex,undefined) of
                        undefined -> Fagg(Aggregates0);
                        Aggregates1 -> Fagg(Aggregates1)
                    end end end,
    MapRes = EtsModule:foldl(fun({_K,{Item,_,_}},Acc) -> F(Item,Acc);
                                ({rec,_K,Item},Acc) when is_map(Item) -> F(Item,Acc) % mnesia
                             end, #{}, ETS),
    maps:fold(fun(X,AggregatesX,Acc) ->
                    R = [{AggrFieldName,?FilterFun:finalize_aggregation_accumulator(AccX)} || {AggrFieldName,_FunAggr,AccX} <- AggregatesX],
                    [maps:merge(X,maps:from_list(R)) | Acc]
              end, [], MapRes).

%% ====================================================================
%% Internal functions
%% ====================================================================

log_mks(Operation,Mks) when Mks > 1000000 -> ?LOG('$warning',"DUR. Ets ~p ms on ~ts", [Mks div 1000,Operation]);
log_mks(Operation,Mks) when Mks > 100000 -> ?LOG('$info',"DUR. Ets ~p ms on ~ts", [Mks div 1000,Operation]);
log_mks(_,_) -> ok.
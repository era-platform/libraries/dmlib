%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 07.05.2021
%%% @doc

-module(dmlib_storage_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([aggr_parse_mask/3]).

-export([
    shrink_tablename/2,
    build_hc/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

aggr_parse_mask(SelectOpts,LimitOpts,ThrowIfNotSet) ->
    MaxMask = ?BU:get_by_key(max_mask,LimitOpts,?DefaultMaxMask),
    case ?BU:get_by_key(<<"mask">>,SelectOpts,[]) of
        [] when ThrowIfNotSet ->
            throw({error,{invalid_params,?BU:strbin("Parameter 'mask' should be defined explicitly when 'aggr' is used",[])}});
        [] -> MaxMask;
        Mask when is_list(Mask), MaxMask==[] -> Mask;
        Mask when is_list(Mask) ->
            Mask -- Mask -- MaxMask;
        Mask when is_binary(Mask), MaxMask==[] ->
            binary:split(Mask,[<<",">>,<<";">>,<<" ">>],[global,trim_all]);
        Mask when is_list(Mask) ->
            Mask = binary:split(Mask,[<<",">>,<<";">>,<<" ">>],[global,trim_all]),
            Mask -- Mask -- MaxMask
    end.

%% -------------------------------------------
%% Shrinks table name limited by MaxSize (postgresql has limit of 63 bytes)
%% MaxSize is set by module
%% -------------------------------------------
shrink_tablename(Original,MaxSize) ->
    Key = {shrinked_tablename,MaxSize,Original},
    case size(Original) of
        S when S < MaxSize -> Original; % pg has max of 63 bytes
        _ ->
            case get(Key) of
                undefined ->
                    Res = shrink_complex(Original,MaxSize),
                    put(Key,Res),
                    Res;
                Res when is_binary(Res) -> Res
            end end.

%% @private
%%   "a/b/c" -> no change
%%   "callcenter/tasks/archives/connections/versions/blablabla" (48) -> "callcenter/tasks/archives/connections/ve..!yAn1E"
%%   "callcenter/tasks/archives/connections/versions/blablabla" (38) -> "callcenter/tasks/archives/conn..!yAn1E"
shrink_complex(Original,MaxSize) ->
    HCB = build_hc(erlang:phash2(Original)),
    Cut = binary:part(Original,0,MaxSize-3-size(HCB)),
    <<Cut/binary,"..!",HCB/binary>>.

%%%% @private
%%%% Non used
%%%%   "a/b/c" -> no change
%%%%   "callcenter/tasks/archives/connections/versions/blablabla" -> no change
%%%%   "callcenter/tasks/archives/connections/versions/blablabla/representatives" -> "call../tasks/arch../conn../vers../blab../repr..!Y7bE3"
%%shrink_by_parts(Original,MaxSize) ->
%%    HCB = build_hc(erlang:phash2(Original)),
%%    Parts = binary:split(Original,<<"/">>,[global]),
%%    Len = length(Parts),
%%    HCBsize = size(HCB),
%%    PartSize = (MaxSize - (3 * Len - 1) - (HCBsize + 1)) div Len,
%%    Parts1 = lists:foldl(fun(Part,Acc) when size(Part) =< PartSize + 2 -> [Part|Acc];
%%                            (Part,Acc) -> P1 = binary:part(Part,0,PartSize), [<<P1/binary,"..">>|Acc]
%%                         end, [], Parts),
%%    <<(?BU:join_binary(lists:reverse(Parts1),<<"/">>))/binary,"!",HCB/binary>>.

%% @private
build_hc(Bin) when is_binary(Bin) -> build_hc(erlang:phash2(Bin));
build_hc(I) when is_integer(I) -> build_hc(I,<<>>).
%% @private
build_hc(0,Acc) -> Acc;
build_hc(I,Acc) ->
    Code = code(I rem 62),
    build_hc(I div 62, <<Acc/binary,Code>>).

%% @private
code(I) when I < 10 -> 48 + I;
code(I) when I < 36 -> 97 + I-10;
code(I) -> 65 + I-36.

%% ====================================================================
%% Internal functions
%% ====================================================================
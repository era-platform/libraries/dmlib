%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.06.2024
%%% @doc In-domain data-model-class server-process for queued operation of dmsquery
%%       Opts:
%%%         domain, classname, class_meta, sync_ref

-module(dmlib_query_worker_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([cast_workf/2]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(state, {
    index :: integer()
}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link(?MODULE, Opts, []).

cast_workf(Pid,Fun) when is_pid(Pid), is_function(Fun,0) ->
    gen_server:cast(Pid, {workf,Fun}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(_Opts) ->
    State = #state{index=0},
    ?LOG('$info', "~ts. query inited", [?APP]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------
handle_cast({workf,F}, State) ->
    try F()
    catch C:R:ST -> ?LOG('$error', "DMS query caught: ~120tp ",[{C,R,ST}])
    end,
    {noreply, State};

handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.06.2024
%%% @doc Domain worker supervisor for in-storage operations

-module(dmlib_query_worker_manager_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behavior(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2]).
-export([workf/2]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(state, {
    free = [],
    used = #{},
    queue = [],
    lastindex = 0
}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link({local,?MODULE}, ?MODULE, Opts, []).

%%
workf(Domain, Fun) ->
    gen_server:cast(?MODULE, {workf,Domain,Fun}).

%% ====================================================================
%% Callbacks
%% ====================================================================

init(_Opts) ->
    State = #state{},
    {ok,State}.

%% -----------------
handle_call(_, _, State) ->
    {noreply, State}.

%% -----------------
%% on new work
handle_cast({workf,Domain,_Fun}=Work, #state{used=Used,queue=Queue}=State) ->
    Max = ?CFG:query_workers_count(),
    case maps:size(Used) < Max of
        true when Queue/=[] ->
            [QueueOne|RestQueue] = Queue,
            {_Result, State1} = run(QueueOne, State#state{queue=RestQueue}),
            handle_cast(Work, State1);
        true ->
            {_Result, State1} = run(Work, State),
            {noreply, State1};
        false ->
            ?LOG('$info',"Query queued (domain: '~ts', used: ~p, queuelen: ~p)",[Domain, maps:size(Used), length(Queue)]),
            {noreply, State#state{queue=Queue ++ [Work]}}
    end;

%% on worker finished work
handle_cast({finished,FreeOnePid}, #state{free=Free,used=Used,queue=[]}=State) ->
    {noreply, State#state{free=[FreeOnePid|Free],
                          used=maps:without([FreeOnePid],Used)}};
handle_cast({finished,FreeOnePid}, #state{free=Free,queue=[QueueOne|RestQueue]}=State) ->
    do_run(FreeOnePid, QueueOne),
    {noreply, State#state{free=[FreeOnePid|Free],queue=RestQueue}};

%% changed option decreased
%% changed option increased
%% free -> handle queue

%% other
handle_cast(_, State) ->
    {noreply, State}.

%% -----------------
handle_info({'DOWN',_MonRef,process,Pid,_Reason}, #state{free=Free,used=Used,queue=Queue}=State) ->
    Free1 = Free -- [Pid],
    Used1 = Used -- [Pid],
    State1 = State#state{free=Free1,used=Used1},
    case Queue of
        [] -> {noreply,State1};
        [QueueOne|RestQueue] ->
            [QueueOne|RestQueue] = Queue,
            {_Result, State2} = run(QueueOne, State1#state{queue=RestQueue}),
            {noreply,State2}
    end;

handle_info(_, State) ->
    {noreply, State}.

%% -----------------
terminate(_, _State) ->
    ok.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% apply work
run({workf,Domain,_Fun}=Work, #state{free=[],used=Used,lastindex=LastIndex}=State) ->
    case start_free_one(LastIndex+1) of
        {ok, FreeOnePid} ->
            do_run(FreeOnePid, Work),
            {ok, State#state{used=Used#{FreeOnePid => ok}, lastindex=LastIndex+1}};
        Res ->
            ?LOG('$error', "DMS query in domain '~ts' failure on worker start: ~120tp.", [Domain,Res]),
            {failure, State}
    end;
run(Work, #state{free=[FreeOnePid|RestFree], used=Used}=State) ->
    do_run(FreeOnePid, Work),
    {ok, State#state{free=RestFree, used=Used#{FreeOnePid => ok}}}.

%% @private
do_run(FreeOnePid, {workf,_Domain,Fun}) ->
    Self = self(),
    Fun1 = fun() ->
                catch Fun(), % throw of expired should be catched to avoid loss of 'finished'
                gen_server:cast(Self, {finished,FreeOnePid})
           end,
    ?QueryWorker:cast_workf(FreeOnePid, Fun1).

 %% @private
start_free_one(NewIndex) ->
    Key = ?BU:to_atom_new(?BU:strbin("qworker#~2..0B",[NewIndex])),
    ChildSpec = {Key, {?QueryWorker, start_link, [[]]}, permanent, 1000, worker, [?QueryWorker]},
    case ?QueryWorkerSupv:start_child(ChildSpec) of
        {ok,Pid} ->
            _MonRef = erlang:monitor(process,Pid),
            {ok,Pid};
        ignore -> ignore;
        {error,_}=Err -> Err
    end.

%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2024 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 16.05.2024
%%% @doc

-module(dmlib_increment_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([push/2, pull/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(state, {
    ets :: ets:tab(),
    ref :: reference(),
    is_applied = true :: boolean()
}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, Opts, []).

%% push last increment value to storage by async queue
push(Key,Value) ->
    gen_server:cast(?MODULE, {push,Key,Value}).

%% return last increment value from local storage (queue to push)
pull(Key) ->
    gen_server:call(?MODULE, {pull,Key}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(_) ->
    State = #state{ets = ets:new(push_ets, [set]),
                   is_applied = true},
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call({pull,Key}, _From, #state{ets=Ets}=State) ->
    Reply = case ets:lookup(Ets,Key) of
                [] -> 0;
                [{_,Value}] -> Value
            end,
    {reply, {ok,Reply}, State};

%% other
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({push,Key,Value}, #state{ets=Ets,is_applied=true}=State) ->
    ets:insert(Ets,{Key,Value}),
    Ref1 = make_ref(),
    gen_server:cast(self(), {apply,Ref1}),
    {noreply, State#state{ref=Ref1,is_applied=false}};

handle_cast({push,Key,Value}, #state{ets=Ets,is_applied=false}=State) ->
    ets:insert(Ets,{Key,Value}),
    {noreply, State};

handle_cast({apply,Ref}, #state{ref=Ref}=State) ->
    do_apply_push(State),
    {noreply, State#state{is_applied=true}};

%% other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

do_apply_push(#state{ets=Ets}) ->
    case ?CFG:store_function() of
        undefined -> ets:delete_all_objects(Ets);
        Fun when is_function(Fun,2) ->
            PushedKeys =
                ets:foldl(fun({Key,Value},Acc) ->
                            case Fun(push,{Key,Value}) of
                                ok -> [Key|Acc];
                                _ -> Acc
                            end end, [], Ets),
            case ets:info(Ets,size) == length(PushedKeys) of
                true -> ets:delete_all_objects(Ets);
                false -> lists:foreach(fun(Key) -> ets:delete(Ets,Key) end, PushedKeys)
            end
    end.
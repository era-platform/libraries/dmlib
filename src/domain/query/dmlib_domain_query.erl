%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 06.12.2022
%%% @doc

-module(dmlib_domain_query).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([execute/3, execute/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% TODO: make async over worker threads

execute({query,Args},FunReply,Domain) ->
    execute({query,Args},FunReply,Domain,60000).

execute({query,Args},FunReply,Domain,SyncTimeout) ->
    case ?BU:get_by_key(<<"sqlText">>,Args,<<>>) of
        <<>> -> FunReply({error,{invalid_params,<<"Invalid 'sqlText'. Expected non empty string">>}});
        SqlText when is_binary(SqlText) ->
            AutoCancelTS = ?BU:get_by_key(autocancel_ts,Args,0),
            ?QueryWorkerMan:workf(Domain,
                fun() ->
                    % Check canceled from queue
                    case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of
                        true ->
                            ?LOG('$warning', "Query expired in queue (domain '~ts')", [Domain]),
                            %FunReply({error,{timeout,<<"Expired in queue">>}}), % caller is not waiting for response already, it got timeout too and send response itself
                            throw({error,{timeout,<<"Expired in queue">>}});
                        _ -> ok
                    end,
                    % Work
                    DbType = <<"postgresql">>,
                    StorageName = ?BU:get_by_key(<<"storageInstance">>,Args,<<"auto">>),
                    case re:run(StorageName, <<"_readonly$">>, [global]) of
                        nomatch ->
                            case ?Postgres:get_db_params(Domain,{instance,<<StorageName/binary,"_readonly">>}) of
                                {ok,{_,_,DbParams}} -> FunReply(execute(Domain,false,DbType,DbParams,SqlText,SyncTimeout));
                                {error,_} ->
                                    case ?Postgres:get_db_params(Domain,{instance,StorageName}) of
                                        {ok,{_,_,DbParams}} -> FunReply(execute(Domain,true,DbType,DbParams,SqlText,SyncTimeout));
                                        {error,_}=Err2 -> FunReply(Err2)
                                    end
                            end;
                        _ ->
                            case ?Postgres:get_db_params(Domain,{instance,StorageName}) of
                                {ok,{_,_,DbParams}} -> FunReply(execute(Domain,false,DbType,DbParams,SqlText,SyncTimeout));
                                {error,_}=Err -> FunReply(Err)
                            end
                    end
                end)
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
%% without ensure/create readonly credentials
%% -----------------------
%% Update login and pwd by readdonly user
%%   build readonly user login and pwd
%%   connect
%%     check functions
%%       create functions
%%     read temp users
%%       check existing readonly user login and pwd
%%         delete login
%%         create readonly login
%%   disconnect
%%   replace login, pwd options in DbParams
%% -----------------------
execute(Domain,true,DbType,DbParams,SqlText,SyncTimeout) ->
    case get_readonly_credentials(Domain,DbType,DbParams) of
        false ->
            do_execute_query(DbParams,SqlText,max(10,SyncTimeout));
        {ok,Login,Pwd} ->
            DbParams1 = replace_credentials(DbParams,Login,Pwd),
            %io:format("DbParams1: ~120tp~n",[DbParams1]),
            Res0 = do_execute_query(DbParams1,SqlText,max(10,SyncTimeout)),
            Frerun = fun() ->
                        ?LOG('$info',"Ensure credentials: ~120tp",[Login]),
                        case ensure_readonly_credentials(DbParams,Login,Pwd) of
                            ok ->
                                case do_execute_query(DbParams1,SqlText,max(10,SyncTimeout)) of
                                    {error,_}=Err -> hide_error(Err);
                                    ResX -> ResX
                                end;
                            {error,_}=Err -> hide_error(Err);
                            ResX -> ResX
                        end end,
            case Res0 of
                {error,invalid_password}=Err ->
                    ?LOG('$error',"Postgresql query error: ~120tp. Ensure user, rerun",[Err]),
                    Frerun();
                {error,{error,fatal,<<"28000">>,invalid_authorization_specification,_,_}}=Err ->
                    ?LOG('$error',"Postgresql query error: ~120tp. Ensure user, rerun",[Err]),
                    Frerun();
                {error,<<"{",_/binary>>}=Err ->
                    case parse_error(Err) of
                        {ok,Map} when is_map(Map) ->
                            case ?BU:to_int(maps:get(<<"code">>,Map,0),0) of
                                42501 ->
                                    % insufficient_privilege. I've seen that it hangs in connection for permitted requests after it once resulted
                                    Frerun();
                                _ -> hide_error(Err)
                            end;
                        _ -> hide_error(Err)
                    end;
                {error,_}=Err -> hide_error(Err);
                _ -> Res0
            end
    end;
%% without ensure/create readonly credentials. Use direct connection named *_readonly
execute(_Domain,false,_DbType,DbParams,SqlText,SyncTimeout) ->
    Res0 = do_execute_query(DbParams,SqlText,max(10,SyncTimeout)),
    case Res0 of
        {error,invalid_password}=Err ->
            hide_error({error,_}=Err);
        {error,{error,fatal,<<"28000">>,invalid_authorization_specification,_,_}}=Err ->
            hide_error({error,_}=Err);
        {error,<<"{",_/binary>>}=Err ->
            case parse_error(Err) of
                {ok,Map} when is_map(Map) ->
                    case ?BU:to_int(maps:get(<<"code">>,Map,0),0) of
                        42501 -> % insufficient_privilege. I've seen that it hangs in connection for permitted requests after it once resulted
                            hide_error(Err);
                        _ -> hide_error(Err)
                    end;
                _ -> hide_error(Err)
            end;
        {error,_}=Err -> hide_error(Err);
        _ -> Res0
    end.

%% @private
hide_error({error,_}=Err) ->
    ?LOG('$error',"Postgresql query error: ~120tp",[Err]),
    {error,{internal_error,?BU:strbin("Query failed. See logs in '~ts'",[node()])}}.

%% @private
get_readonly_credentials(Domain,DbType,DbParams) ->
    case ?CFG:get_readonly_credentials_function() of
        Fun when is_function(Fun,3) ->
            case Fun(Domain,DbType,DbParams) of
                false -> false;
                {ok,Login,Pwd} -> {ok,Login,Pwd}
            end;
        _ -> false
    end.

%% @private
ensure_readonly_credentials(DbParams,Login,Pwd) ->
    Timeout = 10000,
    case timer:tc(fun() -> ?PgConnectionPool:get_connection(DbParams,Timeout) end) of
        {_Mks1,{ok,Conn}} ->
            SqlText1 = sql_functions(DbParams),
            {_Mks2,_ResA} = timer:tc(fun() -> (catch ?PgDBA:query_db_tout(Conn,SqlText1,Timeout)) end),
            ?LOG('$info', "Ensure user functions: ~120tp",[_ResA]),
            SqlText2 = sql_ensure_login(Login,Pwd),
            {_Mks3,_ResB} = timer:tc(fun() -> (catch ?PgDBA:query_db_tout(Conn,SqlText2,Timeout)) end),
            ?LOG('$info', "Ensure user role: ~120tp",[_ResB]),
            {_Mks4,ok} = timer:tc(fun() -> ?PgConnectionPool:release_connection(DbParams,Conn) end),
            ok;
        {_,{error,timeout}} -> {error,{service_unavailable,<<"Connection pool is overloaded. Try later.">>}};
        {_,{error,_}=Err} -> Err
    end.

%% @private
replace_credentials(DbParams,Login,Pwd) ->
    F = fun F(Map) when is_map(Map) -> Map#{login => Login, pwd => Pwd};
            F([Map|_]=List) when is_map(Map) -> lists:map(fun(MapX) -> F(MapX) end, List);
            F([Tuple|_]=P) when is_tuple(Tuple) -> F(maps:from_list(P));
            F([[Tuple|_]|_]=List) when is_tuple(Tuple) -> lists:map(fun(PX) -> F(maps:from_list(PX)) end, List)
        end,
    F(DbParams).

%% -----------------------------------------
%% Execute query request to postgre db
%% Means report usage
%% -----------------------------------------
-spec do_execute_query(DbParams::map()|[map()], SqlText::binary(), SyncTimeout::non_neg_integer()) -> {ok,Data::list()} | {error,Reason::term()}.
%% -----------------------------------------
do_execute_query(DbParams,SqlText,SyncTimeout) ->
    % Select query
    SelectSql = ?BU:to_list(SqlText),
    ?LOG('$trace',"Query SQL: ~ts", [?BU:to_binary(SelectSql)]),
    ?LOG('$trace',"DbParams: ~120tp", [DbParams]),
    case timer:tc(fun() -> ?PgConnectionPool:get_connection(DbParams,SyncTimeout) end) of
        {Mks1,{ok,Conn}} ->
            ?LOG('$trace',"Conn: ~120tp",[Conn]),
            case SyncTimeout - Mks1 div 1000 of
                Timeout1 when Timeout1 > 0 ->
                    {Mks2,ResA} = timer:tc(fun() -> (catch ?PgDBA:query_db_tout(Conn,SelectSql,Timeout1)) end),
                    flush_connection(Conn,ResA), % sometimes need to destroy connection to flush postgresql state (after insufficient_privilege)
                    {Mks3,ok} = timer:tc(fun() -> ?PgConnectionPool:release_connection(DbParams,Conn) end),
                    ?PgU:log_mks(query,Mks1,Mks2,Mks3),
                    ?LOG('$trace',"ResA: ~120tp",[ResA]),
                    Fresp = fun({ok,Cols,Values}=Response) ->
                                    Len = length(Values),
                                    Fun = fun() ->
                                            Values1 = case Len > 1000 of
                                                          true -> lists:sublist(Values,1000);
                                                          false -> Values
                                                      end,
                                            case [{Name,ColType} || {column,Name,ColType,_,_,_,_,_,_} <- Cols] of
                                                Cols1 ->
                                                    Rows = lists:map(fun(Value) ->
                                                                        Row = lists:zip(Cols1,erlang:tuple_to_list(Value)),
                                                                        maps:from_list(lists:map(fun({{Name,Type},Val}) -> {Name,build_value(Type,Val)} end, Row))
                                                                     end, Values1),
                                                    ?LOG('$trace',"Reply: ~120tp",[Response]),
                                                    {ok,Rows}
                                            end end,
                                    {reductions,Reductions0} = process_info(self(), reductions),
                                    {Mks,Result} = timer:tc(Fun),
                                    {reductions,Reductions1} = process_info(self(), reductions),
                                    check_duration(Mks,Len,SelectSql,Reductions1-Reductions0),
                                    Result
                            end,
                    case ResA of
                        {ok,Cols,Values}=Response when is_list(Cols), is_list(Values) -> Fresp(Response);
                        [_|_]=Response ->
                            case lists:last(Response) of
                                {ok,Cols,Values}=Response1 when is_list(Cols), is_list(Values) -> Fresp(Response1);
                                _ -> {error,{invalid_request,<<"Invalid request. Expected select query.">>}}
                            end;
                        {error,_}=Err -> ?PgU:try_transform_err(Err);
                        {'EXIT',Reason} ->
                            ?LOG('$error',"Connection pid ~tp found down: ~120tp",[Conn,Reason]),
                            {error,{service_unavailable,<<"Connection error">>}}
                    end;
                _ ->
                    ?PgConnectionPool:release_connection(DbParams,Conn),
                    {error,{service_unavailable,<<"Connection pool is overloaded. Try later.">>}}
            end;
        {_,{error,timeout}} -> {error,{service_unavailable,<<"Connection pool is overloaded. Try later.">>}};
        {_,{error,_}=Err} -> Err
    end.

%% @private
build_value(bool,Val) -> ?BU:to_bool(Val,Val);
build_value(int2,Val) -> ?BU:to_int(Val,Val);
build_value(int4,Val) -> ?BU:to_int(Val,Val);
build_value(int8,Val) -> ?BU:to_int(Val,Val);
build_value(float4,Val) -> ?BU:to_float(Val,Val);
build_value(float8,Val) -> ?BU:to_float(Val,Val);
build_value(_,Val) -> Val.

%%     {bpchar, 1042, 1014},
%%     {bytea, 17, 1001},
%%     {char, 18, 1002},
%%     {cidr, 650, 651},
%%     {date, 1082, 1182},
%%     {daterange, 3912, 3913},
%%     %% {geometry, 17063, 17071},
%%     %% {hstore, 16935, 16940},
%%     {inet, 869, 1041},
%%     {int4range, 3904, 3905},
%%     {int8range, 3926, 3927},
%%     {interval, 1186, 1187},
%%     {json, 114, 199},
%%     {jsonb, 3802, 3807},
%%     {macaddr, 829, 1040},
%%     {macaddr8, 774, 775},
%%     {point, 600, 1017},
%%     {text, 25, 1009},
%%     {time, 1083, 1183},
%%     {timestamp, 1114, 1115},
%%     {timestamptz, 1184, 1185},
%%     {timetz, 1266, 1270},
%%     {tsrange, 3908, 3909},
%%     {tstzrange, 3910, 3911},
%%     {uuid, 2950, 2951},
%%     {varchar, 1043, 1015}

%% @private
flush_connection(Conn,Res) ->
    case parse_error(Res) of
        {ok,Map} when is_map(Map) ->
            case ?BU:to_int(maps:get(<<"code">>,Map,0),0) of
                42501 ->
                    % insufficient_privilege.
                    % I've seen that it hangs in connection for permitted requests after it once resulted
                    % But is so only if error is binary formatted. Tuple 42501 is not so
                    ?LOG('$warning', "Flushing connection ~120tp due 42501, error: ~120tp", [Conn, Res]),
                    erlang:exit(Conn,kill),
                    true;
                _ -> false
            end;
        _ -> false
    end.

%% @private
parse_error({error,<<"{",_/binary>>=Bin}=Err) ->
    try jsx:decode(Bin,[return_maps]) of
        JSON when is_map(JSON) -> {error,JSON};
        _ -> Err
    catch _:_ -> Err
    end;
parse_error({error,_}=Err) -> Err;
parse_error(Other) -> Other.

%% @private
check_duration(Mks,_,_,_) when Mks =< 100000 -> ok;
check_duration(Mks,RowCount,SelectSql,Reductions) ->
    LogLevel = case Mks > 1000000 of
                   true -> '$warning';
                   false -> '$info'
               end,
    ?LOG(LogLevel,"DUR. Build ~p ms after query (rows: ~p, reds: ~p)", [Mks div 1000, RowCount, Reductions]),
    case ?BLlog:get_loglevel() of
        'TRACE' -> ok;
        'DEBUG' -> ok;
        _ -> ?LOG(LogLevel,"DUR. Query SQL: ~n~ts~n", [SelectSql])
    end.

%% ==============================================================
%% @private
sql_functions(DbParams) ->
    Priv = code:priv_dir(?APP),
    {ok,Data} = file:read_file(filename:join(Priv,"user_functions.sql")),
    F = fun(Map) when is_map(Map) -> maps:get(login,Map);
           ([Map|_]) when is_map(Map) -> maps:get(login,Map);
           ([Tuple|_]=P) when is_tuple(Tuple) -> ?BU:get_by_key(login,P);
           ([[Tuple|_]=P|_]) when is_tuple(Tuple) -> ?BU:get_by_key(login,P)
        end,
    Login = F(DbParams),
    binary:replace(
        binary:replace(Data,<<"{OWNER}">>,?BU:to_binary(Login),[global]),
                            <<"{SCHEMA}">>,?PostgresqlSchema,[global]).

%% @private
sql_ensure_login(Login,Pwd) ->
    ?BU:strbin("SELECT * FROM datamodel.ensure_readonly_user('~ts','~ts','~ts')",[Login,Pwd,Pwd]). % rolpassword Pwd like 'md5...'
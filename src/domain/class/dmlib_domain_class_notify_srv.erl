%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 12.03.2021
%%% @doc In-domain data-model-class server for queued operations of subscribers notify
%%       Opts:
%%%         domain, classname, class_meta, sync_ref

-module(dmlib_domain_class_notify_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([notify_subscribers/3,
         drop_subscriptions/2]).

-export([suspend_notification/2,
         resume_notification/3]).

-export([print_stat/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-define(SlowingQueueLen0, 2000). % lower len of queue to restore performance. (500)
-define(SlowingQueueLen1, 5000). % upper len of queue to reduce performance. (1000)
-define(SlowingTimeout, 10). % pause to sleep on every request before pushing to workers

%% state for helper processes of domain class otp-tree
-record(nstate, {
    facade_regname :: atom(),
    domain :: binary(),
    classname :: binary(),
    sync_ref :: reference(),
    suspended = false :: boolean(),
    slowing_key :: atom(), % key to sync performance with facade without queue
    slowing_pause = 0 :: non_neg_integer(),
    ex_slowing_pause = 0 :: non_neg_integer(),
    exstate :: term(), % for callback usage
    class_meta :: map()
}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link(?MODULE, Opts, []).

%% --------------------------------
%% Send notification. After it reply to sender
%% --------------------------------
-spec notify_subscribers(NotifyPid::pid(), Opts::[{Key::term(),Val::term()}] | map(),
                {Operation::create|update|delete|corrupt,
                 EntityInfo::undefined | {Id::binary(),E0::map(),E1::map()},
                 ModifierInfo::{Type::user|webservice|script|undefined,Id::binary()|undefined}}) -> ok.
%% --------------------------------
notify_subscribers(NotifyPid,Opts,{Operation,undefined=EventInfo,{ModType,ModId}})
  when Operation=='clear'; Operation=='reload' ->
    gen_server:cast(NotifyPid,{notify_subscribers,Opts,{Operation,EventInfo,{ModType,ModId}}});
notify_subscribers(NotifyPid,Opts,{Operation, {EId,E0,E1},{ModType,ModId}}) ->
    gen_server:cast(NotifyPid,{notify_subscribers,Opts,{Operation,{EId,E0,E1},{ModType,ModId}}}).

%% --------------------------------
%% Drop all subscriptions to class (when clear)
%% --------------------------------
drop_subscriptions(NotifyPid,SyncRef) ->
    gen_server:cast(NotifyPid,{drop_subscriptions,SyncRef}).

%% --------------------------------
suspend_notification(NotifyPid,SyncRef) ->
    gen_server:cast(NotifyPid,{suspend_notification,SyncRef}).

%% --------------------------------
resume_notification(NotifyPid,SyncRef,SendReload) ->
    gen_server:cast(NotifyPid,{resume_notification,SyncRef,SendReload}).

%% --------------------------------
print_stat(#cstate{srv_notify=NotifyPid,sync_ref=SyncRef}=State) ->
    gen_server:cast(NotifyPid,{print_stat,SyncRef}),
    State.

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    [Domain,ClassName,ClassMeta,SyncRef] =
        ?BU:extract_required_props(['domain','classname','class_meta','sync_ref'], Opts),
    FacadeRegName = ?ClassSrv:get_regname(Domain,ClassName),
    State = #nstate{domain = Domain,
                    classname = ClassName,
                    class_meta = ClassMeta,
                    slowing_key = ?SlowingKey(Domain,ClassName),
                    slowing_pause = 0,
                    ex_slowing_pause = 0,
                    sync_ref = SyncRef,
                    facade_regname = FacadeRegName},
    gen_server:cast(FacadeRegName, {'register_srv',SyncRef,{?MODULE,self()}}),
    gen_server:cast(FacadeRegName,{'slowing',SyncRef,self(),0}),
    ?LOG('$info', "~ts. '~ts' * '~ts' notifier inited", [?APP,Domain,ClassName]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% other
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({notify_subscribers,_,_}, #nstate{suspended=true}=State) ->
    {noreply,State};
handle_cast({notify_subscribers,Opts,{Operation,EventInfo,{ModType,ModId}}}, #nstate{domain=Domain,classname=CN,class_meta=Meta,exstate=ExState}=State) ->
    State1 = check_performance(messages,State),
    ExState1 = do_notify_subscribers(Domain,CN,Meta,Operation,EventInfo,{ModType,ModId},ExState),
    case ?BU:get_by_key(fun_reply,Opts,undefined) of
        FunReply when is_function(FunReply,0) -> FunReply();
        _ -> ok
    end,
    State2 = check_ex_reducing(State1),
    {noreply,State2#nstate{exstate=ExState1}};

handle_cast({drop_subscriptions,SyncRef}, #nstate{domain=Domain,classname=CN,sync_ref=SyncRef}=State) ->
    do_drop_subscriptions(Domain,CN),
    {noreply,State};

handle_cast({suspend_notification,SyncRef}, #nstate{sync_ref=SyncRef,suspended=true}=State) -> {noreply, State};
handle_cast({suspend_notification,SyncRef}, #nstate{sync_ref=SyncRef}=State) ->
    {noreply,State#nstate{suspended=true}};

handle_cast({resume_notification,SyncRef,_}, #nstate{sync_ref=SyncRef,suspended=false}=State) -> {noreply,State};
handle_cast({resume_notification,SyncRef,SendReload}, #nstate{sync_ref=SyncRef}=State) ->
    case SendReload of
        true ->
            ModifierInfo = {undefined,undefined},
            ?Notify:notify_subscribers(self(),[],{'reload',undefined,ModifierInfo});
        false -> ok
    end,
    {noreply,State#nstate{suspended=false}};

%%
handle_cast({register_stat,{Cnt,Min,Max,Sum}}, State) ->
    Key = 'stat',
    case get(Key) of
        undefined ->
            put(Key, {Cnt,Min,Max,Sum,[]});
        {CntAcc,MinAcc,MaxAcc,SumAcc,SumPartsAcc} ->
            put(Key, {CntAcc+Cnt,min(MinAcc,Min),max(MaxAcc,Max),SumAcc+Sum,SumPartsAcc})
    end,
    {noreply, State};
handle_cast({register_stat,{Cnt,Min,Max,Sum,SumParts}}, State) ->
    Key = 'stat',
    case get(Key) of
        undefined ->
            put(Key, {Cnt,Min,Max,Sum,SumParts});
        {CntAcc,MinAcc,MaxAcc,SumAcc,SumPartsAcc} ->
            SumParts1 = lists:map(fun({X,Y}) -> X+Y end, lists:zip(SumPartsAcc,SumParts)),
            put(Key, {CntAcc+Cnt,min(MinAcc,Min),max(MaxAcc,Max),SumAcc+Sum,SumParts1})
    end,
    {noreply, State};

%% on worker every-minute stat request reply.
handle_cast({print_stat,SyncRef}, #nstate{sync_ref=SyncRef,domain=Domain,classname=CN}=State) ->
    Key = 'stat',
    Reply = case get(Key) of
                undefined -> {0,0,0,0,[]};
                {Cnt,Min,Max,Sum,SumParts} ->
                    erase(Key),
                    {Cnt, Min, Max, case Cnt of 0 -> 0; _ -> Sum div Cnt end, lists:map(fun(X) -> X div 1000 end, lists:reverse([Sum|SumParts]))}
            end,
    case Reply of
        {0,_,_,_,_} -> ok;
        {CntR,MinR,MaxR,AvgR,SumPartsR} ->
            ?LOG('$force', "STAT. '~ts', '~ts'. Notify : count=~p, min=~p mks, max=~p mks, avg=~p mks, sumparts=~w ms", [Domain, CN, CntR, MinR, MaxR, AvgR, SumPartsR])
    end,
    {noreply, State};

%% other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% Commented: not volatile enough
%%%% reducing performance of class by external notifier process (callback)
%%handle_info({reduce_performance,Pause},#nstate{domain=Domain,sync_ref=SyncRef,facade_regname=FacadeRegName}=State) ->
%%    #nstate{domain=Domain,classname=ClassName,ex_slowing_pause=EP0,slowing_pause=P0}=State,
%%    NewPause = case {erlang:max(EP0,P0), erlang:max(Pause,P0)} of
%%                   {P,P} -> P;
%%                   {_,P1} -> gen_server:cast(FacadeRegName,{slowing,SyncRef,self(),P1}), P1
%%               end,
%%    ?LOG('$warning',"~ts. '~ts' * '~ts' notifier overloaded. Reducing performance by ex (~p ms -> ~p ms)",[?APP,Domain,ClassName,Pause,NewPause]),
%%    {noreply,State#nstate{ex_slowing_pause=Pause}};
%%
%%%% restoring performance of class by external notifier process (callback)
%%handle_info({restore_performance},#nstate{domain=Domain,sync_ref=SyncRef,facade_regname=FacadeRegName}=State) ->
%%    #nstate{domain=Domain,classname=ClassName,ex_slowing_pause=EP0,slowing_pause=P0}=State,
%%    NewPause = case {erlang:max(EP0,P0), erlang:max(0,P0)} of
%%                   {P,P} -> P;
%%                   {_,P1} -> gen_server:cast(FacadeRegName,{slowing,SyncRef,self(),P1}), P1
%%               end,
%%    ?LOG('$warning',"~ts. '~ts' * '~ts' notifier normailized. Restoring performance by ex (-> ~p ms)",[?APP,Domain,ClassName,NewPause]),
%%    {noreply,State#nstate{ex_slowing_pause=0}};

%% apply handler to perform function in notify pid
%% used for async purposes of notification callback
%% Fun() -> ok
handle_info({apply,Fun},#nstate{domain=Domain,classname=CN}=State) ->
    try Fun(),
        {noreply,State}
    catch E:R:ST ->
        ?LOG('$crash', "~ts. '~ts' * '~ts' notifier crashed: {~120tp, ~120tp}, ~n\tStack: ~120tp", [?APP,Domain,CN,E,R,ST]),
        {stop,State}
    end;

%% apply handler to perform function in notify pid
%% used for async purposes of notification callback
%% Fun(ExState) -> NewExState
handle_info({apply_exstate,Fun},#nstate{domain=Domain,classname=CN,exstate=ExState}=State) ->
    try ExState1 = Fun(ExState),
        State1 = check_ex_reducing(State),
        {noreply,State1#nstate{exstate=ExState1}}
    catch E:R:ST ->
        ?LOG('$crash', "~ts. '~ts' * '~ts' notifier crashed: {~120tp, ~120tp}, ~n\tStack: ~120tp", [?APP,Domain,CN,E,R,ST]),
        {stop,State}
    end;

%% other
handle_info(_Info, State) -> {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) -> ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) -> {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

check_ex_reducing(#nstate{ex_slowing_pause=EP0}=State) ->
    case erlang:get('ex_slowing_pause') of
        undefined -> State;
        EP0 ->
            erlang:erase('ex_slowing_pause'),
            State;
        EP1 ->
            erlang:erase('ex_slowing_pause'),
            #nstate{domain=Domain,classname=ClassName,slowing_pause=P0,slowing_key=K}=State,
            NewPause = case {erlang:max(EP0,P0), erlang:max(EP1,P0)} of
                           {P,P} -> P;
                           {_,P1} ->
                               %gen_server:cast(FacadeRegName,{slowing,SyncRef,self(),P1}),
                               application:set_env(?APP,K,P1),
                               P1
                       end,
            case EP1 of
                0 -> ?LOG('$warning',"~ts. '~ts' * '~ts' notifier set(ex) normalized. Restore performance by ex (-> ~p ms)",[?APP,Domain,ClassName,NewPause]);
                _ -> ?LOG('$warning',"~ts. '~ts' * '~ts' notifier set(ex) overloaded. Reduce performance by ex (~p ms -> ~p ms)",[?APP,Domain,ClassName,EP1,NewPause])
            end,
            State#nstate{ex_slowing_pause=EP1}
    end.

%% ---------------------------------------------------
check_performance(Where,#nstate{facade_regname=FacadeRegName,slowing_pause=P0,sync_ref=SyncRef}=State) ->
    case explore_queue_len(Where,State) of
        QLen when QLen < ?SlowingQueueLen0, P0/=0 ->
            #nstate{domain=Domain,classname=ClassName,ex_slowing_pause=EP0}=State,
            NewPause = case {erlang:max(EP0,P0), erlang:max(EP0,0)} of
                           {P,P} -> P;
                           {_,P1} -> gen_server:cast(FacadeRegName,{slowing,SyncRef,self(),P1}), P1
                       end,
            ?LOG('$warning',"~ts. '~ts' * '~ts' notifier message_queue normailized. Restoring performance (qlen: ~p; -> ~p ms)",[?APP,Domain,ClassName,QLen,NewPause]),
            State#nstate{slowing_pause=0};
        QLen when QLen > ?SlowingQueueLen1, P0/=?SlowingTimeout  ->
            #nstate{domain=Domain,classname=ClassName,ex_slowing_pause=EP0}=State,
            NewPause = case {erlang:max(EP0,P0), erlang:max(EP0,?SlowingTimeout)} of
                           {P,P} -> P;
                           {_,P1} -> gen_server:cast(FacadeRegName,{slowing,SyncRef,self(),P1}), P1
                       end,
            ?LOG('$warning',"~ts. '~ts' * '~ts' notifier message_queue overloaded. Reducing performance (qlen: ~p; ~p ms -> ~p ms)",[?APP,Domain,ClassName,QLen,?SlowingTimeout,NewPause]),
            State#nstate{slowing_pause=?SlowingTimeout};
        _ -> State
    end.

%% @private
explore_queue_len(messages,_State) ->
    {message_queue_len,QLen} = process_info(self(), message_queue_len),
    QLen.

%% ---------------------------------------------------
-spec do_notify_subscribers(Domain::binary(),CN::binary(),Meta::map(),
                            Operation::create|update|delete|corrupt,
                            EntityInfo::undefined | {Id::binary(),E0::map(),E1::map()},
                            ModifierInfo::undefined | {Type::atom(),Id::binary()|undefined},
                            InternalState::term()) -> ok.
%% ---------------------------------------------------
do_notify_subscribers(Domain, CN, Meta, Operation, EntityInfo, ModifierInfo, ExState) ->
    case ?CFG:notify_function() of
        Fun when is_function(Fun,6) -> Fun(Domain,CN,Meta,Operation,EntityInfo,ModifierInfo);
        Fun when is_function(Fun,7) -> Fun(Domain,CN,Meta,Operation,EntityInfo,ModifierInfo,ExState);
        _ -> ok
    end.

%% ---------------------------------------------------
-spec do_drop_subscriptions(Domain::binary(),CN::binary()) -> ok.
%% ---------------------------------------------------
do_drop_subscriptions(Domain,CN) ->
    case ?CFG:drop_subscriptions_function() of
        Fun when is_function(Fun,2) -> Fun(Domain,CN);
        _ -> ok
    end.
%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 12.03.2021
%%% @doc In-domain data-model-class facade gen_server
%%%      Opts:
%%%         domain, classname, class_meta, sync_ref

-module(dmlib_domain_class_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1, get_regname/2]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([test/0]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-define(RetryStartTimeout, 60000 + ?BU:random(5000)).
-define(CacheFilterTimeout, max(10000, ?CFG:cache_filter_timeout())).
-define(CheckQueueSizePortion, 100). % how many crud operations before check size
-define(OverloadedWorkerQueueSizeHeavy, 500). % size of any-worker queue to pause class with service unavailable until queue is 0 (max queue of workers, readers, notifier).
-define(OverloadedWorkerQueueSizeLight, 10000). % size of any-worker queue to pause class with service unavailable until queue is 0 (max queue of workers, readers, notifier).
-define(OverloadedSelfQueueSize, 20000). % size of self queue (to check on slowing pause)

-define(QuotaPassTimeout, ?CFG:quota_pass_timeout_classes()).
-define(QuotaLimit, ?CFG:load_quota_limit_classes()).
-define(QuotaLimitTL, 10).

-record(waitor, {
    fun_apply :: function(),
    ts :: non_neg_integer(),
    timeout :: timeout()
}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    Domain = ?BU:get_by_key('domain',Opts),
    ClassName = ?BU:get_by_key('classname',Opts),
    RegName = get_regname(Domain,ClassName),
    gen_server:start_link({local, RegName}, ?MODULE, [{'regname', RegName}|Opts], []).

%% returns name of supv for linking to top supv
get_regname(Domain,ClassName) when is_binary(Domain); is_atom(Domain) ->
    ?BU:to_atom_new(<<"dmlib:srv;dom:",(?BU:to_binary(Domain))/binary,";cn:",ClassName/binary>>).

test() -> abc.

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    [RegName,Domain,ClassName,ClassMeta,SyncRef] =
        ?BU:extract_required_props(['regname','domain','classname','class_meta','sync_ref'], Opts),
    % ----
    {Self, Ref} = {self(), make_ref()},
    State = #cstate{regname = RegName,
                    domain = Domain,
                    self = Self,
                    ref = Ref,
                    initts = ?BU:timestamp(),
                    classname = ClassName,
                    class_meta = ClassMeta,
                    class_ets = ets:new(class_ets,[public, set]),
                    storage_mode = ?BU:to_atom_new(maps:get(storage_mode,ClassMeta)), % ets | ram | runtime | category | history | transactionlog
                    integrity_mode = ?BU:to_atom_new(maps:get(integrity_mode,ClassMeta)), % async | sync | sync_fast_read | sync_fast_notify
                    cache_mode = ?BU:to_atom_new(maps:get(cache_mode,ClassMeta)), % full | temp | none
                    cache_filter_interval = maps:get(<<"cache_sec">>,maps:get(opts,ClassMeta),?CacheFilterTimeout div 1000) * 1000,
                    changehistory = ?BU:to_atom_new(maps:get(<<"store_changehistory_mode">>,maps:get(opts,ClassMeta),<<"none">>)), % none | sync
                    crud_counter = 1,
                    sync_ref = SyncRef,
                    slowing_key = ?SlowingKey(Domain,ClassName),
                    slowing_pause = 0},
    % ----
    self() ! {'start',Ref},
    ?LOG('$info', "~ts. '~ts' * '~ts' facade inited as '~ts'", [?APP, Domain, ClassName, RegName]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% --------------
%% returns current node
handle_call({get_node}, _From, State) ->
    {reply, {node(), self()}, State};

%% --------------
%% print state
handle_call({print}, _From, #cstate{domain=Domain,classname=CN}=State) ->
    ?OUT('$force', "~ts. '~ts' * '~ts' facade state: ~n\t~120tp", [?APP, Domain, CN, State]),
    {reply, ok, State};

%% --------------
%% restart gen_serv
handle_call({restart}, _From, State) ->
    {stop, restart, ok, State};

%% --------------
%% other
handle_call(_Msg, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% --------------
handle_cast({register_srv,SyncRef,{Module,Pid}}, #cstate{sync_ref=SyncRef,srv_workers_r=WorkersR,srv_workers_w=WorkersW}=State) ->
    State1 = case Module of
                 ?ClassW -> State#cstate{srv_writer=Pid};
                 ?ClassR -> State#cstate{srv_reader=Pid};
                 ?Notify -> State#cstate{srv_notify=Pid};
                 ?Cache -> State#cstate{srv_cache=Pid};
                 {?ClassWrk,{'r',Index}} -> State#cstate{srv_workers_r=lists:ukeysort(1,lists:keystore(Index,1,WorkersR,{Index,Pid}))};
                 {?ClassWrk,{'w',Index}} -> State#cstate{srv_workers_w=lists:ukeysort(1,lists:keystore(Index,1,WorkersW,{Index,Pid}))}
             end,
    {noreply, State1};

%% --------------
handle_cast({req,FunReply,{crud,_,_}}, #cstate{is_paused=true,pause_reason=Reason,cnt_paused=C}=State) ->
    Reply = {error,{service_unavailable,Reason}},
    FunReply(Reply),
    {noreply,State#cstate{cnt_paused=C+1}};
%%
handle_cast({req,FunReply,{crud,_,_}}, #cstate{is_ready=false}=State) ->
    Reply = {error,{not_loaded,<<"DataModel server is not loaded">>}},
    FunReply(Reply),
    {noreply,State};
%%
handle_cast({req,FunReply,{crud,ClassName,{Operation,Map}}}=Msg, #cstate{classname=ClassName,cnt_crud=C}=State) ->
    case check_overloaded(State) of
        {ok,State1} ->
            State2 = slowing_pause(State1),
            case catch ?Crud:crud(Operation,Map,FunReply,State2#cstate{cnt_crud=C+1}) of
                {noreply,_}=R -> R;
                Term ->
                    ?ClassU:reply_error(FunReply,Term,Operation,ClassName),
                    {noreply,State2}
            end;
        {overloaded,#cstate{}=State1} ->
            handle_cast(Msg,State1)
    end;
%% CRUD on sync wait for load till expire
handle_cast({req,FunReply,{crud,Operation,Map},SyncExpireTS}, #cstate{is_paused=IsPaused,is_ready=IsReady,cnt_wait=CW}=State) when IsPaused; not IsReady ->
    FunApply = fun() -> case catch ?Crud:crud(Operation,Map,FunReply,State#cstate{}) of
                            {noreply,StateX1} -> StateX1;
                            Term -> ?ClassU:reply_error(FunReply,Term,Operation,State#cstate.classname)
                        end end,
    append_waitor(FunApply,SyncExpireTS,State#cstate{cnt_wait=CW+1}),
    {noreply,State};
%%
handle_cast({req,FunReply,{crud,Operation,Map},_SyncExpireTS}, #cstate{cnt_crud=C}=State) ->
    case catch ?Crud:crud(Operation,Map,FunReply,State#cstate{cnt_crud=C+1}) of
        {noreply,_}=R -> R;
        Term ->
            ?ClassU:reply_error(FunReply,Term,Operation,State#cstate.classname),
            {noreply,State}
    end;

%% --------------
%% Request for data hc. After async crud operation of any modification data_hc is changed
%% --------------
handle_cast({get_hc,FunReply}, #cstate{is_paused=true}=State) ->
    FunReply({ok,0}),
    {noreply,State};
handle_cast({get_hc,FunReply}, #cstate{is_ready=false}=State) ->
    FunReply({ok,0}),
    {noreply,State};
handle_cast({get_hc,FunReply}, #cstate{data_hc=HC}=State) ->
    FunReply({ok,HC}),
    {noreply, State};
%% GET HC on sync mode for expire ts (not to reply error if not loaded yet, wait for ExpireTS
handle_cast({get_hc,FunReply,_SyncExpiresTS}, #cstate{data_hc=HC}=State) ->
    FunReply({ok,HC}),
    {noreply, State};

%% --------------
handle_cast({queue_flushed,Ref}, #cstate{domain=Domain,classname=ClassName,sync_ref=Ref}=State) ->
    ?LOG('$warning',"~ts. '~ts' * '~ts' Queue flushed. Service is restored",[?APP,Domain,ClassName]),
    {_,State1} = check_overloaded(State#cstate{is_paused=false,pause_reason= <<>>}),
    {noreply,State1};

%% --------------
handle_cast({slowing,Ref,FromPid,NowPause}, #cstate{sync_ref=Ref,srv_notify=FromPid}=State) ->
    State1 = State#cstate{slowing_pause=NowPause},
    {noreply,State1};

%% --------------
handle_cast({get_status,FunReply}, #cstate{is_ready=R}=State) ->
    FunReply({status,R}),
    {noreply, State};

%% --------------
%% other
handle_cast(_Msg, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% on start -> db initialization
%% --------------
handle_info({'start',Ref}, #cstate{ref=Ref,class_meta=ClassMeta}=State) ->
    State1 = pause_at_start(State#cstate{class_meta = organize_meta(ClassMeta)}),
    report_before_start(State1),
    %{noreply, start(State1#cstate{class_meta = organize_meta(ClassMeta)})};
    % quota
    State2 = case request_quota(State) of
                 {ok,QuotaPid} ->
                     MonRef = erlang:monitor(process,QuotaPid),
                     WQ = #wait_quota{monref=MonRef},
                     State1#cstate{quota=WQ};
                 _ ->
                     Ref1 = make_ref(),
                     erlang:send_after(1000, self(), {'start',Ref1}),
                     State1#cstate{ref=Ref1,quota=undefined}
             end,
    {noreply,State2};

%% --------------
%% Quota on start to load sequentally (reduce pgpool connections, and cpu usage).
%% --------------

%% #quota
%% when quota allow pass
handle_info('pass', #cstate{quota=#wait_quota{monref=MonRef}}=State) ->
    erlang:demonitor(MonRef),
    Ref1 = make_ref(),
    TimerRef1 = erlang:send_after(?QuotaPassTimeout,self(),{'passed_quota_timeout',Ref1}),
    PQ = #pass_quota{ref=Ref1,timerref=TimerRef1},
    #cstate{is_ready=IsReady}=State1 = start(State#cstate{quota=PQ}),
    State2 = case IsReady of
                 true -> report_on_started(State1);
                 false -> State1
             end,
    {noreply,State2};

%% #quota
%% when timeout of passed quota (something wrong on load of any classes)
handle_info({'passed_quota_timeout',Ref}, #cstate{quota=#pass_quota{ref=Ref}}=State) ->
    ?LOG('$warning', "~ts. '~ts' * '~ts' Passed quota TIMEOUT", [?APP, State#cstate.domain, State#cstate.classname]),
    {noreply, release_quota(State)};

%% #quota
%% On quota DOWN while waiting for pass - reattempt start (quota pass one portion more).
%%               If already passed then no need to reattempt.
handle_info({'DOWN',MonRef,process,_Pid,_Reason}, #cstate{quota=#wait_quota{monref=MonRef}}=State) ->
    ?LOG('$warning', "~ts. '~ts' Found quota DOWN. Retry", [?APP, State#cstate.domain, State#cstate.classname]),
    Ref1 = make_ref(),
    erlang:send_after(1000, self(), {start,Ref1}),
    {noreply, State#cstate{ref=Ref1, quota=undefined}};

%% --------------
%% on DOWN of async db initialization
%% --------------
handle_info({'DOWN',MonRef,process,Pid,Reason}=Msg, #cstate{async_pid=Pid,async_monref=MonRef,is_ready=false,classname=CN,domain=Domain,srv_notify=NotifyPid}=State) ->
    ?LOG('$error', "~ts. '~ts' * '~ts'. DB initialization process DOWN: ~n\t~120tp", [?APP,Domain,CN,Msg]),
    State1 = report_on_started(State),
    Ref1 = make_ref(),
    erlang:send_after(?RetryStartTimeout,self(),{start,Ref1}),
    ModifierInfo = {undefined,undefined},
    ?Notify:notify_subscribers(NotifyPid,[],{'reload',undefined,ModifierInfo}),
    State2 = State1#cstate{ref=Ref1,
                           is_ready=false,
                           is_paused=true,
                           pause_reason= ?BU:strbin("Start failed: ~ts",[build_reason(Reason)])},
    {noreply,State2};

%% --------------
%% on result from async db initialization
%% --------------
handle_info({'started',Ref,FunOk,Result}, #cstate{ref=Ref,async_monref=MonRef}=State) ->
    catch erlang:demonitor(MonRef),
    FunOk(),
    State1 = on_started(Result,State),
    State2 = report_on_started(State1),
    {noreply,State2};

%% --------------
%% on stat timer
%% --------------
handle_info({stat,Ref,Mode},#cstate{statref=Ref,domain=Domain}=State) ->
    Enabled = case Mode of
                  'default' ->
                      case ?CFG:stat_enabled() of
                          true -> true;
                          false -> false;
                          undefined ->
                              case ?CFG:stat_enabled_function() of
                                  Fun when is_function(Fun,1) ->
                                      ?BU:to_bool(Fun(Domain));
                                  _ -> true
                              end
                      end;
                  _ -> % low, medium, high
                      true
              end,
    State2 = case Enabled of
                 false -> State;
                 true when Mode=='low' ->
                     stat_heartbeat(Mode,State);
                 true -> % high | medium | default
                     State1 = stat_heartbeat(Mode,State),
                     ?Notify:print_stat(
                        ?Integrity:obtain_workers_stat(Mode, State1))
             end,
    {noreply, start_stat_timer(Mode,State2)};

%% on worker every-minute stat request reply. Register async worker reply for next aggregate logging
handle_info({worker_stat, SyncRef, TS, WrkPid, Reply}, #cstate{sync_ref=SyncRef, wrk_stat=#wrk_stat{ts=TS}=WrkStat}=State) ->
    #wrk_stat{workers=Workers,workers_replies=WorkersReplies} = WrkStat,
    State1 = case lists:keytake(WrkPid, 1, Workers) of
                false -> State;
                {value,WrkKey,[]} ->
                    WrkStat1 = WrkStat#wrk_stat{workers=[],workers_replies=[{WrkKey,Reply}|WorkersReplies]},
                    apply_workers_stat(WrkStat1, State);
                {value,WrkKey,WorkersRest} ->
                    WrkStat1 = WrkStat#wrk_stat{workers=WorkersRest,workers_replies=[{WrkKey,Reply}|WorkersReplies]},
                    State#cstate{wrk_stat=WrkStat1}
            end,
    {noreply,State1};

%% on workers every-minute stat request timeout. Flush and log
handle_info({worker_stat_timeout, SyncRef, TS}, #cstate{sync_ref=SyncRef, wrk_stat=#wrk_stat{ts=TS}=WrkStat, domain=Domain, classname=CN}=State) ->
    #wrk_stat{workers=WorkersRemain} = WrkStat,
    PidsOnTimeout = ?BU:join_binary([?BU:strbin("~p#~p", [Pid, process_info(Pid,message_queue_len)]) || {Pid,_Module} <- WorkersRemain], ", "),
    ?LOG('$warning', "STAT. '~ts', '~ts'. Workers stat timeout: ~ts", [Domain, CN, PidsOnTimeout]),
    {noreply, apply_workers_stat(WrkStat, State)};

%% --------------
%% Timer of cache filtering
%% --------------
handle_info({timer_cache,Ref}, #cstate{cache_ref=Ref}=State) ->
    State1 = filter_cache(State),
    {noreply, State1};

%% --------------
%% Internal process async work cycle
%% --------------
handle_info({do_work_internal,SyncRef,Fun}, #cstate{sync_ref=SyncRef}=State) when is_function(Fun,1) ->
    {noreply, Fun(State)};
handle_info({do_work_internal,SyncRef,Fun}, #cstate{sync_ref=SyncRef}=State) when is_function(Fun,0) ->
    Fun(),
    {noreply, State};

%% --------------
%% Timer of TTL (entities auto delete)
%% --------------
handle_info({timer_ttl,TimerInfo}, State) ->
    State1 = ?Crud:expired(TimerInfo,State),
    {noreply, slowing_pause(State1)};

%% --------------
%% Timer of read aggregated cache
%% --------------
handle_info({timeout_read_aggr_cache,TimerInfo}, State) ->
    State1 = ?Crud:expired_read_aggr_cache(TimerInfo,State),
    {noreply, slowing_pause(State1)};

%% --------------
%% After async crud operation of any modification changes data_hc
%% --------------
handle_info({refresh_data_hc,SyncRef}, #cstate{ref=SyncRef,data_hc=HC}=State) ->
    {noreply, State#cstate{data_hc = HC+1}};

%% --------------
%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, #cstate{}=_State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------------------------------
%% Pause to avoid high load on cycled fall
%% ------------------------------------------------
pause_at_start(#cstate{domain=Domain,classname=CN}=State) ->
    Key = {start_ts,?MODULE,Domain,CN},
    case ?BLstore:find_t(Key) of
        {_,_TS} ->
            ?LOG('$info', "~ts. '~ts' * '~ts'. Pause at start...", [?APP,Domain,CN]),
            timer:sleep(1000);
        _ -> ok
    end,
    ?BLstore:store_t(Key,?BU:timestamp(),1000),
    State.

%% ------------------------------------------------
%% Report about load progress
%% ------------------------------------------------

-define(QuotaGroupKey(Domain), {dmlib,Domain}).
-define(QuotaGroupTLKey(Domain), {dmlib,Domain,transactionlog}).

%% ---
-spec request_quota(#cstate{}) -> {ok,pid()} | {error, already_exists | group_not_found}.
%% ---
request_quota(#cstate{classname=CN,domain=Domain,storage_mode='transactionlog'}) ->
    ?BLquota:setup_group(?QuotaGroupTLKey(Domain), #{limit => max(?QuotaLimit,?QuotaLimitTL)}),
    ?BLquota:request_quota(?QuotaGroupTLKey(Domain), self(), CN);

request_quota(#cstate{classname=CN,domain=Domain}) ->
    ?BLquota:setup_group(?QuotaGroupKey(Domain), #{limit => ?QuotaLimit}),
    ?BLquota:request_quota(?QuotaGroupKey(Domain), self(), CN).

%% ---
-spec release_quota(#cstate{}) -> #cstate{}.
%% ---
release_quota(#cstate{domain=Domain,storage_mode='transactionlog'}=State) ->
    ?BLquota:release_quota(?QuotaGroupTLKey(Domain), self()),
    State#cstate{quota=undefined};

release_quota(#cstate{domain=Domain}=State) ->
    ?BLquota:release_quota(?QuotaGroupKey(Domain), self()),
    State#cstate{quota=undefined}.
%
%% ---
report_before_start(#cstate{}=State) ->
    State.

%% ---
report_on_started(#cstate{is_ready=IsReady,classname=CN,domain=Domain}=State) ->
    RepType = case IsReady of
                  true -> 'started';
                  false -> 'failed'
              end,
    ?DSRV:report_cn_started(RepType,self(),CN,Domain),
    % #quota
    release_quota(State).

%% ------------------------------------------------
%% Organize meta by caches
%% ------------------------------------------------
organize_meta(ClassMeta) ->
    Props = maps:get('properties',ClassMeta),
    %
    XProps = x_props(Props),
    RequiredFields = x_required(Props),
    Defaults = x_defaults(Props),
    Merged = x_merged(Props),
    Appendable = x_appendable(Props),
    Incremented = x_incremented(Props),
    ClassMeta#{'x_props' => XProps,
               'x_required' => RequiredFields,
               'x_defaults' => ?Format:internal_format_entity_filter_errors(maps:from_list(Defaults),XProps),
               'x_merged' => maps:from_list(Merged),
               'x_appendable' => Appendable,
               'x_incremented' => Incremented,
               'x_item_json_data' => ?BU:to_bool(maps:get(<<"store_item_json_data">>, maps:get('opts',ClassMeta,#{}), false))}.

%% @private
x_props(Props) ->
    F = fun(Prop,Idx) -> {maps:get(<<"data_type">>,Prop), maps:get(<<"multi">>,Prop,false), Prop, Idx} end,
    lists:map(fun({Prop,Idx}) -> {maps:get(<<"name">>,Prop),F(Prop,Idx)} end, lists:zip(Props,lists:seq(1,length(Props)))).
%% @private
x_required(Props) ->
    lists:filtermap(
        fun(Prop) ->
            case maps:get(<<"required">>,Prop,false) of
                true -> {true,maps:get(<<"name">>,Prop)};
                _ -> false
            end end, Props).
%% @private
x_defaults(Props) ->
    lists:filtermap(
        fun(Prop) ->
            case maps:get(<<"default">>,Prop,undefined) of
                undefined -> false;
                Default -> {true,{maps:get(<<"name">>,Prop),Default}}
            end end, Props).
%% @private
x_merged(Props) ->
    lists:filtermap(
        fun(Prop) ->
            case {maps:get(<<"data_type">>,Prop), maps:get(<<"multi">>,Prop,false)} of
                {<<"any">>,false} ->
                    case maps:get(<<"merge_levels">>,Prop,0) of
                        0 -> false;
                        N -> {true,{maps:get(<<"name">>,Prop),N}}
                    end;
                _ -> false
            end end, Props).
%% @private
x_appendable(Props) ->
    lists:filtermap(
        fun(Prop) ->
            case maps:get(<<"multi">>,Prop,false) of
                true -> {true,maps:get(<<"name">>,Prop)};
                _ -> false
            end end, Props).
%% @private
x_incremented(Props) ->
    lists:foldl(
        fun(Prop,undefined) ->
            case maps:get(<<"data_type">>,Prop,false) of
                <<"increment">> -> maps:get(<<"name">>,Prop);
                _ -> undefined
            end;
           (_,Acc) -> Acc
        end, undefined, Props).

%% ------------------------------------------------
%% When class started, it should initialize storage
%% ------------------------------------------------
start(#cstate{domain=Domain,classname=CN,class_meta=Meta,storage_mode=StorageMode}=State) ->
    ?LOG('$trace', "~ts. '~ts' * '~ts'. Starting...", [?APP,Domain,CN]),
    Self = self(),
    case StorageMode of
        % ets -> ets
        'ets' ->
            State1 = ready(State),
            ensure_driver_state(State1);
        % ram, runtime -> mnesia
        ST when ST=='ram'; ST=='runtime' ->
            Ref1 = make_ref(),
            {Pid,MonRef} = spawn_monitor(fun() ->
                                            Async = self(),
                                            FunOk = fun() -> Async ! ok end,
                                            Self ! {'started',Ref1,FunOk,(catch ?MnesiaMeta:ensure_table(Domain,CN,Meta))},
                                            receive ok -> ok
                                            after 30000 -> ok
                                            end % not to finish before demonitor in pool
                                         end),
            ?LOG('$trace', "~ts. '~ts' * '~ts'. Mnesia DB initialization started on ~p...", [?APP,Domain,CN,Pid]),
            State1 = State#cstate{ref=Ref1,
                                  async_pid=Pid,
                                  async_monref=MonRef},
            ensure_driver_state(State1);
        % category, history -> pg
        ST when ST=='category'; ST=='history' ->
            Ref1 = make_ref(),
            {Pid,MonRef} = spawn_monitor(fun() ->
                                            Async = self(),
                                            FunOk = fun() -> Async ! ok end,
                                            Self ! {'started',Ref1,FunOk,(catch ?Postgres:ensure_table(Domain,CN,Meta))},
                                            receive ok -> ok
                                            after 30000 -> ok
                                            end % not to finish before demonitor in pool
                                         end),
            Mode = case ST of 'category' -> <<"(non partitioned)">>; 'history' -> <<"(partitioned)">> end,
            ?LOG('$trace', "~ts. '~ts' * '~ts'. Postgres ~ts DB initialization started on ~p...", [?APP,Domain,CN,Mode,Pid]),
            State1 = State#cstate{ref=Ref1,
                                  async_pid=Pid,
                                  async_monref=MonRef},
            ensure_driver_state(State1);
        % transactionlog -> kafka
        'transactionlog' ->
            Ref1 = make_ref(),
            {Pid,MonRef} = spawn_monitor(fun() ->
                                            KafkaRes = (catch ?Kafka:ensure_topic(Domain,CN,Meta)),
                                            FunCH = fun(Map) ->
                                                          case (catch ?ClickHouse:ensure_table(Domain,CN,Meta)) of
                                                              {ok,ChProps} -> {ok,Map#{clickhouse => ChProps}};
                                                              {error,{storage_instance_not_found,_}} -> {ok,Map#{clickhouse => undefined}};
                                                              {error,_}=Err -> Err
                                                          end end,
                                            Res = case KafkaRes of
                                                      {error,_} -> KafkaRes;
                                                      {ok,KafkaMeta} -> FunCH(KafkaMeta);
                                                      ok -> FunCH(#{})
                                                  end,
                                            Async = self(),
                                            FunOk = fun() -> Async ! ok end,
                                            Self ! {'started',Ref1,FunOk,Res},
                                            receive ok -> ok
                                            after 30000 -> ok
                                            end % not to finish before demonitor in pool
                                         end),
            ?LOG('$trace', "~ts. '~ts' * '~ts'. Kafka topic initialization started on ~p...", [?APP,Domain,CN,Pid]),
            State1 = State#cstate{ref=Ref1,
                                  async_pid=Pid,
                                  async_monref=MonRef},
            ensure_driver_state(State1);
        %
        _ST ->
            Ref1 = make_ref(),
            erlang:send_after(?RetryStartTimeout,self(),{'start',Ref1}),
            State#cstate{ref=Ref1,
                         is_ready=false,
                         is_paused=true,
                         pause_reason= ?BU:strbin("~ts. '~ts' * '~ts'. Storage type '~ts' not implemented",[?APP,Domain,CN,_ST])}
    end.

%% @private
ensure_driver_state(#cstate{domain=Domain,classname=CN}=State) ->
    case ?Crud:ensure_driver_state(State) of
        #cstate{}=State1 -> State1;
        {error,_}=Err ->
            Key = {?FUNCTION_NAME,error,Domain,CN},
            Value = case ?BLstore:find_t(Key) of
                        false -> 0;
                        {_,V} -> ?BU:to_int(V,0)
                    end,
            ?BLstore:store_t(Key,Value+1,120000),
            Ref1 = make_ref(),
            erlang:send_after(?BU:growing_pause(?BU:to_int(Value,0),6,1000), self(), {'start',Ref1}),
            State#cstate{ref=Ref1,
                         is_ready=false,
                         is_paused=true,
                         pause_reason= ?BU:strbin("~ts. '~ts' * '~ts'. ~ts", [?APP,Domain,CN,?BU:parse_error_reason(Err)])}
    end.

%% ------------------------------------------------
%% When async process is finished
%% ------------------------------------------------
on_started(Result, #cstate{domain=Domain,classname=CN}=State) ->
    State1 = State#cstate{async_pid=undefined,
                          async_monref=undefined},
    FErr = fun({error,R}) ->
                  Ref1 = make_ref(),
                  erlang:send_after(?RetryStartTimeout,self(),{start,Ref1}),
                  State1#cstate{ref=Ref1,
                                is_ready=false,
                                is_paused=true,
                                pause_reason= ?BU:strbin("Error initializing storage: ~ts",[build_reason(R)])}
           end,
    State2 = case Result of
                 ok ->
                     ?LOG('$info', "~ts. '~ts' * '~ts'. Storage initialized! Now loading...", [?APP,Domain,CN]),
                     ready(State1);
                 {ok,Props} ->
                     ?LOG('$info', "~ts. '~ts' * '~ts'. Storage initialized! Now loading...", [?APP,Domain,CN]),
                     ready(?Crud:update_driver_state(Props,State1));
                 {'EXIT',R} ->
                     ?LOG('$error', "~ts. '~ts' * '~ts'. Exception caught on storage initialization: ~n\t~120tp", [?APP,Domain,CN,R]),
                     FErr({error,?BU:strbin("Exception caught on storage initialization",[])});
                 {error,_}=Err ->
                     ?LOG('$error', "~ts. '~ts' * '~ts'. Error on storage initialization: ~n\t~120tp", [?APP,Domain,CN,Err]),
                     FErr(Err)
             end,
    State2.

%% @private
build_reason({error,R}) -> build_reason(R);
build_reason(R) when is_binary(R) -> R;
build_reason({K,R}) when is_binary(R) -> ?BU:strbin("~1000tp, ~ts",[K,R]);
build_reason(R) -> ?BU:strbin("~1000tp",[R]).

%% ------------------------------------------------
start_stat_timer(Mode,#cstate{}=State) ->
    Ref1 = make_ref(),
    State#cstate{statref=Ref1,
                 stattimerref=erlang:send_after(60000,self(),{stat,Ref1,Mode})}.

%% ------------------------------------------------

%% @private
ready(#cstate{class_meta=Meta,srv_notify=NotifyPid,cache_filter_interval=CacheFilterInterval}=State) ->
    Props = lists:map(fun({N,{T,M,Prop,Idx}}) -> {N,T,M,Prop,Idx} end, maps:get(x_props,Meta)),
    Props1 = lists:sort(fun({_,_,_,_,Idx1},{_,_,_,_,Idx2}) -> Idx1=<Idx2 end, Props),
    PropNames = [N || {N,_,_,_,_} <- Props1],
    %
    State1 = State#cstate{propnames = [<<"id">>|PropNames],
                          proptuples = [{N,T,M} || {N,T,M,_,_} <- Props1],
                          attachment_propnames = lists:filtermap(fun({N,<<"attachment">>,_,_,_}) -> {true,N}; (_) -> false end, Props1)
                         },
    State2 = start_filter_timer(10000 + ?BU:random(CacheFilterInterval), State1),
    case ?Crud:load(State2) of
        {ok,State3} ->
            State4 = State3#cstate{is_ready=true,
                                   is_paused=false,
                                   pause_reason= <<>>},
            Opts = maps:get(opts,Meta),
            State5 = case maps:get(<<"stat_mode">>,Opts,undefined) of
                         <<"high">> -> start_stat_timer('high',State4);
                         <<"medium">> -> start_stat_timer('medium',State4);
                         <<"low">> -> start_stat_timer('low',State4);
                         <<"default">> -> start_stat_timer('default',State4);
                         <<"disabled">> -> State4;
                         _ -> start_stat_timer('default',State4)
                     end,
            log_loaded(State5),
            apply_waitors(State5);
        {error,R} ->
            Ref1 = make_ref(),
            erlang:send_after(?RetryStartTimeout,self(),{start,Ref1}),
            ModifierInfo = {undefined,undefined},
            ?Notify:notify_subscribers(NotifyPid,[],{'reload',undefined,ModifierInfo}),
            State2#cstate{ref=Ref1,
                          is_ready=false,
                          is_paused=true,
                          pause_reason= ?BU:strbin("Error loading cache: ~ts",[build_reason(R)])}
    end.

%% -------------------------------------------
filter_cache(#cstate{cache_filter_interval=CacheFilterInterval}=State) ->
    ?Crud:filter_cache(State),
    start_filter_timer(CacheFilterInterval - 10000 + ?BU:random(20000), State).

%% @private
start_filter_timer(Timeout, State) ->
    Ref1 = make_ref(),
    erlang:send_after(Timeout, self(), {timer_cache,Ref1}),
    State#cstate{cache_ref=Ref1}.

%% -------------------------------------------
slowing_pause(#cstate{slowing_key=Key,slowing_pause=SP,cnt_slow=C}=State) ->
    NewSP = case application:get_env(?APP,Key) of
                {ok,V} when is_integer(V) -> V;
                _ -> 0
            end,
    case NewSP of
        SP when SP==0 -> State;
        SP -> timer:sleep(SP), State#cstate{cnt_slow=C+1};
        _ when NewSP==0 -> State#cstate{slowing_pause=NewSP};
        _ -> timer:sleep(NewSP), State#cstate{slowing_pause=NewSP,cnt_slow=C+1}
    end.

%% -------------------------------------------
check_overloaded(#cstate{crud_counter=Cnt,is_paused=IsPaused,slowing_pause=SP,storage_mode=SM}=State) ->
    PortionSize = case SP of 0 -> ?CheckQueueSizePortion; _ -> 1 end,
    State1 = State#cstate{crud_counter=Cnt+1},
    case {IsPaused, Cnt rem PortionSize} of
        {false,Rem} when Rem/=0 -> {ok,State1};
        {false,_} when SP/=0 ->
            case process_info(self(),message_queue_len) of
                {message_queue_len,QLen} when QLen < ?OverloadedSelfQueueSize -> {ok,State1};
                {message_queue_len,QLen} ->
                    #cstate{domain=Domain,classname=ClassName,sync_ref=SyncRef,self=Self}=State1,
                    State2 = State1#cstate{is_paused=true,
                                           pause_reason= <<"Class queue is overloaded (a)">>},
                    gen_server:cast(Self, {'queue_flushed',SyncRef}), % in top of self queue.
                    ?LOG('$warning',"~ts. '~ts' * '~ts' Queue is overloaded (facade). Service is paused (flushing ~p msgs)",[?APP,Domain,ClassName,QLen]),
                    {overloaded,State2}
            end;
        _ ->
            OverloadedWorkerQueueSize = overloaded_worker_queue_size(SM),
            case get_max_queue_len(State) of
                0 when IsPaused -> {ok, apply_waitors(State1#cstate{is_paused=false,pause_reason= <<>>})};
                QL when IsPaused, QL =< OverloadedWorkerQueueSize/10 -> {ok, apply_waitors(State1#cstate{is_paused=false,pause_reason= <<>>})};
                _ when IsPaused -> {ok,State1};
                QL when QL < OverloadedWorkerQueueSize -> {ok,State1};
                QL ->
                    #cstate{domain=Domain,classname=ClassName,sync_ref=SyncRef,self=Self}=State1,
                    State2 = State1#cstate{is_paused=true,
                                           pause_reason= <<"Class queue is overloaded (b)">>},
                    WPid = ?Integrity:wait_flush_workers(fun() ->
                                                                ?LOG('$warning',"~ts. '~ts' * '~ts' Queue flushed message to ~p",[?APP,Domain,ClassName,Self]),
                                                                gen_server:cast(Self, {'queue_flushed',SyncRef})
                                                         end, State),
                    ?LOG('$warning',"~ts. '~ts' * '~ts' Queue is overloaded (workers). Service is paused (waitor: ~p, ~p msgs)",[?APP,Domain,ClassName,WPid,QL]),
                    {overloaded,State2}
            end
    end.

%% @private
overloaded_worker_queue_size(SM) when SM=='runtime'; SM=='ram'; SM=='ets' -> ?OverloadedWorkerQueueSizeLight;
overloaded_worker_queue_size(_) -> ?OverloadedWorkerQueueSizeHeavy.

%% @private
get_max_queue_len(#cstate{srv_workers_w=WorkersW,srv_workers_r=WorkersR}=State) ->
    #cstate{srv_writer=WriterSrv,srv_reader=ReaderSrv}=State,
    WorkerPidsW = [Pid || {_Index,Pid} <- WorkersW], % ?ClassWrk
    WorkerPidsR = [Pid || {_Index,Pid} <- WorkersR], % ?ClassWrk
    case lists:foldl(fun(undefined,Acc) -> Acc;
                        (Pid,Acc) -> erlang:max(Acc,process_info(Pid,message_queue_len))
                     end, 0, [WriterSrv, ReaderSrv | WorkerPidsW++WorkerPidsR])
    of
        {_,MaxQueueLen} -> MaxQueueLen;
        undefined -> 1
    end.

%% -----------------------------
%% @private
log_loaded(#cstate{domain=Domain,classname=CN}) ->
    ?LOG('$info', "~ts. '~ts' * '~ts'. Storage loaded!", [?APP,Domain,CN]).

%% -----------------------------
%% @private
%% Add call request to waitors of class load
append_waitor(FunApply,SyncTimeout,#cstate{waitors=W}=State) ->
    NowTS = ?BU:timestamp(),
    Waitor = #waitor{fun_apply = FunApply,
                     timeout = SyncTimeout,
                     ts = NowTS},
    W1 = lists:filter(fun(#waitor{ts=TS,timeout=Timeout}) -> TS+Timeout > NowTS end, W),
    State#cstate{waitors=[Waitor|W1]}.

%% Apply waitors when something changed in classes list
apply_waitors(#cstate{waitors=W,cnt_waittimeout=CWT}=State) ->
    NowTS = ?BU:timestamp(),
    State1 = lists:foldl(fun(#waitor{ts=TS,timeout=Timeout},StateX) when TS+Timeout>=NowTS -> StateX#cstate{cnt_waittimeout=CWT+1};
                            (#waitor{fun_apply=FunApply},StateX) -> FunApply(), StateX
                         end, State, W),
    State1#cstate{waitors=[]}.

%% -----------------------------
%% @private
stat_heartbeat(Mode,#cstate{domain=Domain,classname=CN}=State) ->
    #cstate{cnt_crud=CC,cnt_wait=CW,cnt_waittimeout=CWT,cnt_paused=CP,cnt_slow=CS,
        is_paused=IsPaused,is_ready=IsReady}=State,
    {message_queue_len,QLen0} = process_info(self(),message_queue_len),
    QLen1 = get_max_queue_len(State),
    case {CC,CW,CWT,CP,CS,IsPaused,IsReady,QLen0,QLen1} of
        {0,0,0,0,0,false,true,_,_} when QLen0 =< 1, QLen1 =< 1 ->
            ?LOG('$debug', "STAT. '~ts', '~ts'. Requests: Routed=~p",
                [Domain, CN, CC]);
        {_,0,0,0,0,false,true,_,_} when Mode/='high', CC < 100, QLen0 =< 3, QLen1 =< 5 ->
            ?LOG('$trace', "STAT. '~ts', '~ts'. Requests: Routed=~p",
                [Domain, CN, CC]);
        {_,0,0,0,0,false,true,_,_} when Mode=='low' ->
            ?LOG('$info', "STAT. '~ts', '~ts'. Q=~p\tQA=~p\t W=~p. Requests: Routed=~p",
                [Domain, CN, QLen0, QLen1, length(State#cstate.waitors), CC]); % TODO: $info
        {_,0,0,0,0,false,true,_,_} ->
            ?LOG('$force', "STAT. '~ts', '~ts'. Q=~p\tQA=~p\t W=~p. Requests: Routed=~p",
                [Domain, CN, QLen0, QLen1, length(State#cstate.waitors), CC]); % TODO: $info
        _ ->
            ?LOG('$force', "STAT. '~ts', '~ts'. P=~p; R=~p; Q=~p\tQA=~p\t W=~p. Requests: Routed=~p\tPaused=~p\tSlow=~p\tWait=~p\tWTout=~p",
                [Domain, CN, ?BU:to_int(IsPaused), ?BU:to_int(IsReady), QLen0, QLen1, length(State#cstate.waitors), CC,CP,CS,CW,CWT]) % TODO: $warning
    end,
    State#cstate{cnt_crud=0,cnt_wait=0,cnt_waittimeout=0,cnt_paused=0,cnt_slow=0}.

%% @private
apply_workers_stat(#wrk_stat{workers_replies=WorkersReplies,timerref=TRef}, #cstate{domain=Domain,classname=CN}=State) ->
    ?BU:cancel_timer(TRef),
    % [{WrkKey, [{Operation, {Cnt,Min,Max,Sum}]}]
    SumRes = lists:foldl(fun({_WrkKey, Reply}, Acc) ->
                             Map = maps:from_list(Reply),
                             merge_wrk_stat_maps(Map, Acc)
                         end, #{}, WorkersReplies),
    % #{Operation => {Cnt,Min,Max,Sum}}
    AvgRes = maps:map(fun(_K,{Cnt,Min,Max,Sum}) -> {Cnt,Min,Max,case Cnt of 0 -> 0; _ -> Sum div Cnt end} end, SumRes),
    % #{Operation => {Cnt,Min,Max,Avg}}
    case maps:size(AvgRes) of
        0 -> ok;
        1 ->
            [{Op, {Cnt,Min,Max,Avg}}] = maps:to_list(AvgRes),
            case Cnt of
                0 -> ok;
                _ ->
                    ?LOG('$force', "STAT. '~ts', '~ts'. Workers: ~ts count=~p, min=~p mks, max=~p mks, avg=~p mks", [Domain, CN, Op, Cnt, Min, Max, Avg])
            end;
        _ ->
            Head = <<"\t\t      Operation;\t Count; \tMin,mks; \tMax,mks; \tAvg,mks;">>,
            Bin = ?BU:join_binary([?BU:strbin("\t\t~15ts  \t~6.. B  \t~8.. B\t~8.. B\t~8.. B",[Op,Cnt,Min,Max,Avg]) || {Op,{Cnt,Min,Max,Avg}} <- maps:to_list(AvgRes)], "\n"),
            ?LOG('$force', "STAT. '~ts', '~ts'. Workers: ~n~ts~n~ts", [Domain, CN, Head, Bin])
    end,
    State#cstate{wrk_stat=undefined}.
%% @private
merge_wrk_stat_maps(Map1, Map2) ->
    maps:fold(fun(K,V,Acc) ->
                    case maps:get(K,Acc,undefined) of
                        undefined -> Acc#{K => V};
                        {Cnt2,Min2,Max2,Sum2} ->
                            {Cnt1,Min1,Max1,Sum1}=V,
                            Acc#{K => {Cnt2+Cnt1, min(Min2,Min1), max(Max2,Max1), Sum2+Sum1}}
                    end
              end, Map2, Map1).

%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 12.03.2021
%%% @doc In-domain data-model-class server for queued operations of cross-server cache synchronization
%%       Opts:
%%%         domain, classname, class_meta, sync_ref

-module(dmlib_domain_class_cache_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([store/4, find/2, get_cache_id/2]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    Domain = ?BU:get_by_key('domain',Opts),
    CN = ?BU:get_by_key('classname',Opts),
    CacheId = get_cache_id(Domain,CN),
    gen_server:start_link({local,CacheId}, ?MODULE, Opts, []).

%% ------------------------
%% 27.12.2024
store(CacheId,Key,Value,TimeoutMs) ->
    gen_server:cast(CacheId, {store,Key,Value,TimeoutMs}).

find(CacheId,Key) ->
    Ets = CacheId,
    case ets:lookup(Ets, Key) of
        [] -> false;
        [{_,{Value,_}}] -> {ok,Value}
    end.

get_cache_id(Domain,CN) ->
    ?BU:to_atom_new(?BU:strbin("cachebuild:~ts#~ts",[Domain,CN])).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    [Domain,ClassName,ClassMeta,SyncRef] =
        ?BU:extract_required_props(['domain','classname','class_meta','sync_ref'], Opts),
    FacadeRegName = ?ClassSrv:get_regname(Domain,ClassName),
    State = #hstate{
        domain = Domain,
        classname = ClassName,
        class_meta = ClassMeta,
        sync_ref = SyncRef,
        facade_regname = FacadeRegName,
        ets_build = ets:new(get_cache_id(Domain,ClassName),[public,set,named_table]) % 27.12.2024
    },
    gen_server:cast(FacadeRegName, {register_srv,SyncRef,{?MODULE,self()}}),
    ?LOG('$info', "~ts. '~ts' * '~ts' cacher inited", [?APP,Domain,ClassName]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call(_Request, _From, State) ->
    {reply, undefined, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% 27.12.2024
handle_cast({store,Key,Value,TimeoutMs}, #hstate{ets_build=Ets}=State) ->
    TimerRef = erlang:send_after(TimeoutMs, self(), {expired,Key}),
    ets:insert(Ets,{Key,{Value,TimerRef}}),
    {noreply,State};

handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% 27.12.2024
handle_info({expired,Key}, #hstate{ets_build=Ets}=State) ->
    ets:delete(Ets,Key),
    {noreply,State};

% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

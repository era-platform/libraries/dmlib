%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 12.03.2021
%%% @doc In-domain data-model-class server-process for queued operations of storage write
%%       Opts:
%%%         domain, classname, class_meta, sync_ref

-module(dmlib_domain_class_op_w_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([
    work/3,
    call_work/4,
    call_test/4,
    cast_workf/2
]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link(?MODULE, Opts, []).

work(Pid,SyncRef,Fun) ->
    gen_server:cast(Pid,{work,SyncRef,Fun}).

call_work(Pid,SyncRef,Fun,Timeout) ->
    gen_server:call(Pid,{work,SyncRef,Fun},Timeout).

call_test(undefined,_,Fun,_) -> Fun();
call_test(Pid,SyncRef,Fun,Timeout) ->
    call_work(Pid,SyncRef,Fun,Timeout).

cast_workf(Pid,Fun) when is_pid(Pid), is_function(Fun,0) ->
    gen_server:cast(Pid, {workf,Fun}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    [Domain,ClassName,ClassMeta,SyncRef] =
        ?BU:extract_required_props(['domain','classname','class_meta','sync_ref'], Opts),
    FacadeRegName = ?ClassSrv:get_regname(Domain,ClassName),
    State = #hstate{domain = Domain,
                    classname = ClassName,
                    class_meta = ClassMeta,
                    sync_ref = SyncRef,
                    facade_regname = FacadeRegName},
    gen_server:cast(FacadeRegName, {register_srv,SyncRef,{?MODULE,self()}}),
    ?LOG('$info', "~ts. '~ts' * '~ts' writer inited", [?APP,Domain,ClassName]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call({work,SyncRef,Fun},_From,#hstate{sync_ref=SyncRef}=State) when is_function(Fun,0) ->
    Res = try Fun()
          catch C:R:ST -> ?LOG('$error',"DMS writer caught: ~120tp ",[{C,R,ST}]), {C,R}
          end,
    {reply, Res, State};

handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({work,SyncRef,Fun},#hstate{sync_ref=SyncRef}=State) when is_function(Fun,0) ->
    try Fun()
    catch C:R:ST -> ?LOG('$error', "DMS writer caught: ~120tp ",[{C,R,ST}])
    end,
    {noreply, State};

handle_cast({workf,F}, State) ->
    try F()
    catch C:R:ST -> ?LOG('$error', "DMS writer caught: ~120tp ",[{C,R,ST}])
    end,
    {noreply, State};

handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

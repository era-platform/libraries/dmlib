%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 23.03.2021
%%% @doc Utilities to make and format entity.

-module(dmlib_domain_class_format).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([build_entity/2,build_entity/3,
         format_entity/3,
         internal_format_entity_filter_errors/2]).

-export([update_merge/4]).

-export([def_req/2,
         fill_defaults/2,
         check_required/2]).

-export([decorate_entity/2,
         fill_by_nulls/2,
         remove_nulls/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------------------------
%% Build entity from request
%% --------------------------------------------------
build_entity(RequestMap,ClassMeta) ->
    F = fun(Id) -> build_entity(RequestMap,ClassMeta,Id) end,
    case maps:get(object,RequestMap) of
        'collection' -> F(?BU:newid());
        {'entity',IdE} -> F(IdE);
        {'entity',IdE,_EOpts} -> F(IdE)
    end.

%%
build_entity(RequestMap,ClassMeta,Id) ->
    case format_entity(maps:get(content,RequestMap),maps:get(x_props,ClassMeta),[api]) of
        {error,_}=Err -> Err;
        Entity -> Entity#{<<"id">> => Id}
    end.

%% --------------------------------------------------
%% Format entity by JSON and XProps
%% --------------------------------------------------
format_entity(JSON, XProps, [From]) when is_map(JSON), is_list(XProps) ->
    PropNames = [PName || {PName,_} <- XProps],
    JSON1 = maps:with(PropNames, JSON),
    maps:fold(fun(_,_,{error,_}=Err) -> Err;
                 (K,V,Acc) ->
                     {PType,PMulti,PItem,_PIdx} = ?BU:get_by_key(K,XProps),
                     case catch format_field(PType,PMulti,{K,V},PItem,From) of
                         {'EXIT',_} when From==db -> Acc;
                         {'EXIT',_} -> {error,{invalid_params,?BU:strbin("field=~ts|Invalid ~ts. Expected ~ts value",[K,K,PType])}};
                         {error,_} when From==db -> Acc;
                         {error,_}=Err -> Err;
                         skip when From==db -> Acc;
                         skip -> maps:without([K],Acc);
                         {ok,V} -> Acc;
                         {ok,V1} -> Acc#{K => V1}
                     end end, JSON1, JSON1);
format_entity(_,_,_) -> {error,{invalid_params,<<"Invalid content. Expected JSON object">>}}.

%% --------------------------------------------------
%% Format entity by JSON and XProps
%% --------------------------------------------------
internal_format_entity_filter_errors(JSON, XProps) when is_map(JSON), is_list(XProps) ->
    PropNames = [PName || {PName,_} <- XProps],
    JSON1 = maps:with(PropNames, JSON),
    maps:fold(fun(K,V,Acc) ->
                    {PType,PMulti,PItem,_PIdx} = ?BU:get_by_key(K,XProps),
                    case catch format_field(PType,PMulti,{K,V},PItem,undefined) of
                        {'EXIT',_} -> Acc;
                        {error,_} -> Acc;
                        skip -> Acc;
                        {ok,V1} -> Acc#{K => V1}
                    end end, #{}, JSON1).

%% --------------------------------------------------
%% Make merge of entity by new data on update operation
%%   Object props could be merged also instead of replace
%% --------------------------------------------------
update_merge(Entity0, Entity1, Meta, RequestMap) ->
    XAppendable = maps:get('x_appendable',Meta),
    case XAppendable of
        [] -> do_update_merge(Entity0, Entity1, Meta);
        [_|_] ->
            IsAppend = ?BU:to_bool(?BU:get_by_key(<<"append">>,maps:get(qs,RequestMap,[]),false)),
            case IsAppend of
                false -> do_update_merge(Entity0, Entity1, Meta);
                true -> do_update_append(Entity0, Entity1, Meta, XAppendable)
            end
    end.

%% @private
do_update_merge(Entity0, Entity1, Meta) ->
    XMerged = maps:get('x_merged',Meta),
    case maps:size(XMerged) of
        0 -> maps:merge(Entity0, Entity1);
        _ ->
            MergedKeys = maps:keys(XMerged),
            New0 = maps:merge(maps:without(MergedKeys,Entity0),maps:without(MergedKeys,Entity1)),
            maps:fold(fun(K,Levels,Acc) ->
                            V0 = maps:get(K,Entity0,u),
                            V1 = maps:get(K,Entity1,u),
                            merge_values(K,V0,V1,Acc,Levels)
                      end, New0, XMerged)
    end.

%% @private
do_update_append(Entity0, Entity1, Meta, XAppendable) ->
    AppendedEntity1 =
        lists:foldl(fun(K,Acc) ->
                         Acc#{K => maps:get(K,Entity0,[]) ++ maps:get(K,Entity1,[])}
                    end, Entity1, XAppendable),
    do_update_merge(Entity0, AppendedEntity1, Meta).

%% --------
%% @private
merge_values(K,V0,V1,Acc,Levels) ->
    T0 = typeof(V0),
    T1 = typeof(V1),
    case {T0,T1} of
        _ when V1==null -> Acc;
        {u,u} -> Acc;
        {_,u} -> Acc#{K => V0};
        {_,n} -> Acc#{K => V1};
        _ when T0/=T1 -> Acc#{K => V1};
        _ when V0==V1 -> Acc#{K => V1};
        {o,o} -> Acc#{K => V1};
        {l,l} -> Acc#{K => merge_internal(l,V0,V1,Levels)};
        {m,m} -> Acc#{K => merge_internal(m,V0,V1,Levels)}
    end.

%% @private
typeof(V) ->
    case {V, is_map(V), is_list(V)} of
        {u,_,_} -> u; % undefined
        {null,_,_} -> n; % null value
        {_,true,false} -> m; % map
        {_,false,true} -> l; % list
        {_,false,false} -> o % other type
    end.

%% @private
merge_internal(_,_,V1,0) -> V1;
merge_internal(l,_,V1,_) -> V1;
merge_internal(m,V0,V1,Levels) ->
    %?BU:merge_maps(V0,V1,Levels);
    maps:fold(fun(K,Vy,Acc) ->
                    Vx = maps:get(K,V0,u),
                    merge_values(K,Vx,Vy,Acc,Levels-1)
              end, #{}, maps:merge(V0,V1)).

%% --------------------------------------------------
%% Fill entity by defaults and check by required
%% TODO ICP: we could optimize here and remove this check for some runtime classes
%% --------------------------------------------------
%def_req(Entity,_) -> Entity;
def_req(Entity,Meta) ->
    case maps:get(<<"check_required_fill_defaults">>, maps:get(opts,Meta),true) of
        false -> Entity;
        true ->
            XDefaults = maps:get('x_defaults',Meta),
            XRequired = maps:get('x_required',Meta),
            check_required(fill_defaults(Entity,XDefaults),XRequired)
    end.

%% --------------------------------------------------
%% Fill NULL fields by defaults.
%% --------------------------------------------------
fill_defaults({error,_}=Err,_) -> Err;
fill_defaults(Entity,XDefaults) ->
    maps:merge(Entity,maps:merge(XDefaults,remove_nulls(Entity))).

%% --------------------------------------------------
%% Check if entity has NULL in required fields
%% --------------------------------------------------
check_required({error,_}=Err,_) -> Err;
check_required(Entity,[]) -> Entity;
check_required(Entity,XRequiredPropnames) ->
    KVs = maps:to_list(maps:with(XRequiredPropnames,Entity)),
    F = fun([K|KRest]=KAll) ->
            case KRest of
                [] -> {error,{invalid_params,?BU:strbin("field=~ts|Required value for field '~ts'",[K,K])}};
                _ -> {error,{invalid_params,?BU:strbin("field=~ts|Required values for fields: ~ts",[K,?BU:join_binary([<<"'",K1/binary,"'">> || K1<- KAll],", ")])}}
            end end,
    case length(KVs) == length(XRequiredPropnames) of
        false -> F(XRequiredPropnames -- lists:filtermap(fun({_,null}) -> false; ({K,_}) -> {true,K} end, KVs));
        true ->
            case lists:any(fun({_,null}) -> true; (_) -> false end, KVs) of
                true -> F(lists:filtermap(fun({K,null}) -> {true,K}; (_) -> false end, KVs));
                false -> Entity
            end end.

%% --------------------------------------------------
%% Decorates entity. Set null to all fields, or instead, remove nulls totally
%% --------------------------------------------------
decorate_entity({error,_}=Err,_) -> Err;
decorate_entity(Entity,_PropNames) when is_map(Entity) ->
    % TODO ICP: it could be parametrized by class opts
    % If we want to get on API always with nulls
    %fill_by_nulls(Entity,PropNames),
    % If we want to get on API always without nulls
    remove_nulls(Entity).

%% --------------------------------------------------
%% Fill entity by nulls
%% --------------------------------------------------
fill_by_nulls(Entity,PropNames) ->
    maps:merge(maps:from_list([{PN,'null'} || PN <- PropNames]), Entity).

%% --------------------------------------------------
%% Remove null value keys from entity
%% --------------------------------------------------
remove_nulls(Entity) ->
    maps:filter(fun(_K,V) -> V/='null' end, Entity).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
%% undefined value
format_field(PropType,Multi,{K,undefined},Prop,From) -> format_field(PropType,Multi,{K,null},Prop,From);
format_field(_,_,{K,null},Prop,_From) ->
    case maps:get(<<"required">>,Prop,false) of
        false -> {ok,null};
        true -> {error,{invalid_params,?BU:strbin("field=~ts|Invalid ~ts. Field value is required",[K,K])}}
    end;
%% defined value
format_field(<<"string">>,false,{_K,V},_Prop,_From) -> {ok,?BU:to_binary(V)};
format_field(<<"boolean">>,false,{_K,V},_Prop,_From) -> {ok,?BU:to_bool(V)};
format_field(<<"integer">>,false,{_K,V},_Prop,_From) -> {ok,?BU:to_integer(V)};
format_field(<<"increment">>,false,{_K,V},_Prop,_From) -> {ok,?BU:to_integer(V)};
format_field(<<"long">>,false,{_K,V},_Prop,_From) -> {ok,?BU:to_integer(V)};
format_field(<<"float">>,false,{_K,V},_Prop,_From) -> {ok,?BU:to_float(V)};
format_field(<<"datetime">>,false,{K,V},_Prop,_From) ->
    case ?BU:parse_datetime_utc(V) of
        {{1970,1,1},_,_} -> {error,{invalid_params,?BU:strbin("field=~ts|Invalid ~ts (~120tp). Expected valid RFC3339 datetime value",[K,K,V])}};
        {{1969,12,31},_,_} -> {error,{invalid_params,?BU:strbin("field=~ts|Invalid ~ts (~120tp). Expected valid RFC3339 datetime value",[K,K,V])}};
        V1 -> {ok,?BU:strdatetime3339(V1)}
    end;
format_field(<<"date">>,false,{K,V},_Prop,_From) ->
    case ?BU:parse_datetime(V) of
        {{1970,1,1},_,_} -> {error,{invalid_params,?BU:strbin("field=~ts|Invalid ~ts (~120tp). Expected valid time value",[K,K,V])}};
        {D,_,_} -> {ok,?BU:strdate(D)}
    end;
format_field(<<"time">>,false,{K,V},_Prop,_From) ->
    case catch ?BU:parse_time(V) of
        {'EXIT',_} ->
            case ?BU:parse_datetime(V) of
                {{1970,1,1},_,_} -> {error,{invalid_params,?BU:strbin("field=~ts|Invalid ~ts (~120tp). Expected valid time value",[K,K,V])}};
                {_,T,_} -> {ok,?BU:strtime(T)}
            end;
        {_H,_M,_S}=T -> {ok,?BU:strtime(T)}
    end;
format_field(<<"uuid">>,false,{K,V},_Prop,_From) ->
    EmptyId = ?BU:emptyid(),
    case ?BU:to_guid(V) of
        EmptyId when V/=EmptyId -> {error,{invalid_params,?BU:strbin("field=~ts|Invalid ~ts (~120tp). Expected valid uuid",[K,K,V])}};
        UUID -> {ok,UUID}
    end;
format_field(<<"enum">>,false,{K,V},Prop,_From) ->
    Items = maps:get(<<"items">>,Prop),
    case lists:member(V,Items) of
        false -> {error,{invalid_params,?BU:strbin("field=~ts|Invalid ~ts (~120tp). Expected: ~ts",[K,K,V,?BU:join_binary([<<"'",I/binary,"'">> || I <- Items],<<", ">>)])}};
        true -> {ok,V}
    end;
format_field(<<"any">>,false,{_K,V},_Prop,_From) ->
    case V of
        _ when is_binary(V) ->
            case catch jsx:decode(V,[return_maps]) of
                {'EXIT',_} -> {ok,V};
                V1 -> {ok,V1} % perhaps, it could be recursive for some reasons
            end;
        _ -> {ok,V}
    end;
format_field(<<"entity">>,false,{K,V},_Prop,_From) ->
    EmptyId = ?BU:emptyid(),
    case ?BU:to_guid(V) of
        EmptyId when V/=EmptyId -> {error,{invalid_params,?BU:strbin("field=~ts|Invalid ~ts (~120tp). Expected valid uuid",[K,K,V])}};
        UUID -> {ok,UUID}
    end;
%% single attachment
format_field(<<"attachment">>,false,{K,V},Prop,From) ->
    case V of
        {internal,Map} when is_map(Map) -> {ok,Map};
        {internal,null} -> {ok,null};
        _ when From=='db', is_binary(V) -> format_field(<<"any">>,false,{K,V},Prop,From);
        _ -> skip
        %_ -> {error,{invalid_operation,?BU:strbin("Invalid ~ts. Attachment property is readonly. Expected direct request to property endpoint.", [K])}}
    end;
%% array of attachments
format_field(<<"attachment">>,true,{K,V},Prop,From) ->
    case V of
        {internal,List} when is_list(List) -> {ok,List};
        {internal,null} -> {ok,null};
        _ when From=='db', is_binary(V) -> format_field(<<"any">>,true,{K,V},Prop,From);
        _ -> skip
        %_ -> {error,{invalid_operation,?BU:strbin("Invalid ~ts. Multi attachment property is readonly. Expected direct request to property endpoint.", [K])}}
    end;
%% array in json
format_field(PType,true,{K,V},Prop,From) when is_binary(V) ->
    case catch jsx:decode(V,[return_maps]) of
        V1 when is_list(V1) -> format_field(PType,true,{K,V1},Prop,From);
        _ -> {error,{invalid_params,?BU:strbin("field=~ts|Invalid ~ts. Expected list of ~ts",[K,K,PType])}}
    end;
%% array value
format_field(PType,true,{K,V},Prop,From) when is_list(V) ->
    lists:foldr(fun(_,{error,_}=Err) -> Err;
                   (Elt,{ok,Acc}) ->
                        case format_field(PType,false,{K,Elt},Prop,From) of
                            {error,_} -> {error,{invalid_params,?BU:strbin("field=~ts|Invalid ~ts. Expected list of ~ts",[K,K,PType])}};
                            {ok,EltV} -> {ok,[EltV|Acc]}
                        end end, {ok,[]}, V);
%% other
format_field(PType,true,{K,_V},_Prop,_From) ->
    {error,{invalid_params,?BU:strbin("field=~ts|Invalid ~ts. Expected list of ~ts",[K,K,PType])}}.

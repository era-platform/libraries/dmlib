%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 27.03.2021
%%% @doc Routines

-module(dmlib_domain_class_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    max_limit/2,
    reply_error/4,
    validate/4,
    assert_partition_not_changed/4,
    delete_attachments/3,
    clear_attachments/2
]).

-export([
    is_security_enabled/1,
    apply_security/3
]).

-export([
    push_increment/3,
    pull_increment/2
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----------------------------------------
%% return max limit for read query
%% ----------------------------------------
max_limit(Meta,SelectOpts) ->
    case maps:get('no_max_limit',SelectOpts,false) of % atom to avoid option from HTTP API
        false -> ?BU:get_by_key(<<"max_limit">>,maps:get(opts,Meta),?DefaultMaxLimit);
        true -> ?MaxMaxLimit
    end.

%% ----------------------------------------
%% prepare error answer and send reply
%% ----------------------------------------
reply_error(FunReply,CatchTerm,Operation,CN) when is_function(FunReply,1) ->
    case CatchTerm of
        {error,{Key,Reason}}=Err when (is_atom(Key) orelse is_integer(Key)) andalso is_binary(Reason) ->
            Reply = Err,
            FunReply(Reply);
        {error,{Key,Reason}} when (is_atom(Key) orelse is_integer(Key)) andalso is_list(Reason) ->
            Reply = {error,{Key,?BU:to_binary(Reason)}},
            FunReply(Reply);
        {error,Reason} when is_binary(Reason) ->
            Reply = {error,{internal_error,Reason}},
            FunReply(Reply);
        {error,Reason} when is_list(Reason) ->
            Reply = {error,{internal_error,?BU:to_binary(Reason)}},
            FunReply(Reply);
        {'EXIT',_}=Ex ->
            ?LOG('$error', "~ts:~ts crud ~ts on '~ts' exception: ~n\t~120tp", [?MODULE,?FUNCTION_NAME,Operation,CN,Ex]),
            Reply = {error,{internal_error,?BU:strbin("Internal error occured on operation ~ts",[Operation])}},
            FunReply(Reply)
    end.

%% ----------------------------------------
%% Call validator to check before modifications
%% ----------------------------------------
-spec validate(Meta::map(),
               Operation::create | replace | update | delete,
               EntityInfo::{Id::binary(),E0::map()|undefined,E1::map()|undefined} | undefined,
               ModifierInfo::{Type::atom(),Id::binary()} | undefined) ->
    {ok,Entity::map()} | {error,Reason::term()}.
%% ----------------------------------------
validate(Meta,Operation,EntityInfo,ModifierInfo) ->
    case maps:get(<<"validator">>,maps:get(opts,Meta),undefined) of
        undefined -> {ok,defined(EntityInfo)};
        <<>> -> {ok,defined(EntityInfo)};
        Validator ->
            case ?CFG:validate_function() of
                FunValidate when is_function(FunValidate,6) ->
                    Domain = maps:get('domain',Meta),
                    ClassName = maps:get('classname',Meta),
                    case FunValidate(Domain, Validator, ClassName, Operation, EntityInfo, ModifierInfo) of
                        {ok,EntityV} -> {ok,EntityV};
                        {error,_}=Err -> Err
                    end end end.

%% @private
defined({_,_,E1}) when E1/=undefined -> E1;
defined({_,E0,_}) when E0/=undefined -> E0;
defined(_) -> undefined.

%% ----------------------------------------
%% Check if partition datetime is not changed.
%%   Now partition datetime could not be changed
%%   (attachment problems are solved so, although db adapter is ready for this)
%% ----------------------------------------
assert_partition_not_changed(SM,_,_,_) when SM/='history', SM/='transactionlog'-> ok;
assert_partition_not_changed(_,<<>>,_,_) -> ok;
assert_partition_not_changed(_,_,undefined,_) -> ok;
assert_partition_not_changed(_,PartField,Entity0,Entity1) ->
    case {maps:get(PartField,Entity0), maps:get(PartField,Entity1)} of
        {A,A} -> ok;
        _ -> throw({error,{invalid_params,<<"Partition datetime could not be changed">>}})
    end.

%% ----------------------------------------
%% Call attachment storage on delete/clear operations
%% ----------------------------------------
delete_attachments(_ClassItem, [], _Entity) -> ok;
delete_attachments(ClassItem, _AttachmentPropnames, Entity) ->
    case ?CFG:delete_attachments_function() of
        undefined -> ok;
        Fun when is_function(Fun,3) ->
            Domain = maps:get('domain',ClassItem),
            Fun(Domain,ClassItem,Entity)
    end.

%% ----------------------------------------
%% Call attachment storage on delete/clear operations
%% ----------------------------------------
clear_attachments(_ClassItem, []) -> ok;
clear_attachments(ClassItem, _AttachmentPropnames) ->
    case ?CFG:clear_attachments_function() of
        undefined -> ok;
        Fun when is_function(Fun,2) ->
            Domain = maps:get('domain',ClassItem),
            Fun(Domain,ClassItem)
    end.


%% ====================================================================

is_security_enabled(Map) ->
    maps:is_key('x_security_fun',Map).

apply_security(Reply,Map,ErrorTerm) when is_map(Map) ->
    case Reply of
        {ok,Entity} ->
            case maps:get('x_security_fun',Map,undefined) of
                undefined -> Reply;
                SecFun when is_function(SecFun,1) ->
                    case SecFun(Entity) of
                        {ok,EntityX} -> {ok,EntityX};
                        false -> ErrorTerm;
                        ReplyX -> ReplyX
                    end
            end;
        _ -> Reply
    end.

%% ====================================================================

%% push last increment to reserved storage (by local gen_srv)
push_increment(IncrField,IncrValue,State) ->
    ?IncrementSrv:push(build_increment_key(IncrField,State),IncrValue).

%% pull last increment from storage
pull_increment(IncrField,State) ->
    Key = build_increment_key(IncrField,State),
    Local = case catch ?IncrementSrv:pull(Key) of
                {ok,IncrValue1} -> IncrValue1;
                _ -> 0
            end,
    Remote = case ?CFG:store_function() of
                 undefined -> 0;
                 Fun when is_function(Fun,2) ->
                     case Fun(pull,Key) of
                         {ok,IncrValue} -> IncrValue;
                         _ -> 0
                     end;
                 _ -> 0
             end,
    max(Local,Remote).

%% @private
build_increment_key(IncrField,#cstate{domain=Domain,classname=CN}) ->
    {increment,Domain,CN,IncrField}.

%% ====================================================================
%% Internal functions
%% ====================================================================

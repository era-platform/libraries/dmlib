%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 06.07.2021
%%% @doc

-module(dmlib_domain_class_changehistory).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([put/3,
         rollback/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---------
put('none',_,_) -> ok;
put('sync',Meta,{Operation,EntityInfo,ModifierInfo}) ->
    Domain = maps:get(domain,Meta),
    CN = maps:get(classname,Meta),
    Fun = ?CFG:store_changehistory_function(),
    Fun(Domain, CN, Meta, Operation, EntityInfo, modifierinfo(ModifierInfo));
put('async',Meta,{Operation,EntityInfo,ModifierInfo}) ->
    Domain = maps:get(domain,Meta),
    CN = maps:get(classname,Meta),
    Fun = ?CFG:store_changehistory_function(),
    ?DWORKER:cast_workf(Domain,CN,fun() -> Fun(Domain, CN, Meta, Operation, EntityInfo, modifierinfo(ModifierInfo)) end).

%% ---------
rollback('none',_,_) -> ok;
rollback('sync',Meta,{Operation,EntityInfo,ModifierInfo}) ->
    Domain = maps:get(domain,Meta),
    CN = maps:get(classname,Meta),
    Fun = ?CFG:store_changehistory_function(),
    {Operation1, EntityInfo1} = undo(Operation, EntityInfo),
    Fun(Domain, CN, Meta, Operation1, EntityInfo1, modifierinfo(ModifierInfo));
rollback('async',Meta,{Operation,EntityInfo,ModifierInfo}) ->
    Domain = maps:get(domain,Meta),
    CN = maps:get(classname,Meta),
    Fun = ?CFG:store_changehistory_function(),
    {Operation1, EntityInfo1} = undo(Operation, EntityInfo),
    ?DWORKER:cast_workf(Domain,CN,fun() -> Fun(Domain, CN, Meta, Operation1, EntityInfo1, modifierinfo(ModifierInfo)) end).

%% @private
undo('create',{Id,undefined,Entity}) -> {delete, {Id,Entity,undefined}};
undo('delete',{Id,Entity,undefined}) -> {create, {Id,undefined,Entity}};
undo('replace',{Id,undefined,Entity1}) -> {rollback, {Id,Entity1,undefined}};
undo('replace',{Id,Entity0,Entity1}) -> {replace, {Id,Entity1,Entity0}};
undo('update',{Id,Entity0,Entity1}) -> {update, {Id,Entity1,Entity0}};
undo('clear',undefined) -> {undo_clear, undefined}.

%% ====================================================================
%% Internal functions
%% ====================================================================

modifierinfo(undefined) -> {<<"system">>,?BU:emptyid()};
modifierinfo({undefined,undefined}) -> {<<"system">>,?BU:emptyid()};
modifierinfo({Type,undefined}) -> {Type,?BU:emptyid()};
modifierinfo({undefined,Id}) -> {<<"system">>,Id};
modifierinfo({_Type,_Id}=ModifierInfo) -> ModifierInfo.
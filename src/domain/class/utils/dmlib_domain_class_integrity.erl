%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.03.2021
%%% @doc Routines to handle integrity_mode (different sync/async algorythms)
%%%      Methods must be called from general facade process of data-model-class and pass work functions

-module(dmlib_domain_class_integrity).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    get_workers_count/1,
    has_separate_workers/1
]).

-export([
    read_operation/4,
    write_operation/6,
    block_operation/4
]).

-export([wait_flush_workers/2]).

-export([obtain_workers_stat/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------------------------
%% Define how many class workers to create. Check ClassMeta settings
%% TODO ICP: other drivers
%% TODO ICP: individual class properties
%% -----------------------------------------
get_workers_count(ClassMeta) ->
    StorageMode = ?BU:to_atom_new(maps:get(storage_mode,ClassMeta)), % ets | ram | runtime | category | history | transactionlog
    IntegrityMode = ?BU:to_atom_new(maps:get(integrity_mode,ClassMeta)), % async | sync | sync_fast_read | sync_fast_notify
    CacheMode = ?BU:to_atom_new(maps:get(cache_mode,ClassMeta)), % full | temp | none
    BasicCountR0 = ?CFG:class_readers_count(), % 10 -> 4
    BasicCountW0 = ?CFG:class_writers_count(), % 10 -> 4
    BasicCountR = case maps:get(<<"worker_r_count">>, maps:get(opts,ClassMeta), 0) of NR when NR>0 -> NR; _ -> BasicCountR0 end,
    BasicCountW = case maps:get(<<"worker_w_count">>, maps:get(opts,ClassMeta), 0) of NW when NW>0 -> NW; _ -> BasicCountW0 end,
    {R,W} = case {StorageMode,IntegrityMode,CacheMode} of
%%                {'ets',_,_} -> {BasicCountR, BasicCountW};
%%                {'ram',_,_} -> {BasicCountR, BasicCountW};
%%                {'runtime',_,_} -> {BasicCountR, BasicCountW};
%%                {'category','sync',_} -> {0,0};
%%                {'history','sync',_} -> {0,0};
%%                {'transactionlog','sync',_} -> {0,0};
%%                {'category',_,_} -> {0, BasicCountW};
%%                {'history',_,_} -> {0, BasicCountW};
%%                {'transactionlog',_,_} -> {0, BasicCountW};
                % 13.02.2024
                {_,'sync',_} -> {0,0}; % 13.02.2024
                %
                _ -> {BasicCountR, BasicCountW}
            end,
    #{'r' => R,'w' => W}.

%% -----------------------------------------
%% Define if separate reader worker should be created for next usage. Check ClassMeta settings
%% -----------------------------------------
has_separate_workers(ClassMeta) ->
    StorageMode = ?BU:to_atom_new(maps:get(storage_mode,ClassMeta)), % ets | ram | runtime | category | history | transactionlog
    IntegrityMode = ?BU:to_atom_new(maps:get(integrity_mode,ClassMeta)), % async | sync | sync_fast_read | sync_fast_notify
    CacheMode = ?BU:to_atom_new(maps:get(cache_mode,ClassMeta)), % full | temp | none
    case {StorageMode,IntegrityMode,CacheMode} of
        {_,'sync',_} -> {true, true}; % has reader, has writer
        _ -> {false, true} % has only writer
    end.

%% -----------------------------------------
%% Push operation of read (lookup, read_collection, may be read_entity) to work
%% -----------------------------------------
-spec read_operation(Fun::function(),FunReply::function(),Operation::atom(),State::#cstate{}) -> NewState::#cstate{}.
%% -----------------------------------------
read_operation(Fun,FunReply,Operation,State) ->
    #cstate{integrity_mode=IntegrityMode,
            storage_mode=StorageMode,
            srv_reader=ReaderPid,
            srv_writer=WorkerPid,
            srv_workers_r=WorkersR,
            srv_workers_w=WorkersW,
            sync_ref=SyncRef}=State,
    F = fun() ->
            case catch timer:tc(fun() -> Fun() end) of
                {Mks,ok} -> check_duration(Mks,Operation,undefined,State), ok;
                CatchTerm -> ?ClassU:reply_error(FunReply,CatchTerm,Operation,State#cstate.classname)
            end end,
    % TODO ICP: may be over worker process?
    case IntegrityMode of
        'sync' -> ?ClassR:work(ReaderPid,SyncRef,F);
        _ when StorageMode=='ets' -> ?ClassWrk:cast_workf(WorkersR,make_ref(),F); % while ets doesn't implement sync_fast_notify
        _ when StorageMode=='ram' -> ?ClassWrk:cast_workf(WorkersR,make_ref(),F); % while mnesia doesn't implement sync_fast_notify
        _ when StorageMode=='runtime' -> ?ClassWrk:cast_workf(WorkersR,make_ref(),F); % while mnesia doesn't implement sync_fast_notify
        % 'async' -> spawn_link(F);
        'async' -> ?ClassWrk:cast_workf(WorkersR,make_ref(),F); % 13.02.2024
        % 'sync_fast_read' -> spawn_link(F);
        'sync_fast_read' -> ?ClassWrk:cast_workf(WorkersR,make_ref(),F); % 13.02.2024
        'sync_fast_notify' ->
            FunSync = fun() ->
                            % Sync to all class workers (wait for active workers to finish DB change)
                            Additional = {WorkerPid,?ClassW,cast_workf},
                            sync_workers(WorkersW++WorkersR,10000,[Additional]),
                            % Apply function
                            % F()
                            ?ClassWrk:cast_workf(WorkersR,make_ref(),F) % 13.02.2024
                      end,
            spawn_link(FunSync) % spawn is required not to wait in queue of queues.
    end,
    State.

%% -----------------------------------------
%% Push operation of write to work (create, replace, update, delete, may be read_entity)
%% -----------------------------------------
write_operation(Fun,EntityId,HashRingKey,FunReply,Operation,State) ->
    #cstate{integrity_mode=IntegrityMode,
%%            storage_mode=StorageMode,
            srv_writer=WriterPid,
%%            srv_workers_r=WorkersR,
            srv_workers_w=WorkersW,
            sync_ref=SyncRef}=State,
    F = fun() when is_function(Fun,0) ->
               % if requested function has 0 arguments, then it should be applied directly. Ex. such is read_entity.
               case catch timer:tc(fun() -> Fun() end) of
                   {Mks,ok} -> check_duration(Mks,Operation,EntityId,State), ok;
                   CatchTerm -> ?ClassU:reply_error(FunReply,CatchTerm,Operation,State#cstate.classname)
               end;
           () when is_function(Fun,1) ->
               % If requested function has 1 argument, then it should run with 3-d order function (function/1 that gets function/0 to proceed run db query)
               % So here we pass there a dummy function/1 that applies passed db-query function/0 right here.
               % But another way is used when integrity_mode = 'sync_fast_notify'
               FunDummy = fun(FunDbWork) when is_function(FunDbWork,0) -> FunDbWork() end,
               case catch timer:tc(fun() -> Fun(FunDummy) end) of
                   {Mks,ok} -> check_duration(Mks,Operation,EntityId,State), ok;
                   CatchTerm -> ?ClassU:reply_error(FunReply,CatchTerm,Operation,State#cstate.classname)
               end
        end,
    % Workers should be used by hashring of Id to guarantee sequence of notification the same as sequence of write to db.
    case IntegrityMode of
        'sync' ->
            ?ClassW:work(WriterPid,SyncRef,F),
            State;
        'async' ->
            ?ClassWrk:cast_workf(WorkersW,HashRingKey,F),
            State;
        'sync_fast_read' ->
            ?ClassWrk:cast_workf(WorkersW,HashRingKey,F),
            State;
        'sync_fast_notify' ->
            ?ClassWrk:cast_workf(WorkersW,HashRingKey,F),
            State
    end.

%% -----------------------------------------
%% Push blocking operation to work (clear).
%% Setup pause to class, then acts by (W) process. But first, syncs to all class workers, then do work. Finally turn off pause.
%% So, there is no sync to (R) process.
%% -----------------------------------------
block_operation(Fun,FunReply,Operation,State) ->
    #cstate{srv_writer=WriterPid,
            srv_workers_w=WorkersW,
            srv_workers_r=WorkersR,
            sync_ref=SyncRef}=State,
    Self = self(),
    F = fun() ->
            % Sync to all class workers (wait for active workers to finish DB change)
            sync_workers(WorkersW++WorkersR,10000,[]),
            % Apply blocking function (pause is set to class)
            case catch Fun() of
                ok -> ok;
                CatchTerm -> ?ClassU:reply_error(FunReply,CatchTerm,Operation,State#cstate.classname)
            end,
            % Turn pause off
            Self ! {do_work_internal,SyncRef,fun(StateX) -> StateX#cstate{is_paused=false, pause_reason= <<>>} end},
            ok
        end,
    % In queue of (W) process
    ?ClassW:work(WriterPid,SyncRef,F),
    % Turn pause on
    State#cstate{is_paused=true,
                 pause_reason=?BU:strbin("Clear operation is running right now",[])}.

%% -----------------------------------------
%% Wait for all workers to flush queue
%% -----------------------------------------
wait_flush_workers(FunAfter,#cstate{}=State) when is_function(FunAfter,0) ->
    #cstate{srv_writer=WriterPid,
            srv_reader=ReaderPid,
            srv_workers_w=WorkersWZ,
            srv_workers_r=WorkersRZ,
            %srv_notify=NotifyPid,
            sync_ref=SyncRef}=State,
    % Sync to all class workers (wait for active workers to finish DB change)
    spawn(fun() ->
            WaitTimeout = infinity,
            %Self = self(),
            {_,WorkersW} = lists:unzip(WorkersWZ),
            {_,WorkersR} = lists:unzip(WorkersRZ),
            Flog = fun(_Pid,_Stage) -> ok end, % ?LOG('$info',"Queue flush waitor ~p ~ts",[Pid,Stage]) end,
            FunWaitR = fun() -> Flog(ReaderPid, "R ..."), ?ClassR:call_test(ReaderPid,SyncRef,fun() -> ok end,WaitTimeout) end,
            FunWaitW = fun() -> Flog(WriterPid, "W ..."), ?ClassW:call_test(WriterPid,SyncRef,fun() -> ok end,WaitTimeout) end,
            %FunWaitN = fun() -> NotifyPid ! {apply,fun() -> Self ! {ok,SyncRef} end}, receive {ok,SyncRef} -> ok after WaitTimeout -> ok end end,
            FunSync = fun(Pid) -> Flog(Pid, "Wrk ..."), ?ClassWrk:call_testf(Pid,fun() -> ok end,WaitTimeout) end,
            try lists:foreach(fun(Fun) -> Fun() end, [FunWaitR,FunWaitW | [fun() -> FunSync(Pid) end || Pid <- WorkersW++WorkersR]])
            catch E:R:ST -> ?LOG('$crash',"Queue flush waitor crushed: ~120tp, ~120tp~n\tStack: ~120tp",[E,R,ST])
            end,
            FunAfter()
          end).

%% -----------------------------------------
%% Called from class facade to obtain async reports from workers: previous period stat of requests
%% Workers (r,w,rs,ws,notify) handle request and send async reply.
%% Facade gather replies for timeout, compute aggregation and print result
obtain_workers_stat(Mode, #cstate{}=State) when Mode=='medium'; Mode=='high'; Mode=='default' ->
    #cstate{srv_writer=WriterPid,
            srv_reader=ReaderPid,
            srv_workers_w=WorkersWZ,
            srv_workers_r=WorkersRZ,
            %srv_notify=NotifyPid,
            sync_ref=SyncRef}=State,
    Self = self(),
    {_,WorkersW} = lists:unzip(WorkersWZ),
    {_,WorkersR} = lists:unzip(WorkersRZ),
    %
    WrkClassList = [{ReaderPid,?ClassR},
                    {WriterPid,?ClassW}
                    | [{Pid,?ClassWrk} || Pid <- WorkersW++WorkersR]],
    Workers = lists:filter(fun({undefined,_}) -> false;
                              (_) -> true
                           end, WrkClassList),
    %
    TS = ?BU:timestamp(),
    FunReply = fun(Reply) -> Self ! {worker_stat, SyncRef, TS, self(), Reply} end,
    lists:foreach(fun({Pid,Module}) -> Module:work(Pid,SyncRef,fun() -> FunReply(get_stat(Mode)) end) end, Workers),
    Timeout = 30000,
    State#cstate{wrk_stat=#wrk_stat{ts = TS,
                                    timerref = erlang:send_after(Timeout, Self, {worker_stat_timeout, SyncRef, TS}),
                                    workers_count = length(Workers),
                                    workers = Workers,
                                    workers_replies = []}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

sync_workers(Workers0,WaitTimeout,Additional) ->
    %FunSync = fun(Pid) -> ?ClassWrk:call_workf(Pid,fun() -> ok end,WaitTimeout) end,
    %SyncFunctions = [fun() -> FunSync(Pid) end || {_Index,Pid} <- Workers],
    %?BLmulticall:apply(AdditionalFunctions ++ SyncFunctions,WaitTimeout+100).
    Self = self(),
    Ref = make_ref(),
    Workers = [Pid || {_,Pid} <- Workers0],
    FunReport = fun(Pid) -> fun() -> Self ! {sync_report,Ref,Pid} end end,
    lists:foreach(fun(Pid) -> ?ClassWrk:cast_workf(Pid, FunReport(Pid)) end, Workers),
    AdditionalPids = lists:map(fun({Pid,Module,CastWorkF}) -> Module:CastWorkF(Pid, FunReport(Pid)), Pid end, Additional),
    StartTS = ?BU:timestamp(),
    F = fun F([]) -> ok;
            F(Rest) ->
                Timeout = WaitTimeout - ?BU:timestamp() + StartTS,
                receive {sync_report,Ref,Pid} -> F(lists:delete(Pid,Rest))
                after Timeout -> ok
                end
        end,
    F(Workers ++ AdditionalPids).

check_duration(Mks,Operation,_,_) when Mks =< 100000 ->
    update_stat(Mks,Operation),
    ok;
check_duration(Mks,Operation,EntityId,#cstate{domain=Domain,classname=CN}) ->
    update_stat(Mks,Operation),
    LogLevel = case Mks > 1000000 of
                   true -> '$warning';
                   false -> '$info'
               end,
    case EntityId of
        undefined -> ?LOG(LogLevel, "DUR. '~ts', '~ts'. I ~p ms on Op=~ts", [Domain,CN,Mks div 1000,Operation]);
        _ -> ?LOG(LogLevel, "DUR. '~ts', '~ts'. I ~p ms on Op=~ts, Id=~ts", [Domain,CN,Mks div 1000,Operation,EntityId])
    end.

%% @private
update_stat(Mks,Operation) ->
    Key = {stat,Operation},
    case get(Key) of
        undefined ->
            put(Key, {1,Mks,Mks,Mks});
        {Cnt,Min,Max,Sum} ->
            put(Key, {Cnt+1,min(Mks,Min),max(Mks,Max),Sum+Mks})
    end.

%% @private
get_stat('high') ->
    Operations = [clear,write_entity,create,delete,replace,update,read_entity,lookup,read_collection],
    lists:foldl(fun(Operation,Acc) ->
                        Key = {stat,Operation},
                        case get(Key) of
                            undefined -> Acc;
                            {_Cnt,_Min,_Max,_Sum}=T ->
                                erase(Key),
                                [{Operation,T} | Acc]
                        end
                end, [], Operations);
get_stat(_Mode) -> % medium | default
    Operations = [clear,write_entity,create,delete,replace,update,read_entity,lookup,read_collection],
    T = lists:foldl(fun(Operation,{CntAcc,MinAcc,MaxAcc,SumAcc}=Acc) ->
                        Key = {stat,Operation},
                        case get(Key) of
                            undefined -> Acc;
                            {Cnt,Min,Max,Sum} ->
                                erase(Key),
                                {CntAcc+Cnt,min(MinAcc,Min),max(MaxAcc,Max),SumAcc+Sum}
                        end
                    end, {0,10000000000,0,0}, Operations),
    [{mixed,T}].


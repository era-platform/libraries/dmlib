%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.03.2021
%%% @doc Domain-class worker supervisor for in-storage operations
%%%      Generally, storage connections are limited by pool.
%%%      Workers allow to parallel writer requests.
%%%      Hashrings by id of entity.

-module(dmlib_domain_class_worker_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/1]).
-export([init/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-define(WorkerCount,10).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    supervisor:start_link(?MODULE, Opts).

%% ====================================================================
%% Callbacks
%% ====================================================================

init(Opts) ->
    Domain = ?BU:get_by_key('domain',Opts),
    ClassName = ?BU:get_by_key('classname',Opts),
    ClassMeta = ?BU:get_by_key('class_meta',Opts),
    WrkInfo = ?Integrity:get_workers_count(ClassMeta),
    ChildSpecR = prepare_child_specs(Opts,'r',lists:seq(0,maps:get('r',WrkInfo,10)-1)),
    ChildSpecW = prepare_child_specs(Opts,'w',lists:seq(0,maps:get('w',WrkInfo,10)-1)),
    ?LOG('$info', "~ts. '~ts' * '~ts' worker supv inited", [?APP, Domain, ClassName]),
    {ok, {{one_for_one, 10, 2}, ChildSpecR++ChildSpecW}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

prepare_child_specs(Opts,GrCode,Indices) ->
    lists:map(fun(Index) ->
                    Name = ?BU:to_atom_new(?BU:str("worker_~ts_~2..0b",[GrCode,Index])),
                    {Name, {?ClassWrk, start_link, [[{'index',Index},{'group_code',GrCode} | Opts]]}, permanent, 1000, worker, [?ClassWrk]}
              end, Indices).
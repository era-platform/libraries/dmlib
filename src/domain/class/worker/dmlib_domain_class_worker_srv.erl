%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 12.03.2021
%%% @doc Domain-Class worker for in-storage operations
%%%      This implementation doesn't have reg-names, because of too many classes.
%%%      Instead they register directly in class' general facade server by indices.

-module(dmlib_domain_class_worker_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/1, stop/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([
    call_workf/3,
    call_testf/3,
    cast_workf/3,
    cast_workf/2,
    work/3
]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(wstate, {
    domain :: binary(),
    classname :: binary(),
    group_code :: 'r' | 'w',
    index :: integer(),
    sync_ref :: reference(),
    facade_regname :: binary()
}).
%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link(?MODULE, Opts, [{spawn_opt,[{fullsweep_after,10}]}]). % {spawn_opt,[{fullsweep_after,10}]}]).

stop(Name) ->
    gen_server:stop(Name).

%% -----------------------
%% Work F::function/0. Directly by worker's pid
%% -----------------------
call_workf(Pid, F, Timeout) when is_pid(Pid), is_function(F,0) ->
    gen_server:call(Pid, {workf,F}, Timeout).

call_testf(undefined, F, _) when is_function(F,0) -> F();
call_testf(Pid, F, Timeout) -> call_workf(Pid, F, Timeout).

%% -----------------------
%% Work F::function/0. Hashring automatically by Function value. Workers from list
%% -----------------------
-spec cast_workf(Workers::[{Index::non_neg_integer(),Pid::pid()}], HashCode::term(), F::function()) -> ok.
%% -----------------------
cast_workf(Workers, HashCode, F) when is_list(Workers), is_function(F,0) ->
    Length = length(Workers),
    Index = erlang:phash2(HashCode) rem Length,
    case lists:keyfind(Index,1,Workers) of
        {_,Pid} -> gen_server:cast(Pid, {workf,F});
        false -> throw({error,{internal_error,<<"Worker failed">>}})
    end.

%% -----------------------
%% Work F::function/0. Hashring automatically by Function value. Workers from list
%% -----------------------
-spec cast_workf(Pid::pid(), Fun::function()) -> ok.
%% -----------------------
cast_workf(Pid, Fun) when is_pid(Pid), is_function(Fun,0) ->
    gen_server:cast(Pid, {workf,Fun}).

%%
work(Pid, SyncRef, Fun) when is_pid(Pid) ->
    gen_server:cast(Pid,{work,SyncRef,Fun}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    [Domain,ClassName,GroupCode,Index,SyncRef] =
        ?BU:extract_required_props(['domain','classname','group_code','index','sync_ref'], Opts),
    FacadeRegName = ?ClassSrv:get_regname(Domain,ClassName),
    State = #wstate{domain = Domain,
                    classname = ClassName,
                    group_code = GroupCode,
                    index = Index,
                    sync_ref = SyncRef,
                    facade_regname = FacadeRegName},
    gen_server:cast(FacadeRegName, {register_srv,SyncRef,{{?MODULE,{GroupCode,Index}},self()}}),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------
handle_call({workf,F}, _From, State) ->
    Reply = try F()
            catch
                throw:R -> R;
                C:R:ST ->
                    ?LOG('$error', "DMS worker caught: ~120tp ",[{C,R,ST}]),
                    {error,{worker,R}}
            end,
    {reply, Reply, State};

%% other
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------
handle_cast({workf,F}, State) ->
    try F()
    catch C:R:ST -> ?LOG('$error', "DMS worker caught: ~120tp ",[{C,R,ST}])
    end,
    {noreply, State};

handle_cast({work,SyncRef,Fun},#wstate{sync_ref=SyncRef}=State) when is_function(Fun,0) ->
    try Fun()
    catch C:R:ST -> ?LOG('$error',"DMS worker caught: ~120tp ",[{C,R,ST}])
    end,
    {noreply, State};

%% other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.




%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 16.03.2021
%%% @doc In-domain data-model-class supervisor
%%%      Opts:
%%%         domain, classname, class_meta

-module(dmlib_domain_class_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(supervisor).

-export([start_link/1, get_linkname/2]).
-export([init/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    Domain = ?BU:get_by_key('domain',Opts),
    ClassName = ?BU:get_by_key('classname',Opts),
    RegName = get_linkname(Domain,ClassName),
    supervisor:start_link({local,RegName}, ?MODULE, Opts).

%% returns name of supv for linking to top supv
get_linkname(Domain,ClassName) when is_binary(Domain); is_atom(Domain) ->
    ?BU:to_atom_new(?BU:strbin("~ts:supv;dom:~ts;cn:~ts",[?APP,Domain,ClassName])).

%% ====================================================================
%% Callback functions
%% ====================================================================

init(Opts) ->
    Domain = ?BU:get_by_key('domain',Opts),
    ClassName = ?BU:get_by_key('classname',Opts),
    ClassMeta = ?BU:get_by_key('class_meta',Opts),
    SyncRef = make_ref(),
    Opts1 = [{'sync_ref',SyncRef} | Opts],
    % basic children
    ChildSpec = [{?ClassSrv, {?ClassSrv, start_link, [Opts1]}, permanent, 1000, worker, [?ClassSrv]},
                 {?Notify, {?Notify, start_link, [Opts1]}, permanent, 1000, worker, [?Notify]}
                 %{?Cache, {?Cache, start_link, [Opts1]}, permanent, 1000, worker, [?Cache]}
                ],
    % separate workers
    Rchild = {?ClassR, {?ClassR, start_link, [Opts1]}, permanent, 1000, worker, [?ClassR]},
    Wchild = {?ClassW, {?ClassW, start_link, [Opts1]}, permanent, 1000, worker, [?ClassW]},
    AdditionalWorkersSpec =
        case ?Integrity:has_separate_workers(ClassMeta) of
            {true,true} -> [Rchild,Wchild];
            {true,false} -> [Rchild];
            {false,true} -> [Wchild];
            {false,false} -> []
        end,
    % pool of workers
    PoolChild = {?ClassWrkSupv, {?ClassWrkSupv, start_link, [Opts1]}, permanent, 1000, supervisor, [?ClassWrkSupv]},
    WrkInfo = ?Integrity:get_workers_count(ClassMeta),
    PooledWorkersSpec =
        case maps:get('r',WrkInfo,10) + maps:get('w',WrkInfo,10) of
            N when N > 0 -> [PoolChild];
            _ -> []
        end,
    ?LOG('$info', "~ts. '~ts' * '~ts' supv inited", [?APP, Domain, ClassName]),
    {ok, {{rest_for_one, 10, 2}, ChildSpec ++ AdditionalWorkersSpec ++ PooledWorkersSpec}}.
%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 20.04.2021
%%% @doc CRUD routines for 'kafka'+'clickhouse' driver.
%%%      Used only when 'transactionlog' linked to both kafka and clickhouse storages by storage_instance.
%%%      Supports operations: all.

-module(dmlib_domain_class_crud_kafka_clickhouse).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([ensure_driver_state/1,
         update_driver_state/2]).
-export([load/1]).
-export([lookup/3,
         read/3,
         create/3,
         replace/3,
         update/3,
         delete/3,
         clear/3,
         optimize/3,
         read_partitions/3,
         drop_partition/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(ReadTimeout, 60000).

-record(dstate_kafka_clickhouse, {
    db_params :: map(),
    module :: atom(),
    replace_without_read :: boolean(),
    props :: term(), % props from connect/ensure #{brokers,cluster_id,controller_id,topic_metadata}
    topic :: binary(),  % name of topic
    partition_count :: integer(), % count of partitions for topic (from kafka metadata)
    brokers :: [map()], % list of brokers [#{host,port,node_id,rack}]
    partitions_endpoints :: map(), % map from partition index (0-N) to its kafka-node endpoint {host,port}
    clickhouse :: undefined | map(),
    %
    increment_field = undefined :: undefined | binary(), % incremented
    increment = 0 :: integer() % incremented
}).

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------
%% Initialize driver state at start of class process
%% -----------------------
ensure_driver_state(#cstate{driver_state=#dstate_kafka_clickhouse{}}=State) -> State;
ensure_driver_state(#cstate{domain=Domain,class_meta=Meta}=State) ->
    Opts = maps:get(opts,Meta),
    DState = #dstate_kafka_clickhouse{replace_without_read=maps:get(<<"replace_without_read">>,Opts,false)},
    case ?Kafka:get_db_params(Domain,Meta) of
        {ok,DbParams} ->
            State#cstate{driver_state=DState#dstate_kafka_clickhouse{db_params=DbParams,module=?KafkaCrud,
                                                                     increment_field = maps:get('x_incremented',Meta,undefined)}}; % incremented
        {error,_}=Err ->
            timer:sleep(1000),
            exit(Err)
    end.

%% --------------------------------------------------
%% Update state for driver by connect/ensure result
%% --------------------------------------------------
update_driver_state(Props,#cstate{integrity_mode=IntegrityMode,cache_mode=CacheMode,driver_state=#dstate_kafka_clickhouse{}=DState}=State) ->
    % TODO ICP: fill leader partition replica
    Brokers = maps:get(brokers,Props),
    BrokersMap = lists:foldl(fun(Broker,Acc) -> Acc#{maps:get(node_id,Broker) => {maps:get(host,Broker),maps:get(port,Broker)}} end, #{}, Brokers),
    [TopicMeta|_] = maps:get(topics,Props), % topic_metadata
    Topic = maps:get(name,TopicMeta), % topic
    PartitionMeta = maps:get(partitions,TopicMeta), % partition_metadata
    PartitionCount = length(PartitionMeta),
    PartEPs = lists:foldl(fun(Part,Acc) -> Acc#{maps:get(partition_index,Part) => maps:get(maps:get(leader_id,Part),BrokersMap)} end, #{}, PartitionMeta),
    SendModeDefault = case IntegrityMode of 'async' -> <<"async">>; _ -> <<"sync">> end,
    SendMode = case maps:get(kafka_storage_opts,Props,undefined) of 
                   undefined -> SendModeDefault; 
                   KafkaStorageOpts -> 
                       case maps:get(<<"send_mode">>,KafkaStorageOpts,undefined) of
                           <<"sync">> -> <<"sync">>;
                           <<"async">> -> <<"async">>;
                           _ -> SendModeDefault
                       end end,
    ClickHouseProps = maps:get(clickhouse,Props,undefined),
    State#cstate{cache_mode = case ClickHouseProps of
                                  undefined -> 'none';
                                  _ when CacheMode=='full' -> 'temp';
                                  _ -> CacheMode % none should be used only if there is no updates/replaces, because clickhouse got data in async way
                              end,
                 driver_state = DState#dstate_kafka_clickhouse{props = Props#{topic => Topic,
                                                                              partition_count => PartitionCount,
                                                                              partition_endpoints => PartEPs,
                                                                              mode => SendMode},
                                                               brokers = Brokers,
                                                               topic = Topic,
                                                               partition_count = PartitionCount,
                                                               partitions_endpoints = PartEPs,
                                                               clickhouse = ClickHouseProps }}.

%% -----------------------
%% Load dummy, kafka has no cache
%% -----------------------
load(State) ->
    State1 = do_read_max_increment(State), % incremented
    {ok,State1}.


%% @private
%% incremented
do_read_max_increment(State) ->
    #cstate{driver_state=#dstate_kafka_clickhouse{increment_field=IncrField}=DS}=State,
    case IncrField of
        undefined -> State;
        _ when is_binary(IncrField) ->
            StoreMax = ?ClassU:pull_increment(IncrField,State),
            InitiatorInfo = {'system',undefined},
            {Meta,PropNames,_Cache,_DbParams,_KafkaProps,CHProps,_NotifyPid} = extract_params(InitiatorInfo,State),
            case CHProps of
                undefined -> State;
                _ ->
                    FromDt = <<"1970-01-01 00:00:00">>,
                    TillDt = ?BU:strdatetime(?BU:timestamp_to_datetime(?BU:timestamp() + 86400000 * 365 * 10)),
                    SelectOpts = #{
                        <<"aggr">> => #{<<"max">> => [<<"max">>,[<<"property">>,IncrField]]},
                        <<"interval">> => [FromDt,TillDt],
                        <<"filter">> => [],
                        <<"mask">> => [],
                        <<"group">> => #{},
                        <<"order">> => []
                    },
                    case ?ClickHouseCrud:read(Meta,SelectOpts,CHProps,PropNames) of
                        {ok,[AggrItem]} ->
                            DbMax = case maps:get(<<"max">>,AggrItem,0) of
                                        null -> 0;
                                        V -> V
                                    end,
                            Max = max(DbMax,StoreMax),
                            State#cstate{driver_state=DS#dstate_kafka_clickhouse{increment=Max}};
                        {error,_}=Err ->
                            ?LOG('$error', "Load max increment failed: ~120tp", [Err]),
                            State#cstate{driver_state=DS#dstate_kafka_clickhouse{increment=StoreMax}}
                    end
            end
    end.

%% -----------------------
%% LOOKUP operation
%% TODO: use materialized view on lookup properties
%% -----------------------
lookup(_Map,FunReply,#cstate{}=State) ->
    FunReply({error,{405,<<"Method not allowed for class of storage_type='transactionlog'">>}}),
    {noreply,State}.

%% -----------------------
%% READ collection operation
%% READ entity operation
%% -----------------------
read({collection,Map,SelectOpts},FunReply,#cstate{}=State) ->
    InitiatorInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,PropNames,_Cache,_DbParams,_KafkaProps,CHProps,_NotifyPid} = extract_params(InitiatorInfo, State),
    case CHProps of
        undefined ->
            FunReply({error,{405,<<"Method not allowed for class of storage_type='transactionlog' without linked storage of type='clickhouse'">>}}),
            {noreply,State};
        _ ->
            AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
            F = fun() ->
                    % Check canceled from queue
                    case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
                    %
                    Reply = case State#cstate.cache_mode of
                                %@@@@@@@@@@@@@@
                                'none' ->
                                    % DB
                                    ?ClickHouseCrud:read(Meta,SelectOpts,CHProps,PropNames);
                                %@@@@@@@@@@@@@@
                                'temp' ->
                                    % DB
                                    ?ClickHouseCrud:read(Meta,SelectOpts,CHProps,PropNames)
                                % Update by cache when async/sync_fast_...
                                %@@@@@@@@@@@@@@
                            end,
                    % Security filter and masking
                    ReplyX = ?ClassU:apply_security(Reply,Map,{error,{not_found,<<"Entity not found">>}}),
                    % Reply
                    FunReply(ReplyX)
                end,
            State1 = ?Integrity:read_operation(F,FunReply,read_collection,State),
            {noreply,State1}
    end;
%% -----------------------
read({entity,Map,Id,EOpts},FunReply,#cstate{}=State) ->
    InitiatorInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,PropNames,Cache,_DbParams,_KafkaProps,CHProps,_NotifyPid} = extract_params(InitiatorInfo, State),
    case CHProps of
        undefined ->
            FunReply({error,{405,<<"Method not allowed for class of storage_type='transactionlog' without linked storage of type='clickhouse'">>}}),
            {noreply,State};
        _ ->
            % This is read by primary_key, it's fast and is equal part to find operation during create/replace/update/delete request
            OperationMode = 'w',
            PathDt = ?BU:get_by_key('path',EOpts,undefined), % partition dt
            AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
            F = fun() ->
                    % Check canceled from queue
                    case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
                    % DB
                    Reply = case State#cstate.cache_mode of
                                %@@@@@@@@@@@@@@
                                'none' ->
                                    case ?ClickHouseCrud:find(Meta,{PathDt},Id,CHProps,PropNames) of
                                        {ok,Entity} -> {ok,Entity};
                                        {error,_}=Err -> Err;
                                        false -> {error,{not_found,?BU:strbin("Entity not found",[])}}
                                    end;
                                %@@@@@@@@@@@@@@
                                'temp' ->
                                    case ?StorageEts:find(Cache,Id) of
                                        {ok,Entity} -> {ok,Entity};
                                        false ->
                                            case ?ClickHouseCrud:find(Meta,{PathDt},Id,CHProps,PropNames) of
                                                {ok,Entity} when OperationMode=='w' ->
                                                    ?StorageEts:create(Cache,Entity),
                                                    {ok,Entity};
                                                false -> {error,{not_found,?BU:strbin("Entity not found",[])}};
                                                {error,_}=Err -> Err
                                            end end
                                %@@@@@@@@@@@@@@
                            end,
                    % Security filter and masking
                    ReplyX = ?ClassU:apply_security(Reply,Map,{error,{not_found,<<"Entity not found.">>}}),
                    % Reply
                    FunReply(ReplyX)
                end,
            State1 = case OperationMode of
                         'r' -> ?Integrity:read_operation(F,FunReply,read_entity,State);
                         'w' -> ?Integrity:write_operation(F,Id,maps:get(hrkey,Map,Id),FunReply,read_entity,State)
                     end,
            {noreply,State1}
    end.

%% -----------------------
%% POST operation
%% -----------------------
create(Map,FunReply,State) ->
    SkipResponseEntity = maps:get(skip_response_entity,Map,false),
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,PropNames,Cache,DbParams,KafkaProps,_CHProps,NotifyPid} = extract_params(ModifierInfo, State),
    #cstate{cache_mode=CM,integrity_mode=IM,sync_ref=SyncRef}=State,
    Opts = maps:get(opts,Meta),
    PartField = maps:get(<<"partition_property">>,Opts),
    DoNotify = maps:get(<<"notify_transactions">>,Opts,false),
    Fup1 = fun(EntityX) when PartField == <<>> -> EntityX;
              (EntityX) -> case maps:get(PartField,EntityX,null) of
                               null -> EntityX#{PartField => ?BU:strdatetime3339(?BU:ticktoutc(?BU:timestamp()))};
                               _ -> EntityX
                           end end,
    {ok,State1,Fup} = increment(State,Fup1), % incremented
    % Id
    Id = ?BU:newid(),
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun(FunProceedDbWork) ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            % Build
            case ?Format:def_req(?Format:decorate_entity(?Format:build_entity(Map,Meta,Id),PropNames),Meta) of
                {error,_}=Err -> FunReply(Err);
                EntityX ->
                    Entity = Fup(EntityX), % Partition field is required. Default value is CURRENT UTC
                    Id = maps:get(<<"id">>,Entity), % assert
                    % Validator
                    case ?ClassU:validate(Meta,'create',{Id,undefined,Entity},ModifierInfo) of
                        {error,_}=Err1 -> FunReply(Err1);
                        {ok,EntityV} ->
                            % Security function (external) filter
                            case ?ClassU:apply_security({ok,EntityV},Map,{error,{access_denied,<<"Access denied. Restricted value.">>}}) of
                                {ok,EntityS} ->
                                    % Security function (external) masking
                                    EntityN = case EntityV of
                                                  EntityS -> EntityS;
                                                  _ ->
                                                      case ?Format:def_req(EntityS,Meta) of
                                                          {error,_}=ErrS -> throw(ErrS);
                                                          EntityS_ -> Fup(EntityS_)
                                                      end end,
                                    % Notify when 'sync_fast_notify'
                                    mb_notify(DoNotify,IM=='sync_fast_notify',NotifyPid,{'create',{Id,undefined,EntityN},ModifierInfo}),
                                    % Update cache when 'sync_fast_notify'
                                    mb_to_cache(IM=='sync_fast_notify',CM/='none',fun() -> ?StorageEts:create(Cache,EntityN) end),
                                    % DB
                                    FunOnCorruptCache = fun() -> ?StorageEts:delete(Cache,Id) end,
                                    FunOnCorruptNotify = fun() -> mb_notify(DoNotify,true,NotifyPid,{'corrupt',{Id,EntityN,undefined},ModifierInfo}) end,
                                    FunDbWork =
                                        fun() ->
                                            case State#cstate.cache_mode of
                                                %@@@@@@@@@@@@@@
                                                'none' ->
                                                    ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                                  [FunOnCorruptNotify],
                                                                  ?KafkaCrud:create(Meta,EntityN,DbParams,KafkaProps));
                                                %@@@@@@@@@@@@@@
                                                'temp' ->
                                                    ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                                   [FunOnCorruptCache,FunOnCorruptNotify],
                                                                   ?KafkaCrud:create(Meta,EntityN,DbParams,KafkaProps)),
                                                    ok = ?StorageEts:create(Cache,EntityN)
                                                %@@@@@@@@@@@@@@
                                            end,
                                            % Notify when not 'sync_fast_notify'
                                            mb_notify(DoNotify,IM/='sync_fast_notify',NotifyPid,{'create',{Id,undefined,EntityN},ModifierInfo}),
                                            % Reply message
                                            {ok,EntityN}
                                        end,
                                    reply_skip(FunProceedDbWork(FunDbWork),FunReply,?FUNCTION_NAME,SkipResponseEntity);
                                ReplySecurity ->
                                    FunReply(ReplySecurity)
                            end
                    end
            end
        end,
    State2 = ?Integrity:write_operation(F,Id,maps:get(hrkey,Map,Id),FunReply,create,State1),
    {noreply,State2}.

%% -----------------------
%% PUT operation
%% -----------------------
replace(Map,FunReply,State) ->
    SkipResponseEntity = maps:get(skip_response_entity,Map,false),
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,PropNames,Cache,DbParams,KafkaProps,CHProps,NotifyPid} = extract_params(ModifierInfo, State),
    #cstate{storage_mode=SM,cache_mode=CM,integrity_mode=IM,sync_ref=SyncRef,
            driver_state=#dstate_kafka_clickhouse{replace_without_read=ReplaceWithoutRead0,increment_field=IncrField}}=State,
    ReplaceWithoutRead = case ReplaceWithoutRead0 of false -> false; true -> not ?ClassU:is_security_enabled(Map) end,
    Opts = maps:get(opts,Meta),
    PartField = maps:get(<<"partition_property">>,Opts),
    DoNotify = maps:get(<<"notify_transactions">>,Opts,false),
    Fup1 = fun(EntityX,_) when PartField == <<>> -> EntityX;
              (EntityX,DefaultDt) -> case maps:get(PartField,EntityX,null) of
                                         null ->
                                             try ?BU:parse_datetime(DefaultDt) of
                                                 {{1970,1,1},{0,0,0},0} -> EntityX#{PartField => ?BU:strdatetime3339(?BU:ticktoutc(?BU:timestamp()))};
                                                 Dt -> EntityX#{PartField => ?BU:strdatetime3339(Dt)}
                                             catch _:_ -> EntityX#{PartField => ?BU:strdatetime3339(?BU:ticktoutc(?BU:timestamp()))}
                                             end;
                                         _ -> EntityX
                                     end end,
    {ok,State1,Fup} = increment(State,Fup1), % incremented
    % Id
    {'entity',Id,EOpts} = maps:get('object',Map),
    PathDtX0 = ?BU:get_by_key('path',EOpts,undefined), % prev partition dt
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun(FunProceedDbWork) ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            % Build
            case ?Format:def_req(?Format:decorate_entity(?Format:build_entity(Map,Meta),PropNames),Meta) of
                {error,_}=Err -> FunReply(Err);
                EntityX1 ->
                    Entity1 = Fup(EntityX1,PathDtX0), % Partition field is required. Default value is CURRENT UTC
                    Id = maps:get(<<"id">>,Entity1),
                    Entity0 = case ReplaceWithoutRead of
                                  true -> undefined;
                                  false when CHProps==undefined -> undefined;
                                  false ->
                                      case State#cstate.cache_mode of
                                          % TODO: when no notify then no need to find on replace
                                          % _ when false -> undefined;
                                          %@@@@@@@@@@@@@@
                                          'none' -> % ReplaceWithoutRead = true forcely
                                              % Find
                                              case ?ClickHouseCrud:find(Meta,{PathDtX0},Id,CHProps,PropNames) of
                                                  {error,_}=Err -> throw(Err);
                                                  false -> undefined;
                                                  {ok,E0} -> E0
                                              end;
                                          %@@@@@@@@@@@@@@
                                          'temp' ->
                                              % Find
                                              case ?StorageEts:find(Cache,Id) of
                                                  {ok,E0} -> E0;
                                                  %false -> undefined; % ReplaceWithoutRead = true forcely
                                                  false ->
                                                      case ?ClickHouseCrud:find(Meta,{PathDtX0},Id,CHProps,PropNames) of
                                                          {error,_}=Err -> throw(Err);
                                                          false -> undefined;
                                                          {ok,E0} -> E0 % no need to store previous version
                                                      end end
                                          %@@@@@@@@@@@@@@
                                      end end,
                    Reply = case Entity1 of
                                Entity0 ->
                                    % Security function (external) filter
                                    case ?ClassU:apply_security({ok,Entity0},Map,{error,{access_denied,<<"Access denied.">>}}) of
                                        {ok,_} -> {ok,Entity0};
                                        ReplySecurity -> ReplySecurity
                                    end;
                                _ ->
                                    % Validator
                                    case ?ClassU:validate(Meta,'replace',{Id,Entity0,Entity1},ModifierInfo) of
                                        {error,_}=Err1 -> Err1;
                                        {ok,EntityV0} ->
                                            % incremented
                                            EntityV = case Entity0 of
                                                          undefined -> EntityV0;
                                                          _ when IncrField==undefined -> EntityV0;
                                                          _ ->
                                                              case maps:get(IncrField,Entity0,undefined) of
                                                                  undefined -> EntityV0;
                                                                  IncrValue -> EntityV0#{IncrField => IncrValue}
                                                              end end,
                                            % Security function (external) filter
                                            SecOld = case Entity0 of
                                                         undefined -> {ok,undefined};
                                                         _ -> ?ClassU:apply_security({ok,Entity0},Map,{error,{access_denied,<<"Access denied.">>}})
                                                     end,
                                            SecNew = ?ClassU:apply_security({ok,EntityV},Map,{error,{access_denied,<<"Access denied. Restricted value.">>}}),
                                            case {SecOld,SecNew} of
                                                {{ok,_},{ok,EntityS}} ->
                                                    % Security function (external) masking
                                                    EntityN = case EntityV of
                                                                  EntityS -> EntityS;
                                                                  _ ->
                                                                      case Entity0 of
                                                                          undefined ->
                                                                              % Security function (external) masking
                                                                              case ?Format:def_req(EntityS,Meta) of
                                                                                  {error,_}=ErrS -> throw(ErrS);
                                                                                  EntityS_ -> Fup(EntityS_,PathDtX0)
                                                                              end;
                                                                          _ -> maps:merge(Entity0,EntityS)
                                                                      end end,
                                                    % Check partition is not changed
                                                    ?ClassU:assert_partition_not_changed(SM,PartField,Entity0,EntityN),
                                                    % Notify when 'sync_fast_notify'
                                                    case Entity0 of
                                                        undefined -> mb_notify(DoNotify,IM=='sync_fast_notify',NotifyPid,{'create',{Id,undefined,EntityN},ModifierInfo});
                                                        _ -> mb_notify(DoNotify,IM=='sync_fast_notify',NotifyPid,{'update',{Id,Entity0,EntityN},ModifierInfo})
                                                    end,
                                                    % Update cache when 'sync_fast_notify'
                                                    mb_to_cache(IM=='sync_fast_notify',CM/='none',fun() -> ?StorageEts:replace(Cache,EntityN) end),
                                                    % DB
                                                    FunOnCorruptCache = fun() -> ?StorageEts:delete(Cache,Id) end,
                                                    FunOnCorruptNotify = fun() when Entity0==undefined -> mb_notify(DoNotify,true,NotifyPid,{'corrupt',{Id,EntityN,undefined},ModifierInfo});
                                                                            () -> mb_notify(DoNotify,true,NotifyPid,{'corrupt',{Id,EntityN,Entity0},ModifierInfo}) end,
                                                    FunDbWork =
                                                        fun() ->
                                                            case State#cstate.cache_mode of
                                                                %@@@@@@@@@@@@@@
                                                                'none' ->
                                                                    % DB
                                                                    ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                                                   [FunOnCorruptNotify],
                                                                                   ?KafkaCrud:replace(Meta,EntityN,DbParams,KafkaProps));
                                                                %@@@@@@@@@@@@@@
                                                                'temp' ->
                                                                    ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                                                   [FunOnCorruptCache,FunOnCorruptNotify],
                                                                                   ?KafkaCrud:replace(Meta,EntityN,DbParams,KafkaProps)),
                                                                    ok = ?StorageEts:replace(Cache,EntityN)
                                                                %@@@@@@@@@@@@@@
                                                            end,
                                                            % Notify when not 'sync_fast_notify'
                                                            case Entity0 of
                                                                undefined -> mb_notify(DoNotify,IM/='sync_fast_notify',NotifyPid,{'create',{Id,undefined,EntityN},ModifierInfo});
                                                                _ -> mb_notify(DoNotify,IM/='sync_fast_notify',NotifyPid,{'update',{Id,Entity0,EntityN},ModifierInfo})
                                                            end,
                                                            % Reply message
                                                            {ok,EntityN}
                                                        end,
                                                    FunProceedDbWork(FunDbWork);
                                                {{ok,_},ReplySecurity} -> ReplySecurity;
                                                {ReplySecurity,_} -> ReplySecurity
                                            end
                                    end
                            end,
                    reply_skip(Reply,FunReply,?FUNCTION_NAME,SkipResponseEntity)
            end
        end,
    State2 = ?Integrity:write_operation(F,Id,maps:get(hrkey,Map,Id),FunReply,replace,State1),
    {noreply,State2}.

%% -----------------------
%% PATCH operation
%% -----------------------
update(Map,FunReply,State) ->
    SkipResponseEntity = maps:get(skip_response_entity,Map,false),
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,PropNames,Cache,DbParams,KafkaProps,CHProps,NotifyPid} = extract_params(ModifierInfo, State),
    case CHProps of
        undefined ->
            FunReply({error,{405,<<"Method not allowed for class of storage_type='transactionlog' without linked storage of type='clickhouse'">>}}),
            {noreply,State};
        _ ->
            #cstate{storage_mode=SM,cache_mode=CM,integrity_mode=IM,sync_ref=SyncRef,
                    driver_state=#dstate_kafka_clickhouse{increment_field=IncrField}}=State,
            Opts = maps:get(opts,Meta),
            PartField = maps:get(<<"partition_property">>,Opts),
            DoNotify = maps:get(<<"notify_transactions">>,Opts,false),
            % Id
            {'entity',Id,EOpts} = maps:get('object',Map),
            PathDt0 = ?BU:get_by_key('path',EOpts,undefined), % prev partition dt
            AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
            F = fun(FunProceedDbWork) ->
                    % Check canceled from queue
                    case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
                    % Build !!! without decorate to update by nulls
                    case ?Format:build_entity(Map,Meta) of
                        {error,_}=Err -> FunReply(Err);
                        Entity ->
                            % Find
                            Id = maps:get(<<"id">>,Entity),
                            Entity0 =
                                case State#cstate.cache_mode of
                                    %@@@@@@@@@@@@@@
                                    'none' ->
                                        % Find
                                        case ?ClickHouseCrud:find(Meta,{PathDt0},Id,CHProps,PropNames) of
                                            {error,_}=Err -> throw(Err);
                                            false -> throw({error,{not_found,?BU:strbin("Entity not found",[])}});
                                            {ok,E0} -> E0
                                        end;
                                    %@@@@@@@@@@@@@@
                                    'temp' ->
                                        % Find
                                        case ?StorageEts:find(Cache,Id) of
                                            {ok,E0} -> E0;
                                            false ->
                                                case ?ClickHouseCrud:find(Meta,{PathDt0},Id,CHProps,PropNames) of
                                                    {error,_}=Err -> throw(Err);
                                                    false -> throw({error,{not_found,?BU:strbin("Entity not found",[])}});
                                                    {ok,E0} ->
                                                        ?StorageEts:create(Cache,E0),
                                                        E0
                                                end end
                                    %@@@@@@@@@@@@@@
                                end,
                            % Build (no decorate to null fields, decorate return value only)
                            Reply = case ?Format:def_req(?Format:update_merge(Entity0,Entity,Meta,Map),Meta) of
                                        {error,_}=Err1 -> Err1;
                                        Entity0 ->
                                            % Security function (external) filter
                                            case ?ClassU:apply_security({ok,Entity0},Map,{error,{not_found,<<"Entity not found.">>}}) of
                                                {ok,_} -> {ok,Entity0};
                                                ReplySecurity -> ReplySecurity
                                            end;
                                        Entity1 ->
                                            % Validator
                                            case ?ClassU:validate(Meta,'update',{Id,Entity0,Entity1},ModifierInfo) of
                                                {error,_}=Err1 -> Err1;
                                                {ok,EntityV0} ->
                                                    % incremented
                                                    EntityV = case Entity0 of
                                                                  undefined -> EntityV0;
                                                                  _ when IncrField==undefined -> EntityV0;
                                                                  _ ->
                                                                      case maps:get(IncrField,Entity0,undefined) of
                                                                          undefined -> EntityV0;
                                                                          IncrValue -> EntityV0#{IncrField => IncrValue}
                                                                      end end,
                                                    % Security function (external) filter
                                                    SecOld = ?ClassU:apply_security({ok,Entity0},Map,{error,{access_denied,<<"Access denied.">>}}),
                                                    SecNew = ?ClassU:apply_security({ok,EntityV},Map,{error,{access_denied,<<"Access denied. Restricted value.">>}}),
                                                    case {SecOld,SecNew} of
                                                        {{ok,_},{ok,EntityS}} ->
                                                            % Security function (external) masking
                                                            EntityN = maps:merge(Entity0,EntityS),
                                                            % Check partition is not changed
                                                            ?ClassU:assert_partition_not_changed(SM,PartField,Entity0,EntityN),
                                                            % Notify when 'sync_fast_notify'
                                                            mb_notify(DoNotify,IM=='sync_fast_notify',NotifyPid,{'update',{Id,Entity0,EntityN},ModifierInfo}),
                                                            % Update cache when 'sync_fast_notify'
                                                            mb_to_cache(IM=='sync_fast_notify',CM/='none',fun() -> ?StorageEts:update(Cache,EntityN) end),
                                                            % DB
                                                            FunOnCorruptCache = fun() -> ?StorageEts:delete(Cache,Id) end,
                                                            FunOnCorruptNotify = fun() -> mb_notify(DoNotify,true,NotifyPid,{'corrupt',{Id,EntityN,Entity0},ModifierInfo}) end,
                                                            FunDbWork =
                                                                fun() ->
                                                                    case State#cstate.cache_mode of
                                                                        %@@@@@@@@@@@@@@
                                                                        'none' ->
                                                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                                                           [FunOnCorruptNotify],
                                                                                           ?KafkaCrud:update(Meta,EntityN,DbParams,KafkaProps));
                                                                        %@@@@@@@@@@@@@@
                                                                        'temp' ->
                                                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                                                           [FunOnCorruptCache,FunOnCorruptNotify],
                                                                                           ?KafkaCrud:update(Meta,EntityN,DbParams,KafkaProps)),
                                                                            ok = ?StorageEts:update(Cache,EntityN)
                                                                        %@@@@@@@@@@@@@@
                                                                    end,
                                                                    % Notify when not 'sync_fast_notify'
                                                                    mb_notify(DoNotify,IM/='sync_fast_notify',NotifyPid,{'update',{Id,Entity0,EntityN},ModifierInfo}),
                                                                    % Reply message
                                                                    EntityN1 = ?Format:decorate_entity(EntityN,PropNames),
                                                                    {ok,EntityN1}
                                                                end,
                                                            FunProceedDbWork(FunDbWork);
                                                        {{ok,_},ReplySecurity} -> ReplySecurity;
                                                        {ReplySecurity,_} -> ReplySecurity
                                                    end
                                            end
                                    end,
                            reply_skip(Reply,FunReply,?FUNCTION_NAME,SkipResponseEntity)
                    end
                end,
            State1 = ?Integrity:write_operation(F,Id,maps:get(hrkey,Map,Id),FunReply,update,State),
            {noreply,State1}
    end.

%% -----------------------
%% DELETE operation
%% TODO: May be use lightweight "DELETE FROM tab ON CLUSTER cluster WHERE condition" clickhouse operation
%% TODO: May be use heavyweight "ALTER TABLE tab ON CLUSTER cluster DELETE WHERE condition" clickhouse operation
%% -----------------------
delete(Map,FunReply,State) ->
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,PropNames,Cache,_DbParams,_KafkaProps,CHProps,NotifyPid} = extract_params(ModifierInfo, State),
    case CHProps of
        undefined ->
            FunReply({error,{405,<<"Method not allowed for class of storage_type='transactionlog' without linked storage of type='clickhouse'">>}}),
            {noreply,State};
        _ ->
            #cstate{cache_mode=CM,integrity_mode=IM,changehistory=CH,attachment_propnames=AttPs,sync_ref=SyncRef}=State,
            Opts = maps:get(opts,Meta),
            %PartField = maps:get(<<"partition_property">>,Opts),
            DoNotify = maps:get(<<"notify_transactions">>,Opts,false),
            % Id
            {'entity',Id,EOpts} = maps:get('object',Map),
            PathDt0 = ?BU:get_by_key('path',EOpts,undefined), % prev partition dt
            AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
            F = fun(FunProceedDbWork) ->
                    % Check canceled from queue
                    case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
                    % DB
                    Entity0 =
                        case State#cstate.cache_mode of
                            %@@@@@@@@@@@@@@
                            'none' ->
                                % Find
                                case ?ClickHouseCrud:find(Meta,{PathDt0},Id,CHProps,PropNames) of
                                    {error,_}=Err -> throw(Err);
                                    false -> throw({error,{not_found,?BU:strbin("Entity not found",[])}});
                                    {ok,E0} -> E0
                                end;
                            %@@@@@@@@@@@@@@
                            'temp' ->
                                % Find
                                case ?StorageEts:find(Cache,Id) of
                                    {ok,E0} -> E0;
                                    false ->
                                        case ?ClickHouseCrud:find(Meta,{PathDt0},Id,CHProps,PropNames) of
                                            {error,_}=Err -> throw(Err);
                                            false -> throw({error,{not_found,?BU:strbin("Entity not found",[])}});
                                            {ok,E0} ->
                                                ?StorageEts:create(Cache,E0),
                                                E0
                                        end end
                            %@@@@@@@@@@@@@@
                        end,
                    % Validator
                    Reply = case ?ClassU:validate(Meta,'delete',{Id,Entity0,undefined},ModifierInfo) of
                                {error,_}=Err1 -> Err1;
                                {ok,EntityV} ->
                                    % Security function (external) filter
                                    case ?ClassU:apply_security({ok,EntityV},Map,{error,{access_denied,<<"Access denied.">>}}) of
                                        {ok,_} ->
                                            % Notify when 'sync_fast_notify'
                                            mb_notify(DoNotify, IM=='sync_fast_notify',NotifyPid,{'delete',{Id,Entity0,undefined},ModifierInfo}),
                                            % Update cache when 'sync_fast_notify'
                                            mb_to_cache(IM=='sync_fast_notify',CM/='none',fun() -> ?StorageEts:update(Cache,Entity0#{'$is_deleted'=>true}) end),
                                            % Store changes in history
                                            case ?ChangeHistory:put(CH,Meta,{'delete',{Id,Entity0,undefined},ModifierInfo}) of
                                                {error,_}=Err2 -> Err2;
                                                ok ->
                                                    % DB
                                                    FunOnCorruptHistory = fun() -> ?ChangeHistory:rollback(CH,Meta,{'delete',{Id,Entity0,undefined},ModifierInfo}) end,
                                                    FunOnCorruptNotify = fun() -> mb_notify(DoNotify,true,NotifyPid,{'corrupt',{Id,Entity0,undefined},ModifierInfo}) end,
                                                    FunDbWork =
                                                        fun() ->
                                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                                           [FunOnCorruptHistory,FunOnCorruptNotify],
                                                                           ?ClickHouseCrud:delete(Meta,{PathDt0},Id,CHProps)),
                                                            case State#cstate.cache_mode of
                                                                'none' -> ok;
                                                                'full' -> ok = ?StorageEts:delete(Cache,Id);
                                                                'temp' -> ok = ?StorageEts:delete(Cache,Id)
                                                            end,
                                                            % Notify when not 'sync_fast_notify'
                                                            mb_notify(DoNotify, IM/='sync_fast_notify',NotifyPid,{'delete',{Id,Entity0,undefined},ModifierInfo}),
                                                            % delete attachments of entity
                                                            ?ClassU:delete_attachments(Meta,AttPs,Entity0),
                                                            % Reply
                                                            ok
                                                        end,
                                                    FunProceedDbWork(FunDbWork)
                                            end;
                                        ReplySecurity ->
                                            ReplySecurity
                                    end
                            end,
                    FunReply(Reply)
                end,
            State1 = ?Integrity:write_operation(F,Id,maps:get(hrkey,Map,Id),FunReply,delete,State),
            {noreply,State1}
    end.

%% -----------------------
%% CLEAR operation
%% TODO: use "TRUNCATE TABLE tab ON CLUSTER cluster" clickhouse operation
%% -----------------------
clear(Map,FunReply,#cstate{}=State) ->
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,_PropNames,Cache,_DbParams,_KafkaProps,CHProps,NotifyPid,driver_state=DriverState} = extract_params(ModifierInfo, State),
    case CHProps of
        undefined ->
            FunReply({error,{405,<<"Method not allowed for class of storage_type='transactionlog' without linked storage of type='clickhouse'">>}}),
            {noreply,State};
        _ ->
            #cstate{changehistory=CH,attachment_propnames=AttPs,sync_ref=SyncRef}=State,
            Opts = maps:get(opts,Meta),
            DoNotify = maps:get(<<"notify_transactions">>,Opts,false),
            % Assert
            collection = maps:get(object,Map),
            AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
            F = fun() ->
                    % Check canceled from queue
                    case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
                    % Store changes in history
                    case ?ChangeHistory:put(CH,Meta,{'clear',undefined,ModifierInfo}) of
                        {error,_}=Err -> FunReply(Err);
                        ok ->
                            % DB
                            FunOnCorruptHistory = fun() -> ?ChangeHistory:rollback(CH,Meta,{'clear',undefined,ModifierInfo}) end,
                            ok = assert_db(true,NotifyPid,SyncRef,
                                           [FunOnCorruptHistory],
                                           ?ClickHouseCrud:clear(Meta,CHProps)),
                            case State#cstate.cache_mode of
                                'none' -> ok;
                                'full' -> ok = ?StorageEts:clear(Cache);
                                'temp' -> ok = ?StorageEts:clear(Cache)
                            end,
                            % Notify: instead of notifying for all entities
                            %    do send class event 'clear' (or drop subscriptions)
                            mb_notify(DoNotify,true,NotifyPid,{'clear',undefined,ModifierInfo}),
                            % clear all attachments of class
                            ?ClassU:clear_attachments(Meta,AttPs),
                            % Reply
                            FunReply(ok)
                    end
                end,
            State1 = ?Integrity:block_operation(F,FunReply,clear,State),
            DriverState1 = DriverState#dstate_kafka_clickhouse{increment=0}, % incremented
            {noreply,State1#cstate{driver_state=DriverState1}}
    end.

%% -----------------------
%% OPTIMIZE operation
%% TODO: use "OPTIMIZE TABLE tab ON CLUSTER cluster" clickhouse operation
%% -----------------------
optimize(Map,FunReply,#cstate{}=State) ->
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,_PropNames,_Cache,_DbParams,_KafkaProps,CHProps,_NotifyPid} = extract_params(ModifierInfo, State),
    case CHProps of
        undefined ->
            FunReply({error,{405,<<"Method not allowed for class of storage_type='transactionlog' without linked storage of type='clickhouse'">>}}),
            {noreply,State};
        _ ->
            % Assert
            collection = maps:get(object,Map),
            AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
            F = fun() ->
                    % Check canceled from queue
                    case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
                    % DB
                    Reply = ok = ?ClickHouseCrud:optimize(Meta,CHProps),
                    % Reply
                    FunReply(Reply)
                end,
            State1 = ?Integrity:block_operation(F,FunReply,clear,State),
            {noreply,State1}
    end.

%% -----------------------
%% Read partitions operation
%% TODO: use "SELECT partition FROM system.parts WHERE database='' AND table=''" clickhouse operation
%% -----------------------
read_partitions(Map,FunReply,#cstate{}=State) ->
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,_PropNames,_Cache,_DbParams,_KafkaProps,CHProps,_NotifyPid} = extract_params(ModifierInfo, State),
    case CHProps of
        undefined ->
            FunReply({error,{405,<<"Method not allowed for class of storage_type='transactionlog' without linked storage of type='clickhouse'">>}}),
            {noreply,State};
        _ ->
            % Assert
            collection = maps:get(object,Map),
            AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
            F = fun() ->
                    % Check canceled from queue
                    case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
                    % DB
                    Reply = {ok,_Partitions} = ?ClickHouseCrud:read_partitions(Meta,CHProps),
                    % Reply
                    FunReply(Reply)
                end,
            State1 = ?Integrity:read_operation(F,FunReply,read_collection,State),
            {noreply,State1}
    end.

%% -----------------------
%% Drop partition operation
%% TODO: use "ALTER TABLE tab ON CLUSTER cluster DROP PARTITION partition" clickhouse operation
%% -----------------------
drop_partition(Map,FunReply,#cstate{}=State) ->
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,_PropNames,_Cache,_DbParams,_KafkaProps,CHProps,_NotifyPid} = extract_params(ModifierInfo, State),
    case CHProps of
        undefined ->
            FunReply({error,{405,<<"Method not allowed for class of storage_type='transactionlog' without linked storage of type='clickhouse'">>}}),
            {noreply,State};
        _ ->
            % Assert
            collection = maps:get(object,Map),
            Partition = maps:get(partition,Map),
            % TODO: delete attachments
            AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
            F = fun() ->
                    % Check canceled from queue
                    case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
                    % DB
                    Reply = ok = ?ClickHouseCrud:drop_partition(Meta,Partition,CHProps),
                    % Reply
                    FunReply(Reply)
                end,
            State1 = ?Integrity:block_operation(F,FunReply,clear,State),
            {noreply,State1}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
extract_params(ModifierInfo, State) ->
    #cstate{class_meta=Meta,
            propnames=PropNames,
            srv_notify=NotifyPid,
            class_ets=Cache,
            driver_state=DriverState}=State,
    #dstate_kafka_clickhouse{props=KafkaProps,
                             clickhouse=CHProps} = DriverState,
    DbParams = case ModifierInfo of
                   {'system',_} -> DriverState#dstate_kafka_clickhouse.db_params;
                   {'token',_} -> DriverState#dstate_kafka_clickhouse.db_params;
                   _ -> DriverState#dstate_kafka_clickhouse.db_params
               end,
    {Meta,PropNames,Cache,DbParams,KafkaProps,CHProps,NotifyPid}.

%%%% @private
%%internal_sync_store_to_cache_if_absent(Entity,State) ->
%%    {_,_,Cache,_,_,_,_,_} = extract_params(State),
%%    Id = maps:get(<<"id">>,Entity),
%%    case ?StorageEts:find(Cache,Id) of
%%        false -> ?StorageEts:create(Cache,Entity);
%%        {ok,_} -> ok
%%    end,
%%    State.

%% incremented
increment(State,Fup1) ->
    #cstate{driver_state=DriverState}=State,
    #dstate_kafka_clickhouse{increment_field=IncrField,increment=Incr}=DriverState,
    case IncrField of
        undefined -> {ok,State,Fup1};
        _ ->
            NewIncr = Incr+1,
            Fup2 = if is_function(Fup1,1) -> fun(EntityX) -> Fup1(EntityX#{IncrField => NewIncr}) end;
                       is_function(Fup1,2) -> fun(EntityX,Arg) -> Fup1(EntityX#{IncrField => NewIncr},Arg) end;
                       true -> Fup1
                   end,
            State1 = State#cstate{driver_state=DriverState#dstate_kafka_clickhouse{increment=NewIncr}},
            ?ClassU:push_increment(IncrField,NewIncr,State),
            {ok,State1,Fup2}
    end.

%% @private
mb_notify(true,true,NotifyPid,Message) ->
    ?Notify:notify_subscribers(NotifyPid,[],Message);
mb_notify(_,_,_,_) -> ok.

%% @private
mb_to_cache(true,true,Fun) -> Fun();
mb_to_cache(_,_,_) -> ok.

%% @private
assert_db(false,_,_,_,DbResult) -> DbResult;
assert_db(true,_,_,_,ok) -> ok;
assert_db(true,_NotifyPid,_SyncRef,Funs,{error,_}=DbResult) ->
    % Notify was already sent, but Db returned error.
    %?Notify:drop_subscriptions(NotifyPid,SyncRef),
    lists:foreach(fun(Fun) -> Fun() end,Funs),
    DbResult.

%% @private
reply_skip(Reply,FunReply,_,false) -> FunReply(Reply);
reply_skip({ok,Entity},FunReply,create,true) -> FunReply({ok,#{<<"id">> => maps:get(<<"id">>,Entity)}});
reply_skip({ok,_},FunReply,_,true) -> FunReply(ok);
reply_skip(Reply,FunReply,_,true) -> FunReply(Reply).

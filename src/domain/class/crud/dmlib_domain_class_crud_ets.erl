%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 23.03.2021
%%% @doc CRUD routines for 'ets' driver.

-module(dmlib_domain_class_crud_ets).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([ensure_driver_state/1]).
-export([load/1]).
-export([lookup/3,
         read/3,
         create/3,
         replace/3,
         update/3,
         delete/3,
         expired/2,
         expired_read_aggr_cache/2,
         clear/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-record(dstate_ets, {
    ets :: reference(), % ets
    max_size :: integer(),
    replace_without_read :: boolean(),
    %
    expires_mode :: atom(),
    timers :: ets:tab(), % ets
    ttl_property :: binary(),
    ts_property :: binary(),
    %
    aggr_cache :: {Cache::ets:tab(),ExpireMs::integer(),FunRegTimer::function()} % FunRegTimer(Key) -> GuardRef
}).

-record(timerinfo, {
    id :: binary(),
    ts :: integer(),
    ttl :: integer(),
    hrkey :: undefined | term()
}).

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------
%% Initialize driver state at start of class process
%% -----------------------
ensure_driver_state(#cstate{driver_state=#dstate_ets{}}=State) -> State;
ensure_driver_state(#cstate{class_meta=Meta,class_ets=ETS,self=Self}=State) ->
    Opts = maps:get(opts,Meta),
    MaxSize = maps:get(<<"max_size">>,Opts,10000),
    ReplaceWithoutRead = maps:get(<<"replace_without_read">>,Opts,false),
    ExpiresMode = maps:get(<<"expires_mode">>,Opts,<<"none">>),
    FieldTTL = maps:get(<<"expires_ttl_property">>,Opts,<<>>),
    FieldTS = maps:get(<<"expires_ts_property">>,Opts,<<>>),
    AutoTTL = ExpiresMode/=<<"none">> andalso FieldTTL/=<<>> andalso FieldTS/=<<>>,
    AggrCache = case ?BU:to_int(maps:get(<<"aggr_cache_ttl_ms">>,Opts,1000),1000) of
                    ExpireMs when ExpireMs > 0 ->
                        FunRegTimer = fun(Key) ->
                                            Ref = make_ref(),
                                            erlang:send_after(ExpireMs, Self, {timeout_read_aggr_cache,{Key,Ref}}),
                                            Ref
                                      end,
                        {ets:new(read_aggr_cache,[public,set]),ExpireMs,FunRegTimer};
                    _ -> undefined
                end,
    State#cstate{driver_state=#dstate_ets{ets = ETS,
                                          max_size = MaxSize,
                                          replace_without_read = ReplaceWithoutRead,
                                          expires_mode = ?BU:to_atom_new(ExpiresMode),
                                          timers = case AutoTTL of true -> ets:new(timers_ets,[public, set]); false -> undefined end,
                                          ttl_property = FieldTTL,
                                          ts_property = FieldTS,
                                          aggr_cache = AggrCache}}.

%% -----------------------
%% Load dummy
%%   No objects at start
%% -----------------------
load(State) -> {ok,State}.

%% -----------------------
%% LOOKUP operation
%% -----------------------
lookup(Map,FunReply,#cstate{class_meta=Meta,driver_state=#dstate_ets{ets=ETS}}=State) ->
    Content = maps:get(content,Map),
    QS = maps:get(qs,Map),
    LookupField = maps:get(<<"lookup_field">>,QS,<<>>),
    LookupKeyFields = maps:get(<<"lookup_properties">>,maps:get(opts,Meta),[]),
    LookupArg = case LookupField/=<<>> andalso lists:member(LookupField,LookupKeyFields) of
                    true -> LookupField;
                    false -> LookupKeyFields
                end,
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun() ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            % Security filter and masking
            Reply = case ?ClassU:is_security_enabled(Map) of
                        false ->
                            % DB
                            {ok,_} = ?StorageEts:lookup(ETS,Content,LookupArg);
                        true ->
                            % DB
                            {ok,Entities} = ?StorageEts:lookup_ex(ETS,Content,LookupArg),
                            % Security filter and masking
                            {ok, lists:filtermap(fun(Entity) ->
                                                       case ?ClassU:apply_security({ok,Entity},Map,false) of
                                                           {ok,_} -> {true,maps:get(<<"id">>,Entity)};
                                                           _ -> false
                                                       end
                                                 end, Entities)}
                    end,
            % Reply
            FunReply(Reply)
        end,
    State1 = ?Integrity:read_operation(F,FunReply,lookup,State),
    {noreply,State1}.

%% -----------------------
%% READ collection operation
%% READ entity operation
%% -----------------------
read({collection,Map,SelectOpts},FunReply,#cstate{class_meta=Meta,driver_state=DriverState}=State) ->
    #dstate_ets{ets=ETS,
                aggr_cache=AggrCache}=DriverState,
    Opts = maps:get(opts,Meta),
    LimitOpts = [{max_limit,?ClassU:max_limit(Meta,SelectOpts)},
                 {max_mask,maps:get(<<"max_mask">>,Opts,?DefaultMaxMask)}],
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun() ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            % DB
            {ok,_}=Reply = ?StorageEts:read(ETS,SelectOpts,LimitOpts,AggrCache),
            % Reply
            FunReply(Reply)
        end,
    State1 = ?Integrity:read_operation(F,FunReply,read_collection,State),
    {noreply,State1};
%% -----------------------
read({entity,Map,Id,_},FunReply,#cstate{driver_state=#dstate_ets{ets=ETS}}=State) ->
    OperationMode = 'w', % write to ets by key is much more faster than reads by filter
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun() ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            % DB
            Reply = case ?StorageEts:find(ETS,Id) of
                        {ok,Entity} -> {ok,Entity};
                        false -> {error,{not_found,<<"Entity not found">>}}
                    end,
            % Security filter and masking
            ReplyX = ?ClassU:apply_security(Reply,Map,{error,{not_found,<<"Entity not found">>}}),
            % Reply
            FunReply(ReplyX)
        end,
    State1 = case OperationMode of
                 'r' -> ?Integrity:read_operation(F,FunReply,read_entity,State);
                 'w' ->
                     HashRingKey = maps:get(hrkey,Map,Id),
                     ?Integrity:write_operation(F,Id,HashRingKey,FunReply,read_entity,State)
             end,
    {noreply,State1}.

%% -----------------------
%% POST operation
%% -----------------------
create(Map,FunReply,State) ->
    #cstate{self=ClassPid,
            class_meta=Meta,
            propnames=PropNames,
            srv_notify=NotifyPid,
            driver_state=StorageState}=State,
    #dstate_ets{ets=ETS,max_size=MaxSize}=StorageState,
    SkipResponseEntity = maps:get(skip_response_entity,Map,false),
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    % Id
    Id = ?BU:newid(),
    HashRingKey = maps:get(hrkey,Map,Id),
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun() ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            % Build
            CurSize = ?StorageEts:size(ETS),
            case ?Format:def_req(?Format:decorate_entity(?Format:build_entity(Map,Meta,Id),PropNames),Meta) of
                _ when CurSize+1>MaxSize -> {error,{invalid_operation,<<"Storage is full">>}};
                {error,_}=Err -> FunReply(Err);
                EntityX ->
                    Id = maps:get(<<"id">>,EntityX),
                    % TTL
                    {ok,Entity,_UpFields} = update_entity_by_ttl('create',Id,HashRingKey,EntityX,StorageState,ClassPid),
                    % Validator
                    Reply = case ?ClassU:validate(Meta,'create',{Id,undefined,Entity},ModifierInfo) of
                                {error,_}=Err -> Err;
                                {ok,EntityV} ->
                                    % Security function (external) filter
                                    case ?ClassU:apply_security({ok,EntityV},Map,{error,{access_denied,<<"Access denied. Restricted value.">>}}) of
                                        {ok,EntityS} ->
                                            EntityN = EntityS,
                                            % Security function (external) masking
                                            EntityN = case EntityV of
                                                          EntityS -> EntityS;
                                                          _ ->
                                                              Fup = fun(Entity_) -> Entity_ end,
                                                              case ?Format:def_req(EntityS,Meta) of
                                                                  {error,_}=ErrS -> throw(ErrS);
                                                                  EntityS_ -> Fup(EntityS_)
                                                              end end,
                                            % DB
                                            ok = ?StorageEts:create(ETS,EntityN),
                                            % Notify
                                            EntityInfo = {Id,undefined,EntityN},
                                            ?Notify:notify_subscribers(NotifyPid,[],{'create',EntityInfo,ModifierInfo}),
                                            % Reply message
                                            {ok,EntityN};
                                        ReplySecurity ->
                                            ReplySecurity
                                    end
                            end,
                    reply_skip(Reply,FunReply,?FUNCTION_NAME,SkipResponseEntity,Id,StorageState)
            end
        end,
    State1 = ?Integrity:write_operation(F,Id,HashRingKey,FunReply,create,State),
    {noreply,State1}.

%% -----------------------
%% PUT operation
%% -----------------------
replace(Map,FunReply,State) ->
    #cstate{self=ClassPid,
            class_meta=Meta,
            propnames=PropNames,
            srv_notify=NotifyPid,
            driver_state=StorageState}=State,
    #dstate_ets{ets=ETS,max_size=MaxSize,replace_without_read=ReplaceWithoutRead0}=StorageState,
    ReplaceWithoutRead = case ReplaceWithoutRead0 of false -> false; true -> not ?ClassU:is_security_enabled(Map) end,
    SkipResponseEntity = maps:get(skip_response_entity,Map,false),
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    % Id
    {'entity',Id,_} = maps:get(object,Map),
    HashRingKey = maps:get(hrkey,Map,Id),
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun() ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            % Build
            CurSize = ?StorageEts:size(ETS),
            case ?Format:def_req(?Format:decorate_entity(?Format:build_entity(Map,Meta),PropNames),Meta) of
                {error,_}=Err -> FunReply(Err);
                Entity1X ->
                    Id = maps:get(<<"id">>,Entity1X),
                    % TTL
                    {ok,Entity1,_UpFields} = update_entity_by_ttl('create',Id,HashRingKey,Entity1X,StorageState,ClassPid),
                    % Read first?
                    Entity0 = case ReplaceWithoutRead of
                                  true -> undefined;
                                  false ->
                                      % Find
                                      case ?StorageEts:find(ETS,Id) of
                                          false -> undefined;
                                          {ok,E0} -> E0
                                      end
                              end,
                    Reply = case Entity1 of
                                Entity0 ->
                                    % Security function (external)
                                    case ?ClassU:apply_security({ok,Entity0},Map,{error,{access_denied,<<"Access denied.">>}}) of
                                        {ok,_} -> {ok,Entity0};
                                        ReplySecurity -> ReplySecurity
                                    end;
                                _ when Entity0==undefined, CurSize+1>MaxSize ->
                                    {error,{invalid_operation,<<"Storage is full">>}};
                                _ ->
                                    % Validator
                                    case ?ClassU:validate(Meta,'replace',{Id,Entity0,Entity1},ModifierInfo) of
                                        {error,_}=Err -> Err;
                                        {ok,EntityV} ->
                                            % Security function (external)
                                            SecOld = case Entity0 of
                                                         undefined -> {ok,undefined};
                                                         _ -> ?ClassU:apply_security({ok,Entity0},Map,{error,{access_denied,<<"Access denied.">>}})
                                                     end,
                                            SecNew = ?ClassU:apply_security({ok,EntityV},Map,{error,{access_denied,<<"Access denied. Restricted value.">>}}),
                                            case {SecOld,SecNew} of
                                                {{ok,_},{ok,EntityS}} ->
                                                    % Security function (external) masking
                                                    EntityN = case EntityV of
                                                                  EntityS -> EntityS;
                                                                  _ ->
                                                                      Fup = fun(Entity_) -> Entity_ end,
                                                                      case Entity0 of
                                                                          undefined ->
                                                                              case ?Format:def_req(EntityS,Meta) of
                                                                                  {error,_}=ErrS -> throw(ErrS);
                                                                                  EntityS_ -> Fup(EntityS_)
                                                                              end;
                                                                          _ -> maps:merge(Entity0,Fup(EntityS))
                                                                      end end,
                                                    % DB
                                                    ok = ?StorageEts:replace(ETS,EntityN),
                                                    % Notify
                                                    case Entity0 of
                                                        undefined ->
                                                            EntityInfo = {Id,undefined,EntityN},
                                                            ?Notify:notify_subscribers(NotifyPid,[],{'create',EntityInfo,ModifierInfo});
                                                        _ ->
                                                            EntityInfo = {Id,Entity0,EntityN},
                                                            ?Notify:notify_subscribers(NotifyPid,[],{'update',EntityInfo,ModifierInfo})
                                                    end,
                                                    % Reply message
                                                    {ok,EntityN};
                                                {{ok,_},ReplySecurity} -> ReplySecurity;
                                                {ReplySecurity,_} -> ReplySecurity
                                            end
                                    end
                            end,
                    reply_skip(Reply,FunReply,?FUNCTION_NAME,SkipResponseEntity,Id,StorageState)
            end
        end,
    State1 = ?Integrity:write_operation(F,Id,HashRingKey,FunReply,replace,State),
    {noreply,State1}.

%% -----------------------
%% PATCH operation
%% -----------------------
update(Map,FunReply,State) ->
    #cstate{self=ClassPid,
            class_meta=Meta,
            propnames=PropNames,
            srv_notify=NotifyPid,
            driver_state=#dstate_ets{ets=ETS}=StorageState}=State,
    SkipResponseEntity = maps:get(skip_response_entity,Map,false),
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    % Id
    {'entity',Id,_} = maps:get(object,Map),
    HashRingKey = maps:get(hrkey,Map,Id),
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun() ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            % Build !!! without decorate to update by nulls
            case ?Format:build_entity(Map,Meta) of
                {error,_}=Err -> FunReply(Err);
                EntityX ->
                    % Find
                    Id = maps:get(<<"id">>,EntityX),
                    case ?StorageEts:find(ETS,Id) of
                        false -> FunReply({error,{not_found,<<"Entity not found">>}});
                        {ok,Entity0} ->
                            % Build
                            case ?Format:def_req(?Format:decorate_entity(?Format:update_merge(Entity0,EntityX,Meta,Map),PropNames),Meta) of
                                {error,_}=Err -> FunReply(Err);
                                EntityY ->
                                    % TTL
                                    Reply = case update_entity_by_ttl(update,Id,HashRingKey,EntityY,StorageState,ClassPid) of
                                                {ok,Entity0,_} ->
                                                    % Security function (external) filter
                                                    case ?ClassU:apply_security({ok,Entity0},Map,{error,{not_found,<<"Entity not found.">>}}) of
                                                        {ok,_} -> {ok,Entity0};
                                                        ReplySecurity -> ReplySecurity
                                                    end;
                                                {ok,Entity1,_UpFields} ->
                                                    % Validator
                                                    case ?ClassU:validate(Meta,'update',{Id,Entity0,Entity1},ModifierInfo) of
                                                        {error,_}=Err -> Err;
                                                        {ok,EntityV} ->
                                                            % Security function (external) filter
                                                            SecOld = ?ClassU:apply_security({ok,Entity0},Map,{error,{access_denied,<<"Access denied.">>}}),
                                                            SecNew = ?ClassU:apply_security({ok,EntityV},Map,{error,{access_denied,<<"Access denied. Restricted value.">>}}),
                                                            case {SecOld,SecNew} of
                                                                {{ok,_},{ok,EntityS}} ->
                                                                    % Security function (external) masking
                                                                    EntityN = case EntityV of
                                                                                  EntityS -> EntityS;
                                                                                  _ ->
                                                                                      Fup = fun(Entity_) -> Entity_ end,
                                                                                      maps:merge(Entity0,Fup(EntityS))
                                                                              end,
                                                                    % DB
                                                                    ok = ?StorageEts:update(ETS,EntityN),
                                                                    % Notify
                                                                    EntityInfo = {Id,Entity0,EntityN},
                                                                    ?Notify:notify_subscribers(NotifyPid,[],{'update',EntityInfo,ModifierInfo}),
                                                                    % Reply message
                                                                    {ok,EntityN};
                                                                {{ok,_},ReplySecurity} -> ReplySecurity;
                                                                {ReplySecurity,_} -> ReplySecurity
                                                            end
                                                    end
                                            end,
                                    reply_skip(Reply,FunReply,?FUNCTION_NAME,SkipResponseEntity,Id,StorageState)
                            end
                    end
            end
        end,
    State1 = ?Integrity:write_operation(F,Id,HashRingKey,FunReply,update,State),
    {noreply,State1}.

%% -----------------------
%% DELETE operation
%% -----------------------
delete(Map,FunReply,State) ->
    #cstate{class_meta=Meta,
            attachment_propnames=AttPs,
            srv_notify=NotifyPid,
            driver_state=#dstate_ets{ets=ETS}=StorageState}=State,
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    % Id
    {'entity',Id,_} = maps:get(object,Map),
    HashRingKey = maps:get(hrkey,Map,Id),
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun() ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            % Find
            case ?StorageEts:find(ETS,Id) of
                false -> FunReply({error,{not_found,?BU:strbin("Entity not found",[])}});
                {ok,Entity0} ->
                    % Validator
                    Reply = case ?ClassU:validate(Meta,'delete',{Id,Entity0,undefined},ModifierInfo) of
                                {error,_}=Err -> Err;
                                {ok,EntityV} ->
                                    % Security function (external) filter
                                    case ?ClassU:apply_security({ok,EntityV},Map,{error,{access_denied,<<"Access denied.">>}}) of
                                        {ok,_} ->
                                            % Remove from timers
                                            abort_ttl_timer(Id,StorageState),
                                            % DB
                                            ?StorageEts:delete(ETS,Id),
                                            % Notify
                                            EntityInfo = {Id,Entity0,undefined},
                                            ?Notify:notify_subscribers(NotifyPid,[],{'delete',EntityInfo,ModifierInfo}),
                                            % delete attachments of entity
                                            ?ClassU:delete_attachments(Meta,AttPs,Entity0),
                                            % Reply message
                                            ok;
                                        ReplySecurity ->
                                            ReplySecurity
                                    end
                            end,

                    FunReply(Reply)
            end
        end,
    State1 = ?Integrity:write_operation(F,Id,HashRingKey,FunReply,delete,State),
    {noreply,State1}.


%% -----------------------
%% On TTL timer of entity
%% -----------------------
expired(_,#cstate{driver_state=#dstate_ets{timers=undefined}}=State) -> State;
expired(TimerInfo,State) ->
    FunReply = fun() -> ok end,
    #cstate{classname=CN,
            srv_notify=NotifyPid,
            driver_state=#dstate_ets{ets=ETS,
                                     timers=TimersETS,
                                     ttl_property=FieldTTL,
                                     ts_property=FieldTS}}=State,
    #timerinfo{id=Id,ts=TS,ttl=TTL,hrkey=HashRingKey} = TimerInfo,
    F = fun() ->
            % Find
            case ?StorageEts:find(ETS,Id) of
                false -> FunReply();
                {ok,Entity0} ->
                    %NowTS = ?BU:timestamp(),
                    case {maps:get(FieldTS,Entity0,undefined), maps:get(FieldTTL,Entity0,undefined)} of
                        %{A,B} when A==undefined; B==undefined ->
                        %    ok; % something wrong
                        %{TS,TTL} %% when is_integer(TS), is_integer(TTL), TS + TTL*1000 > NowTS ->
                        %    ok; % already refreshed
                        {TS,TTL} ->
                            % Remove from timers (only remove)
                            ets:delete(TimersETS,Id),
                            % from db
                            delete_by_expired(Id,ETS,Entity0,NotifyPid);
                        {TS1,TTL1} ->
                            ?LOG('$info', "~ts '~ts' skip obsolete expired timer on id='~ts' ({~200tp,~200tp} while {~200tp,~200tp}) ", [?MODULE,CN,Id,TS,TTL,TS1,TTL1]),
                            ok
                    end,
                    FunReply()
            end
        end,
    _State1 = ?Integrity:write_operation(F,Id,HashRingKey,FunReply,write_entity,State).

%% -----------------------
%% On timer of read aggregated cache value
%% -----------------------
expired_read_aggr_cache(_TimerInfo, #cstate{driver_state=#dstate_ets{aggr_cache=undefined}}=State) -> State;
expired_read_aggr_cache({Key,Ref}, #cstate{driver_state=#dstate_ets{aggr_cache=AggrCache}}=State) ->
    {AggrCacheEts,_,_} = AggrCache,
    case ets:lookup(AggrCacheEts,Key) of
        [{Key,{_,_,Ref}}] ->
            ets:delete(AggrCacheEts,Key),
            State;
        _ -> State
    end.

%% -----------------------
%% CLEAR operation
%% should not notify for all entities or break), better to break subscriptions)
%% -----------------------
clear(Map,FunReply,State) ->
    #cstate{class_meta=Meta,
            srv_notify=NotifyPid,
            attachment_propnames=AttPs,
            driver_state=#dstate_ets{ets=ETS}=StorageState}=State,
    % Assert
    collection = maps:get(object,Map),
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun() ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            % TTL
            clear_all_timers(StorageState),
            % DB
            ok = ?StorageEts:clear(ETS),
            % Notify: instead of notifying for all entities
            %    do send class event 'clear' (or drop subscriptions)
            ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
            ?Notify:notify_subscribers(NotifyPid,[],{'clear',undefined,ModifierInfo}),
            % clear all attachments of class
            ?ClassU:clear_attachments(Meta,AttPs),
            % Reply
            FunReply(ok)
        end,
    State1 = ?Integrity:block_operation(F,FunReply,clear,State),
    {noreply,State1}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----
%% set TS automatically, start timer
%% -----
-spec update_entity_by_ttl(Op::create|replace|update,Id::binary(),HashRingKey::binary(),Entity::map(),#dstate_ets{},ClassPid::pid())
      -> {ok, Entity1::map(), [{Field::binary(),Value::term()}]}.
%% -----
update_entity_by_ttl(_,_,_,Entity,#dstate_ets{timers=undefined},_) -> {ok,Entity,[]};
update_entity_by_ttl(Op,Id,HashRingKey,Entity,#dstate_ets{timers=TimersETS,ttl_property=FieldTTL,ts_property=FieldTS,expires_mode=ExpiresMode},ClassPid) ->
    case ets:lookup(TimersETS,Id) of
        [] -> ok;
        [{_,TimerRef0}] -> ?BU:cancel_timer(TimerRef0)
    end,
    case maps:get(FieldTTL,Entity,'null') of
        'null' -> {ok,Entity,[]};
        TTL ->
            NowTS = ?BU:timestamp(),
            TS = case ExpiresMode of
                     'modify' -> NowTS;
                     'create' when Op=='create'; Op=='replace' -> NowTS;
                     'create' when Op=='update' -> maps:get(FieldTS,Entity,'null');
                     'custom' -> maps:get(FieldTS,Entity,'null')
                 end,
            case erlang:max(0, TTL * 1000 + TS - NowTS) of
                Interval when Interval > 10*365*86400*1000 ->
                    throw({error,{400,<<"Too long TTL. Should be less than 10 years">>}});
                Interval ->
                    TimerInfo = #timerinfo{id=Id,ts=TS,ttl=TTL,
                                           hrkey=case HashRingKey of undefined -> Id; _ -> HashRingKey end},
                    TimerRef = erlang:send_after(Interval,ClassPid,{timer_ttl,TimerInfo}),
                    ets:insert(TimersETS,{Id,TimerRef}),
                    {ok, Entity#{FieldTS => TS}, [{FieldTS,TS}]}
            end end.

%% stop timer for entity
abort_ttl_timer(_,#dstate_ets{timers=undefined}) -> ok;
abort_ttl_timer(Id,#dstate_ets{timers=TimersETS}) ->
    case ets:lookup(TimersETS,Id) of
        [] -> ok;
        [{_,TimerRef}] ->
            ?BU:cancel_timer(TimerRef),
            ets:delete(TimersETS,Id)
    end.

%% clear all timers for all entities
clear_all_timers(#dstate_ets{timers=undefined}) -> ok;
clear_all_timers(#dstate_ets{timers=TimersETS}) ->
    ets:foldl(fun({_,TimerRef},_) -> ?BU:cancel_timer(TimerRef) end, ok, TimersETS),
    ets:delete_all_objects(TimersETS).

%% delete
delete_by_expired(Id,TableName,Entity,NotifyPid) ->
    % DB
    ?StorageEts:delete(TableName,Id),
    % Notify
    EntityInfo = {Id,Entity,undefined},
    ModifierInfo = {<<"expired">>,undefined},
    ?Notify:notify_subscribers(NotifyPid,[],{'delete',EntityInfo,ModifierInfo}),
    ok.

%% @private
reply_skip({ok,_}=Reply,FunReply,_,false,_,_) -> FunReply(Reply);
reply_skip({ok,Entity},FunReply,create,true,_,_) -> FunReply({ok,#{<<"id">> => maps:get(<<"id">>,Entity)}});
reply_skip({ok,_},FunReply,_,true,_,_) -> FunReply(ok);
reply_skip(Reply,FunReply,_,_,Id,StorageState) ->
    abort_ttl_timer(Id,StorageState),
    FunReply(Reply).
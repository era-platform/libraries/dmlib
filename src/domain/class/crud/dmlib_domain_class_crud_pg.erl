%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 27.03.2021
%%% @doc CRUD routines for 'postgresql' driver.
%%%      Complex implementation (cache: full|temp|none) * (integrity: async|sync|sync_fast_read|sync_fast_notify)

-module(dmlib_domain_class_crud_pg).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([ensure_driver_state/1]).
-export([load/1]).
-export([lookup/3,
         read/3,
         create/3,
         replace/3,
         update/3,
         delete/3,
         clear/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(ReadTimeout, 60000).

-record(dstate_pg, {
    db_params :: map(),
    module :: atom(),
    %
    replace_without_read :: boolean(),
    %
    increment_field = undefined :: undefined | binary(), % incremented
    increment = 0 :: integer() % incremented
}).

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------
%% Initialize driver state at start of class process
%% -----------------------
ensure_driver_state(#cstate{driver_state=#dstate_pg{}}=State) -> State;
ensure_driver_state(State) ->
    #cstate{domain=Domain,class_meta=Meta,storage_mode=StorageMode}=State,
    Opts = maps:get(opts,Meta),
    DState = #dstate_pg{replace_without_read = maps:get(<<"replace_without_read">>,Opts,false),
                        increment_field = maps:get('x_incremented',Meta,undefined)}, % incremented
    case ?Postgres:get_db_params(Domain,Meta) of
        {ok,DbParams} when StorageMode=='category' ->
            State#cstate{driver_state=DState#dstate_pg{db_params=DbParams,module=?PostgresCrud}};
        {ok,DbParams} when StorageMode=='history' ->
            State#cstate{driver_state=DState#dstate_pg{db_params=DbParams,module=?PostgresPartCrud}};
        {error,_}=Err -> Err
    end.

%% -----------------------
%% load cache synchronously by portions
%% TODO ICP: may be work by async process while general facade is paused.
%% -----------------------
-spec load(State::#cstate{}) -> {ok,NewState::#cstate{}} | {error,term()}.
%% -----------------------
load(State0) ->
    State = do_read_max_increment(State0), % incremented
    {Meta,PropNames,Cache,DbParams,PgModule,_} = extract_params(State),
    case State#cstate.cache_mode of
        %@@@@@@@@@@@@@@
        'none' -> {ok,State};
        %@@@@@@@@@@@@@@
        'temp' -> {ok,State};
        %@@@@@@@@@@@@@@
        'full' ->
            PortionSize = 10000,
            MetaX = Meta#{opts=>(maps:get(opts,Meta))#{<<"max_limit">> => PortionSize,
                                                       <<"max_mask">> => []}},
            SelectOpts = #{<<"order">> => [<<"id">>],
                           <<"limit">> => PortionSize,
                           <<"mask">> => [],
                           <<"filter">> => []},
            F = fun F(Offset) ->
                        case PgModule:read(MetaX,SelectOpts#{<<"offset">> => Offset},DbParams,PropNames) of
                            {ok,[]} -> {ok,State};
                            {ok,Entities} when length(Entities) < PortionSize ->
                                lists:foreach(fun(Entity) -> ?StorageEts:create(Cache,Entity) end, Entities),
                                {ok,State};
                            {ok,Entities} ->
                                lists:foreach(fun(Entity) -> ?StorageEts:create(Cache,Entity) end, Entities),
                                F(Offset+PortionSize);
                            {error,_}=Err ->
                                ?StorageEts:clear(Cache),
                                Err
                        end end,
            F(0)
        %@@@@@@@@@@@@@@
    end.

%% @private
%% incremented
do_read_max_increment(State) ->
    #cstate{driver_state=#dstate_pg{increment_field=IncrField}=DS}=State,
    case IncrField of
        undefined -> State;
        _ when is_binary(IncrField) ->
            StoreMax = ?ClassU:pull_increment(IncrField,State),
            {Meta,PropNames,_,DbParams,PgModule,_} = extract_params(State),
            FromDt = <<"1970-01-01 00:00:00">>,
            TillDt = ?BU:strdatetime(?BU:timestamp_to_datetime(?BU:timestamp() + 86400000 * 365 * 10)),
            SelectOpts = #{
                <<"aggr">> => #{<<"max">> => [<<"max">>,[<<"property">>,IncrField]]},
                <<"interval">> => [FromDt,TillDt],
                <<"filter">> => [],
                <<"mask">> => [],
                <<"group">> => #{},
                <<"order">> => []
            },
            case PgModule:read(Meta,SelectOpts,DbParams,PropNames) of
                {ok,[AggrItem]} ->
                    DbMax = case maps:get(<<"max">>,AggrItem,0) of
                                null -> 0;
                                V -> V
                            end,
                    Max = max(DbMax,StoreMax),
                    State#cstate{driver_state=DS#dstate_pg{increment=Max}};
                {error,_}=Err ->
                    ?LOG('$error', "Load max increment failed: ~120tp", [Err]),
                    State#cstate{driver_state=DS#dstate_pg{increment=StoreMax}}
            end
    end.

%% -----------------------
%% LOOKUP operation
%% -----------------------
lookup(Map,FunReply,State) ->
    InitiatorInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,PropNames,Cache,DbParams,PgModule,_} = extract_params(InitiatorInfo, State),
    #cstate{cache_mode=CM}=State,
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun() ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            %
            {LookupFunName,FunApplyItem} = case ?ClassU:is_security_enabled(Map) of
                                               false -> {lookup, fun(I) -> {true,I} end};
                                               true ->
                                                   Fun = fun(Entity) ->
                                                               case ?ClassU:apply_security({ok,Entity},Map,false) of
                                                                   {ok,_} -> {true,maps:get(<<"id">>,Entity)};
                                                                   _ -> false
                                                               end end,
                                                   {lookup_ex, Fun}
                                           end,
            % parameters
            QS = maps:get(qs,Map),
            Content = maps:get(content,Map),
            LookupField = ?BU:get_by_key(<<"lookup_field">>,QS,<<>>),
            LookupKeyFields = maps:get(<<"lookup_properties">>,maps:get(opts,Meta),[]),
            LookupArg = case LookupField/=<<>> andalso lists:member(LookupField,LookupKeyFields) of
                            true -> LookupField;
                            false -> LookupKeyFields
                        end,
            % DB
            {ok,Items} = case CM of
                             %@@@@@@@@@@@@@@
                             'none' ->
                                 {ok,_} = PgModule:LookupFunName(Meta,QS,Content,DbParams,PropNames,LookupArg);
                             %@@@@@@@@@@@@@@
                             'full' ->
                                 {ok,_} = ?StorageEts:LookupFunName(Cache,Content,LookupArg);
                             %@@@@@@@@@@@@@@
                             'temp' ->
                                 case ?StorageEts:LookupFunName(Cache,Content,LookupArg) of
                                     {ok,[_|_]}=Ok -> Ok;
                                     {ok,[]} -> {ok,_} = PgModule:LookupFunName(Meta,QS,Content,DbParams,PropNames,LookupArg)
                                 end
                             %@@@@@@@@@@@@@@
                         end,
            Reply = {ok,lists:filtermap(FunApplyItem, Items)},
            % Reply
            FunReply(Reply)
        end,
    State1 = ?Integrity:read_operation(F,FunReply,lookup,State),
    {noreply,State1}.

%% -----------------------
%% READ collection operation
%% READ entity operation
%% -----------------------
read({collection,Map,SelectOpts},FunReply,State) ->
    InitiatorInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,PropNames,Cache,DbParams,PgModule,_} = extract_params(InitiatorInfo, State),
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun() ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            %
            Reply = case State#cstate.cache_mode of
                        %@@@@@@@@@@@@@@
                        'none' ->
                            % DB
                            PgModule:read(Meta,SelectOpts,DbParams,PropNames);
                        %@@@@@@@@@@@@@@
                        'full' ->
                            Opts = maps:get(opts,Meta),
                            LimitOpts = [{max_limit,?ClassU:max_limit(Meta,SelectOpts)},
                                         {max_mask,maps:get(<<"max_mask">>,Opts,?DefaultMaxMask)}],
                            % DB
                            {ok,_} = ?StorageEts:read(Cache,SelectOpts,LimitOpts,undefined);
                            %% May be event in full cache do not use mnesia...
                            %PgModule:read(Meta,SelectOpts,DbParams,PropNames);
                        %@@@@@@@@@@@@@@
                        'temp' ->
                            % DB
                            PgModule:read(Meta,SelectOpts,DbParams,PropNames)
                        % Update by cache when async/sync_fast_...
                        %@@@@@@@@@@@@@@
                    end,
            % Reply
            FunReply(Reply)
        end,
    State1 = ?Integrity:read_operation(F,FunReply,read_collection,State),
    {noreply,State1};
%% -----------------------
read({entity,Map,Id,EOpts},FunReply,#cstate{class_meta=Meta,sync_ref=SyncRef}=State) ->
    InitiatorInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,PropNames,Cache,DbParams,PgModule,_} = extract_params(InitiatorInfo, State),
    #cstate{cache_mode=CM,driver_state=#dstate_pg{}}=State,
    % This is read by primary_key, it's fast and is equal part to find operation during create/replace/update/delete request
    OperationMode = 'w',
    Self = self(),
    PathDt = ?BU:get_by_key('path',EOpts,undefined), % partition dt
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun() ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            % DB
            Reply = case CM of
                        %@@@@@@@@@@@@@@
                        'none' ->
                            case PgModule:find(Meta,{PathDt},Id,DbParams,PropNames) of
                                {ok,Entity} -> {ok,Entity};
                                {error,_}=Err -> Err;
                                false -> {error,{not_found,<<"Entity not found.">>}}
                            end;
                        %@@@@@@@@@@@@@@
                        'full' ->
                            case ?StorageEts:find(Cache,Id) of
                                {ok,Entity} -> {ok,Entity};
                                false -> {error,{not_found,<<"Entity not found.">>}}
                            end;
                        %@@@@@@@@@@@@@@
                        'temp' ->
                            case ?StorageEts:find(Cache,Id) of
                                {ok,Entity} -> {ok,Entity};
                                false ->
                                    case PgModule:find(Meta,{PathDt},Id,DbParams,PropNames) of
                                        {ok,Entity} when OperationMode=='r' ->
                                            Self ! {do_work_internal,SyncRef,fun(StateX) -> internal_sync_store_to_cache_if_absent(Entity,StateX) end},
                                            {ok,Entity};
                                        {ok,Entity} when OperationMode=='w' ->
                                            ?StorageEts:create(Cache,Entity),
                                            {ok,Entity};
                                        false -> {error,{not_found,<<"Entity not found.">>}};
                                        {error,_}=Err -> Err
                                    end end
                        %@@@@@@@@@@@@@@
                    end,
            % Security filter and masking
            ReplyX = ?ClassU:apply_security(Reply,Map,{error,{not_found,<<"Entity not found.">>}}),
            % Reply
            FunReply(ReplyX)
        end,
    State1 = case OperationMode of
                 'r' -> ?Integrity:read_operation(F,FunReply,read_entity,State);
                 'w' -> ?Integrity:write_operation(F,Id,maps:get(hrkey,Map,Id),FunReply,read_entity,State)
             end,
    {noreply,State1}.

%% -----------------------
%% POST operation
%% -----------------------
create(Map,FunReply,State) ->
    SkipResponseEntity = maps:get(skip_response_entity,Map,false),
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,PropNames,Cache,DbParams,PgModule,NotifyPid} = extract_params(ModifierInfo, State),
    #cstate{cache_mode=CM,integrity_mode=IM,sync_ref=SyncRef,changehistory=CH}=State,
    PartField = maps:get(<<"partition_property">>,maps:get(opts,Meta)),
    Fup1 = fun(EntityX) when PartField == <<>> -> EntityX;
              (EntityX) -> case maps:get(PartField,EntityX,null) of
                               null -> EntityX#{PartField => ?BU:strdatetime3339(?BU:ticktoutc(?BU:timestamp()))};
                               _ -> EntityX
                           end end,
    {ok,State1,Fup} = increment(State,Fup1), % incremented
    % Id
    Id = ?BU:newid(),
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun(FunProceedDbWork) ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            % Build
            case ?Format:def_req(?Format:decorate_entity(?Format:build_entity(Map,Meta,Id),PropNames),Meta) of
                {error,_}=Err -> FunReply(Err);
                EntityX ->
                    Entity = Fup(EntityX), % Partition field is required. Default value is CURRENT UTC
                    PathDt = maps:get(PartField,Entity,undefined), % get partition dt
                    Id = maps:get(<<"id">>,Entity), % assert
                    % Validator
                    case ?ClassU:validate(Meta,'create',{Id,undefined,Entity},ModifierInfo) of
                        {error,_}=Err -> FunReply(Err);
                        {ok,EntityV} ->
                            % Security function (external) filter
                            case ?ClassU:apply_security({ok,EntityV},Map,{error,{access_denied,<<"Access denied. Restricted value.">>}}) of
                                {ok,EntityS} ->
                                    % Security function (external) masking
                                    EntityN = case EntityV of
                                                  EntityS -> EntityS;
                                                  _ ->
                                                      case ?Format:def_req(EntityS,Meta) of
                                                          {error,_}=ErrS -> throw(ErrS);
                                                          EntityS_ -> Fup(EntityS_)
                                                      end end,
                                    % Notify when 'sync_fast_notify'
                                    mb_notify(IM=='sync_fast_notify',NotifyPid,{'create',{Id,undefined,EntityN},ModifierInfo}),
                                    % Update cache when 'sync_fast_notify'
                                    mb_to_cache(IM=='sync_fast_notify',CM/='none',fun() -> ?StorageEts:create(Cache,EntityN) end),
                                    % Store changes in history
                                    case ?ChangeHistory:put(CH,Meta,{'create',{Id,undefined,EntityN},ModifierInfo}) of
                                        {error,_}=Err -> FunReply(Err);
                                        ok ->
                                            % DB
                                            FunOnCorruptHistory = fun() -> ?ChangeHistory:rollback(CH,Meta,{'create',{Id,undefined,EntityN},ModifierInfo}) end,
                                            FunOnCorruptCache = fun() -> ?StorageEts:delete(Cache,Id) end,
                                            FunOnCorruptNotify = fun() -> mb_notify(true,NotifyPid,{'corrupt',{Id,EntityN,undefined},ModifierInfo}) end,
                                            FunDbWork =
                                                fun() ->
                                                    case CM of
                                                        %@@@@@@@@@@@@@@
                                                        'none' ->
                                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                                           [FunOnCorruptHistory,FunOnCorruptNotify],
                                                                           PgModule:create(Meta,{PathDt},EntityN,DbParams,PropNames));
                                                        %@@@@@@@@@@@@@@
                                                        'full' ->
                                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                                           [FunOnCorruptHistory,FunOnCorruptCache,FunOnCorruptNotify],
                                                                           PgModule:create(Meta,{PathDt},EntityN,DbParams,PropNames)),
                                                            ok = ?StorageEts:create(Cache,EntityN);
                                                        %@@@@@@@@@@@@@@
                                                        'temp' ->
                                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                                           [FunOnCorruptHistory,FunOnCorruptCache,FunOnCorruptNotify],
                                                                           PgModule:create(Meta,{PathDt},EntityN,DbParams,PropNames)),
                                                            ok = ?StorageEts:create(Cache,EntityN)
                                                        %@@@@@@@@@@@@@@
                                                    end,
                                                    % Notify when not 'sync_fast_notify'
                                                    mb_notify(IM/='sync_fast_notify',NotifyPid,{'create',{Id,undefined,EntityN},ModifierInfo}),
                                                    % Reply message
                                                    {ok,EntityN}
                                                end,
                                            reply_skip(FunProceedDbWork(FunDbWork),FunReply,?FUNCTION_NAME,SkipResponseEntity)
                                    end;
                                ReplySecurity ->
                                    FunReply(ReplySecurity)
                            end
                    end
            end
        end,
    State2 = ?Integrity:write_operation(F,Id,maps:get(hrkey,Map,Id),FunReply,create,State1),
    {noreply,State2}.

%% -----------------------
%% PUT operation
%% -----------------------
replace(Map,FunReply,State) ->
    SkipResponseEntity = maps:get(skip_response_entity,Map,false),
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,PropNames,Cache,DbParams,PgModule,NotifyPid} = extract_params(ModifierInfo, State),
    #cstate{storage_mode=SM,cache_mode=CM,integrity_mode=IM,sync_ref=SyncRef,changehistory=CH,
            driver_state=#dstate_pg{replace_without_read=ReplaceWithoutRead0,increment_field=IncrField}}=State,
    ReplaceWithoutRead = case ReplaceWithoutRead0 of false -> false; true -> not ?ClassU:is_security_enabled(Map) end,
    PartField = maps:get(<<"partition_property">>,maps:get(opts,Meta)),
    Fup1 = fun(EntityX,_) when PartField == <<>> -> EntityX;
              (EntityX,DefaultDt) -> case maps:get(PartField,EntityX,null) of
                                         null ->
                                             try ?BU:parse_datetime(DefaultDt) of
                                                 {{1970,1,1},{0,0,0},0} -> EntityX#{PartField => ?BU:strdatetime3339(?BU:ticktoutc(?BU:timestamp()))};
                                                 Dt -> EntityX#{PartField => ?BU:strdatetime3339(Dt)}
                                             catch _:_ -> EntityX#{PartField => ?BU:strdatetime3339(?BU:ticktoutc(?BU:timestamp()))}
                                             end;
                                         _ -> EntityX
                                     end end,
    {ok,State1,Fup} = increment(State,Fup1), % incremented
    % Id
    {'entity',Id,EOpts} = maps:get('object',Map),
    PathDtX0 = ?BU:get_by_key('path',EOpts,undefined), % prev partition dt
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun(FunProceedDbWork) ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            % Build
            case ?Format:def_req(?Format:decorate_entity(?Format:build_entity(Map,Meta),PropNames),Meta) of
                {error,_}=Err -> FunReply(Err);
                EntityX1 ->
                    Entity1 = Fup(EntityX1,PathDtX0), % Partition field is required. Default value is CURRENT UTC
                    PathDt1 = maps:get(PartField,Entity1,undefined), % get new partition dt
                    Id = maps:get(<<"id">>,Entity1), % assert
                    Entity0 = case ReplaceWithoutRead of
                                  true -> undefined;
                                  false ->
                                      case CM of
                                          %@@@@@@@@@@@@@@
                                          'none' ->
                                              % Find
                                              case PgModule:find(Meta,{PathDtX0},Id,DbParams,PropNames) of
                                                  {error,_}=Err -> throw(Err);
                                                  false -> undefined;
                                                  {ok,E0} -> E0
                                              end;
                                          %@@@@@@@@@@@@@@
                                          'full' ->
                                              % Find
                                              case ?StorageEts:find(Cache,Id) of
                                                  false -> undefined;
                                                  {ok,E0} -> E0
                                              end;
                                          %@@@@@@@@@@@@@@
                                          'temp' ->
                                              % Find
                                              case ?StorageEts:find(Cache,Id) of
                                                  {ok,E0} -> E0;
                                                  false ->
                                                      case PgModule:find(Meta,{PathDtX0},Id,DbParams,PropNames) of
                                                          {error,_}=Err -> throw(Err);
                                                          false -> undefined;
                                                          {ok,E0} -> E0 % no need to store previous version
                                                      end end
                                          %@@@@@@@@@@@@@@
                                      end end,
                    Reply = case Entity1 of
                                Entity0 ->
                                    % Security function (external) filter
                                    case ?ClassU:apply_security({ok,Entity0},Map,{error,{access_denied,<<"Access denied.">>}}) of
                                        {ok,_} -> {ok,Entity0};
                                        ReplySecurity -> ReplySecurity
                                    end;
                                _ ->
                                    % Validator
                                    case ?ClassU:validate(Meta,'replace',{Id,Entity0,Entity1},ModifierInfo) of
                                        {error,_}=Err1 -> Err1;
                                        {ok,EntityV0} ->
                                            % incremented
                                            EntityV = case Entity0 of
                                                          undefined -> EntityV0;
                                                          _ when IncrField==undefined -> EntityV0;
                                                          _ ->
                                                              case maps:get(IncrField,Entity0,undefined) of
                                                                  undefined -> EntityV0;
                                                                  IncrValue -> EntityV0#{IncrField => IncrValue}
                                                              end end,
                                            % Security function (external) filter
                                            SecOld = case Entity0 of
                                                         undefined -> {ok,undefined};
                                                         _ -> ?ClassU:apply_security({ok,Entity0},Map,{error,{access_denied,<<"Access denied.">>}})
                                                     end,
                                            SecNew = ?ClassU:apply_security({ok,EntityV},Map,{error,{access_denied,<<"Access denied. Restricted value.">>}}),
                                            case {SecOld,SecNew} of
                                                {{ok,_},{ok,EntityS}} ->
                                                    % Security function (external) masking
                                                    EntityN = case EntityV of
                                                                  EntityS -> EntityS;
                                                                  _ ->
                                                                      case Entity0 of
                                                                          undefined ->
                                                                              % Security function (external) masking
                                                                              case ?Format:def_req(EntityS,Meta) of
                                                                                  {error,_}=ErrS -> throw(ErrS);
                                                                                  EntityS_ -> Fup(EntityS_,PathDtX0)
                                                                              end;
                                                                          _ -> maps:merge(Entity0,EntityS)
                                                                      end end,
                                                    % Check partition is not changed
                                                    ?ClassU:assert_partition_not_changed(SM,PartField,Entity0,EntityN),
                                                    PathDt0 = case Entity0 of
                                                                  undefined -> undefined;
                                                                  _ -> PathDtX0
                                                              end,
                                                    % Notify when 'sync_fast_notify'
                                                    case Entity0 of
                                                        undefined -> mb_notify(IM=='sync_fast_notify',NotifyPid,{'create',{Id,undefined,EntityN},ModifierInfo});
                                                        _ -> mb_notify(IM=='sync_fast_notify',NotifyPid,{'update',{Id,Entity0,EntityN},ModifierInfo})
                                                    end,
                                                    % Update cache when 'sync_fast_notify'
                                                    mb_to_cache(IM=='sync_fast_notify',CM/='none',fun() -> ?StorageEts:replace(Cache,EntityN) end),
                                                    % Store changes in history
                                                    case ?ChangeHistory:put(CH,Meta,{'replace',{Id,Entity0,EntityN},ModifierInfo}) of
                                                        {error,_}=Err2 -> Err2;
                                                        ok ->
                                                            % DB
                                                            FunOnCorruptHistory = fun() -> ?ChangeHistory:rollback(CH,Meta,{'replace',{Id,Entity0,EntityN},ModifierInfo}) end,
                                                            FunOnCorruptCache = fun() -> ?StorageEts:delete(Cache,Id) end,
                                                            FunOnCorruptNotify = fun() when Entity0==undefined -> mb_notify(true,NotifyPid,{'corrupt',{Id,Entity1,undefined},ModifierInfo});
                                                                                    () -> mb_notify(true,NotifyPid,{'corrupt',{Id,EntityN,Entity0},ModifierInfo}) end,
                                                            FunDbWork =
                                                                fun() ->
                                                                    case CM of
                                                                        %@@@@@@@@@@@@@@
                                                                        'none' ->
                                                                            % DB
                                                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                                                           [FunOnCorruptHistory,FunOnCorruptNotify],
                                                                                           PgModule:replace(Meta,{PathDt0,PathDt1},EntityN,DbParams,PropNames));
                                                                        %@@@@@@@@@@@@@@
                                                                        'full' ->
                                                                            % DB
                                                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                                                           [FunOnCorruptHistory,FunOnCorruptCache,FunOnCorruptNotify],
                                                                                           PgModule:replace(Meta,{PathDt0,PathDt1},EntityN,DbParams,PropNames)),
                                                                            ok = ?StorageEts:replace(Cache,EntityN);
                                                                        %@@@@@@@@@@@@@@
                                                                        'temp' ->
                                                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                                                           [FunOnCorruptHistory,FunOnCorruptCache,FunOnCorruptNotify],
                                                                                           PgModule:replace(Meta,{PathDt0,PathDt1},EntityN,DbParams,PropNames)),
                                                                            ok = ?StorageEts:replace(Cache,EntityN)
                                                                        %@@@@@@@@@@@@@@
                                                                    end,
                                                                    % Notify when not 'sync_fast_notify'
                                                                    case Entity0 of
                                                                        undefined -> mb_notify(IM/='sync_fast_notify',NotifyPid,{'create',{Id,undefined,EntityN},ModifierInfo});
                                                                        _ -> mb_notify(IM/='sync_fast_notify',NotifyPid,{'update',{Id,Entity0,EntityN},ModifierInfo})
                                                                    end,
                                                                    % Reply message
                                                                    {ok,EntityN}
                                                                end,
                                                            FunProceedDbWork(FunDbWork)
                                                    end;
                                                {{ok,_},ReplySecurity} -> ReplySecurity;
                                                {ReplySecurity,_} -> ReplySecurity
                                            end
                                    end
                            end,
                    reply_skip(Reply,FunReply,?FUNCTION_NAME,SkipResponseEntity)
            end
        end,
    State2 = ?Integrity:write_operation(F,Id,maps:get(hrkey,Map,Id),FunReply,replace,State1),
    {noreply,State2}.

%% -----------------------
%% PATCH operation
%% -----------------------
update(Map,FunReply,State) ->
    SkipResponseEntity = maps:get(skip_response_entity,Map,false),
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,PropNames,Cache,DbParams,PgModule,NotifyPid} = extract_params(ModifierInfo, State),
    #cstate{storage_mode=SM,cache_mode=CM,integrity_mode=IM,sync_ref=SyncRef,changehistory=CH,
            driver_state=#dstate_pg{increment_field=IncrField}}=State,
    PartField = maps:get(<<"partition_property">>,maps:get(opts,Meta)),
    % Id
    {'entity',Id,EOpts} = maps:get('object',Map),
    PathDt0 = ?BU:get_by_key('path',EOpts,undefined), % prev partition dt
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun(FunProceedDbWork) ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            % Build !!! without decorate to update by nulls
            case ?Format:build_entity(Map,Meta) of
                {error,_}=Err -> FunReply(Err);
                Entity ->
                    % Find
                    Id = maps:get(<<"id">>,Entity),
                    Entity0 =
                        case CM of
                            %@@@@@@@@@@@@@@
                            'none' ->
                                % Find
                                case PgModule:find(Meta,{PathDt0},Id,DbParams,PropNames) of
                                    {error,_}=Err -> throw(Err);
                                    false -> throw({error,{not_found,<<"Entity not found.">>}});
                                    {ok,E0} -> E0
                                end;
                            %@@@@@@@@@@@@@@
                            'full' ->
                                % Find
                                case ?StorageEts:find(Cache,Id) of
                                    false -> throw({error,{not_found,<<"Entity not found.">>}});
                                    {ok,E0} -> E0
                                end;
                            %@@@@@@@@@@@@@@
                            'temp' ->
                                % Find
                                case ?StorageEts:find(Cache,Id) of
                                    {ok,E0} -> E0;
                                    false ->
                                        case PgModule:find(Meta,{PathDt0},Id,DbParams,PropNames) of
                                            {error,_}=Err -> throw(Err);
                                            false -> throw({error,{not_found,<<"Entity not found.">>}});
                                            {ok,E0} ->
                                                ?StorageEts:create(Cache,E0),
                                                E0
                                        end end
                            %@@@@@@@@@@@@@@
                        end,
                    % Build (no decorate to null fields, decorate return value only)
                    Reply = case ?Format:def_req(?Format:update_merge(Entity0,Entity,Meta,Map),Meta) of
                                {error,_}=Err1 -> Err1;
                                Entity0 ->
                                    % Security function (external) filter
                                    case ?ClassU:apply_security({ok,Entity0},Map,{error,{not_found,<<"Entity not found.">>}}) of
                                        {ok,_} -> {ok,Entity0};
                                        ReplySecurity -> ReplySecurity
                                    end;
                                Entity1 ->
                                    % Validator
                                    case ?ClassU:validate(Meta,'update',{Id,Entity0,Entity1},ModifierInfo) of
                                        {error,_}=Err2 -> Err2;
                                        {ok,EntityV0} ->
                                            % incremented
                                            EntityV = case Entity0 of
                                                          undefined -> EntityV0;
                                                          _ when IncrField==undefined -> EntityV0;
                                                          _ ->
                                                              case maps:get(IncrField,Entity0,undefined) of
                                                                  undefined -> EntityV0;
                                                                  IncrValue -> EntityV0#{IncrField => IncrValue}
                                                              end end,
                                            % Security function (external) filter
                                            SecOld = ?ClassU:apply_security({ok,Entity0},Map,{error,{access_denied,<<"Access denied.">>}}),
                                            SecNew = ?ClassU:apply_security({ok,EntityV},Map,{error,{access_denied,<<"Access denied. Restricted value.">>}}),
                                            case {SecOld,SecNew} of
                                                {{ok,_},{ok,EntityS}} ->
                                                    % Security function (external) masking
                                                    EntityN = maps:merge(Entity0,EntityS),
                                                    % Check partition is not changed
                                                    ?ClassU:assert_partition_not_changed(SM,PartField,Entity0,EntityN),
                                                    PathDt1 = maps:get(PartField,EntityN,undefined), % get new partition dt
                                                    % Notify when 'sync_fast_notify'
                                                    mb_notify(IM=='sync_fast_notify',NotifyPid,{'update',{Id,Entity0,EntityN},ModifierInfo}),
                                                    % Update cache when 'sync_fast_notify'
                                                    mb_to_cache(IM=='sync_fast_notify',CM/='none',fun() -> ?StorageEts:update(Cache,EntityN) end),
                                                    % Store changes in history
                                                    case ?ChangeHistory:put(CH,Meta,{'update',{Id,Entity0,EntityN},ModifierInfo}) of
                                                        {error,_}=Err3 -> Err3;
                                                        ok ->
                                                            % DB
                                                            FunOnCorruptHistory = fun() -> ?ChangeHistory:rollback(CH,Meta,{'update',{Id,Entity0,EntityN},ModifierInfo}) end,
                                                            FunOnCorruptCache = fun() -> ?StorageEts:delete(Cache,Id) end,
                                                            FunOnCorruptNotify = fun() -> mb_notify(true,NotifyPid,{'corrupt',{Id,EntityN,Entity0},ModifierInfo}) end,
                                                            FunDbWork =
                                                                fun() ->
                                                                    case CM of
                                                                        %@@@@@@@@@@@@@@
                                                                        'none' ->
                                                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                                                           [FunOnCorruptHistory,FunOnCorruptNotify],
                                                                                           PgModule:update(Meta,{PathDt0,PathDt1},EntityN,DbParams,PropNames));
                                                                        %@@@@@@@@@@@@@@
                                                                        'full' ->
                                                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                                                           [FunOnCorruptHistory,FunOnCorruptCache,FunOnCorruptNotify],
                                                                                           PgModule:update(Meta,{PathDt0,PathDt1},EntityN,DbParams,PropNames)),
                                                                            ok = ?StorageEts:update(Cache,EntityN);
                                                                        %@@@@@@@@@@@@@@
                                                                        'temp' ->
                                                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                                                           [FunOnCorruptHistory,FunOnCorruptCache,FunOnCorruptNotify],
                                                                                           PgModule:update(Meta,{PathDt0,PathDt1},EntityN,DbParams,PropNames)),
                                                                            ok = ?StorageEts:update(Cache,EntityN)
                                                                        %@@@@@@@@@@@@@@
                                                                    end,
                                                                    % Notify when not 'sync_fast_notify'
                                                                    mb_notify(IM/='sync_fast_notify',NotifyPid,{'update',{Id,Entity0,EntityN},ModifierInfo}),
                                                                    % Reply message
                                                                    EntityN1 = ?Format:decorate_entity(EntityN,PropNames),
                                                                    {ok,EntityN1}
                                                                end,
                                                            FunProceedDbWork(FunDbWork)
                                                    end;
                                                {{ok,_},ReplySecurity} -> ReplySecurity;
                                                {ReplySecurity,_} -> ReplySecurity
                                            end
                                    end
                            end,
                    reply_skip(Reply,FunReply,?FUNCTION_NAME,SkipResponseEntity)
            end
         end,
    State1 = ?Integrity:write_operation(F,Id,maps:get(hrkey,Map,Id),FunReply,update,State),
    {noreply,State1}.

%% -----------------------
%% DELETE operation
%% -----------------------
delete(Map,FunReply,State) ->
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,PropNames,Cache,DbParams,PgModule,NotifyPid} = extract_params(ModifierInfo, State),
    #cstate{cache_mode=CM,integrity_mode=IM,sync_ref=SyncRef,changehistory=CH,attachment_propnames=AttPs}=State,
    % Id
    {'entity',Id,EOpts} = maps:get('object',Map),
    PathDt = ?BU:get_by_key('path',EOpts,undefined), % partition dt
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun(FunProceedDbWork) ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            % Find
            Entity0 =
                case CM of
                    %@@@@@@@@@@@@@@
                    'none' ->
                        % Find
                        case PgModule:find(Meta,{PathDt},Id,DbParams,PropNames) of
                            {error,_}=Err -> throw(Err);
                            false -> throw({error,{not_found,<<"Entity not found.">>}});
                            {ok,E0} -> E0
                        end;
                    %@@@@@@@@@@@@@@
                    'full' ->
                        % Find
                        case ?StorageEts:find(Cache,Id) of
                            false -> throw({error,{not_found,<<"Entity not found.">>}});
                            {ok,E0} -> E0
                        end;
                    %@@@@@@@@@@@@@@
                    'temp' ->
                        % Find
                        case ?StorageEts:find(Cache,Id) of
                            {ok,E0} ->
                                case maps:get('$is_deleted',E0,false) of
                                    false -> E0;
                                    true -> throw({error,{not_found,<<"Entity not found.">>}})
                                end;
                            false ->
                                case PgModule:find(Meta,{PathDt},Id,DbParams,PropNames) of
                                    {error,_}=Err -> throw(Err);
                                    false -> throw({error,{not_found,<<"Entity not found.">>}});
                                    {ok,E0} -> E0 % no need to store in cache during delete operation
                                end end
                    %@@@@@@@@@@@@@@
                end,
            % Validator
            Reply = case ?ClassU:validate(Meta,'delete',{Id,Entity0,undefined},ModifierInfo) of
                        {error,_}=Err1 -> Err1;
                        {ok,EntityV} ->
                            % Security function (external) filter
                            case ?ClassU:apply_security({ok,EntityV},Map,{error,{access_denied,<<"Access denied.">>}}) of
                                {ok,_} ->
                                    % Notify when 'sync_fast_notify'
                                    mb_notify(IM=='sync_fast_notify',NotifyPid,{'delete',{Id,Entity0,undefined},ModifierInfo}),
                                    % Update cache when 'sync_fast_notify'
                                    mb_to_cache(IM=='sync_fast_notify',CM/='none',fun() -> ?StorageEts:update(Cache,Entity0#{'$is_deleted'=>true}) end),
                                    % Store changes in history
                                    case ?ChangeHistory:put(CH,Meta,{'delete',{Id,Entity0,undefined},ModifierInfo}) of
                                        {error,_}=Err2 -> Err2;
                                        ok ->
                                            % DB
                                            FunOnCorruptHistory = fun() -> ?ChangeHistory:rollback(CH,Meta,{'delete',{Id,Entity0,undefined},ModifierInfo}) end,
                                            FunOnCorruptNotify = fun() -> mb_notify(true,NotifyPid,{'corrupt',{Id,Entity0,undefined},ModifierInfo}) end,
                                            FunDbWork =
                                                fun() ->
                                                    ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                                   [FunOnCorruptHistory,FunOnCorruptNotify],
                                                                   PgModule:delete(Meta,{PathDt},Id,DbParams,PropNames)),
                                                    case CM of
                                                        'none' -> ok;
                                                        'full' -> ok = ?StorageEts:delete(Cache,Id);
                                                        'temp' -> ok = ?StorageEts:delete(Cache,Id)
                                                    end,
                                                    % Notify when not 'sync_fast_notify'
                                                    mb_notify(IM/='sync_fast_notify',NotifyPid,{'delete',{Id,Entity0,undefined},ModifierInfo}),
                                                    % delete attachments of entity
                                                    ?ClassU:delete_attachments(Meta,AttPs,Entity0),
                                                    % Reply
                                                    ok
                                                end,
                                            FunProceedDbWork(FunDbWork)
                                    end;
                                ReplySecurity ->
                                    ReplySecurity
                            end
                    end,
            FunReply(Reply)
        end,
    State1 = ?Integrity:write_operation(F,Id,maps:get(hrkey,Map,Id),FunReply,delete,State),
    {noreply,State1}.

%% -----------------------
%% CLEAR operation
%% -----------------------
clear(Map,FunReply,#cstate{}=State) ->
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    {Meta,_,Cache,DbParams,PgModule,NotifyPid} = extract_params(ModifierInfo, State),
    #cstate{sync_ref=SyncRef,changehistory=CH,cache_mode=CM,attachment_propnames=AttPs,driver_state=PgState}=State,
    % Assert
    collection = maps:get(object,Map),
    AutoCancelTS = ?BU:get_by_key('autocancel_ts',Map,0),
    F = fun() ->
            % Check canceled from queue
            case AutoCancelTS > 0 andalso ?BU:timestamp() > AutoCancelTS of true -> throw({error,{timeout,<<"Expired in queue">>}}); _ -> ok end,
            % Store changes in history
            Reply = case ?ChangeHistory:put(CH,Meta,{'clear',undefined,ModifierInfo}) of
                        {error,_}=Err -> Err;
                        ok ->
                            % DB
                            FunOnCorruptHistory = fun() -> ?ChangeHistory:rollback(CH,Meta,{'clear',undefined,ModifierInfo}) end,
                            ok = assert_db(true,NotifyPid,SyncRef,
                                           [FunOnCorruptHistory],
                                           PgModule:clear(Meta,DbParams)),
                            case CM of
                                'none' -> ok;
                                'full' -> ok = ?StorageEts:clear(Cache);
                                'temp' -> ok = ?StorageEts:clear(Cache)
                            end,
                            % Notify: instead of notifying for all entities
                            %    do send class event 'clear' (or drop subscriptions)
                            ?Notify:notify_subscribers(NotifyPid,[],{'clear',undefined,ModifierInfo}),
                            % clear all attachments of class
                            ?ClassU:clear_attachments(Meta,AttPs),
                            % Reply
                            ok
                    end,
            FunReply(Reply)
        end,
    State1 = ?Integrity:block_operation(F,FunReply,clear,State),
    PgState1 = PgState#dstate_pg{increment=0}, % incremented
    {noreply,State1#cstate{driver_state=PgState1}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
extract_params(State) ->
    extract_params({undefined,undefined}, State).
%%
extract_params(InitiatorInfo, State) ->
    #cstate{class_meta=Meta,
            propnames=PropNames,
            srv_notify=NotifyPid,
            class_ets=Cache,
            driver_state=DriverState}=State,
    #dstate_pg{module=PgModule,
               db_params=DbParams0}=DriverState,
    % DbParams :: {PoolKey,PoolSize,Conns::[map]}
    DbParams = case InitiatorInfo of
                   {'system',_} when is_tuple(DbParams0) -> setelement(1, DbParams0, system);
                   {'token',_} when is_tuple(DbParams0) -> setelement(1, DbParams0, system);
                   _ -> DbParams0
               end,
    {Meta,PropNames,Cache,DbParams,PgModule,NotifyPid}.

%% @private
internal_sync_store_to_cache_if_absent(Entity,State) ->
    {_,_,Cache,_,_,_} = extract_params(State),
    Id = maps:get(<<"id">>,Entity),
    case ?StorageEts:find(Cache,Id) of
        false -> ?StorageEts:create(Cache,Entity);
        {ok,_} -> ok
    end,
    State.

%% incremented
increment(State,Fup1) ->
    #cstate{driver_state=PgState}=State,
    #dstate_pg{increment_field=IncrField,increment=Incr}=PgState,
    case IncrField of
        undefined -> {ok,State,Fup1};
        _ ->
            NewIncr = Incr+1,
            Fup2 = if is_function(Fup1,1) -> fun(EntityX) -> Fup1(EntityX#{IncrField => NewIncr}) end;
                      is_function(Fup1,2) -> fun(EntityX,Arg) -> Fup1(EntityX#{IncrField => NewIncr},Arg) end;
                      true -> Fup1
                   end,
            State1 = State#cstate{driver_state=PgState#dstate_pg{increment=NewIncr}},
            ?ClassU:push_increment(IncrField,NewIncr,State),
            {ok,State1,Fup2}
    end.

%% @private
mb_notify(true,NotifyPid,Message) ->
    ?Notify:notify_subscribers(NotifyPid,[],Message);
mb_notify(_,_,_) -> ok.

%% @private
mb_to_cache(true,true,Fun) -> Fun();
mb_to_cache(_,_,_) -> ok.

%% @private
assert_db(false,_,_,_,DbResult) -> DbResult;
assert_db(true,_,_,_,ok) -> ok;
assert_db(true,_NotifyPid,_SyncRef,Funs,{error,_}=DbResult) ->
    % Notify was already sent, but Db returned error.
    %?Notify:drop_subscriptions(NotifyPid,SyncRef),
    lists:foreach(fun(Fun) -> Fun() end,Funs),
    throw(DbResult).

%% @private
reply_skip(Reply,FunReply,_,false) -> FunReply(Reply);
reply_skip({ok,Entity},FunReply,create,true) -> FunReply({ok,#{<<"id">> => maps:get(<<"id">>,Entity)}});
reply_skip({ok,_},FunReply,_,true) -> FunReply(ok);
reply_skip(Reply,FunReply,_,true) -> FunReply(Reply).
%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.03.2021
%%% @doc
%%% CRUD Args Map::
%%% #{
%%%     object :: collection | {'entity',Id,EOpts} | {path, [Dt,Id]}. EOpts :: [{path,_}]
%%%     initiator :: {Type,Id}
%%%     autocancel_ts :: integer()
%%%     qs (optional for object=collection && method=read | lookup. lookup::[lookup_field], read::[filter,order,mask,limit,offset,aggr,groupby,countonly,interval])
%%%     content (optional for method=setup && object=collection || method==create | replace | update | lookup )
%%%     hrkey :: undefined | binary()
%%%     x_security_fun :: function/1
%%%     skip_response_entity :: boolean(). (optional for method create)
%%% }

-module(dmlib_domain_class_crud).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([ensure_driver_state/1,
         update_driver_state/2]).
-export([load/1]).
-export([crud/4]).
-export([filter_cache/1]).
-export([expired/2]).
-export([expired_read_aggr_cache/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(DefaultCacheExpires, 600).
-define(DefaultMaxCacheSize, 10000).

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------------------------
%% Make state for driver at class-server start-up
%% --------------------------------------------------
ensure_driver_state(State) ->
    Module = crud_module(State),
    case ?BU:function_exported(Module,ensure_driver_state,1) of
        true -> Module:ensure_driver_state(State);
        false -> State
    end.

%% --------------------------------------------------
%% Update state for driver by connect/ensure result
%% --------------------------------------------------
update_driver_state(Props,State) ->
    Module = crud_module(State),
    case ?BU:function_exported(Module,update_driver_state,2) of
        true -> Module:update_driver_state(Props,State);
        false -> State
    end.

%% --------------------------------------------------
%% Load data from storage to full-cache
%% --------------------------------------------------
load(#cstate{}=State) ->
    Module = crud_module(State),
    State1 = new_hc(State),
    case ?BU:function_exported(Module,load,1) of
        true -> Module:load(State1);
        false -> {ok,State1}
    end.

%% --------------------------------------------------
%% Apply CRUD operation to driver
%% --------------------------------------------------
crud(Method,Map,FunReply,#cstate{self=ClassPid,ref=SyncRef,data_hc=HC,storage_mode=StorageMode}=State)
  when Method == 'create';
       Method == 'replace';
       Method == 'update';
       Method == 'delete';
       Method == 'clear' ->
    Module = crud_module(State),
    case format_entity_id(Map,StorageMode) of
        {error,_}=Err -> Err;
        {ok,Map1,_} ->
            ?LOG('$trace',"~ts '~ts' '~ts' -> ~n\t~300tp",[Module,State#cstate.classname,Method,Map]),
            FunReply1 = fun(Reply) ->
                            % change hc for those, who read data when entity is being modificated (read new hc, but old data).
                            case Reply of
                                {ok,_} -> ClassPid ! {refresh_data_hc,SyncRef};
                                ok -> ClassPid ! {refresh_data_hc,SyncRef};
                                _ -> ok
                            end,
                            FunReply(Reply)
                        end,
            % change hc for those, who notified and requests read before finalize of changes (read old hc and skip update data till next iteration).
            %   this makes double change of hc, and change of hc on every modification request, even if it finalized with error.
            State1 = State#cstate{data_hc=HC+1},
            Module:Method(Map1,FunReply1,State1)
    end;
%%
crud(Method,Map,FunReply,#cstate{storage_mode=StorageMode}=State)
  when Method == 'lookup' ->
    Module = crud_module(State),
    case format_entity_id(Map,StorageMode) of
        {error,_}=Err -> Err;
        {ok,Map1,_} ->
            ?LOG('$trace',"~ts '~ts' '~ts' -> ~n\t~300tp",[Module,State#cstate.classname,Method,Map]),
            Module:Method(Map1,FunReply,State)
    end;
%%
crud(Method,Map,FunReply,#cstate{storage_mode=StorageMode}=State)
  when Method == 'read' ->
    Module = crud_module(State),
    case format_entity_id(Map,StorageMode) of
        {error,_}=Err -> Err;
        {ok,Map1,'collection'} ->
            SelectOpts = maps:get(qs,Map1,[]),
            case format_select_opts(SelectOpts) of
                {error,_}=Err -> Err;
                {ok,SelectOpts1} ->
                    ?LOG('$trace',"~ts '~ts' '~ts' -> ~n\t~300tp",[Module,State#cstate.classname,Method,Map]),
                    Module:read({'collection',Map1,SelectOpts1},FunReply,State)
            end;
        {ok,Map1,{'entity',Id,EOpts}} ->
            Module:read({'entity',Map1,Id,EOpts},FunReply,State)
    end;
%%
crud(Method,Map,FunReply,#cstate{sync_ref=SyncRef,srv_notify=SrvNotify}=State)
  when Method == 'setup' ->
    case maps:get('object',Map,undefined) of
        'collection' ->
            case maps:get(content,Map,undefined) of
                Content when is_map(Content) ->
                    case maps:get(<<"cmd">>,Content,undefined) of
                        <<"suspend_notification">> ->
                            Reply = ?Notify:suspend_notification(SrvNotify,SyncRef),
                            FunReply(Reply),
                            {noreply,State};
                        <<"resume_notification">> ->
                            SendReload = ?BU:to_bool(maps:get(<<"send_reload">>,Content,true)),
                            Reply = ?Notify:resume_notification(SrvNotify,SyncRef,SendReload),
                            FunReply(Reply),
                            {noreply,State};
                        <<"optimize">> ->
                            Module = crud_module(State),
                            case ?BU:function_exported(Module,optimize,3) of
                                false -> {error,{400,<<"Invalid cmd. Current storage_mode doesn't implement operation">>}};
                                true -> Module:optimize(Map,FunReply,State)
                            end;
                        <<"read_partitions">> ->
                            Module = crud_module(State),
                            case ?BU:function_exported(Module,read_partitions,3) of
                                false -> {error,{400,<<"Invalid cmd. Current storage_mode doesn't implement operation">>}};
                                true -> Module:read_partitions(Map,FunReply,State)
                            end;
                        <<"drop_partition">> ->
                            Module = crud_module(State),
                            case ?BU:function_exported(Module,drop_partition,3) of
                                false -> {error,{400,<<"Invalid cmd. Current storage_mode doesn't implement operation">>}};
                                true ->
                                    case maps:get(<<"partition">>,Content,undefined) of
                                        undefined -> {error,{400,<<"Invalid partition. Expected existing partition name. Use read_partitions cmd first">>}};
                                        Partition -> Module:drop_partition(Map#{partition => Partition},FunReply,State)
                                    end
                            end;
                        Bin when is_binary(Bin) -> {error,{400,<<"Invalid cmd. Expected: suspend_notification, resume_notification, [optimize, read_partitions, drop_partition]">>}};
                        _ -> {error,{400,<<"Invalid content. Expected 'cmd'::string">>}}
                    end;
                _ -> {error,{400,<<"Invalid content. Expected 'content'::object">>}}
            end;
        _ -> {error,{400,<<"Invalid params. Expected 'object' => 'collection'">>}}
    end;
%%
crud(_Method,_,_,_) ->
    {error,{405,<<"Method not allowed. Expected: read, lookup, create, replace, update, delete, clear">>}}.

%% --------------------------------------------------
%% Delete too old items from temporary cache
%% --------------------------------------------------
filter_cache(#cstate{cache_mode='temp',class_meta=Meta,class_ets=Cache,domain=Domain,classname=ClassName}=_State) ->
    Opts = maps:get(opts,Meta),
    Expires = ?BU:get_by_key(<<"cache_sec">>,Opts,?DefaultCacheExpires),
    MaxCount = ?BU:get_by_key(<<"cache_limit">>,maps:get(opts,Meta),?DefaultMaxCacheSize),
    ?LOG('$trace', "~ts. '~ts' * '~ts' filter cache (~p)", [?APP, Domain, ClassName, ets:info(Cache,size)]),
    ?StorageEts:filter(Cache,Expires*1000,MaxCount);
filter_cache(_) -> ok.

%% --------------------------------------------------
%% On TTL timer of entity
%% --------------------------------------------------
expired(TimerInfo, #cstate{storage_mode=SM}=State) when SM=='runtime'; SM=='ram'; SM=='ets' ->
    Module = crud_module(State),
    Module:expired(TimerInfo,State);
expired(_, State) -> State.

%% --------------------------------------------------
%% On timer of read aggregated cache value
%% --------------------------------------------------
expired_read_aggr_cache(TimerInfo, #cstate{storage_mode=SM}=State) when SM=='runtime'; SM=='ram'; SM=='ets' ->
    Module = crud_module(State),
    Module:expired_read_aggr_cache(TimerInfo,State);
expired_read_aggr_cache(_, State) -> State.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% --------------------------------------------------
%% Set initial data_hc
%%   Uses timestamp and random, to decrease probability of same values after restart.
%%     Frequency of modifications is less than 1 mln/sec
%%   Could be used also for unique random hashing after every modification
%% --------------------------------------------------
new_hc(State) ->
    State#cstate{data_hc=?BU:timestamp()*1000000 + ?BU:random(1000000)}.

%% --------------------------------------------------
%% Format options routine
%% --------------------------------------------------
format_select_opts(SelectOpts) when is_list(SelectOpts) ->
    format_select_opts(maps:from_list(SelectOpts));
format_select_opts(SelectOpts) when is_map(SelectOpts) ->
    SO = SelectOpts,
    BInterval = ?BU:get_by_key(<<"interval">>,SO,<<>>), % for 'history', 'transactionlog'
    BFilter = ?BU:get_by_key(<<"filter">>,SO,<<>>),
    BSort = ?BU:get_by_key(<<"order">>,SO,<<>>),
    BMask = ?BU:get_by_key(<<"mask">>,SO,<<>>),
    BAggr = ?BU:get_by_key(<<"aggr">>,SO,<<>>),
    BGroupBy = ?BU:get_by_key(<<"groupby">>,SO,<<>>),
    CountOnly = ?BU:get_by_key(<<"countonly">>,SO,false),
    try
        FunMake = fun(<<>>,Empty) -> Empty;
                     (Val,_) when is_list(Val); is_map(Val) -> Val;
                     (Val,_) when is_binary(Val) -> jsx:decode(Val, [return_maps])
                  end,
        FunMakeCatch = fun(<<>>,Empty,_) -> Empty;
                          (Val,_,_) when is_list(Val) -> Val;
                          (Val,_,Default) when is_binary(Val) ->
                              case catch jsx:decode(Val, [return_maps]) of
                                  {'EXIT',_} -> Default;
                                  Result -> Result
                              end end,
        Assert = fun(Res,Fun,ErrReason) ->
                        case Fun(Res) of
                            true -> Res;
                            false -> throw({error,{400,ErrReason}})
                        end end,
        %
        IntervalJson = Assert(FunMake(BInterval,[]),fun is_list/1,<<"Invalid 'interval'. Expected list of 2 elements: from datetime and to datetime">>),
        FilterJson = Assert(FunMake(BFilter,[]),fun is_list/1,<<"Invalid 'filter'. Expected expression list">>),
        AggrJson = Assert(FunMake(BAggr,#{}),fun is_map/1,<<"Invalid 'aggr'. Expected object {NewName: ExpressionList}">>),
        GroupByJson = Assert(FunMake(BGroupBy,#{}),fun is_map/1,<<"Invalid 'groupby'. Expected object {NewName: PropertyName or ExpressionList}">>),
        SortJson = Assert(FunMake(BSort,[]),fun is_list/1,<<"Invalid 'order'. Expected list of field names or objects">>),
        Mask = FunMakeCatch(BMask,[],BMask),
        {ok, SO#{<<"interval">> => IntervalJson, % for 'history', 'transactionlog'
                 <<"filter">> => FilterJson,
                 <<"aggr">> => AggrJson,
                 <<"groupby">> => GroupByJson,
                 <<"order">> => SortJson,
                 <<"mask">> => Mask,
                 <<"countonly">> => CountOnly}}
    catch
        throw:{error,_}=Err ->
            ?LOG('$error',"Error parsing read params: ~120tp~n\tSelectOpts: ~120tp",[Err,SelectOpts]),
            Err;
        C:R:ST ->
            ?LOG('$error',"Error parsing read params: ~p:~p~n\tStack: ~120tp~n\tSelectOpts: ~120tp",[C,R,ST,SelectOpts]),
            {error,{invalid_params,?BU:strbin("Invalid params. Check filter, order and mask", [])}}
    end.

%%
crud_module(#cstate{storage_mode=SM}=_State) ->
    case SM of
        'ets' -> ?CrudEts;
        'ram' -> ?CrudMnesia;
        'runtime' -> ?CrudMnesia;
        'category'-> ?CrudPg;
        'history' -> ?CrudPg;
        'transactionlog' -> ?CrudKafkaClickhouse; % ?CrudKafka
        _ -> ?CrudEts
    end.

%% @private
format_entity_id(Map,StorageMode) ->
    case maps:get('object',Map,undefined) of
        'collection'=Obj -> {ok,Map,Obj};
        {'entity',Id,EOpts}=Obj ->
            case ?BU:to_guid(Id) of
                <<"00000000-0000-0000-0000-000000000000">> -> {error,{invalid_params,<<"Invalid id. Expected uuid">>}};
                Id -> {ok,Map,Obj};
                Id1 -> {ok, Map#{object => {'entity',Id1,EOpts}}, {'entity',Id1,EOpts}}
            end;
        {'path',[Dt,Id]} when StorageMode=='history'; StorageMode=='transactionlog' ->
            case ?BU:to_guid(Id) of
                <<"00000000-0000-0000-0000-000000000000">> -> {error,{invalid_params,<<"Invalid id. Expected uuid">>}};
                Id ->
                    Obj = {'entity',Id,[{path,Dt}]},
                    {ok, Map#{object => Obj}, Obj}
            end;
        _ ->
            {error,{400,<<"Invalid params. Expected 'object' => 'collection' | {'entity',Id::binary(),Opts::map()|list()} | {'path',[Part::binary()]}">>}}
    end.

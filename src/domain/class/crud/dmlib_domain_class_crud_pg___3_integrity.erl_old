%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 27.03.2021
%%% @doc CRUD routines for 'postgresql' driver.
%%%      Complex implementation (cache: full|temp|none) * (integrity: async|sync|sync_fast_read|sync_fast_notify)

-module(era_dms_domain_class_crud_pg___3_integrity).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([ensure_driver_state/1]).
-export([load/1]).
-export([lookup/3,
         read/3,
         create/3,
         replace/3,
         update/3,
         delete/3,
         clear/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("era_dms.hrl").

-define(ReadTimeout, 60000).

-record(dstate_pg, {
    db_params :: map(),
    module :: atom()
}).

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------
%% Initialize driver state at start of class process
%% -----------------------
ensure_driver_state(#cstate{driver_state=#dstate_pg{}}=State) -> State;
ensure_driver_state(#cstate{domain=Domain,class_meta=Meta,storage_mode=StorageMode}=State) ->
    case ?Postgres:get_db_params(Domain,Meta) of
        {ok,DbParams} when StorageMode=='category' -> State#cstate{driver_state=#dstate_pg{db_params=DbParams,module=?PostgresCrud}};
        {error,_} -> State#cstate{driver_state=#dstate_pg{db_params=#{}}}
    end.

%% -----------------------
%% load cache synchronously by portions
%% TODO ICP: may be work by async process while general facade is paused.
%% -----------------------
-spec load(State::#cstate{}) -> ok | {error,term()}.
%% -----------------------
load(State) ->
    {Meta,PropNames,Cache,DbParams,PgModule,_} = extract_params(State),
    case State#cstate.cache_mode of
        %@@@@@@@@@@@@@@
        'none' -> {ok,State};
        %@@@@@@@@@@@@@@
        'temp' -> {ok,State};
        %@@@@@@@@@@@@@@
        'full' ->
            PortionSize = 10000,
            MetaX = Meta#{opts=>(maps:get(opts,Meta))#{<<"max_limit">> => PortionSize,
                                                       <<"max_mask">> => []}},
            SelectOpts = #{<<"order">> => [<<"id">>],
                           <<"limit">> => PortionSize,
                           <<"mask">> => [],
                           <<"filter">> => []},
            F = fun F(Offset) ->
                        case PgModule:read(MetaX,SelectOpts#{<<"offset">> => Offset},DbParams,PropNames) of
                            {ok,[]} -> {ok,State};
                            {ok,Entities} when length(Entities) < PortionSize ->
                                lists:foreach(fun(Entity) -> ?StorageEts:create(Cache,Entity) end, Entities),
                                {ok,State};
                            {ok,Entities} ->
                                lists:foreach(fun(Entity) -> ?StorageEts:create(Cache,Entity) end, Entities),
                                F(Offset+PortionSize);
                            {error,_}=Err ->
                                ?StorageEts:clear(Cache),
                                Err
                        end end,
            F(0)
        %@@@@@@@@@@@@@@
    end.

%% -----------------------
%% LOOKUP operation
%% -----------------------
lookup(Map,FunReply,State) ->
    {Meta,PropNames,Cache,DbParams,PgModule,_} = extract_params(State),
    Content = maps:get(content,Map),
    F = fun() ->
            Reply = case State#cstate.cache_mode of
                        %@@@@@@@@@@@@@@
                        'none' ->
                            % DB
                            {ok,_} = PgModule:lookup(Meta,Content,DbParams,PropNames);
                        %@@@@@@@@@@@@@@
                        'full' ->
                            LookupKeyFields = maps:get(<<"lookup_properties">>,maps:get(opts,Meta),[]),
                            % DB
                            {ok,_} = ?StorageEts:lookup(Cache,Content,LookupKeyFields);
                        %@@@@@@@@@@@@@@
                        'temp' ->
                            LookupKeyFields = maps:get(<<"lookup_properties">>,maps:get(opts,Meta),[]),
                            % DB
                            case ?StorageEts:lookup(Cache,Content,LookupKeyFields) of
                                {ok,[_|_]}=Ok -> Ok;
                                {ok,[]} -> {ok,_} = PgModule:lookup(Meta,Content,DbParams,PropNames)
                            end
                        %@@@@@@@@@@@@@@
                    end,
            % Reply
            FunReply(Reply)
        end,
    State1 = ?Integrity:read_operation(F,FunReply,lookup,State),
    {noreply,State1}.

%% -----------------------
%% READ collection operation
%% READ entity operation
%% -----------------------
read({collection,_Map,SelectOpts},FunReply,State) ->
    {Meta,PropNames,Cache,DbParams,PgModule,_} = extract_params(State),
    F = fun() ->
            Reply = case State#cstate.cache_mode of
                        %@@@@@@@@@@@@@@
                        'none' ->
                            % DB
                            PgModule:read(Meta,SelectOpts,DbParams,PropNames);
                        %@@@@@@@@@@@@@@
                        'full' ->
                            Opts = maps:get(opts,Meta),
                            LimitOpts = [{max_limit,maps:get(<<"max_limit">>,Opts,?DefaultMaxLimit)},
                                         {max_mask,maps:get(<<"max_mask">>,Opts,?DefaultMaxMask)}],
                            % DB
                            {ok,_} = ?StorageEts:read(Cache,SelectOpts,LimitOpts);
                        %@@@@@@@@@@@@@@
                        'temp' ->
                            % DB
                            PgModule:read(Meta,SelectOpts,DbParams,PropNames)
                        % Update by cache when async/sync_fast_...
                        %@@@@@@@@@@@@@@
                    end,
            % Reply
            FunReply(Reply)
        end,
    State1 = ?Integrity:read_operation(F,FunReply,read_collection,State),
    {noreply,State1};
%% -----------------------
read({entity,_Map,Id,_},FunReply,#cstate{class_meta=Meta,driver_state=#dstate_pg{},sync_ref=SyncRef}=State) ->
    {Meta,PropNames,Cache,DbParams,PgModule,_} = extract_params(State),
    % This is read by primary_key, it's fast and is equal part to find operation during create/replace/update/delete request
    OperationMode = 'w',
    Self = self(),
    F = fun() ->
            % DB
            Reply = case State#cstate.cache_mode of
                        %@@@@@@@@@@@@@@
                        'none' ->
                            case PgModule:find(Meta,Id,DbParams,PropNames) of
                                {ok,Entity} -> {ok,Entity};
                                {error,_}=Err -> Err;
                                false -> {error,{not_found,?BU:strbin("Entity not found",[])}}
                            end;
                        %@@@@@@@@@@@@@@
                        'full' ->
                            case ?StorageEts:find(Cache,Id) of
                                {ok,Entity} -> {ok,Entity};
                                false -> {error,{not_found,?BU:strbin("Entity not found",[])}}
                            end;
                        %@@@@@@@@@@@@@@
                        'temp' ->
                            case ?StorageEts:find(Cache,Id) of
                                {ok,Entity} -> {ok,Entity};
                                false ->
                                    case PgModule:find(Meta,Id,DbParams,PropNames) of
                                        {ok,Entity} when OperationMode=='r' ->
                                            Self ! {do_work_internal,SyncRef,fun(StateX) -> internal_sync_store_to_cache_if_absent(Entity,StateX) end},
                                            {ok,Entity};
                                        {ok,Entity} when OperationMode=='w' ->
                                            ?StorageEts:create(Cache,Entity),
                                            {ok,Entity};
                                        false -> {error,{not_found,?BU:strbin("Entity not found",[])}};
                                        {error,_}=Err -> Err
                                    end end
                        %@@@@@@@@@@@@@@
                    end,
            % Reply
            FunReply(Reply)
        end,
    State1 = case OperationMode of
                 'r' -> ?Integrity:read_operation(F,FunReply,read_entity,State);
                 'w' -> ?Integrity:write_operation(F,Id,FunReply,read_entity,State)
             end,
    {noreply,State1}.

%% -----------------------
%% POST operation
%% -----------------------
create(Map,FunReply,State) ->
    {Meta,PropNames,Cache,DbParams,PgModule,NotifyPid} = extract_params(State),
    #cstate{cache_mode=CM,integrity_mode=IM,sync_ref=SyncRef}=State,
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    % Id
    Id = ?BU:newid(),
    F = fun(FunProceedDbWork) ->
            % Build
            case ?Format:def_req(?Format:decorate_entity(?Format:build_entity(Map,Meta,Id),PropNames),Meta) of
                {error,_}=Err -> FunReply(Err);
                Entity ->
                    Id = maps:get(<<"id">>,Entity), % assert
                    % Notify when 'sync_fast_notify'
                    mb_notify(IM=='sync_fast_notify',NotifyPid,{'create',{Id,undefined,Entity},ModifierInfo}),
                    % Update cache when 'sync_fast_notify'
                    mb_to_cache(IM=='sync_fast_notify',CM/='none',fun() -> ?StorageEts:create(Cache,Entity) end),
                    % DB
                    FunOnCorruptCache = fun() -> ?StorageEts:delete(Cache,Id) end,
                    FunOnCorruptNotify = fun() -> mb_notify(true,NotifyPid,{'corrupt',{Id,Entity,undefined},ModifierInfo}) end,
                    FunDbWork =
                        fun() ->
                            case State#cstate.cache_mode of
                                %@@@@@@@@@@@@@@
                                'none' ->
                                    ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                   [FunOnCorruptNotify],
                                                   PgModule:create(Meta,Entity,DbParams,PropNames));
                                %@@@@@@@@@@@@@@
                                'full' ->
                                    ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                   [FunOnCorruptCache,FunOnCorruptNotify],
                                                   PgModule:create(Meta,Entity,DbParams,PropNames)),
                                    ok = ?StorageEts:create(Cache,Entity);
                                %@@@@@@@@@@@@@@
                                'temp' ->
                                    ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                   [FunOnCorruptCache,FunOnCorruptNotify],
                                                    PgModule:create(Meta,Entity,DbParams,PropNames)),
                                    ok = ?StorageEts:create(Cache,Entity)
                                %@@@@@@@@@@@@@@
                            end,
                            % Notify when not 'sync_fast_notify'
                            mb_notify(IM/='sync_fast_notify',NotifyPid,{'create',{Id,undefined,Entity},ModifierInfo}),
                            % Reply message
                            FunReply({ok,Entity})
                        end,
                    FunProceedDbWork(FunDbWork)
            end end,
    State1 = ?Integrity:write_operation(F,Id,FunReply,create,State),
    {noreply,State1}.

%% -----------------------
%% PUT operation
%% -----------------------
replace(Map,FunReply,State) ->
    {Meta,PropNames,Cache,DbParams,PgModule,NotifyPid} = extract_params(State),
    #cstate{cache_mode=CM,integrity_mode=IM,sync_ref=SyncRef}=State,
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    % Id
    {'entity',Id,_} = maps:get(object,Map),
    F = fun(FunProceedDbWork) ->
            % Build
            case ?Format:def_req(?Format:decorate_entity(?Format:build_entity(Map,Meta),PropNames),Meta) of
                {error,_}=Err -> FunReply(Err);
                Entity1 ->
                    Id = maps:get(<<"id">>,Entity1),
                    Entity0 =
                        case State#cstate.cache_mode of
                            %@@@@@@@@@@@@@@
                            'none' ->
                                % Find
                                case PgModule:find(Meta,Id,DbParams,PropNames) of
                                    {error,_}=Err -> throw(Err);
                                    false -> undefined;
                                    {ok,E0} -> E0
                                end;
                            %@@@@@@@@@@@@@@
                            'full' ->
                                % Find
                                case ?StorageEts:find(Cache,Id) of
                                    false -> undefined;
                                    {ok,E0} -> E0
                                end;
                            %@@@@@@@@@@@@@@
                            'temp' ->
                                % Find
                                case ?StorageEts:find(Cache,Id) of
                                    {ok,E0} -> E0;
                                    false ->
                                        case PgModule:find(Meta,Id,DbParams,PropNames) of
                                            {error,_}=Err -> throw(Err);
                                            false -> undefined;
                                            {ok,E0} -> E0 % no need to store previous version
                                        end end
                            %@@@@@@@@@@@@@@
                        end,
                    case Entity1 of
                        Entity0 -> FunReply({ok,Entity0});
                        _ ->
                            % Notify when 'sync_fast_notify'
                            case Entity0 of
                                undefined -> mb_notify(IM=='sync_fast_notify',NotifyPid,{'create',{Id,undefined,Entity1},ModifierInfo});
                                _ -> mb_notify(IM=='sync_fast_notify',NotifyPid,{'update',{Id,Entity0,Entity1},ModifierInfo})
                            end,
                            % Update cache when 'sync_fast_notify'
                            mb_to_cache(IM=='sync_fast_notify',CM/='none',fun() -> ?StorageEts:replace(Cache,Entity1) end),
                            % DB
                            FunOnCorruptCache = fun() -> ?StorageEts:delete(Cache,Id) end,
                            FunOnCorruptNotify = fun() when Entity0==undefined -> mb_notify(true,NotifyPid,{'corrupt',{Id,Entity1,undefined},ModifierInfo});
                                                    () -> mb_notify(true,NotifyPid,{'corrupt',{Id,Entity1,Entity0},ModifierInfo}) end,
                            FunDbWork =
                                fun() ->
                                    case State#cstate.cache_mode of
                                        %@@@@@@@@@@@@@@
                                        'none' ->
                                            % DB
                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                           [FunOnCorruptNotify],
                                                           PgModule:replace(Meta,Entity1,DbParams,PropNames));
                                        %@@@@@@@@@@@@@@
                                        'full' ->
                                            % DB
                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                           [FunOnCorruptCache,FunOnCorruptNotify],
                                                           PgModule:replace(Meta,Entity1,DbParams,PropNames)),
                                            ok = ?StorageEts:replace(Cache,Entity1);
                                        %@@@@@@@@@@@@@@
                                        'temp' ->
                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                           [FunOnCorruptCache,FunOnCorruptNotify],
                                                           PgModule:replace(Meta,Entity1,DbParams,PropNames)),
                                            ok = ?StorageEts:replace(Cache,Entity1)
                                        %@@@@@@@@@@@@@@
                                    end,
                                    % Notify when not 'sync_fast_notify'
                                    case Entity0 of
                                        undefined -> mb_notify(IM/='sync_fast_notify',NotifyPid,{'create',{Id,undefined,Entity1},ModifierInfo});
                                        _ -> mb_notify(IM/='sync_fast_notify',NotifyPid,{'update',{Id,Entity0,Entity1},ModifierInfo})
                                    end,
                                    % Reply message
                                    FunReply({ok,Entity1})
                                end,
                            FunProceedDbWork(FunDbWork)
                        end
            end
        end,
    State1 = ?Integrity:write_operation(F,Id,FunReply,replace,State),
    {noreply,State1}.

%% -----------------------
%% PATCH operation
%% -----------------------
update(Map,FunReply,State) ->
    {Meta,PropNames,Cache,DbParams,PgModule,NotifyPid} = extract_params(State),
    #cstate{cache_mode=CM,integrity_mode=IM,sync_ref=SyncRef}=State,
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    % Id
    {'entity',Id,_} = maps:get(object,Map),
    F = fun(FunProceedDbWork) ->
            % Build !!! without decorate to update by nulls
            case ?Format:build_entity(Map,Meta) of
                {error,_}=Err -> FunReply(Err);
                Entity ->
                    % Find
                    Id = maps:get(<<"id">>,Entity),
                    Entity0 =
                        case State#cstate.cache_mode of
                            %@@@@@@@@@@@@@@
                            'none' ->
                                % Find
                                case PgModule:find(Meta,Id,DbParams,PropNames) of
                                    {error,_}=Err -> throw(Err);
                                    false -> throw({error,{not_found,?BU:strbin("Entity not found",[])}});
                                    {ok,E0} -> E0
                                end;
                            %@@@@@@@@@@@@@@
                            'full' ->
                                % Find
                                case ?StorageEts:find(Cache,Id) of
                                    false -> throw({error,{not_found,?BU:strbin("Entity not found",[])}});
                                    {ok,E0} -> E0
                                end;
                            %@@@@@@@@@@@@@@
                            'temp' ->
                                % Find
                                case ?StorageEts:find(Cache,Id) of
                                    {ok,E0} -> E0;
                                    false ->
                                        case PgModule:find(Meta,Id,DbParams,PropNames) of
                                            {error,_}=Err -> throw(Err);
                                            false -> throw({error,{not_found,?BU:strbin("Entity not found",[])}});
                                            {ok,E0} ->
                                                ?StorageEts:create(Cache,E0),
                                                E0
                                        end end
                            %@@@@@@@@@@@@@@
                        end,
                    % Build
                    case ?Format:def_req(?Format:decorate_entity(maps:merge(Entity0,Entity),PropNames),Meta) of
                        {error,_}=Err1 -> FunReply(Err1);
                        Entity0 -> FunReply({ok,Entity0});
                        Entity1 ->
                            % Notify when 'sync_fast_notify'
                            mb_notify(IM=='sync_fast_notify',NotifyPid,{'update',{Id,Entity0,Entity1},ModifierInfo}),
                            % Update cache when 'sync_fast_notify'
                            mb_to_cache(IM=='sync_fast_notify',CM/='none',fun() -> ?StorageEts:update(Cache,Entity1) end),
                            % DB
                            FunOnCorruptCache = fun() -> ?StorageEts:delete(Cache,Id) end,
                            FunOnCorruptNotify = fun() -> mb_notify(true,NotifyPid,{'corrupt',{Id,Entity1,Entity0},ModifierInfo}) end,
                            FunDbWork =
                                fun() ->
                                    case State#cstate.cache_mode of
                                        %@@@@@@@@@@@@@@
                                        'none' ->
                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                           [FunOnCorruptNotify],
                                                           PgModule:update(Meta,Entity1,DbParams,PropNames));
                                        %@@@@@@@@@@@@@@
                                        'full' ->
                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                           [FunOnCorruptCache,FunOnCorruptNotify],
                                                           PgModule:update(Meta,Entity1,DbParams,PropNames)),
                                            ok = ?StorageEts:update(Cache,Entity1);
                                        %@@@@@@@@@@@@@@
                                        'temp' ->
                                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                                           [FunOnCorruptCache,FunOnCorruptNotify],
                                                           PgModule:update(Meta,Entity1,DbParams,PropNames)),
                                            ok = ?StorageEts:update(Cache,Entity1)
                                        %@@@@@@@@@@@@@@
                                    end,
                                    % Notify when not 'sync_fast_notify'
                                    mb_notify(IM/='sync_fast_notify',NotifyPid,{'update',{Id,Entity0,Entity1},ModifierInfo}),
                                    % Reply message
                                    FunReply({ok,Entity1})
                                end,
                            FunProceedDbWork(FunDbWork)
                    end end
        end,
    State1 = ?Integrity:write_operation(F,Id,FunReply,update,State),
    {noreply,State1}.

%% -----------------------
%% DELETE operation
%% -----------------------
delete(Map,FunReply,State) ->
    {Meta,PropNames,Cache,DbParams,PgModule,NotifyPid} = extract_params(State),
    #cstate{cache_mode=CM,integrity_mode=IM,sync_ref=SyncRef}=State,
    ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
    % Id
    {'entity',Id,_} = maps:get(object,Map),
    F = fun(FunProceedDbWork) ->
            % Find
            Entity0 =
                case State#cstate.cache_mode of
                    %@@@@@@@@@@@@@@
                    'none' ->
                        % Find
                        case PgModule:find(Meta,Id,DbParams,PropNames) of
                            {error,_}=Err -> throw(Err);
                            false -> throw({error,{not_found,?BU:strbin("Entity not found",[])}});
                            {ok,E0} -> E0
                        end;
                    %@@@@@@@@@@@@@@
                    'full' ->
                        % Find
                        case ?StorageEts:find(Cache,Id) of
                            false -> throw({error,{not_found,?BU:strbin("Entity not found",[])}});
                            {ok,E0} -> E0
                        end;
                    %@@@@@@@@@@@@@@
                    'temp' ->
                        % Find
                        case ?StorageEts:find(Cache,Id) of
                            {ok,E0} ->
                                case maps:get('$is_deleted',E0,false) of
                                    false -> {ok,E0};
                                    true -> throw({error,{not_found,?BU:strbin("Entity not found",[])}})
                                end;
                            false ->
                                case PgModule:find(Meta,Id,DbParams,PropNames) of
                                    {error,_}=Err -> throw(Err);
                                    false -> throw({error,{not_found,?BU:strbin("Entity not found",[])}});
                                    {ok,E0} -> E0 % no need to store in cache during delete operation
                                end end
                    %@@@@@@@@@@@@@@
                end,
            % Notify when 'sync_fast_notify'
            mb_notify(IM=='sync_fast_notify',NotifyPid,{'delete',{Id,Entity0,undefined},ModifierInfo}),
            % Update cache when 'sync_fast_notify'
            mb_to_cache(IM=='sync_fast_notify',CM/='none',fun() -> ?StorageEts:update(Cache,Entity0#{'$is_deleted'=>true}) end),
            % DB
            FunOnCorruptNotify = fun() -> mb_notify(true,NotifyPid,{'corrupt',{Id,Entity0,undefined},ModifierInfo}) end,
            FunDbWork =
                fun() ->
                    case State#cstate.cache_mode of
                        %@@@@@@@@@@@@@@
                        'none' ->
                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                           [FunOnCorruptNotify],
                                           PgModule:delete(Meta,Id,DbParams,PropNames));
                        %@@@@@@@@@@@@@@
                        'full' ->
                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                           [FunOnCorruptNotify],
                                           PgModule:delete(Meta,Id,DbParams,PropNames)),
                            ok = ?StorageEts:delete(Cache,Id);
                        %@@@@@@@@@@@@@@
                        'temp' ->
                            ok = assert_db(IM=='sync_fast_notify',NotifyPid,SyncRef,
                                           [FunOnCorruptNotify],
                                           PgModule:delete(Meta,Id,DbParams,PropNames)),
                            ok = ?StorageEts:delete(Cache,Id)
                        %@@@@@@@@@@@@@@
                    end,
                    % Notify when not 'sync_fast_notify'
                    mb_notify(IM/='sync_fast_notify',NotifyPid,{'delete',{Id,Entity0,undefined},ModifierInfo}),
                    % Reply
                    FunReply(ok)
                end,
            FunProceedDbWork(FunDbWork)
        end,
    State1 = ?Integrity:write_operation(F,Id,FunReply,delete,State),
    {noreply,State1}.

%% -----------------------
%% CLEAR operation
%% -----------------------
clear(Map,FunReply,#cstate{}=State) ->
    {Meta,_,Cache,DbParams,PgModule,NotifyPid} = extract_params(State),
    % Assert
    collection = maps:get(object,Map),
    F = fun() ->
            % DB
            case State#cstate.cache_mode of
                %@@@@@@@@@@@@@@
                'none' ->
                    ok = PgModule:clear(Meta,DbParams);
                %@@@@@@@@@@@@@@
                'full' ->
                    ok = PgModule:clear(Meta,DbParams),
                    ok = ?StorageEts:clear(Cache);
                %@@@@@@@@@@@@@@
                'temp' ->
                    ok = PgModule:clear(Meta,DbParams),
                    ok = ?StorageEts:clear(Cache)
                %@@@@@@@@@@@@@@
            end,
            % Notify: instead of notifying for all entities
            %    do send class event 'clear' (or drop subscriptions)
            ModifierInfo = maps:get(initiator,Map,{undefined,undefined}),
            ?Notify:notify_subscribers(NotifyPid,[],{'clear',undefined,ModifierInfo}),
            % Reply
            FunReply(ok)
        end,
    State1 = ?Integrity:block_operation(F,FunReply,clear,State),
    {noreply,State1}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
extract_params(State) ->
    #cstate{class_meta=Meta,
            propnames=PropNames,
            srv_notify=NotifyPid,
            class_ets=Cache,
            driver_state=#dstate_pg{db_params=DbParams,module=PgModule}}=State,
    {Meta,PropNames,Cache,DbParams,PgModule,NotifyPid}.

%% @private
internal_sync_store_to_cache_if_absent(Entity,State) ->
    {_,_,Cache,_,_,_} = extract_params(State),
    Id = maps:get(<<"id">>,Entity),
    case ?StorageEts:find(Cache,Id) of
        false -> ?StorageEts:create(Cache,Entity);
        {ok,_} -> ok
    end,
    State.

%% @private
mb_notify(true,NotifyPid,Message) ->
    ?Notify:notify_subscribers(NotifyPid,[],Message);
mb_notify(_,_,_) -> ok.

%% @private
mb_to_cache(true,true,Fun) -> Fun();
mb_to_cache(_,_,_) -> ok.

%% @private
assert_db(false,_,_,_,DbResult) -> DbResult;
assert_db(true,_,_,_,ok) -> ok;
assert_db(true,_NotifyPid,_SyncRef,Funs,{error,_}=DbResult) ->
    % Notify was already sent, but Db returned error.
    %?Notify:drop_subscriptions(NotifyPid,SyncRef),
    lists:foreach(fun(Fun) -> Fun() end,Funs),
    DbResult.

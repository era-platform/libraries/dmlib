%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 16.03.2021
%%% @doc routines of data-model changes appliance
%%%     TODO ICP: apply class<->role_group binding

-module(dmlib_domain_model_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([refresh/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

refresh(NewItems, #dstate{domain=Domain,model_classes=OldItems}=State) ->
    % nesting is external work
    % filtering by role is external work
    % removing automodificated fields is external work
    % just decorate by domain
    NewItems1 = decorate_items(Domain,NewItems),
    {ok,Created,Deleted,_Unchanged,ModifiedEx} = ?BU:juxtapose_ex(OldItems,NewItems1,fun(Item) -> maps:get(id,Item) end),
    ?OUT('$info', "~ts. '~ts' refresh classes: +~120p, -~120p, *~120p, =~120p", [?APP,Domain,length(Created),length(Deleted),length(ModifiedEx),length(_Unchanged)]),
    % TODO ICP: handle if something wrong
    delete(Deleted,State),
    create(Created,State),
    modify(ModifiedEx,State),
    {ok,State#dstate{model_classes=NewItems1}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------------
%% Decorate by domain
%% ------------------------------
decorate_items(Domain,Items) -> [decorate_item(Domain,Item) || Item <- Items].
decorate_item(Domain,Item) -> Item#{'domain' => Domain}.

%% ====================================================================
%% Apply changes routines
%% ====================================================================

%% ----------------------------------
%% starts OTP subtree to handle new class
%% ----------------------------------
create(List,State) ->
    lists:foreach(fun(Item) -> start_class(Item,State) end, List).

%% @private
start_class(Item,#dstate{domain=Domain}) ->
    ClassName = maps:get(classname,Item),
    Opts = [{'domain',Domain},
            {'classname',ClassName},
            {'class_meta',Item}],
    do_start_class(Domain,ClassName,Opts).

%% @private
%% Starts class supervisor
do_start_class(Domain,Class,Opts) ->
    {SM,SF,SA} = {?ClassSupv,start_link,[Opts]},
    ChildSupv = ?ClassSupv:get_linkname(Domain,Class),
    ParentSupv = ?DSUPV:get_regname(Domain),
    case supervisor:get_childspec(ParentSupv, ChildSupv) of
        {ok, _Spec} -> ok;
        {error, _} ->
            ChildSpec={ChildSupv, {SM,SF,SA}, permanent, 1000, supervisor, [?ClassSupv]},
            start_child(ParentSupv,ChildSpec)
    end.

%% @private
start_child(ParentSupv,ChildSpec) ->
    case supervisor:start_child(ParentSupv,ChildSpec) of
        {ok, _} -> ok;
        {ok, _, _} -> ok;
        {error,{{shutdown,{failed_to_start_child,_,_}},_}}=Err ->
            ?LOG('$error',"Start class child ~120tp got ~120tp, stop node...",[ChildSpec,Err]),
            ?BU:stop_node(1000),
            Err;
        Err ->
            ?LOG('$error',"Start class child ~120tp got ~120tp, stop node...",[ChildSpec,Err]),
            ?BU:stop_node(1000),
            Err
    end.

%% ----------------------------------
%% removes OTP subtree for class
%% ----------------------------------
delete(List,State) ->
    lists:foreach(fun(Item) -> stop_class(Item,State) end, List).

%% @private
stop_class(Item,#dstate{domain=Domain}) ->
    ClassName = maps:get(classname,Item),
    do_stop_class(Domain,ClassName).

%% @private
%% Stops class supervisor
do_stop_class(Domain,Class) ->
    ChildSupv = ?ClassSupv:get_linkname(Domain,Class),
    ParentSupv = ?DSUPV:get_regname(Domain),
    case supervisor:get_childspec(ParentSupv,ChildSupv) of
        {ok, _Spec} ->
            supervisor:terminate_child(ParentSupv,ChildSupv),
            supervisor:delete_child(ParentSupv,ChildSupv);
        {error, _} -> ok
    end.

%% ----------------------------------
%% modify for class
%% TODO ICP: optimize simple changes modifications.
%% ----------------------------------
modify(ListEx,State) ->
    F = fun({ItemOld,ItemNew}) ->
            stop_class(ItemOld,State),
            start_class(ItemNew,State)
        end,
    lists:foreach(F, ListEx).

%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 12.03.2021
%%% @doc Domain worker supervisor for in-storage operations

-module(dmlib_domain_worker_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/1]).
-export([init/1]).
-export([get_worker_count/0,
         get_worker_name/2]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-define(WorkerCount, 5).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    supervisor:start_link(?MODULE, Opts).

%% return worker count
get_worker_count() -> ?WorkerCount.

%% return regname of worker
get_worker_name(Domain,Index) when Index>=0; Index<?WorkerCount ->
    ?BU:to_atom_new(?BU:strbin("~ts_worker_[~ts]_~p", [?APP,Domain,Index])).

%% ====================================================================
%% Callbacks
%% ====================================================================

init(Opts) ->
    Domain = ?BU:get_by_key(domain,Opts),
    ChildSpec = prepare_child_specs(Domain,lists:seq(0,get_worker_count()-1)),
    ?LOG('$info', "~ts. '~ts' worker supv inited", [?APP,Domain]),
    {ok, {{one_for_one, 10, 2}, ChildSpec}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

prepare_child_specs(Domain, Indices) ->
    lists:map(fun(Index) ->
                    Name = get_worker_name(Domain,Index),
                    {Name, {?DWORKER, start_link, [Name]}, permanent, 1000, worker, [?DWORKER]}
              end, Indices).
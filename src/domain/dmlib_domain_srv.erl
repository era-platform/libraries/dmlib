%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 12.03.2021
%%% @doc General data model server.
%%%        Has global name, handles all outside, requests.
%%%        Routes request to class servers.
%%%      Opts:
%%%        regname, domain

-module(dmlib_domain_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/2]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([get_regname/1]).

-export([report_cn_started/4]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-define(QuotaPassTimeout, ?CFG:quota_pass_timeout_domains()).
-define(QuotaLimit, ?CFG:load_quota_limit_domains()).

-record(waitor, {
    classname :: binary(),
    regname :: atom(),
    fun_apply :: function(),
    expire_ts :: non_neg_integer()
}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(RegName, Opts) ->
    gen_server:start_link({local, RegName}, ?MODULE, [{'regname', RegName}|Opts], []).

get_regname(Domain) when is_binary(Domain); is_atom(Domain) ->
    ?BU:to_atom_new(?BU:strbin("~ts:srv;dom:~ts",[?APP,Domain])).

%% from class pid about started to count domain's quota
report_cn_started(Type,Pid,CN,Domain) ->
    gen_server:cast(get_regname(Domain), {report_cn_started,Type,Pid,CN}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    [RegName, Domain] = ?BU:extract_required_props(['regname','domain'], Opts),
    Ref = make_ref(),
    State = #dstate{regname = RegName,
                    domain = Domain,
                    self = self(),
                    initts = ?BU:timestamp(),
                    model_classes = [],
                    model_hc = erlang:phash2([]),
                    statref = Ref},
    self() ! {init,Ref},
    ?LOG('$info', "~ts. '~ts' facade inited", [?APP, Domain]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% --------------
%% returns current node
handle_call({get_node}, _From, State) ->
    {reply, {node(), self()}, State};

%% --------------
%% print state
handle_call({print}, _From, #dstate{domain=Domain}=State) ->
    ?LOG('$force', " ~ts. '~ts' facade state: ~n\t~120tp", [?APP, Domain, State]),
    {reply, ok, State};

%% --------------
%% restart gen_serv
handle_call({restart}, _From, State) ->
    {stop, restart, ok, State};

%% --------------
handle_call({crud,ClassName,_Args}=Req, From, #dstate{counter=C}=State) ->
    case get_pid(ClassName,State) of
        Pid when is_pid(Pid) ->
            FunReply = fun(Reply) -> gen_server:reply(From,Reply) end,
            gen_server:cast(Pid,{req,FunReply,Req}),
            {noreply,State#dstate{counter=C+1}};
        _ ->
            Reply = {error,{internal_error,<<"Class server not found">>}},
            {reply, Reply, State#dstate{counter=C+1}}
    end;

%% --------------
handle_call({query,Args}=Req, From, #dstate{domain=Domain,counter=C}=State) ->
    FunReply = fun(Reply) -> gen_server:reply(From,Reply) end,
    Timeout = maps:get(timeout,Args,60000),
    ?Query:execute(Req,FunReply,Domain,Timeout),
    {noreply,State#dstate{counter=C+1}};

%% --------------
%% other
handle_call(_Msg, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% --------------
%% GET HC async
handle_cast({get_hc,ClassName,FunReply}, #dstate{}=State) when is_function(FunReply,1) ->
    case get_pid(ClassName,State) of
        Pid when is_pid(Pid) -> gen_server:cast(Pid,{get_hc,FunReply});
        _ -> FunReply({error,{internal_error,<<"Class server not found">>}})
    end,
    {noreply,State};

%% GET HC sync wait for class load for timeout
handle_cast({get_hc,ClassName,FunReply,SyncTimeout}, #dstate{}=State)
    when is_integer(SyncTimeout), SyncTimeout > 0, is_function(FunReply,1) ->
    State1 = case get_pid(ClassName,State) of
                 Pid when is_pid(Pid) ->
                     gen_server:cast(Pid,{get_hc,FunReply}),
                     State;
                 _ ->
                     NowTS = ?BU:timestamp(),
                     FunApply = fun(Pid) -> gen_server:cast(Pid,{get_hc,FunReply,NowTS+SyncTimeout}) end,
                     append_waitor(ClassName,FunApply,NowTS,SyncTimeout,State)
             end,
    {noreply,State1};
handle_cast({get_hc,ClassName,FunReply,_SyncTimeout}, #dstate{}=State)
    when is_function(FunReply,1) ->
    handle_cast({get_hc,ClassName,FunReply}, State);

%% --------------
%% CRUD async
handle_cast({req,FunGot,FunReply,{crud,ClassName,_Args}=Req}, #dstate{counter=C}=State) when is_function(FunReply,1) ->
    case is_function(FunGot,0) of true -> FunGot(); _ -> ok end,
    case get_pid(ClassName,State) of
        Pid when is_pid(Pid) -> gen_server:cast(Pid,{req,FunReply,Req});
        _ -> FunReply({error,{internal_error,<<"Class server not found">>}})
    end,
    {noreply,State#dstate{counter=C+1}};

%% CRUD sync wait for class load for timeout
handle_cast({req,FunGot,FunReply,{crud,ClassName,_Args}=Req,SyncTimeout}, #dstate{counter=C}=State)
  when is_integer(SyncTimeout), SyncTimeout > 0, is_function(FunReply,1) ->
    case is_function(FunGot,0) of true -> FunGot(); _ -> ok end,
    State1 = case get_pid(ClassName,State) of
                 Pid when is_pid(Pid) ->
                     gen_server:cast(Pid,{req,FunReply,Req}),
                     State;
                 _ ->
                     NowTS = ?BU:timestamp(),
                     FunApply = fun(Pid) -> gen_server:cast(Pid,{req,FunReply,Req,NowTS+SyncTimeout}) end,
                     append_waitor(ClassName,FunApply,NowTS,SyncTimeout,State)
             end,
    {noreply,State1#dstate{counter=C+1}};
handle_cast({req,FunGot,FunReply,{crud,_,_}=Req,_SyncTimeout}, #dstate{}=State)
  when is_function(FunReply,1) ->
    handle_cast({req,FunGot,FunReply,Req}, State);

%% --------------
handle_cast({req,FunGot,FunReply,{query,_Args}=Req}, #dstate{domain=Domain,counter=C}=State) when is_function(FunReply,1) ->
    case is_function(FunGot,0) of true -> FunGot(); _ -> ok end,
    ?Query:execute(Req,FunReply,Domain),
    {noreply,State#dstate{counter=C+1}};

handle_cast({req,FunGot,FunReply,{query,_Args}=Req,SyncTimeout}, #dstate{domain=Domain,counter=C}=State) when is_function(FunReply,1) ->
    case is_function(FunGot,0) of true -> FunGot(); _ -> ok end,
    ?Query:execute(Req,FunReply,Domain,SyncTimeout),
    {noreply,State#dstate{counter=C+1}};

%% --------------
%% TODO ICP: make async
handle_cast({setup_classes,NewClasses}, #dstate{model_hc=HC,quota=QS}=State) when is_list(NewClasses) ->
    case erlang:phash2(NewClasses) of
        HC -> {noreply, State};
        HC1 ->
            case QS of
                #pass_quota{} ->
                    % when next setup while passing over quota
                    {ok,State1} = ?ModelCfg:refresh(NewClasses, State),
                    State2 = apply_waitors(State1#dstate{model_hc=HC1}),
                    % count ready class servers for quota counter
                    State3 = count_loaded_cn(State2),
                    {noreply, State3};
                #wait_quota{}=WQ0 ->
                    % when override setup while waiting quota
                    WQ = WQ0#wait_quota{new_classes = NewClasses,
                                        hc = HC1},
                    State1 = State#dstate{quota=WQ},
                    {noreply, State1};
                undefined ->
                    % when new setup in idle, or override setup while waiting quota
                    case catch request_quota(State) of
                        {ok,Pid} ->
                            MonRef = erlang:monitor(process,Pid),
                            WQ = #wait_quota{new_classes = NewClasses,
                                             hc = HC1,
                                             monref = MonRef},
                            State1 = State#dstate{quota=WQ},
                            {noreply, State1};
                        _ ->
                            % retry later
                            erlang:send_after(1000, self(), {setup_classes,NewClasses}),
                            {noreply, State}
                    end
            end
    end;

%% #quota
%% when some cn pid reported started (or failed)
%% RepType :: 'started' | 'failed'.
handle_cast({report_cn_started,RepType,Pid,CN}, #dstate{quota=#pass_quota{}}=State) when RepType=='started' ->
    State1 = on_cn_ready(CN,Pid,State),
    {noreply, State1};

%% --------------
%% other
handle_cast(_Msg, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

handle_info({init,Ref}, #dstate{statref=Ref}=State) ->
    State1 = start_stat_timer(State),
    {noreply,State1};

%% --------------
handle_info({stat,Ref},#dstate{statref=Ref,domain=Domain,counter=Counter=0}=State) ->
    ?LOG('$trace', "STAT. '~ts' facade handled ~p requests", [Domain, Counter]),
    State1 = start_stat_timer(State),
    {noreply,State1};
handle_info({stat,Ref},#dstate{statref=Ref,domain=Domain,counter=Counter}=State) ->
    ?LOG('$force', "STAT. '~ts' facade handled ~p requests", [Domain, Counter]), % TODO: $info
    State1 = start_stat_timer(State),
    {noreply,State1#dstate{counter=0}};

%% --------------
%% Quota events
%% --------------

%% #quota
%% delayed setup_classes handler
handle_info({setup_classes,_}=Msg, State) ->
    gen_server:cast(self(), Msg),
    {noreply, State};

%% #quota
%% when quota allow pass
handle_info('pass', #dstate{quota=#wait_quota{new_classes=NewClasses,hc=HC1,monref=MonRef}}=State) ->
    %?LOG('$info', "~ts. '~ts' Passed quota", [?APP, State#dstate.domain]),
    {ok,State1} = ?ModelCfg:refresh(NewClasses, State),
    State2 = apply_waitors(State1#dstate{model_hc=HC1}),
    % quota works
    erlang:demonitor(MonRef),
    Ref1 = make_ref(),
    TimerRef1 = erlang:send_after(?QuotaPassTimeout,self(),{'passed_quota_timeout',Ref1}),
    % setup passed quota
    PQ = #pass_quota{ref=Ref1,
                     timerref=TimerRef1,
                     loaded_classes=[]},
    % count ready class servers for quota counter
    State3 = count_loaded_cn(State2#dstate{quota=PQ}),
    {noreply, State3};

%% #quota
%% when timeout of passed quota (something wrong on load of any classes)
handle_info({'passed_quota_timeout',Ref}, #dstate{quota=#pass_quota{ref=Ref}}=State) ->
    ?LOG('$info', "~ts. '~ts' Passed quota TIMEOUT", [?APP, State#dstate.domain]),
    State1 = release_quota(State),
    {noreply, State1};

%% #quota
%% On quota DOWN while waiting for pass - reattempt setup classes (quota pass one portion more).
%%               If already passed then no need to reattempt.
handle_info({'DOWN',MonRef,process,_Pid,_Reason}, #dstate{quota=#wait_quota{monref=MonRef}=WQ}=State) ->
    ?LOG('$warning', "~ts. '~ts' Found quota DOWN. Retry", [?APP, State#dstate.domain]),
    #wait_quota{new_classes=NewClasses}=WQ,
    erlang:send_after(1000, self(), {setup_classes,NewClasses}),
    {noreply, State#dstate{quota=undefined}};

%% #quota
%% when flow with states from class servers (on passed quota start)
handle_info({cn_status,CN,Pid,{status,IsReady}}, #dstate{quota=#pass_quota{}}=State) ->
    State1 = case IsReady of
                 false -> State;
                 true -> on_cn_ready(CN,Pid,State)
             end,
    {noreply, State1};

%% --------------
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, #dstate{}=_State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% --------------
%% Quota routines
%% --------------

-define(QuotaGroupKey, dmlib_domains).

%% #quota
%% release quota
request_quota(#dstate{domain=Domain}) ->
    ?BLquota:setup_group(?QuotaGroupKey, #{limit => ?QuotaLimit}),
    ?BLquota:request_quota(?QuotaGroupKey, self(), Domain).

%% #quota
%% release quota
release_quota(State) ->
    ?BLquota:release_quota(?QuotaGroupKey, self()),
    State#dstate{quota=undefined}.

%% #quota
%% when some cn pid found ready on passed
on_cn_ready(CN,Pid,#dstate{quota=#pass_quota{}=PQ}=State) ->
    #dstate{model_classes=Classes}=State,
    #pass_quota{loaded_classes=LoadedClasses,timerref=TRef}=PQ,
    LoadedClasses1 = lists:keystore(CN,1,LoadedClasses,{CN,Pid,'ready'}),
    case length(LoadedClasses1) == length(Classes) of
        true ->
            %?LOG('$info', "~ts. '~ts' Quota released", [?APP, State#dstate.domain]),
            ?BU:cancel_timer(TRef),
            release_quota(State);
        false ->
            State#dstate{quota=PQ#pass_quota{loaded_classes=LoadedClasses1}}
    end.

%% #quota
%% cast requests to cn pids to get status (if is ready)
count_loaded_cn(#dstate{model_classes=ModelClasses, quota=#pass_quota{}=PQ}=State) ->
    Self = self(),
    lists:foreach(fun(CN) ->
                        case get_pid(CN,State) of
                            Pid when is_pid(Pid) ->
                                FunReply = fun(Reply) -> Self ! {cn_status,CN,Pid,Reply} end,
                                gen_server:cast(Pid,{get_status,FunReply});
                            _ -> ok
                        end
                  end, [maps:get(classname,ClassItem) || ClassItem <- ModelClasses]),
    State#dstate{quota=PQ#pass_quota{loaded_classes=[]}}.

%% ----------------------------------------
get_pid(ClassName,#dstate{domain=Domain}) ->
    case get(ClassName) of
        undefined ->
            RegName = ?ClassSrv:get_regname(Domain,ClassName),
            put(ClassName,RegName),
            whereis(RegName);
        RegName ->
            whereis(RegName)
    end.

%% ----------------------------------------
start_stat_timer(State) ->
    Ref1 = make_ref(),
    State#dstate{statref=Ref1,
                 stattimerref=erlang:send_after(60000,self(),{stat,Ref1})}.

%% @private
%% Add call request to waitors of class load
append_waitor(ClassName,FunApply,NowTS,SyncTimeout,#dstate{waitors=W}=State) ->
    ExpireTS = NowTS + SyncTimeout,
    Waitor = #waitor{classname = ClassName,
                     fun_apply = FunApply,
                     expire_ts = ExpireTS},
    W1 = lists:filter(fun(#waitor{expire_ts=ExpTS}) -> ExpTS =< NowTS end, W),
    State#dstate{waitors=[Waitor|W1]}.

%% Apply waitors when something changed in classes list
apply_waitors(#dstate{waitors=W}=State) ->
    NowTS = ?BU:timestamp(),
    W1 = lists:foldr(fun(#waitor{expire_ts=ExpTS},Acc) when ExpTS =< NowTS -> Acc;
                        (#waitor{classname=CN,fun_apply=FunApply}=Waitor,Acc) ->
                            case get_pid(CN,State) of
                                undefined -> [Waitor|Acc];
                                Pid when is_pid(Pid) ->
                                    FunApply(Pid),
                                    Acc
                            end
                     end, [], W),
    State#dstate{waitors=W1}.

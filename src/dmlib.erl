%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.05.2021
%%% @doc dmlib application (data-model).
%%%   Provides CRUD service to collections that setup as metadata items.
%%%   Every class is mapped to real storage (ets, (distributed) mnesia, (replicated) postgresql, partitioned postgresql, kafka, kafka+clickhouse).
%%%   Application could be started or it's modules could be used in other apps as OTP items.
%%%   It uses basiclib and mnesialib. They should be started and setup previously.
%%%      If not then starts itself.
%%%      And setup default output to console and mnesia schema to current node single.
%%%   It use single domain in supvervision tree as default. But could be setup to domain's forest.
%%%   Application could be setup by application:set_env:
%%%      log_destination
%%%          descriptor to define log file folder and prefix.
%%%          Default: {'env','dm'}
%%%      get_storages_function
%%%             :: function(Domain::term(), StorageType::binary(), StorageInstanceTerm::binary() | map()) -> {ok,[StorageParams]} | {error,Reason::term()}.
%%%          Called to get params of storages, that has selected type and corresponds to selected instance.
%%%          It's required by classes mapped to postgresql, kafka, clickhouse.
%%%          * Postgresql storageoptions: {
%%%                 endpoints :: [{host, port, login, pwd}],
%%%                 host, port, login, pwd,
%%%                 database,
%%%                 max_connections (=pg_max_connections_default())
%%%               }
%%%          * Kafka storage options: {
%%%                 endpoints :: [{host,port}],
%%%                 default_reconnect_cool_down_seconds (=10),
%%%                 partitioner (=random_by_key),
%%%                 send_mode, replication_factor, partition_count, [domain]
%%%               },
%%%          * Clickhouse storage options: {
%%%                 endpoints :: [{scheme,host,port,login,password}],
%%%                 scheme, host, port, login, password,
%%%                 cluster, [domain]
%%%               }
%%%          Default: {error,_}
%%%      decrypt_function
%%%             :: function(Pwd0::binary()) -> Pwd::binary().
%%%          Called to build decrypted passwords, got from storage's options.
%%%          If undefined then use pwd as is.
%%%      get_readonly_credentials_function
%%%             :: function(Domain::term(), DbType::binary(), DbParams::map()|[map()]) -> {ok,Login,Pwd} | false.
%%%          Called to get readonly credentials to execute arbitrary report query in db of selected storage instance.
%%%          If undefined or return false then report query executed by default credentials (non readonly).
%%%      notify_function
%%%             :: function(Domain::term(), CN::binary(), ClassMeta::map(), Operation::atom(), EntityInfo::{Id,E0,E1}|undefined, ModifierInfo::{Type,Id}|undefined) -> ok.
%%%             :: function(Domain::term(), CN::binary(), ClassMeta::map(), Operation::atom(), EntityInfo::{Id,E0,E1}|undefined, ModifierInfo::{Type,Id}|undefined, PrevExState::undefined | term()) -> NewExState::term().
%%%          Called to notify subscribers about operation.
%%%      drop_subscriptions_function
%%%             :: function(Domain::term(), CN::binary()) -> ok.
%%%          Called to drop subscriptions on corruption case
%%%      validate_function
%%%             :: function(Domain::term(), Validator::term(), CN::binary(), Operation::atom(), EntityInfo::{Id,E0,E1}, ModifierInfo::{Type,Id}|undefined) -> {ok,E2} | {error,Reason::term()}.
%%%          Called to validate modifications before apply
%%%      store_changehistory_function
%%%             :: function(Domain::term(), CN::binary(), ClassMeta::map(), Operation::atom(), EntityInfo::{Id,E0,E1}|undefined, ModifierInfo::{Type,Id}|undefined) -> ok.
%%%          Called to store changes in history when it is enabled
%%%      validate_function
%%%             :: function(Domain::term(), Validator::term(), CN::binary(), Operation::atom(), EntityInfo::{Id,E0,E1}, ModifierInfo::{Type,Id}|undefined) -> {ok,E2} | {error,Reason::term()}.
%%%          Called to validate modifications before apply
%%%      store_function
%%%             :: function(push, {Key::term(), Value::term()}) -> ok | {error,Reason::term()}.
%%%             :: function(pull, Key::term()) -> {ok, Value} | false | {error,Reason::term()}.
%%%          Called to operate with external key-value storage (push value by key, pull by key)
%%%      delete_attachments_function
%%%             :: function(Domain::term(), ClassItem::map(), Entity::map()) -> ok | {error,Reason::term()}.
%%%          Called to delete attachments of entity (when deleting entity)
%%%      clear_attachments_function
%%%             :: function(Domain::term(), ClassItem::map()) -> ok | {error,Reason::term()}.
%%%          Called to clear attachments folder for class
%%%      stat_enabled_function
%%%             :: function(Domain::term()) -> boolean().
%%%          Called to check if stat logging should be enabled. Default: enabled
%%%      stat_enabled
%%%             :: boolean() | undefined
%%%          Turn on stat logging. Default: enabled. If not found then stat_enabled_function called.
%%%      cache_filter_timeout
%%%          Force cache filter timeout, ms. Default: 300000.
%%%      load_quota_limit_domains
%%%          How many domains could load at the same time. Default: 3.
%%%      load_quota_limit_classes
%%%          How many classes per domain could load at the same time. Default: 3.
%%%      quota_pass_timeout_domains
%%%          Timeout on passed state for domain. Default: 5000.
%%%      quota_pass_timeout_classes
%%%          Timeout on passed state for class. Default: 1000.
%%%      class_readers_count
%%%          How many reader-workers in hash-ring parallel could handle class read requests. From 2 to 32. Default: 10.
%%%      class_writers_count
%%%          How many writer-workers in hash-ring parallel could handle class write requests. From 2 to 32. Default: 4.
%%%      pg_max_connections_default
%%%          How many connections to postgresql could be in parallel (default value, could be overriden by 'max_connections' of storage)
%%%      query_workers_count
%%%          How many query-workers could handle 'dmsquery' requests in parallel for all domains. From 1 to 32. Default: 4.
%%%   Expected structure of class (used fields):
%%%      classname :: binary()
%%%      storage_mode :: <<"ets">> | <<"ram">> | <<"runtime">> | <<"category">> | <<"history">>, | <<"transactionlog">>
%%%      cache_mode :: <<"none">>, <<"temp">>, <<"full">>
%%%      integrity_mode :: <<"async">>, <<"sync_fast_read">>, <<"sync_fast_notify">>, <<"sync">>
%%%      properties :: [#{<<"name">>,<<"data_type">>,<<"multi">>,<<"required">>}]
%%%             'data_type'::
%%%                     <<"string">> | <<"boolean">> | <<"integer">> | <<"long">> | <<"increment">> | <<"float">> | <<"date">> | <<"time">> | <<"datetime">> |
%%%                     <<"uuid">> | <<"enum">> | <<"any">> | <<"entity">> | <<"attachment">>
%%%             specific fields and behaviour of data_types:
%%%                     'attachment' contains meta-info about files and should be passed as {internal,Value::binary() | number() | map() | list() } to avoid direct setup by user.
%%%                     'enum' specific fields: <<"items">> :: [string()]
%%%                     'entity' specific fields: <<"idclass">> :: binary()
%%%                     'any' specific fields: <<"merge_levels">> :: non_neg_integer(), only when not 'multi'
%%%      opts: #{ <<"validator">> => <<>>, % global name of validator service that should be requested to validate changes before apply (save/notify). When entity is ready to store, process synchronously calls validator to check and modify entity.
%%%               <<"check_required_fill_defaults">> => true,  % if entities should be checked for required fields and filled by default values by the server. Model can economy resources by disable this on high-load collections.
%%%               <<"max_limit">> => 100, % how many items could be requested on read in max. Any read of collection is limited by this value. 1-10000
%%%               <<"max_mask">> => [], % max mask of properties, that could be returned on collection read.
%%%               <<"replace_without_read">> => false, % setup implementation when replace operation doesn't use previously find/read. Notifications would be filtered only by new replacing entity.
%%%               ? <<"notify">> => true, % if subscribers should be notified on modifications. External property.
%%%               ? <<"notify_integrity_timeout">> => 0, % if notify is failed on getting subscriptions from subscr (noproc, etc), then it could wait and retry for timeout, other notifications would wait in queue. External property.
%%%               ? <<"appserver">> => <<>>, % global name of appserver service (call from ws and other clients instead of dms, it's responsibility of appserver to store to dms). External property.
%%%               <<"store_changehistory_mode">> => <<"none">>, % if class changes should be stored in history. none | sync | async
%%%               <<"stat_mode">> => <<"default">> (high | medium | low | disabled | default)
%%%               <<"worker_r_count">> => 0 (count of read workers, 0 means default role setting)
%%%               <<"worker_w_count">> => 0 (count of write workers, 0 means default role setting)
%%%             % for storage_mode = 'ram', 'runtime', 'ets'
%%%               <<"max_size">> => 10000, % max size of storage (only for 'ram', 'runtime', 'ets'). 10-1000000.
%%%               <<"expires_mode">> => <<"none">>, % if dms should automatically delete after expires. none | modify | create | custom. Select when to auto set (reset) timestamp in ts property.
%%%               <<"expires_ttl_property">> => <<>>, % if dms should automatically delete after expires. name of property containing initial ttl in seconds.
%%%               <<"expires_ts_property">> => <<>>, % if dms should automatically delete after expires. name of property for auto storing modify timestamp.
%%%               <<"aggr_cache_ttl_ms">> => 1000, % aggregation requests cache ttl in milliseconds. 0 means disable aggregation requests cache.
%%%             % for storage_mode = 'ram', 'runtime'
%%%               <<"dirty_write_enabled">> => false,
%%%             % for storage_mode = 'category' (pg), 'history' (pg+partition), 'transactionlog' (kafka)
%%%               <<"storage_instance">> => <<>>, % InstanceKey or #{Site => InstanceKey} (only for 'category', 'history', 'transactionlog')
%%%               <<"store_item_json_data">> => false, % if db should obtain field 'item_json_data' containing full entity json
%%%               <<"previous_classname">> => <<>>, % if class was renamed then it's tables could automatically be renamed.
%%%             % for storage_mode = 'category', 'history', 'runtime', 'ram', 'ets'
%%%               <<"lookup_properties">> => [], % list of property names for lookup operation (indexes, separate fields etc).
%%%             % for storage_mode = 'history' (pg+partition) and 'transactionlog' (for clickhouse, not kafka)
%%%               <<"partition_property">> => <<>>, % property of type=datetime to make partitioned history storage. For 'transactionlog' could be empty, then clickhouse sharding used automatically hash of 'id'.
%%%               <<"partition_interval">> => <<"month">>, % size of partition in partitioned history storage.
%%%             % for storage_mode = 'transactionlog' (for kafka only, clickhouse's shards and replicas are configured in its config files).
%%%               <<"partition_count">> => 2, % how many partitions does topic have (1-10). This parameter directly passed to kafka.
%%%               <<"replication_factor">> => 2, % how many replicas does topic have (1-4). This parameter directly passed to kafka.
%%%               <<"notify_transactions">> => false, % as default subscribers are not notified by 'transactionlog' changes. But notification could be enabled.
%%%             % for cache_mode = 'temp'
%%%               <<"cache_sec">> => 600, % how long temporarily cache holds modified items
%%%               <<"cache_limit">> => 1000 % how many modified items are holded in temporarily cache
%%%             %
%%%             }
%%%   Expected structure of storage, returned by function:
%%%      * postgresql: #{<<"host">>, <<"port">>(5432), <<"login">>, <<"pwd">>, <<"database">>}
%%%      * clickhouse: #{<<"host">>, <<"port">>(8123), <<"login">>, <<"password">>, <<"cluster">>(<<"">>)}
%%%      * kafka: #{<<"endpoints">>:#{<<"host">>,<<"port">>}, <<"send_mode">>, <<"partition_count">>(from class.opts), <<"replication_factor">>(from class.opts), <<"default_reconnect_cool_down_seconds">>(10), <<"partitioner">>(<<"random_by_key">>)}
%%% TODO: log file setup from opts.
%%% TODO: validation strategy callback.
%%% TODO: notify pids N count.
%%% TODO: field type as_is only for ets and mnesia.
%%% TODO: forward data outside, handle forward messages.
%%% TODO: implement sync outside method, check integrity to outside.

-module(dmlib).
-author('Peter <tbotc@yandex.ru>').

-behaviour(application).

-export([start/2,
         stop/1]).

-export([add_domain/1,
         delete_domain/1,
         get_domain_childspec/2,
         get_regname/1]).

-export([setup_classes/2]).

-export([get_hc/3, get_hc/4,
         crud/3, crud/4, crud_ex/4, crud_ex/5,
         query/3, query/4, query_ex/4, query_ex/5]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public
%% ====================================================================

%% -------------------------------------------
%% Append domain supervision tree
%% -------------------------------------------
-spec add_domain(Domain::binary() | atom()) -> {ok,Pid::pid(),RegName::atom()} | ignore | {error,Reason::term()}.
%% -------------------------------------------
add_domain(Domain) when is_binary(Domain); is_atom(Domain) ->
    RegName = get_regname(Domain),
    ChildSpec = get_domain_childspec(Domain,RegName),
    case ?SUPV:start_child(ChildSpec) of
        {ok,Pid} -> {ok,Pid,RegName};
        ignore -> ignore;
        {error,_}=Err -> Err
    end.

%% -------------------------------------------
%% Remove domain supervision tree. Used only if domain added to dmlib application's supervisor by add_domain/1
%% -------------------------------------------
-spec delete_domain(Domain::binary() | atom()) -> ok | {error,Reason::term()}.
%% -------------------------------------------
delete_domain(Domain)
  when is_binary(Domain); is_atom(Domain) ->
    LinkName = ?DSUPV:get_linkname(Domain),
    case ?SUPV:get_childspec(LinkName) of
        {ok,_} ->
            ?SUPV:terminate_child(LinkName),
            ?SUPV:delete_child(LinkName);
        {error,_}=Err -> Err
    end.

%% -------------------------------------------
%% Return childspec for domain's supervisor (to start under external supervisor)
%% -------------------------------------------
-spec get_domain_childspec(Domain::binary() | atom(), RegName::atom()) -> ChildSpec::tuple().
%% -------------------------------------------
get_domain_childspec(Domain,RegName)
  when (is_binary(Domain) orelse is_atom(Domain)) andalso is_atom(RegName) ->
    LinkName = ?DSUPV:get_linkname(Domain),
    Opts = [{'regname',RegName},
            {'domain',Domain}],
    {LinkName, {?DSUPV, start_link, [Opts]}, permanent, 1000, supervisor, [?DSUPV]}.

%% -------------------------------------------
%% Return default regname for domain's facade server
%% -------------------------------------------
-spec get_regname(Domain::binary() | atom()) -> RegName::atom().
%% -------------------------------------------
get_regname(Domain) when is_binary(Domain); is_atom(Domain) ->
    ?DSRV:get_regname(Domain).


%% -------------------------------------------
%% Load new classes metadata for selected domain. NewClasses
%% -------------------------------------------
-spec setup_classes(Domain::binary() | atom(), [Item::map()]) -> ok.
%% -------------------------------------------
setup_classes(Domain,Classes) when is_binary(Domain); is_atom(Domain) ->
    RegName = ?DSRV:get_regname(Domain),
    case whereis(RegName) of
        Pid when is_pid(Pid) -> gen_server:cast(Pid, {setup_classes,Classes});
        undefined -> {error,not_found}
    end.

%% -------------------------------------------
%% Request for data hashcode of classname (check overall changes).
%%   Result would be passed to FunReply/1.
%% -------------------------------------------
-spec get_hc(RegNameOrPid::atom() | pid(), ClassName::binary(), FunReply::function()) -> ok.
%% -------------------------------------------
get_hc(RegNameOrPid, ClassName, FunReply)
  when is_function(FunReply,1) ->
    gen_server:cast(RegNameOrPid,{get_hc,ClassName,FunReply}).

%% -------------------------------------------
%% Request for data hashcode of classname (check overall changes).
%%   Result would be passed to FunReply/1.
%%   Sync for timeout if gen-server is not ready
%% -------------------------------------------
-spec get_hc(RegNameOrPid::atom() | pid(), ClassName::binary(), FunReply::function(), SyncTimeout::non_neg_integer()) -> ok.
%% -------------------------------------------
get_hc(RegNameOrPid, ClassName, FunReply, SyncTimeout)
  when is_function(FunReply,1) ->
    gen_server:cast(RegNameOrPid,{get_hc,ClassName,FunReply,SyncTimeout}).

%% =====================================================================================================================

%% -------------------------------------------
%% Obsolete facade (no FunGot)
%% Register crud request to domain's server.
%% Result would be passed to FunReply/1.
%% -------------------------------------------
-spec crud(RegNameOrPid::atom() | pid(), FunReply::function(),
            CrudRequest::{crud,CN::binary(), {Operation::atom(),Map::map()}})
      -> ok.
%% -------------------------------------------
crud(RegNameOrPid, FunReply, {crud,_CN,{_Operation,_Map}}=CrudRequest) ->
    crud_ex(RegNameOrPid, undefined, FunReply, CrudRequest).

%% -------------------------------------------
%% Obsolete facade (no FunGot)
%% Register crud request to domain's server.
%% Result would be passed to FunReply/1.
%%   Sync for timeout if gen-server is not ready
%% -------------------------------------------
-spec crud(RegNameOrPid::atom() | pid(), FunReply::function(),
           CrudRequest::{crud,CN::binary(), {Operation::atom(),Map::map()}}, SyncTimeout::non_neg_integer())
      -> ok.
%% -------------------------------------------
crud(RegNameOrPid, FunReply, {crud,_CN,{_Operation,_Map}}=CrudRequest, SyncTimeout) ->
    crud_ex(RegNameOrPid, undefined, FunReply, CrudRequest, SyncTimeout).

%% -------------------------------------------
%% Register crud request to domain's server.
%%   Got is passed to FunGot/0, Result would be passed to FunReply/1.
%% -------------------------------------------
-spec crud_ex(RegNameOrPid::atom() | pid(), FunGot::undefined|function(), FunReply::function(),
            CrudRequest::{crud,CN::binary(), {Operation::atom(),Map::map()}})
      -> ok.
%% -------------------------------------------
crud_ex(RegNameOrPid, FunGot, FunReply, {crud,_CN,{_Operation,_Map}}=CrudRequest)
  when is_function(FunReply,1), is_binary(_CN), is_atom(_Operation), is_map(_Map) ->
    gen_server:cast(RegNameOrPid,{req,FunGot,FunReply,CrudRequest}).

%% -------------------------------------------
%% Register crud request to domain's server.
%%   Got is passed to FunGot/0, Result would be passed to FunReply/1.
%%   Sync for timeout if gen-server is not ready
%% -------------------------------------------
-spec crud_ex(RegNameOrPid::atom() | pid(), FunGot::undefined|function(), FunReply::function(),
           CrudRequest::{crud,CN::binary(), {Operation::atom(),Map::map()}}, SyncTimeout::non_neg_integer())
      -> ok.
%% -------------------------------------------
crud_ex(RegNameOrPid, FunGot, FunReply, {crud,_CN,{_Operation,_Map}}=CrudRequest, SyncTimeout)
  when is_function(FunReply,1), is_binary(_CN), is_atom(_Operation), is_map(_Map) ->
    gen_server:cast(RegNameOrPid,{req,FunGot,FunReply,CrudRequest,SyncTimeout}).

%% =====================================================================================================================

%% -------------------------------------------
%% Request for execute sql query in storage db (project feature).
%%   Got is passed to FunGot/0, Result would be passed to FunReply/1.
%% -------------------------------------------
-spec query(RegNameOrPid::atom() | pid(), FunReply::function(),
             Request::{query,Map::map()})
      -> ok.
%% -------------------------------------------
query(RegNameOrPid, FunReply, {query,_Map}=Request)
  when is_function(FunReply,1), is_map(_Map) ->
    query_ex(RegNameOrPid, undefined, FunReply, Request).

%% -------------------------------------------
%% Register crud request to domain's server.
%%   Got is passed to FunGot/0, Result would be passed to FunReply/1.
%%   Sync for timeout if gen-server is not ready
%% -------------------------------------------
-spec query(RegNameOrPid::atom() | pid(), FunReply::function(),
             Request::{query,Map::map()}, SyncTimeout::non_neg_integer())
      -> ok.
%% -------------------------------------------
query(RegNameOrPid, FunReply, {query,_Map}=Request, SyncTimeout)
  when is_function(FunReply,1), is_map(_Map) ->
    query_ex(RegNameOrPid, undefined, FunReply, Request, SyncTimeout).

%% -------------------------------------------
%% Request for execute sql query in storage db (project feature).
%%   Got is passed to FunGot/0, Result would be passed to FunReply/1.
%% -------------------------------------------
-spec query_ex(RegNameOrPid::atom() | pid(), FunGot::undefined|function(), FunReply::function(),
            Request::{query,Map::map()})
      -> ok.
%% -------------------------------------------
query_ex(RegNameOrPid, FunGot, FunReply, {query,_Map}=Request)
  when is_function(FunReply,1), is_map(_Map) ->
    gen_server:cast(RegNameOrPid,{req,FunGot,FunReply,Request}).

%% -------------------------------------------
%% Register crud request to domain's server.
%%   Got is passed to FunGot/0, Result would be passed to FunReply/1.
%%   Sync for timeout if gen-server is not ready
%% -------------------------------------------
-spec query_ex(RegNameOrPid::atom() | pid(), FunGot::undefined|function(), FunReply::function(),
            Request::{query,Map::map()}, SyncTimeout::non_neg_integer())
      -> ok.
%% -------------------------------------------
query_ex(RegNameOrPid, FunGot, FunReply, {query,_Map}=Request, SyncTimeout)
  when is_function(FunReply,1), is_map(_Map) ->
    gen_server:cast(RegNameOrPid,{req,FunGot,FunReply,Request,SyncTimeout}).

%% ===================================================================
%% Callback functions (application)
%% ===================================================================

%% -------------------------------------
%% @doc Starts application
%% -------------------------------------
start(_, _) ->
    setup_start_ts(),
    setup_dependencies(),
    ensure_deps_started(),
    ?SUPV:start_link(undefined).

%% -------------------------------------
%% @doc Stops application.
%% -------------------------------------
stop(_State) ->
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% --------------------------------------
%% @private
%% setup start_utc to applicaiton's env for monitor purposes
%% --------------------------------------
setup_start_ts() ->
    application:set_env(?APP,start_utc,?BU:timestamp()).

%% @private
setup_dependencies() ->
    % basiclib
    case ?BU:get_env(?BASICLIB, 'log_function',undefined) of
        undefined -> ?BU:set_env(?BASICLIB, log_function, fun(_FileKey,{Fmt,Args}) -> io:format(Fmt++"\n",Args);
                                                             (_FileKey,Txt) -> io:format(Txt,[])
                                                          end);
        _ -> ok
    end,
    % mnesialib
    case ?BU:get_env(?MNESIALIB, 'schema_nodes',undefined) of
        undefined -> ?BU:set_env(?MNESIALIB, 'schema_nodes', [node()]);
        _ -> ok
    end,
    ok.

%% --------------------------------------
%% @private
%% starts deps applications, that out of app link
%% --------------------------------------
ensure_deps_started() ->
    {ok,_} = application:ensure_all_started(?BASICLIB, permanent),
    {ok,_} = application:ensure_all_started(?MNESIALIB, permanent),
    {ok,_} = application:ensure_all_started(?PGLIB, permanent),
    {ok,_} = application:ensure_all_started(brod, permanent),
    ok.